package com.balleh.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.activities.ActivitySplashScreen;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;

/**
 * Created by Sasikumar on 3/22/2018.
 * Exception Message to review
 */

public class DialogCustomMessage extends DialogFragment {

    private View v_CustomMessage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_CustomMessage = inflater.inflate(R.layout.fragment_custom_message, container, false);
        setting();
        return v_CustomMessage;
    }

    private void setting() {
        Bundle bundle = getArguments();

        ImageView iv_CustomMessage = v_CustomMessage.findViewById(R.id.iv_custom_message);

        if (bundle != null) {
            String mType = bundle.getString("type");
            if (mType != null) {
                switch (mType) {
                    case "Exception":
                        iv_CustomMessage.setImageDrawable(ContextCompat.getDrawable(ApplicationContext.getApplicationInstance(), R.drawable.placeholder));
                        break;
                    case "Error":
                        iv_CustomMessage.setImageDrawable(ContextCompat.getDrawable(ApplicationContext.getApplicationInstance(), R.drawable.placeholder));
                        break;
                    default:
                        iv_CustomMessage.setImageDrawable(ContextCompat.getDrawable(ApplicationContext.getApplicationInstance(), R.drawable.placeholder));
                        break;
                }
            } else {
                iv_CustomMessage.setImageDrawable(ContextCompat.getDrawable(ApplicationContext.getApplicationInstance(), R.drawable.placeholder));
            }
        } else {
            iv_CustomMessage.setImageDrawable(ContextCompat.getDrawable(ApplicationContext.getApplicationInstance(), R.drawable.placeholder));
        }

        iv_CustomMessage.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ActivitySplashScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
    }
}
