package com.balleh.fragments.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.ProductListAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelProductDetailList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentSearch extends Fragment implements View.OnClickListener {

    private Activity activity;
    private MenuHandler menuHandler;
    private View v_SearchContent;
    private SearchView sv_ProductListHint;
    private AppCompatTextView atv_ViewTypeSearchProductList, atv_SearchResultHint,atv_SortingSearchProductList;
    private RecyclerView rc_SearchProductList;
    private int mViewType = 1, page = 1;
    private ProgressBar pb_SearchProductListLoader;
    private ProductListAdapter productListAdapter;
    private String mQuery = "a";
    private PopupWindow mSortingList;
    private ArrayList<ModelProductDetailList> modelProductDetailLists = new ArrayList<>();
    private LinearLayout ll_ProductSetting;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_SearchContent = inflater.inflate(R.layout.fragment_search_product, container, false);
        load();
        return v_SearchContent;
    }

    private void load() {

        atv_SortingSearchProductList = v_SearchContent.findViewById(R.id.atv_sorting_search_product_list);

        sv_ProductListHint = v_SearchContent.findViewById(R.id.sv_search_product_list);
        ll_ProductSetting = v_SearchContent.findViewById(R.id.ll_product_setting_container);

        atv_ViewTypeSearchProductList = v_SearchContent.findViewById(R.id.atv_view_type_search_product_list);
        atv_SearchResultHint = v_SearchContent.findViewById(R.id.atv_search_default_content);

        rc_SearchProductList = v_SearchContent.findViewById(R.id.rc_search_product_list);

        pb_SearchProductListLoader = v_SearchContent.findViewById(R.id.pb_search_product_list_loader);

        ll_ProductSetting.setVisibility(View.GONE);

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.searchHistory).equals("Empty")) {
            loadSearchProductList("default");
        } else {
            loadSearchProductList("prL-prH");
        }

        sv_ProductListHint.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                atv_SearchResultHint.setVisibility(View.GONE);
                sv_ProductListHint.clearFocus();
                mQuery = query.replace(" ", "%20");
                loadSearchProductList("default");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setDefaultPageCount();
                return false;
            }
        });

        atv_SortingSearchProductList.setOnClickListener(this::sorting);

        /*sv_ProductListHint.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                emptyList();
            }
        });*/

        loadType("1");
        fontSetup();
    }

    /*private void emptyList() {
        rc_SearchProductList.setAdapter(null);
    }*/

    private void loadListener() {
        atv_ViewTypeSearchProductList.setOnClickListener(view -> {
            if (mViewType == 1) {
                mViewType = 2;
                atv_ViewTypeSearchProductList.setText(getString(R.string.product_list_list_view));
                atv_ViewTypeSearchProductList.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(activity, R.drawable.ic_view_list_black_24dp),
                        null, null);
                loadProductListViewType("2");
            } else {
                mViewType = 1;
                atv_ViewTypeSearchProductList.setText(getString(R.string.product_list_grid_view));
                atv_ViewTypeSearchProductList.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(activity, R.drawable.ic_view_module_black_24dp),
                        null, null);
                loadProductListViewType("1");
            }
        });
    }

    private void setDefaultPageCount() {
        page = 1;
    }

    private void loadProductListViewType(String type) {
        loadType(type);

        if (modelProductDetailLists != null) {
            if (modelProductDetailLists.size() > 0) {
                atv_SearchResultHint.setVisibility(View.GONE);
                if (mViewType == 1) {
                    productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, true, menuHandler, rc_SearchProductList);
                    rc_SearchProductList.setAdapter(productListAdapter);
                } else {
                    productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, false, menuHandler, rc_SearchProductList);
                    rc_SearchProductList.setAdapter(productListAdapter);
                }
            } else {
                atv_SearchResultHint.setVisibility(View.VISIBLE);
            }
        } else {
            atv_SearchResultHint.setVisibility(View.VISIBLE);
        }
    }

    private void loadType(String type) {
        if (type.equals("1")) {
            rc_SearchProductList.setLayoutManager(new GridLayoutManager(activity, 2));
        } else {
            rc_SearchProductList.setLayoutManager(new LinearLayoutManager(activity));
        }
    }

    private void sorting(View v) {
        mSortingList = new PopupWindow(activity);
        @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.pw_sorting_list, null);
        mSortingList.setContentView(view);

        AppCompatTextView atv_NameAZ, atv_NameZA, atv_PriceLH, atv_PriceHL,
                atv_PositionLH, atv_PositionHL;

        atv_NameAZ = view.findViewById(R.id.atv_name_az_sorting);
        atv_NameZA = view.findViewById(R.id.atv_name_za_sorting);
        atv_PriceLH = view.findViewById(R.id.atv_price_lh_sorting);
        atv_PriceHL = view.findViewById(R.id.atv_price_hl_sorting);
        atv_PositionLH = view.findViewById(R.id.atv_position_az_sorting);
        atv_PositionHL = view.findViewById(R.id.atv_position_za_sorting);

        mSortingList.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSortingList.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSortingList.setOutsideTouchable(true);
        mSortingList.setFocusable(true);
        mSortingList.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mSortingList.showAtLocation(v, Gravity.CENTER, 0, 0);

        atv_NameAZ.setOnClickListener(this);
        atv_NameZA.setOnClickListener(this);
        atv_PriceLH.setOnClickListener(this);
        atv_PriceHL.setOnClickListener(this);
        atv_PositionLH.setOnClickListener(this);
        atv_PositionHL.setOnClickListener(this);

        fontSetup(atv_NameAZ,Constant.LIGHT,Constant.C_TEXT_VIEW);
        fontSetup(atv_NameZA,Constant.LIGHT,Constant.C_TEXT_VIEW);
        fontSetup(atv_PriceLH,Constant.LIGHT,Constant.C_TEXT_VIEW);
        fontSetup(atv_PriceHL,Constant.LIGHT,Constant.C_TEXT_VIEW);
        fontSetup(atv_PositionLH,Constant.LIGHT,Constant.C_TEXT_VIEW);
        fontSetup(atv_PositionHL,Constant.LIGHT,Constant.C_TEXT_VIEW);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    /*@Override
    public void onResume() {
        super.onResume();

        if (modelProductDetailLists != null) {
            if (modelProductDetailLists.size() > 0) {
                atv_SearchResultHint.setVisibility(View.GONE);
            } else {
                atv_SearchResultHint.setVisibility(View.VISIBLE);
            }
        } else {
            atv_SearchResultHint.setVisibility(View.VISIBLE);
        }
    }*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuHandler.changeBottomView(2);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.atv_name_az_sorting:
                mSortingList.dismiss();
                loadSearchProductList("A-Z");
                break;
            case R.id.atv_name_za_sorting:
                mSortingList.dismiss();
                loadSearchProductList("Z-A");
                break;
            case R.id.atv_price_lh_sorting:
                mSortingList.dismiss();
                loadSearchProductList("prL-prH");
                break;
            case R.id.atv_price_hl_sorting:
                mSortingList.dismiss();
                loadSearchProductList("prH-prL");
                break;
            case R.id.atv_position_az_sorting:
                mSortingList.dismiss();
                loadSearchProductList("poL-poH");
                break;
            case R.id.atv_position_za_sorting:
                mSortingList.dismiss();
                loadSearchProductList("poH-poL");
                break;
        }
    }

    void loadSearchProductList(final String type) {
        setDefaultPageCount();
        if (menuHandler.networkError()) {
            pb_SearchProductListLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getSearchProductList(mQuery, page, type, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_SearchProductListLoader.setVisibility(View.GONE);

                        ll_ProductSetting.setVisibility(View.VISIBLE);

                        modelProductDetailLists = ConstantDataParser.getProductList(response);

                        if (mViewType == 1) {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, true, menuHandler, rc_SearchProductList);
                        } else {
                            productListAdapter = new ProductListAdapter(activity, modelProductDetailLists, false, menuHandler, rc_SearchProductList);
                        }

                        rc_SearchProductList.setAdapter(productListAdapter);

                        loadListener();

                        productListAdapter.setOnLoadMoreListener(() -> {
                            page++;
                            if (modelProductDetailLists != null) {
                                if (modelProductDetailLists.size() > 0) {
                                    if (modelProductDetailLists.get(0) != null) {
                                        if (modelProductDetailLists.get(0).getTotal_count() > 0) {
                                            if (page <= ((modelProductDetailLists.get(0).getTotal_count() / 10) + 1)) {
                                                if (menuHandler.networkError()) {
                                                    pb_SearchProductListLoader.setVisibility(View.VISIBLE);
                                                    StringRequest stringPageRequest = new StringRequest(Request.Method.GET, ConstantFields.getSearchProductList(mQuery, page, type, ConstantFields.currentLanguage().equals("en")),
                                                            response1 -> {
                                                                pb_SearchProductListLoader.setVisibility(View.GONE);
                                                                ArrayList<ModelProductDetailList> tempList;
                                                                tempList = ConstantDataParser.getProductList(response1);
                                                                if (modelProductDetailLists.size() > 0) {
                                                                    if (tempList != null) {
                                                                        if (tempList.size() > 0) {
                                                                            modelProductDetailLists.addAll(tempList);
                                                                        }
                                                                    }
                                                                }
                                                                productListAdapter.notifyDataSetChanged();
                                                                productListAdapter.setLoaded();
                                                            }, error -> pb_SearchProductListLoader.setVisibility(View.GONE)) {

                                                        @Override
                                                        public Map<String, String> getHeaders() {
                                                            Map<String, String> params = new HashMap<>();
                                                            params.put("Content-Type", "application/json; charset=utf-8");
                                                            //params.put("Authorization", ConstantFields.getGuestToken());
                                                            return params;
                                                        }
                                                    };

                                                    stringPageRequest.setShouldCache(false);
                                                    ApplicationContext.getInstance().addToRequestQueue(stringPageRequest, "SearchProductListPagination");
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        });

                    }, error -> {
                pb_SearchProductListLoader.setVisibility(View.GONE);

                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        showToast(jsonObject.getString("message"));
                    } else {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    //params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SearchProductList");
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    private void fontSetup() {
        fontSetup(atv_SearchResultHint, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ViewTypeSearchProductList, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_SearchResultHint, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_SortingSearchProductList, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
