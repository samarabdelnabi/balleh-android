package com.balleh.fragments.home;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.db.WishList;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelBanner;
import com.balleh.model.ModelBrandList;
import com.balleh.model.ModelProductDetailList;
import com.balleh.model.ModelProductList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class FragmentHome extends Fragment {

    private View v_HomeContainer;
    private RecyclerView rc_HomePageLister;
    private Activity activity;
    private ArrayList<String> type = new ArrayList<>();
    private ArrayList<ModelBanner> bannersList, categoryList;
    private MenuHandler menuHandler;
    private ArrayList<ModelProductList> ProductList;
    private ProgressBar pb_HomeLoader;
    private ViewPager vp_Banner;
    private Handler handler;
    private int delay = 4000;
    HomeSliderPageAdapter homeSliderPageAdapter;
    private int page = 0;
    Runnable runnable = new Runnable() {
        public void run() {
            if (homeSliderPageAdapter != null) {
                if (homeSliderPageAdapter.getCount() == page) {
                    page = 0;
                } else {
                    page++;
                }
            } else {
                page = 0;
            }
            if (vp_Banner != null)
                vp_Banner.setCurrentItem(page, true);
            handler.postDelayed(this, delay);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_HomeContainer = inflater.inflate(R.layout.fragment_home, container, false);
        load();
        return v_HomeContainer;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (bannersList != null) {
            if (bannersList.size() > 0) {
                handler.removeCallbacks(runnable);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bannersList != null) {
            if (bannersList.size() > 0) {
                handler.postDelayed(runnable, delay);
            }
        }

        menuHandler.changeBottomView(1);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuHandler.changeBottomView(1);
    }

    private void load() {
        rc_HomePageLister = v_HomeContainer.findViewById(R.id.rc_home_page);
        rc_HomePageLister.setLayoutManager(new LinearLayoutManager(activity));
        pb_HomeLoader = v_HomeContainer.findViewById(R.id.pb_home_loader);

        loadBanner();
    }

    private void loadBanner() {
        if (menuHandler.networkError()) {
            pb_HomeLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.mBanner,
                    response -> {
                        Log.e("BannerOnResponse: ", response);
                        pb_HomeLoader.setVisibility(View.GONE);

                        if (type != null) {
                            if (type.size() > 0) {
                                type.clear();
                            }
                        }

                        bannersList = ConstantDataParser.getBannerCategoryList(response, 0);

                        if (bannersList != null) {
                            if (bannersList.size() > 0) {
                                type.add("Banner");
                            }
                        }

                        categoryList = ConstantDataParser.getBannerCategoryList(response, 1);

                        if (categoryList != null) {
                            if (categoryList.size() > 0) {
                                type.add("Category");
                            }
                        }

                        productList();

                    }, error -> {
                pb_HomeLoader.setVisibility(View.GONE);
                productList();
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    }
                } catch (Exception e) {
                    Log.e("onErrorResponse: ", e.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "BannerList");
        }
    }

    private void productList() {
        if (menuHandler.networkError()) {
            pb_HomeLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.mHomeContent,
                    response -> {
                        Log.e("productList: ", ConstantFields.mHomeContent);
                        Log.e("ProductOnResponse: ", response);
                        pb_HomeLoader.setVisibility(View.GONE);

                        ProductList = ConstantDataParser.getHomeProductList(response);

                        if (ProductList != null) {
                            if (ProductList.size() > 0) {
                                for (int i = 0; i < ProductList.size(); i++) {
                                    type.add("CategoryProduct");
                                }
                            }
                        }

                    /*if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mBrandTag).equals("Empty")) {
                        type.add("Brand");
                    }*/

                        type.add("Static");

                        if (type != null) {
                            if (type.size() > 0) {
                                rc_HomePageLister.setAdapter(new HomePageAdapter(type));
                            }
                        }
                    }, error -> {
                pb_HomeLoader.setVisibility(View.GONE);

                    /*if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mBrandTag).equals("Empty")) {
                        type.add("Brand");
                    }*/

                type.add("Static");

                if (type != null) {
                    if (type.size() > 0) {
                        rc_HomePageLister.setAdapter(new HomePageAdapter(type));
                    }
                }

                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    }
                } catch (Exception e) {
                    Log.e("onErrorResponse: ", e.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "HomeContentList");
        }
    }

    class HomePageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<String> type;

        HomePageAdapter(ArrayList<String> type) {
            this.type = type;
        }

        private int Banner = 1, Category = 2, ProductListByCategory = 3, StaticContent = 5, Brand = 6;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder holder;
            if (viewType == StaticContent) {
                holder = new HPStaticContent(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_static_content, parent, false));
            } else if (viewType == Banner) {
                holder = new HPBanner(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_banner, parent, false));
            } else if (viewType == Category) {
                holder = new HPCategory(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_category, parent, false));
            } else if (viewType == ProductListByCategory) {
                holder = new HPProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_product_list, parent, false));
            } else if (viewType == Brand) {
                holder = new HPBrandList(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_brand_list, parent, false));
            } else {
                holder = new HPRecentProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_product_list, parent, false));
            }
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            if (holder.getItemViewType() == StaticContent) {
                HPStaticContent viewHolderStaticContent = (HPStaticContent) holder;
                String copy_right = getString(R.string.hp_static_copy_rights) + " " + Calendar.getInstance().get(Calendar.YEAR);
                viewHolderStaticContent.atv_CopyRights.setText(copy_right);
            } else if (holder.getItemViewType() == Banner) {
                final HPBanner hpBanner = (HPBanner) holder;
                homeSliderPageAdapter = new HomeSliderPageAdapter();
                hpBanner.vp_Banner.setAdapter(homeSliderPageAdapter);

                handler.postDelayed(runnable, delay);

                vp_Banner = hpBanner.vp_Banner;

                int pos = hpBanner.vp_Banner.getCurrentItem();
                for (int i = 0; i < bannersList.size(); i++) {
                    View view = LayoutInflater.from(activity).inflate(R.layout.v_home_banner_btn, null, false);
                    final Button button = view.findViewById(R.id.btn_image_indicator);
                    button.setPadding(100, 100, 100, 100);
                    int size = (int) getResources().getDisplayMetrics().density;
                    if (size == 3) {
                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(30, 30);
                        button.setLayoutParams(layoutParams);
                    } else if (size == 2) {
                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(25, 25);
                        button.setLayoutParams(layoutParams);
                    } else {
                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(20, 20);
                        button.setLayoutParams(layoutParams);
                    }

                    if (pos == i) {
                        button.setBackgroundResource(R.drawable.draw_btn_banner_selected);
                    } else {
                        button.setBackgroundResource(R.drawable.draw_btn_banner_non_select);
                    }
                    button.setId(i);
                    button.setOnClickListener(v -> hpBanner.vp_Banner.setCurrentItem(button.getId()));
                    hpBanner.ll_BannerBtn.addView(button);
                }


                hpBanner.vp_Banner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        page = position;
                        hpBanner.ll_BannerBtn.removeAllViews();
                        if (bannersList != null) {
                            if (bannersList.size() > 0) {
                                for (int i = 0; i < bannersList.size(); i++) {
                                    View view = LayoutInflater.from(activity).inflate(R.layout.v_home_banner_btn, null, false);
                                    final Button button = view.findViewById(R.id.btn_image_indicator);
                                    int size = (int) getResources().getDisplayMetrics().density;
                                    if (size == 3) {
                                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(30, 30);
                                        button.setLayoutParams(layoutParams);
                                    } else if (size == 2) {
                                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(25, 25);
                                        button.setLayoutParams(layoutParams);
                                    } else {
                                        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(20, 20);
                                        button.setLayoutParams(layoutParams);
                                    }

                                    if (position == i) {
                                        button.setBackgroundResource(R.drawable.draw_btn_banner_selected);
                                    } else {
                                        button.setBackgroundResource(R.drawable.draw_btn_banner_non_select);
                                    }
                                    button.setId(i);
                                    button.setOnClickListener(v -> hpBanner.vp_Banner.setCurrentItem(button.getId()));
                                    hpBanner.ll_BannerBtn.addView(button);
                                }
                            }
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });


            } else if (holder.getItemViewType() == Category) {
                HPCategory hpCategory = (HPCategory) holder;

                if (ConstantFields.currentLanguage().equals("en")) {
                    hpCategory.ib_Next.setImageResource(R.drawable.ic_navigate_next_black_24dp);
                    hpCategory.ib_Before.setImageResource(R.drawable.ic_navigate_before_black_24dp);
                } else {
                    hpCategory.ib_Next.setImageResource(R.drawable.ic_navigate_before_black_24dp);
                    hpCategory.ib_Before.setImageResource(R.drawable.ic_navigate_next_black_24dp);
                }

                hpCategory.rc_CategoryList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                hpCategory.rc_CategoryList.setAdapter(new HomeCategoryAdapter(hpCategory.ib_Next, hpCategory.ib_Before, hpCategory.rc_CategoryList));


            } else if (holder.getItemViewType() == ProductListByCategory) {

                final int listPosition;
                if (type.size() > ProductList.size() + 1) {
                    if (position >= ProductList.size()) {
                        listPosition = position - ProductList.size();
                    } else {
                        listPosition = position;
                    }
                } else {
                    listPosition = position;
                }

                HPProductList hpProductList = (HPProductList) holder;
                if (ConstantFields.currentLanguage().equals("en")) {
                    hpProductList.atv_ProductListTitle.setText(ProductList.get(listPosition).getTitle());
                } else {
                    hpProductList.atv_ProductListTitle.setText(ProductList.get(listPosition).getTitle_ar());
                }
                hpProductList.rc_ProductList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                hpProductList.rc_ProductList.setNestedScrollingEnabled(true);
                hpProductList.rc_ProductList.setHasFixedSize(true);
                hpProductList.rc_ProductList.setAdapter(new HomeProductListAdapter(ProductList.get(listPosition)));
                hpProductList.atv_ViewAll.setOnClickListener(view -> menuHandler.toProductList(ProductList.get(listPosition).getCategoryId(), "default", ""));
            } else if (holder.getItemViewType() == Brand) {
                HPBrandList hpBrandList = (HPBrandList) holder;
                hpBrandList.rc_BrandList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                hpBrandList.rc_BrandList.setAdapter(new HomeBrandAdapter());
            } else {
                HPRecentProductList hpRecentProductList = (HPRecentProductList) holder;
                hpRecentProductList.atv_ProductListTitle.setText(getString(R.string.hp_recent_product_list_title));
            }
        }

        @Override
        public int getItemCount() {
            return type.size();
        }

        @Override
        public int getItemViewType(int position) {
            switch (type.get(position)) {
                case "Static":
                    return StaticContent;
                case "Banner":
                    return Banner;
                case "Category":
                    return Category;
                case "CategoryProduct":
                    return ProductListByCategory;
                case "Brand":
                    return Brand;
                default:
                    return 4;
            }
        }

        private class HPBanner extends RecyclerView.ViewHolder {
            ViewPager vp_Banner;
            LinearLayout ll_BannerBtn;

            HPBanner(View view) {
                super(view);
                vp_Banner = view.findViewById(R.id.vp_home_banner);
                ll_BannerBtn = view.findViewById(R.id.ll_banner_button_holder);
            }
        }

        private class HPCategory extends RecyclerView.ViewHolder {
            RecyclerView rc_CategoryList;
            LinearLayout ll_BannerBtnHolder;
            ImageButton ib_Next, ib_Before;

            HPCategory(View itemView) {
                super(itemView);
                rc_CategoryList = itemView.findViewById(R.id.rc_home_category);
                ll_BannerBtnHolder = itemView.findViewById(R.id.ll_banner_button_holder);
                ib_Next = itemView.findViewById(R.id.ib_next_category);
                ib_Before = itemView.findViewById(R.id.ib_back_category);
            }
        }

        private class HPProductList extends RecyclerView.ViewHolder {

            AppCompatTextView atv_ProductListTitle, atv_ViewAll;
            RecyclerView rc_ProductList;

            HPProductList(View itemView) {
                super(itemView);
                atv_ProductListTitle = itemView.findViewById(R.id.atv_home_product_list_title);
                atv_ViewAll = itemView.findViewById(R.id.atv_home_view_all);
                rc_ProductList = itemView.findViewById(R.id.rc_home_product_list);
                fontSetup(atv_ProductListTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ViewAll, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

        private class HPRecentProductList extends RecyclerView.ViewHolder {

            AppCompatTextView atv_ProductListTitle;
            RecyclerView rc_ProductList;

            HPRecentProductList(View itemView) {
                super(itemView);
                atv_ProductListTitle = itemView.findViewById(R.id.atv_home_product_list_title);
                itemView.findViewById(R.id.atv_home_view_all).setVisibility(View.GONE);
                rc_ProductList = itemView.findViewById(R.id.rc_home_product_list);
                fontSetup(atv_ProductListTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

        private class HPBrandList extends RecyclerView.ViewHolder {
            RecyclerView rc_BrandList;

            HPBrandList(View itemView) {
                super(itemView);
                rc_BrandList = itemView.findViewById(R.id.rc_home_brand_list);
            }
        }

        private class HPStaticContent extends RecyclerView.ViewHolder {
            AppCompatTextView atv_CopyRights;

            HPStaticContent(View itemView) {
                super(itemView);
                atv_CopyRights = itemView.findViewById(R.id.atv_hp_copy_rights);
                fontSetup(atv_CopyRights, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
    }

    class HomeCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        ImageButton ib_Next, ib_Before;

        HomeCategoryAdapter(ImageButton ib_Next, ImageButton ib_Before, RecyclerView rc_CategoryHolder) {
            this.ib_Before = ib_Before;
            this.ib_Next = ib_Next;

            if (categoryList != null) {
                if (categoryList.size() > 0) {
                    if (categoryList.size() > 3) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rc_CategoryHolder.getLayoutManager();
                        ib_Before.setOnClickListener(v -> {

                            if (categoryList.size() > 4) {
                                rc_CategoryHolder.getLayoutManager().scrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                            } else {
                                rc_CategoryHolder.getLayoutManager().scrollToPosition(0);
                            }
                        });
                        ib_Next.setOnClickListener(v -> {
                            if (categoryList.size() > 4) {
                                rc_CategoryHolder.getLayoutManager().scrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                            } else {
                                rc_CategoryHolder.getLayoutManager().scrollToPosition(categoryList.size() - 1);
                            }
                        });
                    } else {
                        ib_Before.setVisibility(View.GONE);
                        ib_Next.setVisibility(View.GONE);
                    }
                } else {
                    ib_Before.setVisibility(View.GONE);
                    ib_Next.setVisibility(View.GONE);
                }
            } else {
                ib_Before.setVisibility(View.GONE);
                ib_Next.setVisibility(View.GONE);
            }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new HPCategory(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_category_image, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            HPCategory category = (HPCategory) holder;
            ImageLoader.glide_image_loader(categoryList.get(position).getImage_url(), category.iv_CategoryImage, "Static");

            category.atv_CategoryTitle.setText(categoryList.get(position).getCategoryTitle());

            category.iv_CategoryImage.setOnClickListener(view -> menuHandler.toProductList(Integer.valueOf(categoryList.get(position).getCategory_id()),
                    "default", ""));
        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        private class HPCategory extends RecyclerView.ViewHolder {
            ImageView iv_CategoryImage;
            AppCompatTextView atv_CategoryTitle;

            HPCategory(View itemView) {
                super(itemView);
                iv_CategoryImage = itemView.findViewById(R.id.iv_home_category_link);
                atv_CategoryTitle = itemView.findViewById(R.id.atv_home_category_title);
                fontSetup(atv_CategoryTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

    }

    class HomeSliderPageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return bannersList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {

            LayoutInflater inflater = LayoutInflater.from(activity);
            ViewGroup v_BannerImageContainer = (ViewGroup) inflater.inflate(R.layout.vp_row_banner, container, false);
            ImageView iv_BannerImage = v_BannerImageContainer.findViewById(R.id.iv_home_banner_image);
            ImageLoader.glide_image_loader(bannersList.get(position).getImage_url(), iv_BannerImage, "Default");

            iv_BannerImage.setOnClickListener(view -> {

                if (bannersList.get(position).getCategory_id() != null) {
                    if (bannersList.get(position).getCategory_id().length() > 0) {
                        if (!bannersList.get(position).getCategory_id().equals("0")) {
                            menuHandler.toProductList(Integer.valueOf(bannersList.get(position).getCategory_id()), "default", "");
                        } else {

                            if (bannersList.get(position).getProduct_id() != null) {
                                if (bannersList.get(position).getProduct_id().length() > 0) {
                                    menuHandler.toProductDetail(bannersList.get(position).getProduct_id());
                                }
                            }
                        }
                    } else {
                        if (bannersList.get(position).getProduct_id() != null) {
                            if (bannersList.get(position).getProduct_id().length() > 0) {
                                menuHandler.toProductDetail(bannersList.get(position).getProduct_id());
                            }
                        }
                    }
                } else {
                    if (bannersList.get(position).getProduct_id() != null) {
                        if (bannersList.get(position).getProduct_id().length() > 0) {
                            menuHandler.toProductDetail(bannersList.get(position).getProduct_id());
                        }
                    }
                }
            });

            container.addView(v_BannerImageContainer);
            return v_BannerImageContainer;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }
    }

    class HomeProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelProductDetailList> productDetailLists = new ArrayList<>();
        private ModelProductList modelProductList;

        HomeProductListAdapter(ModelProductList modelProductList) {
            this.modelProductList = modelProductList;
            if (menuHandler.networkError())
                loadProductList();
        }

        private void loadProductList() {
            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    ConstantFields.getProductList(String.valueOf(modelProductList.getCategoryId()), 1, "Default", "", ConstantFields.currentLanguage().equals("en")),
                    response -> {

                        Log.e("loadProductList: ", response + "");

                        productDetailLists = ConstantDataParser.getProductList(response);
                        notifyDataSetChanged();
                    }, error -> {
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    }
                } catch (Exception e) {
                    Log.e("onErrorResponse: ", e.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "HomeProductList");
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder;
            if (viewType == 1) {
                holder = new ProductListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_product, parent, false));
            } else {
                holder = new ProgressViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_pb_loader, parent, false));
            }
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 1) {
                ProductListViewHolder productListViewHolder = (ProductListViewHolder) holder;

                if (productDetailLists != null) {
                    if (productDetailLists.size() > 0) {

                        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                            if (WishList.getInstance(activity.getApplicationContext()).getSizeWishList() > 0)
                                if (WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetailLists.get(position).getSku())) {
                                    productListViewHolder.ib_ProductFavHome.setImageResource(R.drawable.ic_favorite_selected_24dp);
                                } else {
                                    productListViewHolder.ib_ProductFavHome.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                                }
                        }

                        if (productDetailLists.get(position).getAttribute_result() != null) {
                            if (productDetailLists.get(position).getAttribute_result().size() > 0) {
                                for (int i = 0; i < productDetailLists.get(position).getAttribute_result().size(); i++) {
                                    if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("small_image")) {
                                        ImageLoader.glide_image_loader(ConstantFields.ImageURL + productDetailLists.get(position).getAttribute_result().get(i)[1],
                                                productListViewHolder.iv_ProductImageHome, "Original");
                                    }
                                }
                            }
                        }

                        if (productDetailLists.get(position).getAttribute_result() != null) {
                            if (productDetailLists.get(position).getAttribute_result().size() > 0) {
                                for (int i = 0; i < productDetailLists.get(position).getAttribute_result().size(); i++) {
                                    if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("special_price")) {
                                        productDetailLists.get(position).setSpecial_price(Double.valueOf(productDetailLists.get(position).getAttribute_result().get(i)[1].trim()).intValue());
                                    }

                                   /* if (productDetailLists.get(position).getAttribute_result().get(i)[0] != null) {
                                        if (productDetailLists.get(position).getAttribute_result().get(i)[0].toLowerCase().equals("meta_title")) {
                                            String title = productDetailLists.get(position).getAttribute_result().get(i)[1];
                                            if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                                title = title.substring(4);
                                            }
                                            productListViewHolder.atv_ProductTitleHome.setText(title);
                                        }
                                    }*/


                                    if (ConstantFields.currentLanguage().equals("ar")) {
                                        if (!productDetailLists.get(position).getAr_title().equals("a")) {
                                            productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getAr_title());
                                        } else {
                                            if (productDetailLists.get(position).getAttribute_result().get(i)[0] != null) {
                                                if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                                    String title = productDetailLists.get(position).getAttribute_result().get(i)[1];
                                                    if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                                        title = title.substring(4);
                                                    }
                                                    productListViewHolder.atv_ProductTitleHome.setText(title);
                                                }
                                            }
                                        }
                                    } else {
                                        if (!productDetailLists.get(position).getEn_title().equals("a")) {
                                            productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getEn_title());
                                        } else {
                                            if (productDetailLists.get(position).getAttribute_result().get(i)[0] != null) {
                                                if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                                    String title = productDetailLists.get(position).getAttribute_result().get(i)[1];
                                                    if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                                        title = title.substring(4);
                                                    }
                                                    productListViewHolder.atv_ProductTitleHome.setText(title);
                                                }
                                            }
                                        }
                                    }

                                }

                                if (productListViewHolder.atv_ProductTitleHome.getText().toString().length() == 0) {
                                    productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                                }

                            } else {
                                productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                            }
                        } else {
                            productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                        }

                        if (productDetailLists.get(position).getSpecial_price() != 0) {
                            productListViewHolder.atv_ProductPriceHome.setVisibility(View.VISIBLE);
                            productListViewHolder.atv_ProductPriceHome.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                            productListViewHolder.atv_ProductPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getPrice()));
                            productListViewHolder.atv_ProductSpecialPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getSpecial_price()));
                        } else {
                            productListViewHolder.atv_ProductSpecialPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getPrice()));
                            productListViewHolder.atv_ProductPriceHome.setVisibility(View.GONE);
                        }
                    }
                }

            } else {
                ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
                progressViewHolder.pb_Loader.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            if (productDetailLists != null) {
                if (productDetailLists.size() > 0) {
                    return productDetailLists.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (productDetailLists != null) {
                if (productDetailLists.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        private class ProgressViewHolder extends RecyclerView.ViewHolder {
            ProgressBar pb_Loader;

            ProgressViewHolder(View itemView) {
                super(itemView);
                pb_Loader = itemView.findViewById(R.id.pb_loader);
            }
        }

        private class ProductListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView iv_ProductImageHome;
            AppCompatTextView atv_ProductTitleHome, atv_ProductPriceHome, atv_ProductSpecialPriceHome;
            ImageButton ib_ProductFavHome;

            ProductListViewHolder(View view) {
                super(view);
                iv_ProductImageHome = view.findViewById(R.id.iv_product_image_product_list_home);
                atv_ProductTitleHome = view.findViewById(R.id.atv_product_title_product_list_home);
                atv_ProductPriceHome = view.findViewById(R.id.atv_product_price_product_list_home);
                atv_ProductSpecialPriceHome = view.findViewById(R.id.atv_product_special_price_product_list_home);
                ib_ProductFavHome = view.findViewById(R.id.ib_product_wish_list_product_list_home);
                ib_ProductFavHome.setOnClickListener(this);
                iv_ProductImageHome.setOnClickListener(this);
                atv_ProductTitleHome.setOnClickListener(this);
                atv_ProductPriceHome.setOnClickListener(this);
                atv_ProductSpecialPriceHome.setOnClickListener(this);

                fontSetup(atv_ProductTitleHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductPriceHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductSpecialPriceHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }

            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.ib_product_wish_list_product_list_home:
                        if (getAdapterPosition() != -1)
                            addToWishList(ib_ProductFavHome, getAdapterPosition());
                        break;
                    case R.id.iv_product_image_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_title_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_price_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_special_price_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                }
            }
        }

        private void toProductDetail(int position) {
            menuHandler.toProductDetail(productDetailLists.get(position).getSku());
        }

        private void addToWishList(ImageButton ib_ProductFav, int position) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                if (!WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetailLists.get(position).getSku())) {
                    WishList.getInstance(activity.getApplicationContext()).add_to_wish_list(productDetailLists.get(position).getSku(),
                            productDetailLists.get(position).getProductDetail());
                    ib_ProductFav.setImageResource(R.drawable.ic_favorite_selected_24dp);
                } else {
                    WishList.getInstance(activity.getApplicationContext()).remove_from_wish_list(productDetailLists.get(position).getSku());
                    ib_ProductFav.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                }
            } else {
                menuHandler.toSignIn();
            }
        }

    }

    class HomeBrandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelBrandList> brandLists = new ArrayList<>();

        HomeBrandAdapter() {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mBrandTag).equals("Empty")) {
                brandLists = ConstantDataParser.getBrandList(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mBrandTag));
            }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new HPCategory(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_category_image, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            HPCategory category = (HPCategory) holder;
            ImageLoader.glide_image_loader(brandLists.get(position).getImagePath(), category.iv_BrandImage, "Static");

            category.atv_BrandTitle.setText(brandLists.get(position).getName());

            category.iv_BrandImage.setOnClickListener(view -> {
                //menuHandler.toProductList(Integer.valueOf(brandLists.get(position).getId()), 10, "default");
            });
        }

        @Override
        public int getItemCount() {
            return brandLists.size();
        }

        private class HPCategory extends RecyclerView.ViewHolder {
            ImageView iv_BrandImage;
            AppCompatTextView atv_BrandTitle;

            HPCategory(View itemView) {
                super(itemView);
                iv_BrandImage = itemView.findViewById(R.id.iv_home_category_link);
                atv_BrandTitle = itemView.findViewById(R.id.atv_home_category_title);
                fontSetup(atv_BrandTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
