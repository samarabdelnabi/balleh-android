package com.balleh.fragments.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.instentTransfer.MenuHandler;

public class FragmentAccount extends Fragment {

    private MenuHandler menuHandler;
    private View v_AccountHolder;
    private AppCompatTextView atv_EditAccount, atv_ChangePassword, atv_AddressBook,
            atv_OrderList, atv_DownloadableProducts, atv_AboutUs, atv_ContactUs, atv_Logout,
            atv_AccountTitle, atv_OrderTitle, atv_ExtraTitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_AccountHolder = inflater.inflate(R.layout.fragment_account_home, container, false);
        load();
        return v_AccountHolder;
    }

    private void load() {

        atv_EditAccount = v_AccountHolder.findViewById(R.id.atv_edit_account);
        atv_ChangePassword = v_AccountHolder.findViewById(R.id.atv_change_password);
        atv_AddressBook = v_AccountHolder.findViewById(R.id.atv_address_book);
        atv_OrderList = v_AccountHolder.findViewById(R.id.atv_orders);
        atv_DownloadableProducts = v_AccountHolder.findViewById(R.id.atv_downloadable_products);
        atv_AboutUs = v_AccountHolder.findViewById(R.id.atv_about_us);
        atv_ContactUs = v_AccountHolder.findViewById(R.id.atv_contact_us);
        atv_Logout = v_AccountHolder.findViewById(R.id.atv_logout);
        atv_AccountTitle = v_AccountHolder.findViewById(R.id.atv_account_title);
        atv_OrderTitle = v_AccountHolder.findViewById(R.id.atv_order_title);
        atv_ExtraTitle = v_AccountHolder.findViewById(R.id.atv_extra_title);

        atv_EditAccount.setOnClickListener(view -> menuHandler.loadEditAccount());
        atv_ChangePassword.setOnClickListener(view -> menuHandler.loadChangePassword());
        atv_AddressBook.setOnClickListener(view -> menuHandler.loadAddressBook());
        atv_OrderList.setOnClickListener(view -> menuHandler.loadOrderList());
        atv_DownloadableProducts.setOnClickListener(view -> menuHandler.loadDownloadableProduct());
        atv_AboutUs.setOnClickListener(view -> menuHandler.loadAboutUs());
        atv_ContactUs.setOnClickListener(view -> menuHandler.loadContactUs());
        atv_Logout.setOnClickListener(view -> menuHandler.loadLogout());

        fontSetup();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuHandler.changeBottomView(5);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        menuHandler = (MenuHandler) context;
    }

    private void fontSetup() {
        fontSetup(atv_EditAccount, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ChangePassword, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AddressBook, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_OrderList, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_DownloadableProducts, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AboutUs, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ContactUs, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_Logout, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AccountTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_OrderTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ExtraTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
