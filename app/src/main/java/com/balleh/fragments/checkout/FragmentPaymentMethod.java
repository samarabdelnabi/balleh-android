package com.balleh.fragments.checkout;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.instentTransfer.CheckOutHandler;
import com.balleh.fragments.DialogLoader;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelPaymentMethod;
import com.balleh.model.ModelShippingMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentPaymentMethod extends Fragment {

    private View v_PaymentHolder;
    private Activity activity;
    private RadioGroup rg_PaymentList;
    private AppCompatTextView atv_ToConfirmOrder, atv_PaymentTitle;
    private int check_list[];
    private RadioGroup.LayoutParams layoutParams;
    private CheckOutHandler checkOutHandler;
    private String type;
    private DialogLoader dialogLoader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            type = "Customer";
        } else {
            type = "Guest";
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_PaymentHolder = inflater.inflate(R.layout.fragment_checkout_payment_method, container, false);
        load();
        return v_PaymentHolder;
    }

    private void load() {

        ImageButton mDeliveryDetail, mDeliveryType;

        atv_PaymentTitle = v_PaymentHolder.findViewById(R.id.atv_payment_method_title);
        rg_PaymentList = v_PaymentHolder.findViewById(R.id.rg_payment_list);
        atv_ToConfirmOrder = v_PaymentHolder.findViewById(R.id.atv_to_confirm_order);
        mDeliveryDetail = v_PaymentHolder.findViewById(R.id.ib_payment_type_delivery_detail);
        mDeliveryType = v_PaymentHolder.findViewById(R.id.ib_payment_type_delivery_type);

        dialogLoader = new DialogLoader();
        dialogLoader.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogLoader.setCancelable(false);

        if (type.equals("Customer")) {
            getCustomerShippingMethod();
        } else {
            getGuestShippingMethod();
        }

        mDeliveryDetail.setOnClickListener(v -> checkOutHandler.backPressed("ToAddress", "Payment"));

        mDeliveryType.setOnClickListener(v -> checkOutHandler.backPressed("ToShipping", "Payment"));

        fontSetup();
    }

    private void getCustomerShippingMethod() {

        try {
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("customer_id", modelAddress.getCustomerId());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());

                JSONObject jsonObjectPaymentAddress = new JSONObject();

                jsonObjectPaymentAddress.put("shipping_address", jsonObjectAddress);
                jsonObjectPaymentAddress.put("billing_address", jsonObjectAddress);

                ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();

                if (modelShippingMethod != null) {
                    jsonObjectPaymentAddress.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                    jsonObjectPaymentAddress.put("shipping_method_code", modelShippingMethod.getMethod_code());
                }

                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("addressInformation", jsonObjectPaymentAddress);

                Log.e("getShippingMethod: ", jsonObject.toString());
                dialogHandler(1);

                Log.e("CustomerShipMethod: ", ConstantFields.getCustomerPaymentMethod(ConstantFields.currentLanguage().equals("en")));

                if (checkOutHandler.networkChecker()) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerPaymentMethod(ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                Log.e("onResponse: ", response + "");
                                dialogHandler(2);

                                final ArrayList<ModelPaymentMethod> paymentList = ConstantDataParser.getPaymentMethod(response);
                                if (paymentList != null) {
                                    if (paymentList.size() > 0) {
                                        check_list = new int[paymentList.size()];
                                        for (int i = 0; i < paymentList.size(); i++) {
                                            RadioButton radioButton = new RadioButton(getActivity());
                                            int id = i + 10;
                                            check_list[i] = id;
                                            radioButton.setId(id);
                                            radioButton.setText(paymentList.get(i).getTitle());
                                            fontSetup(radioButton,Constant.LIGHT,Constant.C_RADIO_BUTTON);
                                            layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            rg_PaymentList.addView(radioButton, layoutParams);

                                        }

                                        atv_ToConfirmOrder.setOnClickListener(view -> {
                                            int id = rg_PaymentList.getCheckedRadioButtonId();
                                            int count = 0;
                                            ModelPaymentMethod modelPaymentMethod = new ModelPaymentMethod();
                                            for (int t = 0; t < check_list.length; t++) {
                                                if (id == check_list[t]) {
                                                    if (CheckOut.getInstance(activity).getSizePayment() == 0) {
                                                        modelPaymentMethod.setTitle(paymentList.get(t).getTitle());
                                                        modelPaymentMethod.setCode(paymentList.get(t).getCode());
                                                        CheckOut.getInstance(activity).insertPaymentType(modelPaymentMethod);
                                                    } else {
                                                        modelPaymentMethod.setTitle(paymentList.get(t).getTitle());
                                                        modelPaymentMethod.setCode(paymentList.get(t).getCode());
                                                        CheckOut.getInstance(activity).updatePaymentType(modelPaymentMethod);
                                                    }
                                                    count++;
                                                }
                                            }
                                            if (count == 0) {
                                                ConstantFields.CustomToast(getString(R.string.payment_error), activity);
                                            } else {
                                                checkOutHandler.loadPlaceOrder(response);
                                            }
                                        });
                                    }
                                }
                            }, error -> {
                        dialogHandler(2);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject1 = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject1.toString());
                            }
                        } catch (Exception e) {
                            Log.e("onErrorResponse: ", e.toString());
                        }
                    }) {

                        @Override
                        public byte[] getBody() {
                            return jsonObject.toString().getBytes();
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "PaymentMethod");
                }
            }

        } catch (Exception e) {
            Log.e("getShippingMethod: ", e.toString());
        }
    }

    private void getGuestShippingMethod() {

        try {
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());

                JSONObject jsonObjectPaymentAddress = new JSONObject();

                jsonObjectPaymentAddress.put("shipping_address", jsonObjectAddress);
                jsonObjectPaymentAddress.put("billing_address", jsonObjectAddress);

                ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();

                if (modelShippingMethod != null) {
                    jsonObjectPaymentAddress.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                    jsonObjectPaymentAddress.put("shipping_method_code", modelShippingMethod.getMethod_code());
                }

                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("addressInformation", jsonObjectPaymentAddress);

                Log.e("getShippingMethod: ", jsonObject.toString());
                dialogHandler(1);


                if (checkOutHandler.networkChecker()) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getGuestPaymentMethod(ConstantDataSaver.
                            mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                Log.e("onResponse: ", response + "");
                                dialogHandler(2);
                                final ArrayList<ModelPaymentMethod> paymentList = ConstantDataParser.getPaymentMethod(response);
                                if (paymentList != null) {
                                    if (paymentList.size() > 0) {
                                        check_list = new int[paymentList.size()];
                                        for (int i = 0; i < paymentList.size(); i++) {
                                            RadioButton radioButton = new RadioButton(getActivity());
                                            int id = i + 10;
                                            check_list[i] = id;
                                            radioButton.setId(id);
                                            radioButton.setText(paymentList.get(i).getTitle());
                                            fontSetup(radioButton,Constant.LIGHT,Constant.C_RADIO_BUTTON);
                                            layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            rg_PaymentList.addView(radioButton, layoutParams);

                                        }

                                        atv_ToConfirmOrder.setOnClickListener(view -> {
                                            int id = rg_PaymentList.getCheckedRadioButtonId();
                                            int count = 0;
                                            ModelPaymentMethod modelPaymentMethod = new ModelPaymentMethod();
                                            for (int t = 0; t < check_list.length; t++) {
                                                if (id == check_list[t]) {
                                                    if (CheckOut.getInstance(activity).getSizePayment() == 0) {
                                                        modelPaymentMethod.setTitle(paymentList.get(t).getTitle());
                                                        modelPaymentMethod.setCode(paymentList.get(t).getCode());
                                                        CheckOut.getInstance(activity).insertPaymentType(modelPaymentMethod);
                                                    } else {
                                                        modelPaymentMethod.setTitle(paymentList.get(t).getTitle());
                                                        modelPaymentMethod.setCode(paymentList.get(t).getCode());
                                                        CheckOut.getInstance(activity).updatePaymentType(modelPaymentMethod);
                                                    }
                                                    count++;
                                                }
                                            }
                                            if (count == 0) {
                                                ConstantFields.CustomToast(getString(R.string.payment_error), activity);
                                            } else {
                                                checkOutHandler.loadPlaceOrder(response);
                                            }
                                        });
                                    }
                                }
                            }, error -> {

                        dialogHandler(2);

                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject1 = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject1.toString());
                            }
                        } catch (Exception e) {
                            Log.e("onErrorResponse: ", e.toString());
                        }
                    }) {

                        @Override
                        public byte[] getBody() {
                            return jsonObject.toString().getBytes();
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "PaymentMethod");
                }
            }

        } catch (Exception e) {
            Log.e("getShippingMethod: ", e.toString());

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        checkOutHandler = (CheckOutHandler) context;
    }

    private void dialogHandler(int type) {
        if (type == 1) {
            dialogLoader.show(getChildFragmentManager(), "DialogLoader");
        } else {
            dialogLoader.dismiss();
        }
    }

    private void fontSetup() {
        fontSetup(atv_PaymentTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ToConfirmOrder, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
