package com.balleh.fragments.checkout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.OrderProductListAdapter;
import com.balleh.adapter.OrderTotalListAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.instentTransfer.CheckOutHandler;
import com.balleh.fragments.DialogLoader;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelOrderDetail;
import com.balleh.model.ModelPaymentMethod;
import com.balleh.model.ModelShippingMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentPlaceOrder extends Fragment {

    private Activity activity;
    private CheckOutHandler checkOutHandler;
    private View v_PlaceOrderHolder;
    private String result, total;
    private RecyclerView rc_TotalList;
    private String type;
    private DialogLoader dialogLoader;
    private TextInputLayout til_CouponTitle;
    private EditText et_CouponCode;
    private Button btn_CouponCodeApply;
    private boolean isCouponLoaded = false;
    private ModelPaymentMethod modelPaymentMethod;
    private AppCompatTextView atv_ShippingAddress, atv_PaymentAddress, atv_ShippingMethod, atv_PaymentMethod, atv_Email;

    public FragmentPlaceOrder() {
    }

    public static FragmentPlaceOrder getInstance(String data) {
        FragmentPlaceOrder fragment_payment_typeCheckOut = new FragmentPlaceOrder();
        Bundle bundle = new Bundle();
        bundle.putString("PaymentResponse", data);
        fragment_payment_typeCheckOut.setArguments(bundle);
        return fragment_payment_typeCheckOut;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            result = getArguments().getString("PaymentResponse");
        }

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            type = "Customer";
        } else {
            type = "Guest";
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_PlaceOrderHolder = inflater.inflate(R.layout.fragment_checkout_place_order, container, false);
        load();
        return v_PlaceOrderHolder;
    }

    private void load() {

        ImageButton ib_DeliveryDetail, ib_DeliveryType, ib_PaymentType;

        atv_ShippingAddress = v_PlaceOrderHolder.findViewById(R.id.atv_shipping_address_order);
        atv_PaymentAddress = v_PlaceOrderHolder.findViewById(R.id.atv_payment_address_order);
        atv_ShippingMethod = v_PlaceOrderHolder.findViewById(R.id.atv_shipping_method_order);
        atv_PaymentMethod = v_PlaceOrderHolder.findViewById(R.id.atv_payment_method_order);
        atv_Email = v_PlaceOrderHolder.findViewById(R.id.atv_email_order);

        RecyclerView rc_ProductList = v_PlaceOrderHolder.findViewById(R.id.rc_order_product_list_order);
        rc_TotalList = v_PlaceOrderHolder.findViewById(R.id.rc_order_total_list_order);

        ib_DeliveryDetail = v_PlaceOrderHolder.findViewById(R.id.ib_delivery_detail_place_order);
        ib_DeliveryType = v_PlaceOrderHolder.findViewById(R.id.ib_delivery_type_place_order);
        ib_PaymentType = v_PlaceOrderHolder.findViewById(R.id.ib_payment_type_place_order);

        rc_ProductList.setLayoutManager(new LinearLayoutManager(activity));
        rc_TotalList.setLayoutManager(new LinearLayoutManager(activity));

        til_CouponTitle = v_PlaceOrderHolder.findViewById(R.id.til_coupon_title_holder);
        et_CouponCode = v_PlaceOrderHolder.findViewById(R.id.et_coupon_value);
        btn_CouponCodeApply = v_PlaceOrderHolder.findViewById(R.id.btn_coupon_apply);

        dialogLoader = new DialogLoader();
        dialogLoader.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogLoader.setCancelable(false);

        ib_DeliveryDetail.setOnClickListener(v -> checkOutHandler.backPressed("ToAddress", "PlaceOrder"));

        ib_DeliveryType.setOnClickListener(v -> checkOutHandler.backPressed("ToShipping", "PlaceOrder"));

        ib_PaymentType.setOnClickListener(v -> checkOutHandler.backPressed("ToPayment", "PlaceOrder"));

        ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();

        if (modelAddress != null) {

            String address = null;

            if (modelAddress.getFirstName() != null)
                if (modelAddress.getFirstName().length() > 0)
                    address = modelAddress.getFirstName();

            if (modelAddress.getLastName() != null)
                if (modelAddress.getLastName().length() > 0)
                    address = address + " " + modelAddress.getLastName();

            if (modelAddress.getStreet() != null)
                if (modelAddress.getStreet().length() > 0)
                    address = address + "," + modelAddress.getStreet();

            if (modelAddress.getCity() != null)
                if (modelAddress.getCity().length() > 0)
                    address = address + "," + modelAddress.getCity();

            if (modelAddress.getRegionName() != null)
                if (modelAddress.getRegionName().length() > 0)
                    address = address + "," + modelAddress.getRegionName();

            if (modelAddress.getPost_code() != null)
                if (modelAddress.getPost_code().length() > 0)
                    address = address + "," + modelAddress.getPost_code();

            if (modelAddress.getCountryId() != null)
                if (modelAddress.getCountryId().length() > 0)
                    address = address + "," + modelAddress.getCountryId();

            if (modelAddress.getTelephone() != null)
                if (modelAddress.getTelephone().length() > 0)
                    address = address + "," + modelAddress.getTelephone();

            atv_ShippingAddress.setText(address);
            atv_PaymentAddress.setText(address);

            if (modelAddress.getEmail_id() != null) {
                if (modelAddress.getEmail_id().length() > 0) {
                    String email = getString(R.string.user_reg_email) + " : " + modelAddress.getEmail_id();
                    atv_Email.setVisibility(View.VISIBLE);
                    atv_Email.setText(email);
                } else {
                    atv_Email.setVisibility(View.GONE);
                }
            } else {
                atv_Email.setVisibility(View.GONE);
            }
        }

        ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();
        if (modelShippingMethod != null) {
            String title = modelShippingMethod.getCarrier_title() + " - " + modelShippingMethod.getMethod_title();
            atv_ShippingMethod.setText(title);
        }

        ModelPaymentMethod modelPaymentMethod = CheckOut.getInstance(activity).getPaymentType();
        if (modelPaymentMethod != null) {
            atv_PaymentMethod.setText(modelPaymentMethod.getTitle());
        }

        ArrayList<ModelOrderDetail> orderProductList = ConstantDataParser.getOrderProductList(result);

        if (orderProductList != null) {
            if (orderProductList.size() > 0) {

                OrderProductListAdapter orderProductListAdapter = new OrderProductListAdapter(activity, orderProductList);
                rc_ProductList.setAdapter(orderProductListAdapter);

                /*String response = ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mCartDetailTag);

                if (response != null) {
                    if (!response.equals("Empty")) {
                        orderProductList = ConstantDataParser.getCartOrderProductList(response);
                        OrderProductListAdapter orderProductListAdapter = new OrderProductListAdapter(activity, orderProductList);
                        rc_ProductList.setAdapter(orderProductListAdapter);
                    } else {
                        OrderProductListAdapter orderProductListAdapter = new OrderProductListAdapter(activity, orderProductList);
                        rc_ProductList.setAdapter(orderProductListAdapter);
                    }
                } else {
                    OrderProductListAdapter orderProductListAdapter = new OrderProductListAdapter(activity, orderProductList);
                    rc_ProductList.setAdapter(orderProductListAdapter);
                }*/
            } else {
                rc_ProductList.setVisibility(View.GONE);
            }
        } else {
            rc_ProductList.setVisibility(View.GONE);
        }

        if (type.equals("Customer")) {
            getCustomerQuoteTotal();
        } else {
            getGuestQuoteTotal();
        }

        v_PlaceOrderHolder.findViewById(R.id.atv_place_order_order).setOnClickListener(view -> {
            toPlaceOrder();
        });

        if (checkOutHandler.networkChecker()) {
            couponValidate();
        }

        fontSetup();

    }

    private void toPlaceOrder() {
        if (type.equals("Customer")) {
            customerPlaceOrder();
        } else {
            guestPlaceOrder();
        }
    }

    private void customerPlaceOrder() {
        try {

            modelPaymentMethod = CheckOut.getInstance(activity).getPaymentType();
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            JSONObject paymentMethodJsonObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag).equals("Empty")) {
                //jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag));
            }

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("customer_id", modelAddress.getCustomerId());
                jsonObjectAddress.put("same_as_billing", "1");
                //jsonObject.put("billing_address", jsonObjectAddress);
            }

            validatePlaceOrder(jsonObject.toString());
            Log.e("placeOrder: ", jsonObject.toString() + "");


        } catch (Exception e) {
            Log.e("placeOrder: ", e.toString());
        }
    }

    private void guestPlaceOrder() {
        try {

            modelPaymentMethod = CheckOut.getInstance(activity).getPaymentType();
            ModelAddress modelAddress = CheckOut.getInstance(activity).getAccountDetail();
            JSONObject paymentMethodJsonObject;
            final JSONObject jsonObject = new JSONObject();

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag));
            }

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject = new JSONObject();
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();

            if (modelShippingMethod != null) {
                paymentMethodJsonObject = new JSONObject();
                paymentMethodJsonObject.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                paymentMethodJsonObject.put("shipping_method_code", modelShippingMethod.getMethod_code());
                //jsonObject.put("shippingMethod", paymentMethodJsonObject);
            }

            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("same_as_billing", "1");
                //jsonObject.put("billing_address", jsonObjectAddress);

                jsonObject.put("email", modelAddress.getEmail_id());
            }

            Log.e("placeOrder: ", jsonObject.toString() + "");

            validatePlaceOrder(jsonObject.toString());

        } catch (Exception e) {
            Log.e("placeOrder: ", e.toString());
        }
    }

    private void validatePlaceOrder(String request) {
        if (modelPaymentMethod != null) {
            if (modelPaymentMethod.getCode().contains("hyperpay")) {
                checkOutHandler.loadPayment(total, "-1", "Hyper");
            } else if (modelPaymentMethod.getCode().contains("checkout_com")) {
                checkOutHandler.loadPayment(total, "-1", "CheckOut.com");
            } else {
                new AsyncTaskForPost().execute(request);
            }
        }
    }

    private void getCustomerQuoteTotal() {
        try {

            final JSONObject jsonObject = new JSONObject();

            ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();
            ModelPaymentMethod modelPaymentMethod = CheckOut.getInstance(activity).getPaymentType();

            if (modelShippingMethod != null) {
                jsonObject.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                jsonObject.put("shipping_method_code", modelShippingMethod.getMethod_code());
            }

            JSONObject paymentMethodJsonObject = new JSONObject();

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            dialogHandler(1);

            if (checkOutHandler.networkChecker()) {
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.mCustomerQuoteTotal,
                        response -> {
                            dialogHandler(2);
                            Log.e("onResponse: ", response + "");

                            total = ConstantDataParser.getTotal(response);

                            ArrayList<String[]> totalList = ConstantDataParser.getTotalListFromQuote(response);

                            couponHandler(response);

                            if (totalList != null) {
                                if (totalList.size() > 0) {
                                    OrderTotalListAdapter orderTotalListAdapter = new OrderTotalListAdapter(activity, totalList);
                                    rc_TotalList.setAdapter(orderTotalListAdapter);
                                } else {
                                    rc_TotalList.setVisibility(View.GONE);
                                }
                            } else {
                                rc_TotalList.setVisibility(View.GONE);
                            }
                        }, error -> {
                    dialogHandler(2);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "QuoteCustomer");
            }
        } catch (Exception e) {
            showToast(getString(R.string.common_error));
        }
    }

    private void couponHandler(String response) {
        String couponCode = ConstantDataParser.getCouponCode(response);
        if (couponCode != null) {
            if (couponCode.length() > 0) {
                et_CouponCode.setText(couponCode);
                btn_CouponCodeApply.setText(getString(R.string._cancel));
                isCouponLoaded = true;
            }
        }
    }

    private void getGuestQuoteTotal() {
        try {

            final JSONObject jsonObject = new JSONObject();

            ModelShippingMethod modelShippingMethod = CheckOut.getInstance(activity).getShippingType();
            ModelPaymentMethod modelPaymentMethod = CheckOut.getInstance(activity).getPaymentType();

            if (modelShippingMethod != null) {
                jsonObject.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                jsonObject.put("shipping_method_code", modelShippingMethod.getMethod_code());
            }

            JSONObject paymentMethodJsonObject = new JSONObject();

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            Log.e("getGuestQuoteTotal: ", jsonObject.toString());
            dialogHandler(1);

            if (checkOutHandler.networkChecker()) {
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.getGuestQuoteTotal(ConstantDataSaver.
                        mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            dialogHandler(2);
                            Log.e("onResponse: ", response + "");

                            total = ConstantDataParser.getTotal(response);

                            ArrayList<String[]> totalList = ConstantDataParser.getTotalListFromQuote(response);
                            couponHandler(response);

                            if (totalList != null) {
                                if (totalList.size() > 0) {
                                    OrderTotalListAdapter orderTotalListAdapter = new OrderTotalListAdapter(activity, totalList);
                                    rc_TotalList.setAdapter(orderTotalListAdapter);
                                } else {
                                    rc_TotalList.setVisibility(View.GONE);
                                }
                            } else {
                                rc_TotalList.setVisibility(View.GONE);
                            }
                        }, error -> {
                    dialogHandler(2);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "QuetoGuest");
            }

        } catch (Exception e) {
            showToast(getString(R.string.common_error));
        }
    }

    private void couponValidate() {
        btn_CouponCodeApply.setOnClickListener(view -> {

            if (!isCouponLoaded) {
                if (et_CouponCode.getText().toString().length() > 0) {
                    til_CouponTitle.setErrorEnabled(false);

                    dialogHandler(1);

                    if (type.equals("Customer")) {

                        StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.applyCustomerCoupon(et_CouponCode.getText().toString()),
                                response -> {
                                    isCouponLoaded = true;

                                    btn_CouponCodeApply.setText(getString(R.string._cancel));

                                    dialogHandler(2);
                                    Log.e("onResponse: ", response + "");
                                    rc_TotalList.setAdapter(null);

                                    if (type.equals("Customer")) {
                                        getCustomerQuoteTotal();
                                    } else {
                                        getGuestQuoteTotal();
                                    }

                                }, error -> {
                            dialogHandler(2);
                            isCouponLoaded = false;
                            btn_CouponCodeApply.setText(getString(R.string.order_coupon_apply));
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject = new JSONObject(responseBody);
                                    if (error.networkResponse.statusCode == 404) {
                                        showToast(jsonObject.getString("message") + "");
                                    } else {
                                        Log.e("onErrorResponse: ", jsonObject.toString());
                                        showToast(getString(R.string.common_error));
                                    }
                                } else {
                                    showToast(getString(R.string.common_error));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.common_error));
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> params = new HashMap<>();
                                params.put("Content-Type", "application/json; charset=utf-8");
                                params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                        ConstantFields.mTokenTag));
                                return params;
                            }
                        };

                        stringRequest.setShouldCache(false);
                        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ApplyCustomerCoupon");

                    } else {
                        StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.applyGuestCoupon(ConstantDataSaver.
                                mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), et_CouponCode.getText().toString()),
                                response -> {
                                    isCouponLoaded = true;

                                    btn_CouponCodeApply.setText(getString(R.string._cancel));

                                    dialogHandler(2);
                                    Log.e("onResponse: ", response + "");
                                    rc_TotalList.setAdapter(null);

                                    if (type.equals("Customer")) {
                                        getCustomerQuoteTotal();
                                    } else {
                                        getGuestQuoteTotal();
                                    }

                                }, error -> {
                            dialogHandler(2);
                            isCouponLoaded = false;
                            btn_CouponCodeApply.setText(getString(R.string.order_coupon_apply));
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject = new JSONObject(responseBody);
                                    if (error.networkResponse.statusCode == 404) {
                                        showToast(jsonObject.getString("message") + "");
                                    } else {
                                        Log.e("onErrorResponse: ", jsonObject.toString());
                                        showToast(getString(R.string.common_error));
                                    }
                                } else {
                                    showToast(getString(R.string.common_error));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.common_error));
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> params = new HashMap<>();
                                params.put("Content-Type", "application/json; charset=utf-8");
                                return params;
                            }
                        };

                        stringRequest.setShouldCache(false);
                        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ApplyGuestCoupon");
                    }

                } else {
                    til_CouponTitle.setErrorEnabled(true);
                    til_CouponTitle.setError(getString(R.string.order_coupon_error));
                }
            } else {
                dialogHandler(1);

                if (type.equals("Customer")) {
                    StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.CustomerCoupon,
                            response -> {
                                et_CouponCode.setText("");
                                isCouponLoaded = false;
                                btn_CouponCodeApply.setText(getString(R.string.order_coupon_apply));
                                dialogHandler(2);
                                Log.e("onResponse: ", response + "");
                                rc_TotalList.setAdapter(null);

                                if (type.equals("Customer")) {
                                    getCustomerQuoteTotal();
                                } else {
                                    getGuestQuoteTotal();
                                }

                            }, error -> {
                        dialogHandler(2);
                        isCouponLoaded = true;
                        btn_CouponCodeApply.setText(getString(R.string._cancel));
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);
                                if (error.networkResponse.statusCode == 404) {
                                    showToast(jsonObject.getString("message") + "");
                                } else {
                                    Log.e("onErrorResponse: ", jsonObject.toString());
                                    showToast(getString(R.string.common_error));
                                }
                            } else {
                                showToast(getString(R.string.common_error));
                            }
                        } catch (Exception e) {
                            showToast(getString(R.string.common_error));
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerCouponDelete");
                } else {
                    StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.deleteGuestCoupon(ConstantDataSaver.
                            mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag)),
                            response -> {
                                et_CouponCode.setText("");
                                isCouponLoaded = false;
                                btn_CouponCodeApply.setText(getString(R.string.order_coupon_apply));
                                dialogHandler(2);
                                Log.e("onResponse: ", response + "");
                                rc_TotalList.setAdapter(null);

                                if (type.equals("Customer")) {
                                    getCustomerQuoteTotal();
                                } else {
                                    getGuestQuoteTotal();

                                }

                            }, error -> {
                        dialogHandler(2);
                        isCouponLoaded = true;
                        btn_CouponCodeApply.setText(getString(R.string._cancel));
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);
                                if (error.networkResponse.statusCode == 404) {
                                    showToast(jsonObject.getString("message") + "");
                                } else {
                                    Log.e("onErrorResponse: ", jsonObject.toString());
                                    showToast(getString(R.string.common_error));
                                }
                            } else {
                                showToast(getString(R.string.common_error));
                            }
                        } catch (Exception e) {
                            showToast(getString(R.string.common_error));
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCouponDelete");
                }

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskForPost extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogHandler(1);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url;
                if (type.equals("Customer")) {
                    url = new URL(ConstantFields.mCustomerPlaceOrder);
                } else {
                    url = new URL(ConstantFields.getGuestPlaceOrder(ConstantDataSaver
                            .mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")));
                }
                Log.e("doInBackground", url.toString());
                Log.e("doInBackground: ", params[0]);
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("POST");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");

                if (type.equals("Customer")) {
                    mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver
                            .mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                }

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(params[0]);
                mBufferedWriter.close();
                mOutputStream.close();

                String mResponse;
                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                    ConstantDataSaver.mRemoveSharedPreferenceString(activity, ConstantFields.mGuestCartIdTag);
                    ConstantDataSaver.mRemoveSharedPreferenceString(activity, ConstantFields.mCustomerCartIdTag);
                    //mResponse = "success";
                } else {
                    mResponse = "Error";
                    String xmResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getErrorStream())));
                    Log.e("doInBackground: ", xmResponse + "");
                    try {
                        JSONObject jsonObject = new JSONObject(xmResponse);
                        if (!jsonObject.isNull("message")) {
                            activity.runOnUiThread(() -> {
                                try {
                                    showToast(jsonObject.getString("message"));
                                } catch (Exception e) {

                                }
                            });
                        }
                    } catch (Exception e) {

                    }
                }
                Log.e("doInBackground: ", mResponse);
                return mResponse;
            } catch (Exception e) {
                Log.e("doInBackground: ", e.toString());
                return "Error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            dialogHandler(2);

            ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag);
            Log.e("onPostExecute: ", s + "");

            if (!s.toLowerCase().equals("Error")) {
                showToast(getString(R.string.order_placed));
                checkOutHandler.orderSuccess(s.replace("\"", ""));
                /*if (modelPaymentMethod != null) {
                    if (modelPaymentMethod.getCode() != null) {
                        if (modelPaymentMethod.getCode().contains("hyperpay")) {
                            checkOutHandler.loadPayment(total, s.replace("\"", ""),"");
                            Log.e("load: ", "Hyper pay :" + total);
                        } else {
                            checkOutHandler.placeOrder(s.replace("\"", ""));
                        }
                    } else {
                        checkOutHandler.placeOrder(s.replace("\"", ""));
                    }
                } else {
                    checkOutHandler.placeOrder(s.replace("\"", ""));
                }*/
            } else {
                showProgressDialog(2);
            }
        }
    }

    @Nullable
    public static String inputStreamToStringConversion(BufferedReader bufferedReader) {
        try {
            String line;
            StringBuilder result = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        checkOutHandler = (CheckOutHandler) context;
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    private void dialogHandler(int type) {
        if (type == 1) {
            dialogLoader.show(getChildFragmentManager(), "DialogLoader");
        } else {
            dialogLoader.dismiss();
        }
    }

    private void showProgressDialog(int type) {
        if (type == 1) {
            new AlertDialog.Builder(activity)
                    .setMessage(getString(R.string.order_payment_status))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        dialog.dismiss();
                    })
                    .setCancelable(false)
                    .show();
        } else {
            new AlertDialog.Builder(activity)
                    .setMessage(getString(R.string.order_failed_payment_status))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        toStartAgain();
                        dialog.dismiss();
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void toStartAgain() {
       /* Intent intent = new Intent(ActivityHyperPG.this, ActivitySplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();*/
        Intent returnIntent = new Intent();
        activity.setResult(Activity.RESULT_OK, returnIntent);
        activity.finish();

    }

    private void fontSetup() {
        fontSetup(til_CouponTitle, Constant.LIGHT, Constant.C_TEXT_INPUT_LAYOUT);

        fontSetup(et_CouponCode, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(btn_CouponCodeApply, Constant.LIGHT, Constant.C_BUTTON);

        fontSetup(atv_ShippingAddress, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_PaymentAddress, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ShippingMethod, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_PaymentMethod, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_Email, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
