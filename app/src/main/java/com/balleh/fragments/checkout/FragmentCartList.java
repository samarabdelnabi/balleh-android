package com.balleh.fragments.checkout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.activities.user.ActivityLogin;
import com.balleh.adapter.CartListAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.CartInstantHelper;
import com.balleh.additional.instentTransfer.CheckOutHandler;
import com.balleh.model.ModelCartListProduct;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentCartList extends Fragment implements CartInstantHelper {

    private Activity activity;
    private CheckOutHandler checkOutHandler;
    private View v_CartList;
    private RecyclerView rc_CartList;
    private AppCompatTextView atvContinue;
    private CartInstantHelper cartInstantHelper;
    private ProgressBar pb_CartLoader;
    private int mCartTotal = 0;
    private String type;
    private ArrayList<ModelCartListProduct> cartProductList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            type = "Customer";
        } else {
            type = "Guest";
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_CartList = inflater.inflate(R.layout.fragment_checkout_cart_list, container, false);
        load();
        return v_CartList;
    }

    private void load() {
        atvContinue = v_CartList.findViewById(R.id.atv_continue_to_check_out);
        rc_CartList = v_CartList.findViewById(R.id.rc_cart_list_check_out);

        pb_CartLoader = v_CartList.findViewById(R.id.pb_cart_loader);
        rc_CartList.setLayoutManager(new LinearLayoutManager(activity));

        if (type.equals("Customer")) {
            CustomerCartListLoader();
        } else {
            GuestCartListLoader();
        }
        fontSetup();
    }

    private void setContinueClickListener() {
        atvContinue.setOnClickListener(view -> {
            if (mCartTotal > 0) {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                    checkOutHandler.loadAddressList();
                } else {
                    checkOutHandler.askPurchaseType();
                }
            } else {
                showToast(getString(R.string.cart_empty_message));
            }
        });
    }

    private void CustomerCartListLoader() {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            Log.e("CustomerCartList: ", ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")));
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("CustomerCartListLoader: ", response + "");
                        pb_CartLoader.setVisibility(View.GONE);
                        ConstantDataSaver.mStoreSharedPreferenceString(activity, ConstantFields.mCartDetailTag, response);
                        cartProductList = ConstantDataParser.getCartDetails(response);
                        CustomerUpdateTotal(cartProductList);
                    }, error -> {
                if (error.networkResponse != null)
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartList");
        }
    }

    private void CustomerUpdateTotal(final ArrayList<ModelCartListProduct> cartProductList) {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            StringRequest stringTotalRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerCartTotalList(ConstantFields.currentLanguage().equals("en")),
                    response -> {

                        Log.e("CustomerUpdateTotal: ", ConstantFields.getCustomerCartTotalList(ConstantFields.currentLanguage().equals("en")) + "");

                        pb_CartLoader.setVisibility(View.GONE);
                        mCartTotal = ConstantDataParser.getCartTotal(response);
                        ArrayList<String[]> totalList = ConstantDataParser.getTotalListFromQuote(response);
                        CartListAdapter cartListAdapter = new CartListAdapter(activity, cartProductList, totalList, cartInstantHelper);
                        rc_CartList.setAdapter(cartListAdapter);

                        setContinueClickListener();
                    }, error -> {
                pb_CartLoader.setVisibility(View.GONE);
                setContinueClickListener();
                if (error.networkResponse.statusCode == 401) {
                    ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringTotalRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringTotalRequest, "CartTotalList");
        }
    }

    private void GuestCartListLoader() {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            Log.e("GuestCartListLoader: ", ConstantFields.getGuestCartItems(ConstantDataSaver.
                    mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")));
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getGuestCartItems(ConstantDataSaver.
                    mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_CartLoader.setVisibility(View.GONE);
                        Log.e("onResponse: ", response);

                        ConstantDataSaver.mStoreSharedPreferenceString(activity, ConstantFields.mCartDetailTag, response);

                        cartProductList = ConstantDataParser.getCartDetails(response);

                        GuestTotalRequest(cartProductList);
                    }, error -> {
                pb_CartLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    }
                } catch (Exception e) {
                    Log.e("onErrorResponse: ", e.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartList");
        }
    }

    private void GuestTotalRequest(final ArrayList<ModelCartListProduct> cartProductList) {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            StringRequest stringTotalRequest = new StringRequest(Request.Method.GET, ConstantFields.getGuestCartItemsTotal(ConstantDataSaver.
                    mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_CartLoader.setVisibility(View.GONE);
                        mCartTotal = ConstantDataParser.getCartTotal(response);
                        ArrayList<String[]> totalList = ConstantDataParser.getTotalListFromQuote(response);
                        CartListAdapter cartListAdapter = new CartListAdapter(activity, cartProductList, totalList, cartInstantHelper);
                        rc_CartList.setAdapter(cartListAdapter);

                        setContinueClickListener();
                    }, error -> {
                pb_CartLoader.setVisibility(View.GONE);
                setContinueClickListener();
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };

            stringTotalRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringTotalRequest, "GuestCartTotalList");
        }
    }

    @Override
    public void cartUpdate(ModelCartListProduct CartDetail, String type) {
        switch (type) {
            case "Add":
                addProduct(1, CartDetail);
                break;
            case "Reduce":
                removeProduct(2, CartDetail);
                break;
            case "Remove":
                removeProduct(1, CartDetail);
                break;
        }
    }

    private void addProduct(int type, final ModelCartListProduct CartDetail) {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            try {
                pb_CartLoader.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("quote_id", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                        ConstantFields.mCustomerCartIdTag));

                if (type == 1) {
                    jsonObject.put("qty", 1);
                } else {
                    jsonObject.put("qty", CartDetail.getQuantity());
                }

                jsonObject.put("sku", CartDetail.getSku());

                if (CartDetail.getProductType().toLowerCase().equals("simple")) {
                    if (CartDetail.getOptionList() != null) {
                        if (CartDetail.getOptionList().size() > 0) {
                            JSONArray selectedOptionList = new JSONArray();
                            for (int i = 0; i < CartDetail.getOptionList().size(); i++) {
                                JSONObject jsonValue = new JSONObject();
                                jsonValue.put("option_value", CartDetail.getOptionList().get(i).getOption_value_id());
                                jsonValue.put("option_id", CartDetail.getOptionList().get(i).getOption_id());
                                selectedOptionList.put(jsonValue);
                            }

                            JSONObject jsonOption = new JSONObject();
                            JSONObject jsonObjectSubName = new JSONObject();
                            jsonOption.put("extension_attributes", jsonObjectSubName);
                            jsonObjectSubName.put("custom_options", selectedOptionList);
                            jsonObject.put("product_option", jsonOption);
                        }
                    }
                }

                JSONObject jsonAddToCart = new JSONObject();
                jsonAddToCart.put("cart_item", jsonObject);


                if (checkOutHandler.networkChecker()) {
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")), jsonAddToCart,
                            response -> {
                                pb_CartLoader.setVisibility(View.GONE);
                                CustomerCartListLoader();
                            }, error -> {
                        pb_CartLoader.setVisibility(View.GONE);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject1 = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject1.toString());
                            } else {
                                showToast(getString(R.string.common_error));
                            }
                        } catch (Exception e) {
                            showToast(getString(R.string.common_error));
                        }
                    }) {

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mTokenTag));
                            return params;
                        }
                    };

                    jsonObjectRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "SignUp");
                }

            } catch (Exception e) {
                pb_CartLoader.setVisibility(View.GONE);
            }
        } else {

            if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                toUser();
            } else {
                try {
                    pb_CartLoader.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("quote_id", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                            ConstantFields.mGuestCartIdTag));

                    if (type == 1) {
                        jsonObject.put("qty", 1);
                    } else {
                        jsonObject.put("qty", CartDetail.getQuantity());
                    }

                    jsonObject.put("sku", CartDetail.getSku());

                    JSONObject jsonAddToCart = new JSONObject();
                    jsonAddToCart.put("cart_item", jsonObject);

                    if (checkOutHandler.networkChecker()) {

                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.getGuestCartItems(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")), jsonAddToCart,
                                response -> {
                                    pb_CartLoader.setVisibility(View.GONE);
                                    GuestCartListLoader();
                                }, error -> {
                            pb_CartLoader.setVisibility(View.GONE);
                            try {
                                if (error.networkResponse != null) {
                                    String responseBody = new String(error.networkResponse.data, "utf-8");
                                    JSONObject jsonObject12 = new JSONObject(responseBody);
                                    Log.e("onErrorResponse: ", jsonObject12.toString());
                                } else {
                                    showToast(getString(R.string.common_error));
                                }
                            } catch (Exception e) {
                                showToast(getString(R.string.common_error));
                            }
                        }) {

                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> params = new HashMap<>();
                                params.put("Content-Type", "application/json; charset=utf-8");
                                return params;
                            }
                        };

                        jsonObjectRequest.setShouldCache(false);
                        ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "GuestCartItem");
                    }

                } catch (Exception e) {
                    pb_CartLoader.setVisibility(View.GONE);
                }
            }
        }
    }

    private void removeProduct(final int type, final ModelCartListProduct CartDetail) {
        if (checkOutHandler.networkChecker()) {

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_CartLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.deleteCartItem(CartDetail.getItemId(), ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_CartLoader.setVisibility(View.GONE);
                            if (type == 1) {
                                CustomerCartListLoader();
                            } else {
                                CartDetail.setQuantity(CartDetail.getQuantity() - 1);
                                addProduct(2, CartDetail);
                            }

                        }, error -> {
                    pb_CartLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerCartItemRemove");
            } else {
                if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                    toUser();
                } else {
                    pb_CartLoader.setVisibility(View.VISIBLE);
                    StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.getGuestCartItemDelete(ConstantDataSaver.
                            mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), CartDetail.getItemId(), ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                pb_CartLoader.setVisibility(View.GONE);
                                if (type == 1) {
                                    GuestCartListLoader();
                                } else {
                                    CartDetail.setQuantity(CartDetail.getQuantity() - 1);
                                    addProduct(2, CartDetail);
                                }

                            }, error -> {
                        pb_CartLoader.setVisibility(View.GONE);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);
                                Log.e("onErrorResponse: ", jsonObject.toString());
                            } else {
                                showToast(getString(R.string.common_error));
                            }
                        } catch (Exception e) {
                            showToast(getString(R.string.common_error));
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartItemRemove");
                }
            }
        }
    }

    private void toUser() {
        //Intent toUser = new Intent(activity, ActivityUser.class);
        Intent toUser = new Intent(activity, ActivityLogin.class);
        toUser.putExtra("WhatToLoad", "Login");
        startActivity(toUser);
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        cartHandler();
    }

    private void cartHandler() {
        if (!type.equals("Customer")) {
            if (!ConstantFields.getGuestCartItems(ConstantDataSaver
                    .mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")).equals("Empty")) {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                        ConstantFields.mTokenTag).equals("Empty")) {
                    setCartId();
                }
            }
        }
    }

    private void addToCart(final ArrayList<ModelCartListProduct> cartProductList) {
        if (cartProductList != null) {
            if (cartProductList.size() > 0) {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                    type = "Customer";
                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag).equals("Empty")) {
                        try {
                            pb_CartLoader.setVisibility(View.VISIBLE);
                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("quote_id", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag));
                            jsonObject.put("qty", cartProductList.get(0).getQuantity());
                            jsonObject.put("sku", cartProductList.get(0).getSku());

                            JSONObject jsonAddToCart = new JSONObject();
                            jsonAddToCart.put("cart_item", jsonObject);

                            Log.e("onClick: ", jsonAddToCart.toString());

                            if (checkOutHandler.networkChecker()) {
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")), jsonAddToCart,
                                        response -> {
                                            pb_CartLoader.setVisibility(View.GONE);

                                            if (cartProductList.get(0).getName() != null) {
                                                showToast(getString(R.string.product_detail_add_to_cart_success_1) + " " + cartProductList.get(0).getName()
                                                        + " " + getString(R.string.product_detail_add_to_cart_success_2));
                                            } else {
                                                showToast(getString(R.string.product_detail_add_to_cart_success));
                                            }

                                            removeGuestItem(cartProductList.get(0));
                                            cartProductList.remove(0);
                                            addToCart(cartProductList);
                                        }, error -> {
                                    addToCart(cartProductList);
                                    pb_CartLoader.setVisibility(View.GONE);
                                    try {
                                        if (error.networkResponse != null) {
                                            String responseBody = new String(error.networkResponse.data, "utf-8");
                                            JSONObject jsonObject1 = new JSONObject(responseBody);
                                            Log.e("onErrorResponse: ", jsonObject1.toString());
                                        } else {
                                            showToast(getString(R.string.common_error));
                                        }
                                    } catch (Exception e) {
                                        showToast(getString(R.string.common_error));
                                    }
                                }) {

                                    @Override
                                    public Map<String, String> getHeaders() {
                                        Map<String, String> params = new HashMap<>();
                                        params.put("Content-Type", "application/json; charset=utf-8");
                                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                                ConstantFields.mTokenTag));
                                        return params;
                                    }
                                };

                                jsonObjectRequest.setShouldCache(false);
                                ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "AddToCart");
                            }

                        } catch (Exception e) {
                            pb_CartLoader.setVisibility(View.GONE);
                        }
                    } else {
                        setCartId();
                    }
                } else {
                    rc_CartList.setAdapter(null);
                    CustomerCartListLoader();
                }
            } else {
                rc_CartList.setAdapter(null);
                CustomerCartListLoader();
            }
        }
    }

    private void CreateCartId() {
        if (checkOutHandler.networkChecker()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_CartLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_CartLoader.setVisibility(View.GONE);
                            Log.e("onResponse: ", response + "");
                        /*ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mCustomerCartIdTag, response.replace("\"", ""));
                        addToCart(cartProductList);*/
                            setCartId();
                        }, error -> {
                    pb_CartLoader.setVisibility(View.GONE);
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartIdGuestToCustomer");

            }
        }
    }

    private void setCartId() {
        if (checkOutHandler.networkChecker()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_CartLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_CartLoader.setVisibility(View.GONE);
                            Log.e("onResponse: Set Cart ID", response);
                            ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag, String.valueOf(ConstantDataParser.getCartId(response)));

                        /*ArrayList<ModelCartListProduct> listProducts = ConstantDataParser.getSetCartIDCartDetails(response);

                        if (listProducts != null) {
                            if (listProducts.size() > 0) {
                                addToCart(cartProductList);
                            } else {
                                assignToCustomer();
                            }
                        } else {
                            assignToCustomer();
                        }*/

                            dummyShippingAddress();
                        }, error -> {
                    pb_CartLoader.setVisibility(View.GONE);
                    CreateCartId();
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }

                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartId");
            }
        }
    }

    private void removeGuestItem(final ModelCartListProduct CartDetail) {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.getGuestCartItemDelete(ConstantDataSaver.
                    mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), CartDetail.getItemId(), ConstantFields.currentLanguage().equals("en")),
                    response -> pb_CartLoader.setVisibility(View.GONE), error -> {
                pb_CartLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    } else {
                        showToast(getString(R.string.common_error));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.common_error));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartItemRemove");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        checkOutHandler = (CheckOutHandler) context;
        cartInstantHelper = FragmentCartList.this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void dummyShippingAddress() {
        if (checkOutHandler.networkChecker()) {
            pb_CartLoader.setVisibility(View.VISIBLE);
            try {
                final JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObject.put("address", jsonObjectAddress);
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));

                StringRequest stringRequestBillingAddress = new StringRequest(Request.Method.POST, ConstantFields.mCustomerBillingAddress,
                        response -> {
                            pb_CartLoader.setVisibility(View.GONE);
                            addToCart(cartProductList);
                            Log.e("onResponse yrdy: ", response + "");
                        }, error -> {
                    pb_CartLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequestBillingAddress.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequestBillingAddress, "CustomerBillingAddress");


            } catch (Exception e) {

            }
        }
    }

    private void assignToCustomer() {

        if (checkOutHandler.networkChecker()) {


            pb_CartLoader.setVisibility(View.VISIBLE);
            try {

                final JSONObject mAccountDetail = new JSONObject();
                mAccountDetail.put("customerId", 262);
                mAccountDetail.put("storeId", 2);

                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.getGuestToCustomer(ConstantDataSaver.
                        mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_CartLoader.setVisibility(View.GONE);

                            Log.e("onResponse yrdy: ", response + "");
                        }, error -> {
                    pb_CartLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return mAccountDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartToCustomerCart");

            } catch (Exception e) {

            }
        }
    }

    private void fontSetup() {
        fontSetup(atvContinue, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
