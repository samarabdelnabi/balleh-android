package com.balleh.fragments.checkout;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.instentTransfer.CheckOutHandler;

public class FragmentCheckoutChooser extends BottomSheetDialogFragment {

    private View v_CheckoutChooser;
    private AppCompatTextView atv_AsGuest, atv_AsCustomer, atv_Cancel;
    private CheckOutHandler checkOutHandler;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_CheckoutChooser = inflater.inflate(R.layout.fragment_checkout_chooser, container, false);
        load();
        return v_CheckoutChooser;
    }

    private void load() {
        atv_AsGuest = v_CheckoutChooser.findViewById(R.id.atv_check_out_as_guest_b_sheet);
        atv_AsCustomer = v_CheckoutChooser.findViewById(R.id.atv_check_out_as_customer_b_sheet);
        atv_Cancel = v_CheckoutChooser.findViewById(R.id.atv_cancel_b_sheet);

        atv_AsGuest.setOnClickListener(view -> {
            checkOutHandler.loadAddressList();
            dismiss();
        });

        atv_AsCustomer.setOnClickListener(view -> {
            checkOutHandler.loadLogin();
            dismiss();
        });

        atv_Cancel.setOnClickListener(view -> dismiss());
        fontSetup();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        checkOutHandler = (CheckOutHandler) context;
    }

    private void fontSetup() {
        fontSetup(atv_AsGuest, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_AsCustomer, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_Cancel, Constant.LIGHT, Constant.C_TEXT_VIEW);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
