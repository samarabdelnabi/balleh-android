package com.balleh.fragments.account;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ConstantFields;

public class DialogWebUrlLoader extends DialogFragment {

    private View v_WebContentLoader;
    private String from;
    private ProgressBar pb_WebViewLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            from = bundle.getString("which");
        } else {
            from = ConstantFields.getAboutUsLink(ConstantFields.currentLanguage());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_WebContentLoader = inflater.inflate(R.layout.dialog_web_loader, container, false);
        load();
        return v_WebContentLoader;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void load() {

        WebView wv_Loader;
        ImageButton ib_Close;

        wv_Loader = v_WebContentLoader.findViewById(R.id.wv_web_link);
        ib_Close = v_WebContentLoader.findViewById(R.id.ib_web_content_close);
        pb_WebViewLoader = v_WebContentLoader.findViewById(R.id.pb_web_view_loader);
        wv_Loader.loadUrl(from);
        ib_Close.setOnClickListener(view -> dismiss());

        WebSettings setting = wv_Loader.getSettings();
        setting.setJavaScriptEnabled(true);
        setting.setDomStorageEnabled(true);
        setting.setJavaScriptCanOpenWindowsAutomatically(true);
        setting.setAllowContentAccess(true);

        WebClientClass webViewClient = new WebClientClass();
        wv_Loader.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        wv_Loader.setWebChromeClient(webChromeClient);
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);
    }

    public class WebClientClass extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            pb_WebViewLoader.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            pb_WebViewLoader.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
    }
}
