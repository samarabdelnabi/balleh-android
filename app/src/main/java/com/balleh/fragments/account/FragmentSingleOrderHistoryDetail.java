package com.balleh.fragments.account;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelOrderHistory;
import com.balleh.model.ModelOrderHistoryProductDetail;

import java.util.ArrayList;

public class FragmentSingleOrderHistoryDetail extends Fragment {

    private View v_SingleOrderHistoryDetail;
    private String orderDetail;
    private MenuHandler menuHandler;
    private Activity activity;
    private AppCompatTextView atv_OrderInfoTitle;
    private Button btn_Continue_SOH;

    public FragmentSingleOrderHistoryDetail() {

    }

    public static FragmentSingleOrderHistoryDetail newInstance(String orderDetail) {
        FragmentSingleOrderHistoryDetail fragmentSingleOrderHistoryDetail = new FragmentSingleOrderHistoryDetail();
        Bundle bundle = new Bundle();
        bundle.putString("OrderDetail", orderDetail);
        fragmentSingleOrderHistoryDetail.setArguments(bundle);
        return fragmentSingleOrderHistoryDetail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            orderDetail = getArguments().getString("OrderDetail");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_SingleOrderHistoryDetail = inflater.inflate(R.layout.fragment_single_order_history_detail, container, false);
        load();
        return v_SingleOrderHistoryDetail;
    }

    private void load() {
        btn_Continue_SOH = v_SingleOrderHistoryDetail.findViewById(R.id.btn_continue_soh);
        atv_OrderInfoTitle=v_SingleOrderHistoryDetail.findViewById(R.id.atv_order_info_title);
        RecyclerView rc_SingleOrderInformation = v_SingleOrderHistoryDetail.findViewById(R.id.rc_single_order_history);

        btn_Continue_SOH.setOnClickListener(view -> menuHandler.backBtnPressed());
        rc_SingleOrderInformation.setLayoutManager(new LinearLayoutManager(activity));

        ModelOrderHistory modelOrderHistory = ConstantDataParser.getSingleOrderDetail(orderDetail);
        if (modelOrderHistory != null) {
            rc_SingleOrderInformation.setAdapter(new SingleOrderAdapter(modelOrderHistory));
        }
        fontSetup();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        menuHandler = (MenuHandler) context;
        activity = (Activity) context;
    }

    class SingleOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        ModelOrderHistory modelOrderHistory;

        SingleOrderAdapter(ModelOrderHistory modelOrderHistory) {
            this.modelOrderHistory = modelOrderHistory;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder holder;

            if (viewType == 1) {
                holder = new OrderProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_product_list_soh, parent, false));
            } else {
                holder = new OrderInfo(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_info_soh, parent, false));
            }

            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 1) {
                OrderProductList orderProductList = (OrderProductList) holder;
                String title = "Order #" + modelOrderHistory.getOrderId();
                orderProductList.atv_OrderId.setText(title);
                orderProductList.rc_OrderProductList.setLayoutManager(new LinearLayoutManager(activity));
                orderProductList.rc_OrderProductList.setAdapter(new OrderProductAdapter(modelOrderHistory.getOrderProductDetail()));
                orderProductList.tv_OrderStatus.setText(modelOrderHistory.getOrderStatus());
            } else {
                OrderInfo orderInfo = (OrderInfo) holder;

                if (modelOrderHistory.getOrderSubtotal() != null) {
                    if (modelOrderHistory.getOrderSubtotal().length() > 0) {
                        orderInfo.atv_TotalTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_TotalValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_TotalValue.setText(modelOrderHistory.getOrderSubtotal());
                    } else {
                        orderInfo.atv_TotalTitle.setVisibility(View.GONE);
                        orderInfo.atv_TotalValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_TotalTitle.setVisibility(View.GONE);
                    orderInfo.atv_TotalValue.setVisibility(View.GONE);
                }

                if (modelOrderHistory.getOrderShippingPrice() != null) {
                    if (modelOrderHistory.getOrderShippingPrice().length() > 0) {
                        orderInfo.atv_ShippingAndHandlingTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_ShippingAndHandlingValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_ShippingAndHandlingValue.setText(modelOrderHistory.getOrderShippingPrice());
                    } else {
                        orderInfo.atv_ShippingAndHandlingTitle.setVisibility(View.GONE);
                        orderInfo.atv_ShippingAndHandlingValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_ShippingAndHandlingTitle.setVisibility(View.GONE);
                    orderInfo.atv_ShippingAndHandlingValue.setVisibility(View.GONE);
                }

                if (modelOrderHistory.getOrderAdditionCharges() != null) {
                    if (modelOrderHistory.getOrderAdditionCharges().length() > 0) {
                        orderInfo.atv_PaymentTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_PaymentValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_PaymentValue.setText(modelOrderHistory.getOrderAdditionCharges());
                    } else {
                        orderInfo.atv_PaymentTitle.setVisibility(View.GONE);
                        orderInfo.atv_PaymentValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_PaymentTitle.setVisibility(View.GONE);
                    orderInfo.atv_PaymentValue.setVisibility(View.GONE);
                }

                if (modelOrderHistory.getOrderGrandTotalExclTax() != null) {
                    if (modelOrderHistory.getOrderGrandTotalExclTax().length() > 0) {
                        orderInfo.atv_GrandTotalExclTaxTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_GrandTotalExclTaxValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_GrandTotalExclTaxValue.setText(modelOrderHistory.getOrderGrandTotalExclTax());
                    } else {
                        orderInfo.atv_GrandTotalExclTaxTitle.setVisibility(View.GONE);
                        orderInfo.atv_GrandTotalExclTaxValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_GrandTotalExclTaxTitle.setVisibility(View.GONE);
                    orderInfo.atv_GrandTotalExclTaxValue.setVisibility(View.GONE);
                }

                if (modelOrderHistory.getOrderTaxTotal() != null) {
                    if (modelOrderHistory.getOrderTaxTotal().length() > 0) {
                        orderInfo.atv_TaxTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_TaxValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_TaxValue.setText(modelOrderHistory.getOrderTaxTotal());
                    } else {
                        orderInfo.atv_TaxTitle.setVisibility(View.GONE);
                        orderInfo.atv_TaxValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_TaxTitle.setVisibility(View.GONE);
                    orderInfo.atv_TaxValue.setVisibility(View.GONE);
                }

                if (modelOrderHistory.getOrderTotal() != null) {
                    if (modelOrderHistory.getOrderTotal().length() > 0) {
                        orderInfo.atv_GrandTotalInclTaxTitle.setVisibility(View.VISIBLE);
                        orderInfo.atv_GrandTotalInclTaxValue.setVisibility(View.VISIBLE);
                        orderInfo.atv_GrandTotalInclTaxValue.setText(modelOrderHistory.getOrderTotal());
                    } else {
                        orderInfo.atv_GrandTotalInclTaxTitle.setVisibility(View.GONE);
                        orderInfo.atv_GrandTotalInclTaxValue.setVisibility(View.GONE);
                    }
                } else {
                    orderInfo.atv_GrandTotalInclTaxTitle.setVisibility(View.GONE);
                    orderInfo.atv_GrandTotalInclTaxValue.setVisibility(View.GONE);
                }

                orderInfo.atv_ShippingAddress.setText(modelOrderHistory.getModelAddress().getAddress());
                orderInfo.atv_BillingAddress.setText(modelOrderHistory.getModelAddress().getAddress());
                orderInfo.atv_ShippingMethod.setText(modelOrderHistory.getOrderShippingDescription());
                orderInfo.atv_PaymentMethod.setText(modelOrderHistory.getOrderPaymentMethod());
                String orderDate = getString(R.string.single_order_date) + " " + modelOrderHistory.getOrderDate();
                orderInfo.atv_OrderDate.setText(orderDate);
            }
        }

        @Override
        public int getItemCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 1;
            } else {
                return 2;
            }
        }

        class OrderProductList extends RecyclerView.ViewHolder {

            RecyclerView rc_OrderProductList;
            AppCompatTextView atv_OrderId;
            TextView tv_OrderStatus;

            OrderProductList(View itemView) {
                super(itemView);
                rc_OrderProductList = itemView.findViewById(R.id.rc_order_product_list_soh);
                atv_OrderId = itemView.findViewById(R.id.atv_order_id_soh);
                tv_OrderStatus = itemView.findViewById(R.id.tv_order_status);

                fontSetup(atv_OrderId,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(tv_OrderStatus,Constant.LIGHT,Constant.C_TEXT_VIEW);
            }
        }

        class OrderInfo extends RecyclerView.ViewHolder {

            AppCompatTextView atv_TotalTitle, atv_TotalValue, atv_ShippingAndHandlingTitle, atv_ShippingAndHandlingValue, atv_PaymentTitle,
                    atv_PaymentValue, atv_GrandTotalExclTaxTitle, atv_GrandTotalExclTaxValue, atv_TaxTitle, atv_TaxValue, atv_GrandTotalInclTaxTitle,
                    atv_GrandTotalInclTaxValue, atv_ShippingAddress, atv_BillingAddress, atv_ShippingMethod, atv_PaymentMethod, atv_OrderDate;

            OrderInfo(View itemView) {
                super(itemView);
                atv_TotalTitle = itemView.findViewById(R.id.atv_order_total_title_soh);
                atv_TotalValue = itemView.findViewById(R.id.atv_order_total_value_soh);
                atv_ShippingAndHandlingTitle = itemView.findViewById(R.id.atv_order_shipping_and_handling_title_soh);
                atv_ShippingAndHandlingValue = itemView.findViewById(R.id.atv_order_shipping_and_handling_value_soh);
                atv_PaymentTitle = itemView.findViewById(R.id.atv_product_payment_charge_title_soh);
                atv_PaymentValue = itemView.findViewById(R.id.atv_product_payment_charge_value_soh);
                atv_GrandTotalExclTaxTitle = itemView.findViewById(R.id.atv_grand_total_excl_tax_title_soh);
                atv_GrandTotalExclTaxValue = itemView.findViewById(R.id.atv_grand_total_excl_tax_value_soh);
                atv_TaxTitle = itemView.findViewById(R.id.atv_tax_title_soh);
                atv_TaxValue = itemView.findViewById(R.id.atv_tax_value_soh);
                atv_GrandTotalInclTaxTitle = itemView.findViewById(R.id.atv_grand_total_incl_tax_title_soh);
                atv_GrandTotalInclTaxValue = itemView.findViewById(R.id.atv_grand_total_incl_tax_value_soh);
                atv_ShippingAddress = itemView.findViewById(R.id.atv_shipping_address_soh);
                atv_BillingAddress = itemView.findViewById(R.id.atv_payment_address_soh);
                atv_ShippingMethod = itemView.findViewById(R.id.atv_shipping_method_soh);
                atv_PaymentMethod = itemView.findViewById(R.id.atv_payment_method_soh);
                atv_OrderDate = itemView.findViewById(R.id.atv_order_date_soh);

                fontSetup(atv_TotalTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_TotalValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ShippingAndHandlingTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ShippingAndHandlingValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_PaymentTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_PaymentValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_GrandTotalExclTaxTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_GrandTotalExclTaxValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_TaxTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_TaxValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_GrandTotalInclTaxTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_GrandTotalInclTaxValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ShippingAddress,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_BillingAddress,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ShippingMethod,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_PaymentMethod,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_OrderDate,Constant.LIGHT,Constant.C_TEXT_VIEW);

            }
        }
    }

    class OrderProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        ArrayList<ModelOrderHistoryProductDetail> modelOrderHistoryProductDetails;

        OrderProductAdapter(ArrayList<ModelOrderHistoryProductDetail> modelOrderHistoryProductDetails) {
            this.modelOrderHistoryProductDetails = modelOrderHistoryProductDetails;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new OrderProduct(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_product_soh, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            OrderProduct orderProduct = (OrderProduct) holder;
            orderProduct.atv_ProductTitle.setText(modelOrderHistoryProductDetails.get(position).getName());

            String sku = "SKU :" + modelOrderHistoryProductDetails.get(position).getSku();
            orderProduct.atv_ProductSku.setText(sku);

            String productPrice = "SAR " + modelOrderHistoryProductDetails.get(position).getProductPrice();
            String productTotalPrice = "Total : SAR " + modelOrderHistoryProductDetails.get(position).getTotalPrice();
            orderProduct.atv_ProductPrice.setText(productPrice);
            orderProduct.atv_ProductTotalPrice.setText(productTotalPrice);

            if (modelOrderHistoryProductDetails.get(position).getOrderQuantity() != null) {
                if (modelOrderHistoryProductDetails.get(position).getOrderQuantity().length() > 0) {
                    orderProduct.atv_ProductOrderQty.setVisibility(View.VISIBLE);
                    String productOrderQty = "Ordered   : " + modelOrderHistoryProductDetails.get(position).getOrderQuantity();
                    orderProduct.atv_ProductOrderQty.setText(productOrderQty);
                } else {
                    orderProduct.atv_ProductOrderQty.setVisibility(View.GONE);
                }
            } else {
                orderProduct.atv_ProductOrderQty.setVisibility(View.GONE);
            }


            if (modelOrderHistoryProductDetails.get(position).getCancelQuantity() != null) {
                if (modelOrderHistoryProductDetails.get(position).getCancelQuantity().length() > 0) {
                    orderProduct.atv_ProductCancelQty.setVisibility(View.VISIBLE);
                    String productCancelQty = "Canceled  : " + modelOrderHistoryProductDetails.get(position).getCancelQuantity();
                    orderProduct.atv_ProductCancelQty.setText(productCancelQty);
                } else {
                    orderProduct.atv_ProductCancelQty.setVisibility(View.GONE);
                }
            } else {
                orderProduct.atv_ProductCancelQty.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return modelOrderHistoryProductDetails.size();
        }

        class OrderProduct extends RecyclerView.ViewHolder {

            AppCompatTextView atv_ProductTitle, atv_ProductSku, atv_ProductPrice, atv_ProductTotalPrice, atv_ProductOrderQty, atv_ProductCancelQty;
            ImageView iv_ProductImage;

            OrderProduct(View itemView) {
                super(itemView);
                atv_ProductTitle = itemView.findViewById(R.id.atv_product_title_order_soh);
                atv_ProductSku = itemView.findViewById(R.id.atv_product_sku_order_soh);
                atv_ProductPrice = itemView.findViewById(R.id.atv_product_price_order_soh);
                atv_ProductTotalPrice = itemView.findViewById(R.id.atv_product_total_price_order_soh);
                atv_ProductOrderQty = itemView.findViewById(R.id.atv_product_order_qty_soh);
                atv_ProductCancelQty = itemView.findViewById(R.id.atv_product_cancel_qty_soh);
                iv_ProductImage = itemView.findViewById(R.id.iv_product_image_order_soh);

                fontSetup(atv_ProductTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductSku,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductPrice,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductTotalPrice,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductOrderQty,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductCancelQty,Constant.LIGHT,Constant.C_TEXT_VIEW);

            }
        }
    }


    private void fontSetup() {
        fontSetup(atv_OrderInfoTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_Continue_SOH, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
