package com.balleh.fragments.account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.additional.instentTransfer.ReloadHelper;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelAddress;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentAddressBook extends Fragment implements ReloadHelper {

    private View v_AddressBookHolder;
    private Activity activity;
    private RecyclerView rc_AddressListContainer;
    private MenuHandler menuHandler;
    private Button btn_NewAddress;
    private ProgressBar pb_AddressBookLoader;
    private AppCompatTextView atv_Title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_AddressBookHolder = inflater.inflate(R.layout.fragment_address_book, container, false);
        load();
        return v_AddressBookHolder;
    }

    private void load() {
        rc_AddressListContainer = v_AddressBookHolder.findViewById(R.id.rc_address_list_ab);
        btn_NewAddress = v_AddressBookHolder.findViewById(R.id.btn_new_address_ab);
        pb_AddressBookLoader = v_AddressBookHolder.findViewById(R.id.pb_address_book_loader);
        atv_Title = v_AddressBookHolder.findViewById(R.id.atv_address_book_title);

        rc_AddressListContainer.setLayoutManager(new LinearLayoutManager(activity));

        loadAddressList();
        fontSetup();
    }

    private void loadAddressList() {
        if (menuHandler.networkError()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_AddressBookLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_AddressBookLoader.setVisibility(View.GONE);
                            Log.e("onResponse: ", response + "");
                            ArrayList<ModelAddress> addressList = ConstantDataParser.getAddressList(response);
                            if (addressList != null) {
                                if (addressList.size() > 0) {
                                    rc_AddressListContainer.setAdapter(new AddressBookAdapter(addressList, response));
                                } else {
                                    rc_AddressListContainer.setAdapter(new AddressBookAdapter(null, response));
                                }
                            } else {
                                rc_AddressListContainer.setAdapter(new AddressBookAdapter(null, response));
                            }

                            btn_NewAddress.setOnClickListener(view -> menuHandler.loadAddress(response, 0, "New"));

                        }, error -> {
                    pb_AddressBookLoader.setVisibility(View.GONE);
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    @Override
    public void reloadDetails() {
        rc_AddressListContainer.setAdapter(null);
        loadAddressList();
    }

    class AddressBookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelAddress> addressList;
        private ArrayList<ModelAddress> tempAddressList = new ArrayList<>();
        private String accountDetail;

        AddressBookAdapter(ArrayList<ModelAddress> addressList, String accountDetail) {
            this.addressList = addressList;
            this.accountDetail = accountDetail;
            if (addressList != null)
                if (addressList.size() > 0)
                    for (int i = 0; i < addressList.size(); i++) {
                        if (!addressList.get(i).isBillingAddress() && !addressList.get(i).isShippingAddress()) {
                            tempAddressList.add(addressList.get(i));
                        }
                    }
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            if (viewType == 1) {
                viewHolder = new DefaultAddress(LayoutInflater.from(activity).inflate(R.layout.rc_row_default_ab, parent, false));
            } else if (viewType == 2) {
                viewHolder = new AdditionalAddress(LayoutInflater.from(activity).inflate(R.layout.rc_row_additional_ab, parent, false));
            } else {
                viewHolder = new EmptyAddressList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 1) {
                DefaultAddress defaultAddress = (DefaultAddress) holder;

                if (addressList != null) {
                    if (addressList.size() > 0) {
                        for (int i = 0; i < addressList.size(); i++) {

                            if (addressList.get(i).isShippingAddress()) {
                                defaultAddress.atv_ShippingAddressValue.setText(addressList.get(i).getAddress());
                                final int finalI = i;
                                defaultAddress.ib_ShippingEdit.setOnClickListener(view -> {
                                    if (addressList.get(finalI).isBillingAddress()) {
                                        menuHandler.loadAddress(accountDetail, addressList.get(finalI).getAddressId(), "Both");
                                    } else {
                                        menuHandler.loadAddress(accountDetail, addressList.get(finalI).getAddressId(), "Shipping");
                                    }
                                });
                            }
                            if (addressList.get(i).isBillingAddress()) {
                                defaultAddress.atv_BillingAddressValue.setText(addressList.get(i).getAddress());
                                final int finalI = i;
                                defaultAddress.ib_BillingEdit.setOnClickListener(view -> {
                                    if (addressList.get(finalI).isShippingAddress()) {
                                        menuHandler.loadAddress(accountDetail, addressList.get(finalI).getAddressId(), "Both");
                                    } else {
                                        menuHandler.loadAddress(accountDetail, addressList.get(finalI).getAddressId(), "Billing");
                                    }
                                });
                            }
                        }
                    } else {
                        defaultAddress.atv_ShippingAddressValue.setText(getString(R.string.address_add_shipping_address));
                        defaultAddress.atv_BillingAddressValue.setText(getString(R.string.address_add_billing_address));

                        defaultAddress.ib_ShippingEdit.setOnClickListener(view -> menuHandler.loadAddress(null, 0, "NewShipping"));

                        defaultAddress.ib_BillingEdit.setOnClickListener(view -> menuHandler.loadAddress(null, 0, "NewBilling"));
                    }
                } else {
                    defaultAddress.atv_ShippingAddressValue.setText(getString(R.string.address_add_shipping_address));
                    defaultAddress.atv_BillingAddressValue.setText(getString(R.string.address_add_billing_address));
                    defaultAddress.ib_ShippingEdit.setOnClickListener(view -> menuHandler.loadAddress(null, 0, "NewShipping"));

                    defaultAddress.ib_BillingEdit.setOnClickListener(view -> menuHandler.loadAddress(null, 0, "NewBilling"));
                }
            } else if (holder.getItemViewType() == 2) {
                AdditionalAddress additionalAddress = (AdditionalAddress) holder;
                additionalAddress.rc_AdditionalAddressList.setLayoutManager(new LinearLayoutManager(activity));
                additionalAddress.rc_AdditionalAddressList.setAdapter(new AddressListAdapter(tempAddressList, accountDetail));
            } else {
                EmptyAddressList emptyCartProductList = (EmptyAddressList) holder;
                emptyCartProductList.atv_EmptyMessage.setText(R.string.address_address_empty_hint);
            }
        }

        @Override
        public int getItemCount() {
            if (addressList != null) {
                if (addressList.size() > 0) {
                    return 2;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (addressList != null) {
                if (addressList.size() > 0) {
                    if (position == 0) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 3;
                }
            } else {
                return 3;
            }
        }

        class DefaultAddress extends RecyclerView.ViewHolder {
            AppCompatTextView atv_ShippingAddressValue, atv_BillingAddressValue;
            ImageButton ib_ShippingEdit, ib_BillingEdit;

            DefaultAddress(View itemView) {
                super(itemView);
                atv_ShippingAddressValue = itemView.findViewById(R.id.atv_order_history_single_shipping_address_value);
                atv_BillingAddressValue = itemView.findViewById(R.id.atv_order_history_single_payment_address_value);

                ib_ShippingEdit = itemView.findViewById(R.id.ib_shipping_address_edit);
                ib_BillingEdit = itemView.findViewById(R.id.ib_billing_address_edit);

                fontSetup(atv_BillingAddressValue, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ShippingAddressValue, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

        class AdditionalAddress extends RecyclerView.ViewHolder {
            RecyclerView rc_AdditionalAddressList;

            AdditionalAddress(View itemView) {
                super(itemView);
                rc_AdditionalAddressList = itemView.findViewById(R.id.rc_additional_address_list_ab);
            }
        }

        private class EmptyAddressList extends RecyclerView.ViewHolder {
            AppCompatTextView atv_EmptyMessage;

            EmptyAddressList(View view) {
                super(view);
                atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
                fontSetup(atv_EmptyMessage, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }

    }

    class AddressListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelAddress> addressList;
        private String accountDetail;

        AddressListAdapter(ArrayList<ModelAddress> addressList, String accountDetail) {
            this.addressList = addressList;
            this.accountDetail = accountDetail;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;

            if (viewType == 1) {
                viewHolder = new AddressViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_address, parent, false));
            } else {
                viewHolder = new EmptyProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

            if (holder.getItemViewType() == 1) {
                AddressViewHolder addressViewHolder = (AddressViewHolder) holder;


                addressViewHolder.atv_FullAddress.setText(addressList.get(position).getAddress());

                /*if (addressList.get(position).getFirstName() != null && addressList.get(position).getLastName() != null) {
                    String name = addressList.get(position).getFirstName() + " " + addressList.get(position).getLastName();
                    addressViewHolder.atv_Name.setText(name);
                } else if (addressList.get(position).getFirstName() != null && addressList.get(position).getLastName() == null) {
                    addressViewHolder.atv_Name.setText(addressList.get(position).getFirstName());
                } else if (addressList.get(position).getFirstName() == null && addressList.get(position).getLastName() != null) {
                    addressViewHolder.atv_Name.setText(addressList.get(position).getLastName());
                }

                if (addressList.get(position).getStreet() != null) {
                    addressViewHolder.atv_StreetAddress.setText(addressList.get(position).getStreet());
                }

                if (addressList.get(position).getCountryId() != null) {
                    addressViewHolder.atv_Country.setText(addressList.get(position).getCountryId());
                }

                if (addressList.get(position).getCity() != null && addressList.get(position).getPost_code() != null) {
                    String cityAndZip = addressList.get(position).getCity() + ", " + addressList.get(position).getPost_code();
                    addressViewHolder.atv_CityAndZipCode.setText(cityAndZip);
                } else if (addressList.get(position).getCity() != null && addressList.get(position).getPost_code() == null) {
                    addressViewHolder.atv_CityAndZipCode.setText(addressList.get(position).getCity());
                } else if (addressList.get(position).getCity() == null && addressList.get(position).getPost_code() != null) {
                    addressViewHolder.atv_CityAndZipCode.setText(addressList.get(position).getPost_code());
                }

                if (addressList.get(position).getTelephone() != null) {
                    addressViewHolder.atv_MobileNumber.setText(addressList.get(position).getTelephone());
                }*/

                addressViewHolder.btn_Edit.setOnClickListener(view -> menuHandler.loadAddress(accountDetail, addressList.get(position).getAddressId(), "edit"));

                addressViewHolder.btn_Delete.setOnClickListener(view -> showDeleteAlert(addressList.get(position).getAddressId(), ConstantDataParser.getAccountDetail(accountDetail)));

            } else {
                EmptyProductList empty_view = (EmptyProductList) holder;
                empty_view.atv_EmptyMessage.setText(activity.getString(R.string.address_book_empty_text));
            }
        }

        @Override
        public int getItemCount() {
            if (addressList != null) {
                if (addressList.size() > 0) {
                    return addressList.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (addressList != null) {
                if (addressList.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        class AddressViewHolder extends RecyclerView.ViewHolder {

            AppCompatTextView atv_Name, atv_StreetAddress, atv_CityAndZipCode, atv_Country,
                    atv_MobileNumber, atv_FullAddress;
            Button btn_Edit, btn_Delete;

            AddressViewHolder(View view) {
                super(view);
                atv_Name = view.findViewById(R.id.atv_name_ap);
                atv_StreetAddress = view.findViewById(R.id.atv_address_ab);
                atv_CityAndZipCode = view.findViewById(R.id.atv_city_and_zip_code_ab);
                atv_Country = view.findViewById(R.id.atv_country_ab);
                atv_MobileNumber = view.findViewById(R.id.atv_telephone_ab);
                atv_FullAddress = view.findViewById(R.id.atv_address_full_ap);

                btn_Edit = view.findViewById(R.id.btn_edit_ap);
                btn_Delete = view.findViewById(R.id.btn_delete_ap);

                fontSetup(atv_Name, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_StreetAddress, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_CityAndZipCode, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_Country, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_MobileNumber, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_FullAddress, Constant.LIGHT, Constant.C_TEXT_VIEW);

                fontSetup(btn_Edit, Constant.LIGHT, Constant.C_BUTTON);
                fontSetup(btn_Delete, Constant.LIGHT, Constant.C_BUTTON);
            }
        }

        private class EmptyProductList extends RecyclerView.ViewHolder {
            AppCompatTextView atv_EmptyMessage;

            EmptyProductList(View view) {
                super(view);
                atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
                fontSetup(atv_EmptyMessage, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
    }

    private void showDeleteAlert(final int addressId, final ModelAccountDetail accountDetail) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage(getString(R.string.action_delete_address_title));
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton(getString(R.string.action_delete_yes), (arg0, arg1) -> deleteAddress(addressId, accountDetail));

        alertDialogBuilder.setNegativeButton(getString(R.string.action_delete_no), (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void deleteAddress(int addressId, ModelAccountDetail accountDetail) {
        try {
            JSONArray jsonArray = new JSONArray();
            if (accountDetail != null) {
                for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                    if (addressId != accountDetail.getAddressList().get(i).getAddressId()) {
                        JSONArray jsonStreetArrayDefault = new JSONArray();
                        jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                        JSONObject jsonAddress = new JSONObject();
                        jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                        jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                        jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                        jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                        jsonAddress.put("street", jsonStreetArrayDefault);
                        jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                        jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                        jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                        jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                        jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());
                        if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                            jsonAddress.put("default_billing", true);
                        } else {
                            jsonAddress.put("default_billing", false);
                        }
                        if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                            jsonAddress.put("default_shipping", true);
                        } else {
                            jsonAddress.put("default_shipping", false);
                        }
                        jsonArray.put(jsonAddress);
                    }
                }

                JSONObject jsonObjectCustomer = new JSONObject();
                jsonObjectCustomer.put("firstname", accountDetail.getFirstName());
                jsonObjectCustomer.put("lastname", accountDetail.getLastName());
                jsonObjectCustomer.put("email", accountDetail.getEmailId());
                jsonObjectCustomer.put("websiteId", 0);
                jsonObjectCustomer.put("addresses", jsonArray);

                JSONObject jsonFinalObject = new JSONObject();
                jsonFinalObject.put("customer", jsonObjectCustomer);

                new AsyncTaskForPost().execute(jsonFinalObject.toString());

                Log.e("onClick: ", jsonFinalObject.toString());
            } else {
                showToast(getString(R.string.common_error));
            }
        } catch (Exception e) {
            Log.e("onClick: ", e.toString());
            showToast(getString(R.string.common_error));
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskForPost extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb_AddressBookLoader.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")));
                Log.d("POST url", url.toString());
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("PUT");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");
                mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver.
                        mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag));

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(params[0]);
                mBufferedWriter.close();
                mOutputStream.close();

                String mResponse;
                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                } else {
                    mResponse = "Error";
                }
                Log.e("doInBackground: ", mResponse);
                return mResponse;
            } catch (Exception e) {
                Log.e("doInBackground: ", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            pb_AddressBookLoader.setVisibility(View.GONE);
            reloadDetails();
        }

        @Nullable
        String inputStreamToStringConversion(BufferedReader bufferedReader) {
            try {
                String line;
                StringBuilder result = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            } catch (Exception e) {
                return null;
            }
        }
    }

    private void fontSetup() {
        fontSetup(atv_Title, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_NewAddress, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
