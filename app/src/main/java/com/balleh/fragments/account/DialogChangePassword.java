package com.balleh.fragments.account;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DialogChangePassword extends DialogFragment {

    private View v_ChangePassword;
    private Activity activity;
    private ProgressBar pb_ChangePassword;
    private MenuHandler menuHandler;
    private EditText et_CurrentPassword, et_NewPassword, et_ConfirmNewPassword;
    private AppCompatTextView atv_CurrentPasswordError, atv_NewPasswordError, atv_ConfirmNewPasswordError,
            atv_Title, atv_CurrentPwdTitle, atv_NewPwdTitle, atv_ConformPwdTitle;
    private Button btn_Back, btn_Continue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ChangePassword = inflater.inflate(R.layout.dialog_change_password, container, false);
        load();
        return v_ChangePassword;
    }

    private void load() {

        btn_Back = v_ChangePassword.findViewById(R.id.btn_back_cp);
        btn_Continue = v_ChangePassword.findViewById(R.id.btn_continue_cp);

        et_CurrentPassword = v_ChangePassword.findViewById(R.id.et_current_cp);
        et_NewPassword = v_ChangePassword.findViewById(R.id.et_new_cp);
        et_ConfirmNewPassword = v_ChangePassword.findViewById(R.id.et_confirm_cp);

        atv_CurrentPasswordError = v_ChangePassword.findViewById(R.id.atv_current_error_cp);
        atv_NewPasswordError = v_ChangePassword.findViewById(R.id.atv_new_error_cp);
        atv_ConfirmNewPasswordError = v_ChangePassword.findViewById(R.id.atv_confirm_error_cp);

        atv_Title = v_ChangePassword.findViewById(R.id.atv_change_pwd_title);
        atv_CurrentPwdTitle = v_ChangePassword.findViewById(R.id.atv_current_pwd_cp);
        atv_NewPwdTitle = v_ChangePassword.findViewById(R.id.atv_new_pwd_cp);
        atv_ConformPwdTitle = v_ChangePassword.findViewById(R.id.atv_confirm_new_pwd_cp);

        pb_ChangePassword = v_ChangePassword.findViewById(R.id.pb_cp);

        btn_Back.setOnClickListener(view -> dismiss());

        btn_Continue.setOnClickListener(view -> {
            if (!et_CurrentPassword.getText().toString().equals("")
                    && (et_CurrentPassword.getText().toString().length() >= 8)

                    && !et_NewPassword.getText().toString().equals("")
                    && (et_NewPassword.getText().toString().length() >= 8)

                    && !et_ConfirmNewPassword.getText().toString().equals("")
                    && (et_ConfirmNewPassword.getText().toString().length() >= 8)

                    && et_NewPassword.getText().toString().equals(et_ConfirmNewPassword.getText().toString())) {

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("currentPassword", et_CurrentPassword.getText().toString());
                    jsonObject.put("newPassword", et_NewPassword.getText().toString());

                    ChangePassword(jsonObject);

                } catch (Exception e) {
                    ConstantFields.CustomToast(getString(R.string.common_error), getActivity());
                }
            }

            if (et_CurrentPassword.getText().toString().equals("")) {
                atv_CurrentPasswordError.setText(getString(R.string.change_password_current_error));
                atv_CurrentPasswordError.setVisibility(View.VISIBLE);
            } else {
                atv_CurrentPasswordError.setVisibility(View.GONE);
                if ((et_CurrentPassword.getText().toString().length() >= 8)) {
                    atv_CurrentPasswordError.setVisibility(View.GONE);
                } else {
                    atv_CurrentPasswordError.setText(getString(R.string.change_password_length_error));
                    atv_CurrentPasswordError.setVisibility(View.VISIBLE);
                }
            }

            if (et_NewPassword.getText().toString().equals("")) {
                atv_NewPasswordError.setText(getString(R.string.change_password_new_error));
                atv_NewPasswordError.setVisibility(View.VISIBLE);
            } else {
                atv_CurrentPasswordError.setVisibility(View.GONE);
                if ((et_NewPassword.getText().toString().length() >= 8)) {
                    atv_NewPasswordError.setVisibility(View.GONE);
                } else {
                    atv_NewPasswordError.setText(getString(R.string.change_password_length_error));
                    atv_NewPasswordError.setVisibility(View.VISIBLE);
                }
            }

            if (et_ConfirmNewPassword.getText().toString().equals("")) {
                atv_ConfirmNewPasswordError.setText(getString(R.string.change_password_confirm_error_1));
                atv_ConfirmNewPasswordError.setVisibility(View.VISIBLE);
            } else {
                atv_ConfirmNewPasswordError.setVisibility(View.GONE);
                if ((et_ConfirmNewPassword.getText().toString().length() >= 8)) {
                    if (et_NewPassword.getText().toString().equals(et_ConfirmNewPassword.getText().toString())) {
                        atv_ConfirmNewPasswordError.setVisibility(View.GONE);
                    } else {
                        atv_ConfirmNewPasswordError.setText(getString(R.string.change_password_confirm_error_2));
                        atv_ConfirmNewPasswordError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_ConfirmNewPasswordError.setText(getString(R.string.change_password_length_error));
                    atv_ConfirmNewPasswordError.setVisibility(View.VISIBLE);
                }
            }

        });
        fontSetup();
    }

    private void ChangePassword(final JSONObject jsonObject) {
        try {
            if (menuHandler.networkError()) {
                pb_ChangePassword.setVisibility(View.VISIBLE);
                Log.e("ChangePassword: ", jsonObject.toString());
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.mChangePassword,
                        response -> {
                            Log.e("onResponse: ", response);
                            pb_ChangePassword.setVisibility(View.GONE);
                            dismiss();
                        }, error -> {
                    pb_ChangePassword.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            showToast(jsonObject1.getString("message"));
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        Log.e("onErrorResponse: ", e.toString());
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                }) {
                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ChangePassword");
            }
        } catch (Exception e) {
            pb_ChangePassword.setVisibility(View.GONE);

        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    private void fontSetup() {
        fontSetup(et_CurrentPassword, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_NewPassword, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(et_ConfirmNewPassword, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(atv_CurrentPasswordError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_NewPasswordError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ConfirmNewPasswordError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_Title, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_CurrentPwdTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_NewPwdTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ConformPwdTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_Back, Constant.LIGHT, Constant.C_BUTTON);
        fontSetup(btn_Continue, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
