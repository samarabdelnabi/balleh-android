package com.balleh.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.balleh.R;
import com.balleh.activities.ActivitySplashScreen;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantFields;

public class DialogNetworkError extends DialogFragment {

    private View v_NetworkError;
    private Activity activity;
    private TextView tv_NoConnection;
    private Button btn_Retry;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_NetworkError = inflater.inflate(R.layout.dialog_network_error, container, false);
        load();
        return v_NetworkError;
    }

    private void load() {
        btn_Retry = v_NetworkError.findViewById(R.id.btn_retry);
        tv_NoConnection = v_NetworkError.findViewById(R.id.tv_no_connection);
        btn_Retry.setOnClickListener(v -> {
            if (!ConstantFields.isNetworkAvailable()) {
                ConstantFields.CustomToast(getString(R.string._retry), activity);
            } else {
                Intent intent = new Intent(activity, ActivitySplashScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        fontSetup();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
    }

    private void fontSetup() {
        fontSetup(tv_NoConnection, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(btn_Retry, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
