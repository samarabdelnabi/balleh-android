package com.balleh.fragments.user;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.User;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sasikumar on 3/24/2018.
 * User Registration
 */

public class FragmentUserRegistration extends Fragment {

    private View v_RegistrationContainer;
    private User mUserRegistrationInstantTransfer;
    private AppCompatTextView atv_RegDOB;
    private AppCompatTextView atv_RegFirstNameError;
    private AppCompatTextView atv_RegLastNameError;
    private AppCompatTextView atv_RegMobileNumberError;
    private AppCompatTextView atv_RegEmailError1;
    private AppCompatTextView atv_RegEmailError2;
    private AppCompatTextView atv_PasswordError1;
    private AppCompatTextView atv_PasswordError2;
    private AppCompatTextView atv_ConfirmPasswordError1;
    private AppCompatTextView atv_ConfirmPasswordError2;
    private EditText et_FirstName, et_LastName, et_MobileNumber, et_EmailId, et_Password, et_ConfirmPassword;
    private String mGender = null, mDOBDate = null;
    private DatePickerDialog datePickerDialog;
    private Activity activity;
    private Button btn_CreateAnAccount;

    /*Social Login*/
    /*Twitter*/
    private TwitterLoginButton mTwitterLoginButton;
    /*Facebook*/
    CallbackManager callbackManager;
    LoginButton loginButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(activity);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(activity);
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_RegistrationContainer = inflater.inflate(R.layout.fragment_user_registration, container, false);
        load();
        return v_RegistrationContainer;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logout();
    }

    private void logout() {
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
    }

    private void load() {

        mTwitterLoginButton = v_RegistrationContainer.findViewById(R.id.btn_twitter_social_login_dummy);
        btn_CreateAnAccount = v_RegistrationContainer.findViewById(R.id.btn_sign_up_submit);
        AppCompatTextView atv_BackToLogin = v_RegistrationContainer.findViewById(R.id.atv_reg_to_login);
        Spinner s_GenderList = v_RegistrationContainer.findViewById(R.id.s_gender_list);

        mTwitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e("success: ", result.data.getUserId() + "");



            }

            @Override
            public void failure(TwitterException exception) {
                Log.e("failure: ", exception.getMessage() + "");
            }
        });

        et_FirstName = v_RegistrationContainer.findViewById(R.id.et_reg_first_name);
        et_LastName = v_RegistrationContainer.findViewById(R.id.et_reg_last_name);
        et_MobileNumber = v_RegistrationContainer.findViewById(R.id.et_reg_mobile_no);
        et_EmailId = v_RegistrationContainer.findViewById(R.id.et_reg_email);
        et_Password = v_RegistrationContainer.findViewById(R.id.et_reg_password);
        et_ConfirmPassword = v_RegistrationContainer.findViewById(R.id.et_reg_confirm_password);

        atv_RegFirstNameError = v_RegistrationContainer.findViewById(R.id.atv_reg_error_first_name);
        atv_RegLastNameError = v_RegistrationContainer.findViewById(R.id.tv_reg_error_last_name);
        atv_RegDOB = v_RegistrationContainer.findViewById(R.id.atv_reg_dob);
        atv_RegEmailError1 = v_RegistrationContainer.findViewById(R.id.atv_reg_email_error_1);
        atv_RegEmailError2 = v_RegistrationContainer.findViewById(R.id.atv_reg_email_error_2);
        atv_PasswordError1 = v_RegistrationContainer.findViewById(R.id.atv_reg_password_error_1);
        atv_PasswordError2 = v_RegistrationContainer.findViewById(R.id.atv_reg_password_error_2);
        atv_ConfirmPasswordError1 = v_RegistrationContainer.findViewById(R.id.tv_reg_confirm_password_error1);
        atv_ConfirmPasswordError2 = v_RegistrationContainer.findViewById(R.id.tv_reg_confirm_password_error2);
        atv_RegMobileNumberError = v_RegistrationContainer.findViewById(R.id.atv_reg_error_mobile);

        int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(activity, (datePicker, Year, Month, Day) -> {
            mDOBDate = Day + "-" + (Month + 1) + "-" + Year;
            atv_RegDOB.setText(mDOBDate);
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.user_reg_gender_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_GenderList.setAdapter(adapter);

        s_GenderList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString() != null) {
                    if (adapterView.getSelectedItem().toString().length() > 0) {
                        mGender = adapterView.getSelectedItem().toString().trim();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        atv_RegDOB.setOnClickListener(view -> datePickerDialog.show());

        btn_CreateAnAccount.setOnClickListener(view -> {


            if (!et_FirstName.getText().toString().equals("")
                    && (et_FirstName.getText().toString().length() > 3)

                    && !et_LastName.getText().toString().equals("")
                    && (et_LastName.getText().toString().length() > 3)

                    && !et_EmailId.getText().toString().equals("")
                    && ConstantFields.isEmailValidator(et_EmailId.getText().toString())

                    && !et_MobileNumber.getText().toString().equals("")

                    && !et_Password.getText().toString().equals("")
                    && !et_ConfirmPassword.getText().toString().equals("")

                    && (et_Password.getText().toString().length() >= 8)
                    && (et_ConfirmPassword.getText().toString().length() >= 8)

                    && ConstantFields.isValidPassword(et_Password.getText().toString())

                    && et_Password.getText().toString().equals(et_ConfirmPassword.getText().toString())) {

                try {
                    JSONObject jsonObjectCustom = new JSONObject();
                    jsonObjectCustom.put("attribute_code", "reg_mobile_no");
                    jsonObjectCustom.put("value", et_MobileNumber.getEditableText().toString());

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObjectCustom);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("firstname", et_FirstName.getEditableText().toString());
                    jsonObject.put("lastname", et_LastName.getEditableText().toString());
                    jsonObject.put("email", et_EmailId.getEditableText().toString());
                    jsonObject.put("custom_attributes", jsonArray);
                    if (mGender != null) {
                        if (mGender.length() > 0) {
                            switch (mGender) {
                                case "Male":
                                    jsonObject.put("gender", "1");
                                    break;
                                case "Female":
                                    jsonObject.put("gender", "2");
                                    break;
                                case "Not Specified":
                                    jsonObject.put("gender", "3");
                                    break;
                            }
                        }
                    }

                    if (atv_RegDOB.getText().toString().length() > 0) {
                        jsonObject.put("dob", atv_RegDOB.getText().toString());
                    }

                    JSONObject mSignUpDetail = new JSONObject();
                    mSignUpDetail.put("customer", jsonObject);
                    mSignUpDetail.put("password", et_Password.getEditableText().toString());

                    JSONObject jsonObjectSignIn = new JSONObject();
                    jsonObjectSignIn.put("username", et_EmailId.getEditableText().toString());
                    jsonObjectSignIn.put("password", et_Password.getEditableText().toString());

                    //mUserRegistrationInstantTransfer.SignUp(mSignUpDetail, jsonObjectSignIn);
                } catch (Exception e) {
                    ConstantFields.CustomToast(getString(R.string.common_error), getActivity());
                }
            }

            if (et_FirstName.getText().toString().equals("")) {
                atv_RegFirstNameError.setVisibility(View.VISIBLE);
            } else {
                atv_RegFirstNameError.setVisibility(View.GONE);
            }
            if (et_LastName.getText().toString().equals("")) {
                atv_RegLastNameError.setVisibility(View.VISIBLE);
            } else {
                atv_RegLastNameError.setVisibility(View.GONE);
            }
            if (et_MobileNumber.getText().toString().equals("")) {
                atv_RegMobileNumberError.setVisibility(View.VISIBLE);
            } else {
                if(et_MobileNumber.getText().toString().length()==10){
                    atv_RegMobileNumberError.setVisibility(View.GONE);
                }else {
                    atv_RegMobileNumberError.setVisibility(View.VISIBLE);
                }
            }
            if (et_EmailId.getText().toString().equals("")) {
                atv_RegEmailError1.setVisibility(View.VISIBLE);
            } else {
                atv_RegEmailError1.setVisibility(View.GONE);
                if (!ConstantFields.isEmailValidator(et_EmailId.getText().toString())) {
                    atv_RegEmailError2.setVisibility(View.VISIBLE);
                } else {
                    atv_RegEmailError2.setVisibility(View.GONE);
                }
            }

            if (et_Password.getText().toString().equals("")) {
                atv_PasswordError1.setVisibility(View.VISIBLE);
            } else {
                atv_PasswordError1.setVisibility(View.GONE);
                if ((et_Password.getText().toString().length() >= 8)) {
                    if (!ConstantFields.isValidPassword(et_Password.getText().toString())) {
                        atv_PasswordError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_PasswordError2.setVisibility(View.GONE);
                    }
                } else {
                    atv_PasswordError2.setVisibility(View.VISIBLE);
                }
            }

            if (et_ConfirmPassword.getText().toString().equals("")) {
                atv_ConfirmPasswordError1.setVisibility(View.VISIBLE);
            } else {
                atv_ConfirmPasswordError1.setVisibility(View.GONE);
                if ((et_ConfirmPassword.getText().toString().length() >= 8)) {
                    if (!et_Password.getText().toString().equals(et_ConfirmPassword.getText().toString())
                            && !et_ConfirmPassword.getText().toString().equals("") && !et_Password.getText().toString().equals("")) {
                        atv_ConfirmPasswordError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_ConfirmPasswordError2.setVisibility(View.GONE);
                    }
                } else {
                    atv_ConfirmPasswordError2.setVisibility(View.VISIBLE);
                }
            }
        });

       // atv_BackToLogin.setOnClickListener(view -> mUserRegistrationInstantTransfer.loadSignInFromSignUp());
        fontSetup();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mTwitterLoginButton.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        mUserRegistrationInstantTransfer = (User) context;
        activity = getActivity();
    }

    private void fontSetup() {

        fontSetup(atv_RegDOB, Constant.LIGHT, 1);
        fontSetup(atv_RegFirstNameError, Constant.LIGHT, 1);
        fontSetup(atv_RegLastNameError, Constant.LIGHT, 1);
        fontSetup(atv_RegMobileNumberError, Constant.LIGHT, 1);
        fontSetup(atv_RegEmailError1, Constant.LIGHT, 1);
        fontSetup(atv_RegEmailError2, Constant.LIGHT, 1);
        fontSetup(atv_PasswordError1, Constant.LIGHT, 1);
        fontSetup(atv_PasswordError2, Constant.LIGHT, 1);
        fontSetup(atv_ConfirmPasswordError1, Constant.LIGHT, 1);
        fontSetup(atv_ConfirmPasswordError2, Constant.LIGHT, 1);

        fontSetup(et_FirstName, Constant.LIGHT, 5);
        fontSetup(et_LastName, Constant.LIGHT, 5);
        fontSetup(et_MobileNumber, Constant.LIGHT, 5);
        fontSetup(et_EmailId, Constant.LIGHT, 5);
        fontSetup(et_Password, Constant.LIGHT, 5);
        fontSetup(et_ConfirmPassword, Constant.LIGHT, 5);

        fontSetup(btn_CreateAnAccount, Constant.LIGHT, 2);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
