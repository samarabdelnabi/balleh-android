package com.balleh.fragments.user;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.User;

import org.json.JSONObject;

/**
 * Created by Sasikumar on 3/24/2018.
 * Forget password
 */

public class DialogFragmentForgotPassword extends DialogFragment {

    private View v_ForgetPasswordDialog;
    private User userForgotPassword;
    private Button btn_ResetPassword;
    private EditText et_EmailForgotPassword;
    private AppCompatTextView atv_Title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ForgetPasswordDialog = inflater.inflate(R.layout.dialog_user_forgot_password, container, false);
        load();
        return v_ForgetPasswordDialog;
    }

    private void load() {
        atv_Title = v_ForgetPasswordDialog.findViewById(R.id.atv_title_forget_pwd);
        btn_ResetPassword = v_ForgetPasswordDialog.findViewById(R.id.btn_reset_password);
        et_EmailForgotPassword = v_ForgetPasswordDialog.findViewById(R.id.et_email_forget_pwd);

        btn_ResetPassword.setOnClickListener(view -> {

            if (!et_EmailForgotPassword.getText().toString().isEmpty()
                    && ConstantFields.isEmailValidator(et_EmailForgotPassword.getText().toString())) {
                try {
                    JSONObject jsonForgotPassword = new JSONObject();
                    jsonForgotPassword.put("email", et_EmailForgotPassword.getText().toString());
                    jsonForgotPassword.put("template", "email_reset");
                    jsonForgotPassword.put("websiteId", "1");

                    userForgotPassword.ForgetPassword(jsonForgotPassword);

                } catch (Exception e) {
                    showMessage(getString(R.string.common_error));
                }
            }

            if (et_EmailForgotPassword.getText().toString().isEmpty()) {
                showMessage(getString(R.string.user_error_email));
            } else {
                if (!ConstantFields.isEmailValidator(et_EmailForgotPassword.getText().toString())) {
                    showMessage(getString(R.string.user_error_invalid_email));
                }
            }

        });

        fontSetup();
    }

    private void showMessage(String Message) {
        ConstantFields.CustomToast(Message, getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        userForgotPassword = (User) context;
    }


    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);
    }

    private void fontSetup() {

        fontSetup(et_EmailForgotPassword, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(atv_Title, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_ResetPassword, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
