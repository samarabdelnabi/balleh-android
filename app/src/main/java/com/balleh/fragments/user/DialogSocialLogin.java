package com.balleh.fragments.user;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.CustomerResponse;

import org.json.JSONObject;

/**
 * Created by user on 7/4/2018.
 * Social Login
 */

public class DialogSocialLogin extends DialogFragment implements View.OnClickListener {

    private View mSocialLogin;
    private String FirstName, LastName, user_id, type, reason, EmailID, from;
    private TextInputLayout emailHolder,firstNameHolder,lastNameHolder;
    private EditText email_id,firstName,lastName;
    private CustomerResponse customerResponse;
    private Button mSocialLoginBtn;
    private TextView required_email_id_title;

    public DialogSocialLogin() {
    }

    public static DialogSocialLogin getInstance(String first_name, String last_name, String user_id, String type, String email_id, String reason, String from) {
        DialogSocialLogin socialLogin = new DialogSocialLogin();
        Bundle bundle = new Bundle();
        bundle.putString("FirstName", first_name);
        bundle.putString("LastName", last_name);
        bundle.putString("UserId", user_id);
        bundle.putString("Type", type);
        bundle.putString("EmailID", email_id);
        bundle.putString("From", from);
        bundle.putString("For", reason);
        socialLogin.setArguments(bundle);
        return socialLogin;
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = getResources().getDimensionPixelSize(R.dimen._300sdp);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            FirstName = bundle.getString("FirstName");
            LastName = bundle.getString("LastName");
            user_id = bundle.getString("UserId");
            type = bundle.getString("Type");
            reason = bundle.getString("For");
            EmailID = bundle.getString("EmailID");
            from = bundle.getString("From");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        customerResponse = (CustomerResponse) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mSocialLogin = inflater.inflate(R.layout.dialog_user_social_login, container, false);
        if (getDialog().getWindow() != null)
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setting();
        return mSocialLogin;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.social_login_continue:
                check();
                break;
            case R.id.dialog_closer_social_login:
                customerResponse.getResult("Exit", "false");
                dismiss();
                break;
        }
    }

    private void check() {
        with_email();
    }

    private void with_email() {
        if (!email_id.getText().toString().isEmpty()
                && ConstantFields.isEmailValidator(email_id.getText().toString())
                ) {
            try {
                JSONObject jsonObject = new JSONObject();
                if (type.equals("Facebook")) {
                    jsonObject.put("socialType", "Facebook");
                } else {
                    jsonObject.put("socialType", "Twitter");
                }

                if (email_id.getText().toString().length() > 0) {
                    jsonObject.put("email", email_id.getText().toString());
                } else {
                    jsonObject.put("email", "");
                }

                jsonObject.put("socialId", user_id);
                jsonObject.put("firstname", FirstName);
                jsonObject.put("lastname", LastName);
                customerResponse.getResult(jsonObject.toString(), "false");

            } catch (Exception e) {

            }
        }
        if (!email_id.getText().toString().isEmpty()) {
            if (ConstantFields.isEmailValidator(email_id.getText().toString())) {
                emailHolder.setErrorEnabled(false);
            } else {
                emailHolder.setErrorEnabled(true);
                emailHolder.setError(getString(R.string.user_error_invalid_email));
            }
        } else {
            emailHolder.setErrorEnabled(true);
            emailHolder.setError(getString(R.string.user_error_invalid_email));
        }
    }

    private void setting() {
        emailHolder = mSocialLogin.findViewById(R.id.social_email_id_holder);
        email_id = mSocialLogin.findViewById(R.id.social_email_id);

        firstNameHolder = mSocialLogin.findViewById(R.id.social_first_name_holder);
        firstName = mSocialLogin.findViewById(R.id.social_first_name);
        lastNameHolder = mSocialLogin.findViewById(R.id.social_last_name_holder);
        lastName = mSocialLogin.findViewById(R.id.social_last_name);

        ImageButton mCloser = mSocialLogin.findViewById(R.id.dialog_closer_social_login);
        required_email_id_title = mSocialLogin.findViewById(R.id.social_login_title);
        mSocialLoginBtn = mSocialLogin.findViewById(R.id.social_login_continue);

        mSocialLoginBtn.setOnClickListener(this);

        String title = "Hi " + FirstName + " " + LastName + getActivity().getString(R.string.social_login_email_hint);
        required_email_id_title.setText(title);

        mCloser.setOnClickListener(this);


        if (reason != null) {
            if (reason.equals("Phone")) {
                emailHolder.setVisibility(View.GONE);
            } else {
                emailHolder.setVisibility(View.VISIBLE);
            }
        } else {
            emailHolder.setVisibility(View.VISIBLE);
        }

        fontSetup();

    }

    private void fontSetup() {

        fontSetup(emailHolder, Constant.LIGHT, Constant.C_TEXT_INPUT_LAYOUT);
        fontSetup(firstNameHolder, Constant.LIGHT, Constant.C_TEXT_INPUT_LAYOUT);
        fontSetup(lastNameHolder, Constant.LIGHT, Constant.C_TEXT_INPUT_LAYOUT);

        fontSetup(required_email_id_title, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(email_id, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(firstName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(lastName, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(mSocialLoginBtn, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
