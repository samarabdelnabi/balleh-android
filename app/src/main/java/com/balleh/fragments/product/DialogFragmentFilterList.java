package com.balleh.fragments.product;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelFilterList;
import com.balleh.model.ModelFilterValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 7/25/2018.
 * Product Filter List by Category
 */

public class DialogFragmentFilterList extends DialogFragment {

    private Activity activity;
    private View v_Filter;
    private Button btn_Filter, btn_Reset;
    private RecyclerView rc_FilterTitleList, rc_FilterValueList;
    private CardView cv_EmptyFilterMessage;
    private ProgressBar pb_FilterLoader;
    private int CategoryId = 0;
    private ArrayList<ModelFilterList> filterList = new ArrayList<>();
    private MenuHandler menuHandler;
    private AppCompatTextView atv_FilterNotAvailable;

    public DialogFragmentFilterList() {
    }

    public static DialogFragmentFilterList getInstance(int categoryId) {
        DialogFragmentFilterList dialogFragmentFilterList = new DialogFragmentFilterList();
        Bundle bundle = new Bundle();
        bundle.putInt("CategoryId", categoryId);
        dialogFragmentFilterList.setArguments(bundle);
        return dialogFragmentFilterList;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            CategoryId = getArguments().getInt("CategoryId");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_Filter = inflater.inflate(R.layout.fragment_product_filter_list, container, false);
        setting();
        return v_Filter;
    }

    private void setting() {
        ImageButton ib_FilterClose = v_Filter.findViewById(R.id.ib_close_filter);
        btn_Reset = v_Filter.findViewById(R.id.btn_filter_reset);
        btn_Filter = v_Filter.findViewById(R.id.btn_filter);
        rc_FilterTitleList = v_Filter.findViewById(R.id.rc_filter_title_list);
        rc_FilterValueList = v_Filter.findViewById(R.id.rc_filter_value_list);
        cv_EmptyFilterMessage = v_Filter.findViewById(R.id.cv_no_filter);
        pb_FilterLoader = v_Filter.findViewById(R.id.pb_filter_loader);
        atv_FilterNotAvailable=v_Filter.findViewById(R.id.atv_filter_not_available);

        rc_FilterTitleList.setLayoutManager(new LinearLayoutManager(activity));
        rc_FilterValueList.setLayoutManager(new LinearLayoutManager(activity));

        ib_FilterClose.setOnClickListener(v -> dismiss());
        btn_Reset.setOnClickListener(v -> reset());

        if(menuHandler.networkError()){
            pb_FilterLoader.setVisibility(View.VISIBLE);
            StringRequest stringFilterRequest = new StringRequest(Request.Method.GET, ConstantFields.getFilterList(CategoryId),
                    response -> {
                        pb_FilterLoader.setVisibility(View.GONE);
                        cv_EmptyFilterMessage.setVisibility(View.GONE);
                        filterList = ConstantDataParser.getFilterList(response);

                        if (filterList != null) {
                            if (filterList.size() > 0) {
                                rc_FilterTitleList.setAdapter(new FilterTitleList(filterList));
                                rc_FilterValueList.setAdapter(new FilterValueList(filterList.get(0).getFilterValueList()));

                                btn_Filter.setOnClickListener(v -> filterResult());

                            }
                        }

                    },
                    error -> {
                        pb_FilterLoader.setVisibility(View.GONE);
                        cv_EmptyFilterMessage.setVisibility(View.GONE);
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    return params;
                }
            };

            stringFilterRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringFilterRequest, "FilterList");
        }


        fontSetup();
    }

    private void filterResult() {

        StringBuilder URL = new StringBuilder();

        if (filterList != null) {
            if (filterList.size() > 0) {
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getFilterValueList() != null) {
                        if (filterList.get(i).getFilterValueList().size() > 0) {
                            for (int j = 0; j < filterList.get(i).getFilterValueList().size(); j++) {
                                if (filterList.get(i).getFilterValueList().get(j).isSelect()) {
                                    if (filterList.get(i).getFilterTitle().toLowerCase().equals("price")) {
                                        String result[] = filterList.get(i).getFilterValueList().get(j).getValueId().split("-");
                                        if (result.length > 0) {
                                            if (result.length == 2) {
                                                if (result[0].length() > 0) {
                                                    URL.append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                            .append(filterList.get(i).getAttributeName())
                                                            .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                            .append(result[0])
                                                            .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=from")

                                                            .append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                            .append(filterList.get(i).getAttributeName())
                                                            .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                            .append(result[1])
                                                            .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=to");
                                                } else {
                                                    URL.append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                            .append(filterList.get(i).getAttributeName())
                                                            .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                            .append(result[1])
                                                            .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=lteq");
                                                }

                                            } else {
                                                if (filterList.get(i).getFilterValueList().get(j).getValueId().trim().substring(0, 1).equals("-")) {
                                                    URL.append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                            .append(filterList.get(i).getAttributeName())
                                                            .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                            .append(result[0])
                                                            .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=lteq");
                                                } else {
                                                    URL.append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                            .append(filterList.get(i).getAttributeName())
                                                            .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                            .append(result[0])
                                                            .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=gt");
                                                }
                                            }
                                        }
                                    } else {
                                        URL.append("&searchCriteria[filter_groups][1][filters][0][field]=")
                                                .append(filterList.get(i).getAttributeName())
                                                .append("&searchCriteria[filter_groups][1][filters][0][value]=")
                                                .append(filterList.get(i).getFilterValueList().get(j).getValueId())
                                                .append("&searchCriteria[filter_groups][1][filters][0][condition_type]=eq");
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        menuHandler.toProductList(CategoryId, "Filter", URL.toString());
        dismiss();

    }

    private void reset() {
        if (filterList != null) {
            if (filterList.size() > 0) {
                for (int i = 0; i < filterList.size(); i++) {
                    if (i == 0) {
                        filterList.get(i).setSelect(true);
                    } else {
                        filterList.get(i).setSelect(false);
                    }

                    if (filterList.get(i).getFilterValueList() != null) {
                        if (filterList.get(i).getFilterValueList().size() > 0) {
                            for (int j = 0; j < filterList.get(i).getFilterValueList().size(); j++) {
                                filterList.get(i).getFilterValueList().get(j).setSelect(false);
                            }
                        }
                    }
                }

                rc_FilterTitleList.setAdapter(new FilterTitleList(filterList));
                rc_FilterValueList.setAdapter(new FilterValueList(filterList.get(0).getFilterValueList()));

            } else {
                dismiss();
            }
        } else {
            dismiss();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);

    }

    private class FilterTitleList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelFilterList> mTitleList;
        private int last_position = -1;

        FilterTitleList(ArrayList<ModelFilterList> mReturnValue) {
            this.mTitleList = mReturnValue;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            View view;
            LayoutInflater inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.rc_row_filter_title, parent, false);
            viewHolder = new List_ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            List_ViewHolder list_viewHolder = (List_ViewHolder) holder;
            list_viewHolder.title.setText(mTitleList.get(position).getAttributeName().toUpperCase());

            if (mTitleList.get(position).isSelect()) {
                list_viewHolder.title.setBackgroundResource(R.color.colorPrimary);
                list_viewHolder.title.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
            } else {
                list_viewHolder.title.setBackgroundResource(R.color.grey_200);
                list_viewHolder.title.setTextColor(ContextCompat.getColor(activity, R.color.grey_500));
            }
        }

        @Override
        public int getItemCount() {
            return mTitleList.size();
        }

        private class List_ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView title;

            List_ViewHolder(View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.atv_filter_title);
                title.setOnClickListener(this);

                fontSetup(title,Constant.LIGHT,Constant.C_TEXT_VIEW);
            }

            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.atv_filter_title:
                        if (getAdapterPosition() != -1) {
                            if (filterList != null) {
                                if (filterList.size() > 0) {
                                    if (filterList.get(getAdapterPosition()).getAttributeName().equals(mTitleList.get(getAdapterPosition()).getAttributeName())) {
                                        for (int i = 0; i < mTitleList.get(getAdapterPosition()).getFilterValueList().size(); i++) {
                                            if (filterList.get(getAdapterPosition()).getFilterValueList().size() == mTitleList.get(getAdapterPosition()).getFilterValueList().size()) {
                                                if (filterList.get(getAdapterPosition()).getFilterValueList().get(i).isSelect()) {
                                                    mTitleList.get(getAdapterPosition()).getFilterValueList().get(i).setSelect(true);
                                                }
                                            }
                                        }
                                        rc_FilterValueList.setAdapter(new FilterValueList(mTitleList.get(getAdapterPosition()).getFilterValueList()));
                                    }
                                }
                            }

                            if (last_position != getAdapterPosition())
                                if (last_position == -1) {
                                    mTitleList.get(0).setSelect(false);
                                    mTitleList.get(getAdapterPosition()).setSelect(true);
                                    last_position = getAdapterPosition();
                                    notifyDataSetChanged();
                                } else {
                                    mTitleList.get(last_position).setSelect(false);
                                    mTitleList.get(getAdapterPosition()).setSelect(true);
                                    last_position = getAdapterPosition();
                                    notifyDataSetChanged();
                                }
                        }
                        break;
                }
            }
        }
    }

    private class FilterValueList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelFilterValue> filterValueList;

        FilterValueList(ArrayList<ModelFilterValue> filterValueList) {
            this.filterValueList = filterValueList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View list = LayoutInflater.from(activity).inflate(R.layout.rc_row_filter_value, parent, false);
            return new List_ViewHolder(list);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            List_ViewHolder list_viewHolder = (List_ViewHolder) holder;
            list_viewHolder.atv_OptionTitle.setText(filterValueList.get(position).getValueTitle());

            if (filterValueList.get(position).isSelect()) {
                list_viewHolder.cb_OptionSelection.setChecked(true);
            }
        }

        @Override
        public int getItemCount() {
            return filterValueList.size();
        }

        class List_ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
            AppCompatTextView atv_OptionTitle;
            CheckBox cb_OptionSelection;

            List_ViewHolder(View itemView) {
                super(itemView);
                atv_OptionTitle = itemView.findViewById(R.id.atv_filter_value_name);
                cb_OptionSelection = itemView.findViewById(R.id.cb_filter_value_select);
                cb_OptionSelection.setOnCheckedChangeListener(this);

                fontSetup(atv_OptionTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
                fontSetup(cb_OptionSelection,Constant.LIGHT,Constant.C_CHECKBOX);
            }

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < filterList.size(); i++) {
                        if (filterList.get(i).getFilterValueList() != null) {
                            if (filterList.get(i).getFilterValueList().size() > 0) {
                                for (int j = 0; j < filterList.get(i).getFilterValueList().size(); j++) {
                                    if (filterValueList.get(getAdapterPosition()).getParentCode().equals(filterList.get(i).getFilterValueList().get(j).getParentCode())) {
                                        if (filterValueList.get(getAdapterPosition()).getValueId().equals(filterList.get(i).getFilterValueList().get(j).getValueId())) {
                                            filterList.get(i).getFilterValueList().get(j).setSelect(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < filterList.size(); i++) {
                        if (filterList.get(i).getFilterValueList() != null) {
                            if (filterList.get(i).getFilterValueList().size() > 0) {
                                for (int j = 0; j < filterList.get(i).getFilterValueList().size(); j++) {
                                    if (filterValueList.get(getAdapterPosition()).getParentCode().equals(filterList.get(i).getFilterValueList().get(j).getParentCode())) {
                                        if (filterValueList.get(getAdapterPosition()).getValueId().equals(filterList.get(i).getFilterValueList().get(j).getValueId())) {
                                            filterList.get(i).getFilterValueList().get(j).setSelect(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void fontSetup() {
        fontSetup(atv_FilterNotAvailable, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(btn_Filter, Constant.LIGHT, Constant.C_BUTTON);
        fontSetup(btn_Reset, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
