package com.balleh.fragments.product;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.ProductDetailImageList;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.db.WishList;
import com.balleh.additional.db.wishlist.WishListDBHandler;
import com.balleh.additional.db.wishlist.WishListDao;
import com.balleh.additional.db.wishlist.WishListRoom;
import com.balleh.additional.instentTransfer.CustomerResponse;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelBrandList;
import com.balleh.model.ModelProductDetail;
import com.balleh.model.ModelProductDetailList;
import com.balleh.model.ModelProductDetailOptions;
import com.balleh.model.ModelRelatedProductList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentProductDetails extends Fragment implements CustomerResponse {

    private View v_ProductDetail;
    private String productSku;
    private ProgressBar pb_ProductDetailLoader;
    private AppCompatTextView atv_ProductTitle, atv_ProductSpecialPrice,
            atv_WriteReview, atv_RelatedProductTitle, atv_ProductPrice,atv_DescriptionTitle;
    private RecyclerView rc_RelatedProductList;
    private WebView wv_ProductDescription;
    private ViewPager vp_ProductImageLister;
    private LinearLayout ll_ImageListListener;
    private MenuHandler menuHandler;
    private Activity activity;
    private ModelProductDetail productDetail;
    private ImageButton ib_AddToWishList;
    private Button btn_AddToCart;
    private int qty = 0;
    private Spinner s_QuantityList;
    private WishListDao wishListDao;


    public FragmentProductDetails() {
    }

    public static FragmentProductDetails getInstance(String ProductSku) {
        FragmentProductDetails fragmentProductDetails = new FragmentProductDetails();
        Bundle bundle = new Bundle();
        bundle.putString("ProductSku", ProductSku);
        fragmentProductDetails.setArguments(bundle);
        return fragmentProductDetails;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productSku = getArguments().getString("ProductSku");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ProductDetail = inflater.inflate(R.layout.fragment_product_detail, container, false);
        load();
        return v_ProductDetail;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        menuHandler.visibility();
    }

    private void load() {

        WishListDBHandler wishListDBHandler = Room.databaseBuilder(activity, WishListDBHandler.class, "wishList")
                .allowMainThreadQueries()
                .build();
        wishListDao = wishListDBHandler.getWishList();

        atv_ProductTitle = v_ProductDetail.findViewById(R.id.atv_title_product_detail);
        atv_ProductSpecialPrice = v_ProductDetail.findViewById(R.id.atv_special_price_product_detail);
        atv_ProductPrice = v_ProductDetail.findViewById(R.id.atv_price_product_detail);
        atv_WriteReview = v_ProductDetail.findViewById(R.id.atv_write_review);
        atv_DescriptionTitle=v_ProductDetail.findViewById(R.id.atv_description_title_product_detail);
        atv_RelatedProductTitle = v_ProductDetail.findViewById(R.id.atv_related_product_title_product_detail);

        rc_RelatedProductList = v_ProductDetail.findViewById(R.id.rc_related_product_list);

        wv_ProductDescription = v_ProductDetail.findViewById(R.id.wv_description_product_detail);

        vp_ProductImageLister = v_ProductDetail.findViewById(R.id.vp_image_list_product_detail);

        ll_ImageListListener = v_ProductDetail.findViewById(R.id.ll_image_list_pointer_product_detail);

        pb_ProductDetailLoader = v_ProductDetail.findViewById(R.id.pb_product_detail_loader);

        ib_AddToWishList = v_ProductDetail.findViewById(R.id.ib_product_detail_wish_list);

        btn_AddToCart = v_ProductDetail.findViewById(R.id.btn_add_to_cart_product_detail);

        s_QuantityList = v_ProductDetail.findViewById(R.id.sp_quantity_dialog_list);

        rc_RelatedProductList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        stockStatus();

    }

    private void stockStatus() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            StringRequest stringStockStatusRequest = new StringRequest(Request.Method.GET, ConstantFields.getProductStatus(productSku, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductDetailLoader.setVisibility(View.GONE);
                        int totalQty = ConstantDataParser.getStockQuantity(response);

                        if (totalQty > 0) {
                            btn_AddToCart.setText(getString(R.string.product_detail_add_to_cart));
                        } else {
                            btn_AddToCart.setText(getString(R.string.cart_out_of_stack_message));
                        }

                        int qtySize;
                        if (totalQty == 0) {
                            qtySize = 0;
                        } else if (totalQty > 0 && totalQty < 10) {
                            qtySize = totalQty;
                        } else {
                            qtySize = 10;
                        }

                        if (qtySize == 0) {
                            ArrayList<Integer> quantityList = new ArrayList<>();
                            quantityList.add(0);
                            QuantitySpinnerAdapter quantitySpinnerAdapter = new QuantitySpinnerAdapter(activity, quantityList);
                            s_QuantityList.setAdapter(quantitySpinnerAdapter);
                        } else {
                            final ArrayList<Integer> quantityList = new ArrayList<>();
                            for (int i = 0; i < qtySize; i++) {
                                quantityList.add(i + 1);
                            }
                            QuantitySpinnerAdapter quantitySpinnerAdapter = new QuantitySpinnerAdapter(activity, quantityList);
                            s_QuantityList.setAdapter(quantitySpinnerAdapter);
                            s_QuantityList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    qty = quantityList.get(i);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                        Log.e("onResponse: ", response);

                        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mSizeTag).equals("Empty")) {
                            productDetail();
                        } else {
                            loadSizeList();
                        }
                    }, error -> {
                pb_ProductDetailLoader.setVisibility(View.GONE);
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mSizeTag).equals("Empty")) {
                    productDetail();
                } else {
                    loadSizeList();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringStockStatusRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringStockStatusRequest, "ProductStockStatus");
        }
    }

    private void productDetail() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            Log.e("productDetail: ", ConstantFields.getProductDetails(productSku, ConstantFields.currentLanguage().equals("en")));
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getProductDetails(productSku, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductDetailLoader.setVisibility(View.GONE);

                        Log.e("onResponse: ", response + "");

                        productDetail = ConstantDataParser.getProductDetail(response);

                        if (productDetail != null) {

                            if (productDetail.getProductType() != null) {
                                if (productDetail.getProductType().toLowerCase().equals("simple")) {
                                    if (!productDetail.isSubscribable()) {
                                        simpleProductOptionSetting();
                                    }
                                }
                            }

                            getRelatedProductList(productDetail.getSku());

                            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
//                                if (WishList.getInstance(activity.getApplicationContext()).getSizeWishList() > 0)
//                                    if (WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetail.getSku())) {
//                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_selected_24dp);
//                                    } else {
//                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
//                                    }

                                activity.runOnUiThread(() -> {
                                    if (wishListDao.isAvailable(productDetail.getSku())) {
                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_selected_24dp);
                                    } else {
                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                                    }
                                });
                            }

                            ib_AddToWishList.setOnClickListener(view -> {
                                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
//                                    if (!WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetail.getSku())) {
//                                        WishList.getInstance(activity.getApplicationContext()).add_to_wish_list(productDetail.getSku(),
                                    if (!wishListDao.isAvailable(productDetail.getSku())) {
                                        WishListRoom wishListRoom = new WishListRoom();
                                        wishListRoom.setProductId(productDetail.getSku());
                                        wishListRoom.setProductDetails(productDetail.getProductDetail());
                                        wishListDao.insert(wishListRoom);
                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_selected_24dp);
                                        String message = productDetail.getTitle() + " " + activity.getString(R.string.wish_list_add);
                                        showToast(message);
                                    } else {
//                                        WishList.getInstance(activity.getApplicationContext()).remove_from_wish_list(productDetail.getSku());
                                        wishListDao.deleteProduct(productDetail.getSku());
                                        ib_AddToWishList.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                                        String message = productDetail.getTitle() + " " + activity.getString(R.string.wish_list_remove);
                                        showToast(message);
                                    }
                                } else {
                                    menuHandler.toSignIn();
                                }
                            });

                            atv_WriteReview.setOnClickListener(view -> {
                                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag).equals("Empty")) {
                                    FragmentReviewList dialogFragmentWriteReview = FragmentReviewList.getInstance(productDetail.getProductId());
                                    dialogFragmentWriteReview.show(getChildFragmentManager(), "Reviews");
                                } else {
                                    menuHandler.toSignIn();
                                }
                            });

                            if (productDetail.getImageList() != null) {
                                if (productDetail.getImageList().size() > 0) {
                                    ProductDetailImageList productDetailImageList = new ProductDetailImageList(activity,
                                            productDetail.getImageList(), menuHandler);

                                    vp_ProductImageLister.setAdapter(productDetailImageList);

                                    int pos = vp_ProductImageLister.getCurrentItem();

                                    for (int i = 0; i < productDetail.getImageList().size(); i++) {
                                        @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.slide_indicator, null, false);
                                        final Button button = view.findViewById(R.id.btn_indicator);
                                        button.setPadding(100, 100, 100, 100);
                                        int size = (int) activity.getResources().getDisplayMetrics().density;
                                        if (size == 3) {
                                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(40, 40);
                                            layoutParams.setMargins(10, 0, 10, 0);
                                            button.setLayoutParams(layoutParams);
                                        } else if (size == 2) {
                                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
                                            layoutParams.setMargins(10, 0, 10, 0);
                                            button.setLayoutParams(layoutParams);
                                        } else {
                                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(24, 24);
                                            layoutParams.setMargins(10, 0, 10, 0);
                                            button.setLayoutParams(layoutParams);
                                        }
                                        if (pos == i) {
                                            button.setBackgroundResource(R.drawable.draw_btn_non_select_slider);
                                        } else {
                                            button.setBackgroundResource(R.drawable.draw_btn_select_slider);
                                        }
                                        button.setId(i);
                                        button.setOnClickListener(v -> vp_ProductImageLister.setCurrentItem(button.getId()));

                                        LinearLayout.LayoutParams layoutParamsMargin = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        layoutParamsMargin.setMargins(16, 0, 16, 10);
                                        layoutParamsMargin.gravity = Gravity.BOTTOM;
                                        ll_ImageListListener.setLayoutParams(layoutParamsMargin);
                                        ll_ImageListListener.setGravity(Gravity.CENTER);
                                        ll_ImageListListener.addView(button);
                                    }


                                    vp_ProductImageLister.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                        @Override
                                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                        }

                                        @Override
                                        public void onPageSelected(int position) {
                                            ll_ImageListListener.removeAllViews();

                                            if (productDetail.getImageList().size() > 0) {
                                                for (int i = 0; i < productDetail.getImageList().size(); i++) {
                                                    @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.slide_indicator, null, false);
                                                    final Button button = view.findViewById(R.id.btn_indicator);
                                                    int size = (int) activity.getResources().getDisplayMetrics().density;
                                                    if (size == 3) {
                                                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(40, 40);
                                                        layoutParams.setMargins(10, 0, 10, 0);
                                                        button.setLayoutParams(layoutParams);
                                                    } else if (size == 2) {
                                                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(30, 30);
                                                        layoutParams.setMargins(10, 0, 10, 0);
                                                        button.setLayoutParams(layoutParams);
                                                    } else {
                                                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(24, 24);
                                                        layoutParams.setMargins(10, 0, 10, 0);
                                                        button.setLayoutParams(layoutParams);
                                                    }
                                                    if (position == i) {
                                                        button.setBackgroundResource(R.drawable.draw_btn_non_select_slider);
                                                    } else {
                                                        button.setBackgroundResource(R.drawable.draw_btn_select_slider);
                                                    }
                                                    button.setId(i);
                                                    button.setOnClickListener(v -> vp_ProductImageLister.setCurrentItem(button.getId()));
                                                    ll_ImageListListener.addView(button);
                                                }
                                            }

                                        }

                                        @Override
                                        public void onPageScrollStateChanged(int state) {

                                        }
                                    });

                                } else {
                                    menuHandler.toProductImageList(null);
                                }
                            } else {
                                menuHandler.toProductImageList(null);
                            }

                            if (productDetail.getCustomAttributes() != null) {
                                if (productDetail.getCustomAttributes().size() > 0) {

                                    for (int i = 0; i < productDetail.getCustomAttributes().size(); i++) {
                                        if (productDetail.getCustomAttributes().get(i)[0].toLowerCase().equals("description")) {
                                            wv_ProductDescription.loadData(productDetail.getCustomAttributes().get(i)[1], "text/html", "utf-8");
                                        } else if (productDetail.getCustomAttributes().get(i)[0].toLowerCase().equals("special_price")) {
                                            productDetail.setSpecial_price(Double.valueOf(productDetail.getCustomAttributes().get(i)[1]).intValue());
                                        }

                                        if (ConstantFields.currentLanguage().equals("ar")) {
                                            if (!productDetail.getAr_title().equals("a")) {
                                                atv_ProductTitle.setText(productDetail.getAr_title());
                                            } else {
                                                if (productDetail.getCustomAttributes().get(i)[0] != null) {
                                                    if (productDetail.getCustomAttributes().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                                        String title = productDetail.getCustomAttributes().get(i)[1];
                                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                                            title = title.substring(4);
                                                        }
                                                        atv_ProductTitle.setText(title);
                                                    }
                                                }
                                            }
                                        } else {
                                            if (!productDetail.getEn_title().equals("a")) {
                                                atv_ProductTitle.setText(productDetail.getEn_title());
                                            } else {
                                                if (productDetail.getCustomAttributes().get(i)[0] != null) {
                                                    if (productDetail.getCustomAttributes().get(i)[0].trim().toLowerCase().equals("meta_title")) {
                                                        String title = productDetail.getCustomAttributes().get(i)[1];
                                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                                            title = title.substring(4);
                                                        }
                                                        atv_ProductTitle.setText(title);
                                                    }
                                                }
                                            }
                                        }

                                        if (productDetail.getCustomAttributes().get(i)[0] != null) {
                                            if (productDetail.getCustomAttributes().get(i)[0].trim().toLowerCase().equals("count")) {
                                                String title = atv_ProductTitle.getText().toString() + " " + getCountTitle(productDetail.getCustomAttributes().get(i)[1]);
                                                atv_ProductTitle.setText(title);
                                            }
                                        }

                                        if (productDetail.getCustomAttributes().get(i)[0] != null) {
                                            if (productDetail.getCustomAttributes().get(i)[0].trim().toLowerCase().equals("size")) {
                                                String title = atv_ProductTitle.getText().toString() + " " + getSizeTitle(productDetail.getCustomAttributes().get(i)[1]);
                                                atv_ProductTitle.setText(title);
                                            }
                                        }

                                    }

                                    if (atv_ProductTitle.getText().toString().length() == 0) {
                                        atv_ProductTitle.setText(productDetail.getTitle());
                                    }

                                } else {
                                    atv_ProductTitle.setText(productDetail.getTitle());
                                }
                            } else {
                                atv_ProductTitle.setText(productDetail.getTitle());
                            }

                            if (productDetail.getSpecial_price() != 0) {
                                atv_ProductPrice.setVisibility(View.VISIBLE);
                                atv_ProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                                atv_ProductPrice.setText(String.valueOf("SAR " + productDetail.getPrice()));
                                atv_ProductSpecialPrice.setText(String.valueOf("SAR " + productDetail.getSpecial_price()));
                            } else {
                                atv_ProductSpecialPrice.setText(String.valueOf("SAR " + productDetail.getPrice()));
                                atv_ProductPrice.setVisibility(View.GONE);
                            }


                        }
                    }, error -> pb_ProductDetailLoader.setVisibility(View.GONE)) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ProductDetail");
        }

        btn_AddToCart.setOnClickListener(view -> addToCartValidate());

        fontSetup();
    }

    private void simpleProductOptionSetting() {
        RecyclerView rc_OptionList = v_ProductDetail.findViewById(R.id.rc_option_list);
        View v_OptionDivider = v_ProductDetail.findViewById(R.id.v_option_divider);

        if (productDetail.getOptionList() != null) {
            if (productDetail.getOptionList().size() > 0) {
                if (!productDetail.isSubscribable()) {
                    rc_OptionList.setLayoutManager(new LinearLayoutManager(activity));
                    rc_OptionList.setAdapter(new SimpleProductOption());
                } else {
                    rc_OptionList.setVisibility(View.GONE);
                    v_OptionDivider.setVisibility(View.GONE);
                }
            } else {
                rc_OptionList.setVisibility(View.GONE);
                v_OptionDivider.setVisibility(View.GONE);
            }
        } else {
            rc_OptionList.setVisibility(View.GONE);
            v_OptionDivider.setVisibility(View.GONE);
        }

    }

    private void getRelatedProductList(String productSku) {
        if (menuHandler.networkError()) {
            StringRequest stringRequestRelatedProductList = new StringRequest(Request.Method.GET, ConstantFields.getRelatedProductList(productSku, ConstantFields.currentLanguage().equals("en")),
                    response -> {

                        Log.e("getRelatedProductList: ", response);

                        ArrayList<ModelRelatedProductList> relatedList = ConstantDataParser.getRelatedProductSKU(response);
                        if (relatedList != null) {
                            if (relatedList.size() > 0) {
                                String productList = null;

                                for (int i = 0; i < relatedList.size(); i++) {
                                    if (productList != null) {
                                        if (productList.length() > 0) {
                                            productList = "," + relatedList.get(i).getProductSKU();
                                        } else {
                                            productList = relatedList.get(i).getProductSKU();
                                        }
                                    } else {
                                        productList = relatedList.get(i).getProductSKU();
                                    }
                                }

                                if (menuHandler.networkError()) {
                                    StringRequest stringRequestRelatedProductValue = new StringRequest(Request.Method.GET, ConstantFields.getRelatedProductListValue(productList, 6, 1, ConstantFields.currentLanguage().equals("en")),
                                            responseProductList -> {
                                                Log.e("getRelatedProductList: ", responseProductList + "");

                                                ArrayList<ModelProductDetailList> productDetailLists = ConstantDataParser.getProductList(responseProductList);

                                                if (productDetailLists != null) {
                                                    if (productDetailLists.size() > 0) {
                                                        RelatedProductListAdapter relatedProductListAdapter = new RelatedProductListAdapter(productDetailLists);
                                                        rc_RelatedProductList.setAdapter(relatedProductListAdapter);
                                                    } else {
                                                        hideRelatedList();
                                                    }
                                                } else {
                                                    hideRelatedList();
                                                }
                                            },
                                            error -> {
                                                hideRelatedList();
                                            }) {
                                        @Override
                                        public Map<String, String> getHeaders() {
                                            Map<String, String> params = new HashMap<>();
                                            params.put("Content-Type", "application/json");
                                            //params.put("Authorization", ConstantFields.getGuestToken());
                                            params.put("Authorization", "Bearer jhjowbukxd14yolglclcjm28un97edt3");
                                            return params;
                                        }

                                    };
                                    stringRequestRelatedProductValue.setShouldCache(false);
                                    ApplicationContext.getInstance().addToRequestQueue(stringRequestRelatedProductValue, "RelatedProductListValue");
                                }

                            } else {
                                hideRelatedList();
                            }
                        } else {
                            hideRelatedList();
                        }

                    }, error -> {
                hideRelatedList();
                Log.e("getRelatedProductList: ", error.getMessage() + "");
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    //params.put("Authorization", ConstantFields.getGuestToken());
                    params.put("Authorization", "Bearer jhjowbukxd14yolglclcjm28un97edt3");
                    return params;
                }
            };

            stringRequestRelatedProductList.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequestRelatedProductList, "RelatedProductList");
        }
    }

    private void hideRelatedList() {
        View v = v_ProductDetail.findViewById(R.id.v_related_product_divider);
        atv_RelatedProductTitle.setVisibility(View.GONE);
        rc_RelatedProductList.setVisibility(View.GONE);
        v.setVisibility(View.GONE);
    }

    private String getCountTitle(String id) {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCountTag).equals("Empty")) {
            ArrayList<ModelBrandList> countList = ConstantDataParser
                    .getAttributeList(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCountTag));
            if (countList != null) {
                if (countList.size() > 0) {
                    for (int i = 0; i < countList.size(); i++) {
                        if (countList.get(i).getId().equals(id)) {
                            return countList.get(i).getName();
                        }
                    }
                }
            }
        }

        return "";
    }

    private String getSizeTitle(String id) {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mSizeTag).equals("Empty")) {
            ArrayList<ModelBrandList> sizeList = ConstantDataParser
                    .getAttributeList(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mSizeTag));
            if (sizeList != null) {
                if (sizeList.size() > 0) {
                    for (int i = 0; i < sizeList.size(); i++) {
                        if (sizeList.get(i).getId().equals(id)) {
                            return sizeList.get(i).getName();
                        }
                    }
                }
            }
        }
        return "";
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, activity);
    }

    @Override
    public void getResult(String response, String source) {
        if (response != null) {
            if (!response.equals("Error")) {
                if (source.equals("CartIdProductDetail")) {
                    addToCartValidate();
                }
            }
        }
    }

    private void loadSizeList() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getAttributeList("Size", ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductDetailLoader.setVisibility(View.GONE);
                        Log.e("onResponse: ", response);
                        ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mSizeTag, response);

                        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCountTag).equals("Empty")) {
                            productDetail();
                        } else {
                            loadCountList();
                        }

                    }, error -> {
                pb_ProductDetailLoader.setVisibility(View.GONE);
                Log.e("onErrorResponse: ", "Error");
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCountTag).equals("Empty")) {
                    productDetail();
                } else {
                    loadCountList();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "BrandListSplashScreen");
        }
    }

    private void loadCountList() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getAttributeList("Count", ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductDetailLoader.setVisibility(View.GONE);
                        Log.e("onResponse: ", response);
                        ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mCountTag, response);
                        productDetail();

                    }, error -> {
                pb_ProductDetailLoader.setVisibility(View.GONE);
                Log.e("onErrorResponse: ", "Error");
                productDetail();
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CountryList");
        }
    }

    private boolean checkOption() {
        if (productDetail.getOptionList() != null) {
            if (productDetail.getOptionList().size() > 1) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < productDetail.getOptionList().size(); i++) {
                    if (productDetail.getOptionList().get(i).getOptionValueList() != null) {
                        //if (productDetail.getOptionList().get(i).isOptionRequired()) {
                        if (productDetail.getOptionList().get(i).getOptionValueList().size() > 0) {
                            int count = 0;
                            for (int j = 0; j < productDetail.getOptionList().get(i).getOptionValueList().size(); j++) {
                                if (!productDetail.getOptionList().get(i).getOptionValueList().get(j).isSelected()) {
                                    count = count + 1;
                                }
                            }

                            if (count == productDetail.getOptionList().get(i).getOptionValueList().size()) {
                                if (stringBuilder.length() > 0) {
                                    stringBuilder.append(",").append(productDetail.getOptionList().get(i).getOptionTitle());
                                } else {
                                    stringBuilder.append(getString(R.string.product_detail_option_error_1)).append(" ").append(productDetail.getOptionList().get(i).getOptionTitle());
                                }
                            }
                        }
                        //}
                    }
                }
                if (stringBuilder.length() > 0) {
                    showToast(stringBuilder.toString());
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void addToCartValidate() {
        if (!btn_AddToCart.getText().toString().trim().toLowerCase().equals("out of stock!")) {
            addToCart();
        } else {
            showToast(getString(R.string.cart_out_of_stack_message));
        }
    }

    private void addToCart() {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag).equals("Empty")) {
                addToCartCustomer();
            } else {
                getCartId();
            }
        } else {
            if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                CreateGuestCartId();
            } else {
                addToCartGuest();
            }
        }
    }

    private void CreateCartId() {
        if (menuHandler.networkError()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_ProductDetailLoader.setVisibility(View.VISIBLE);

                Log.e("CreateCartId: ", ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")));

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                            Log.e("CreateCartId: ", response + "");
                            ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag, response.replace("\"", ""));
                            customerDummyShippingAddress();
                        }, error -> {
                    pb_ProductDetailLoader.setVisibility(View.GONE);
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartIdHome");

            }
        }
    }

    private void CreateGuestCartId() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);

            Log.e("CreateGuestCartId: ", ConstantFields.getGuestCartId(ConstantFields.currentLanguage().equals("en")));
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getGuestCartId(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        pb_ProductDetailLoader.setVisibility(View.GONE);
                        Log.e("CreateGuestCartId: ", response);
                        ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mGuestCartIdTag, response.replace("\"", ""));

                        guestDummyShippingAddress();
                    }, error -> {
                pb_ProductDetailLoader.setVisibility(View.GONE);
                try {
                    if (error.networkResponse != null) {
                        String responseBody = new String(error.networkResponse.data, "utf-8");
                        JSONObject jsonObject = new JSONObject(responseBody);
                        Log.e("onErrorResponse: ", jsonObject.toString());
                    } else {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                } catch (Exception e) {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartId");
        }

    }

    private void getCartId() {
        if (menuHandler.networkError()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_ProductDetailLoader.setVisibility(View.VISIBLE);
                Log.e("getCartId: ", ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")));
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                            Log.e("getCartId: ", response);
                            ConstantDataSaver.mStoreSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag, String.valueOf(ConstantDataParser.getCartId(response)));

                            addToCartValidate();
                        }, error -> {
                    pb_ProductDetailLoader.setVisibility(View.GONE);
                    CreateCartId();
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag);
                    }

                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }

                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartId");
            }
        }
    }

    private void addToCartGuest() {
        if (!btn_AddToCart.getText().toString().trim().toLowerCase().equals("out of stock!")) {
            try {
                if (qty > 0) {
                    if (checkOption()) {
                        try {
                            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("quote_id", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mGuestCartIdTag));
                            jsonObject.put("qty", String.valueOf(qty));
                            jsonObject.put("sku", productDetail.getSku());

                            if (productDetail.getProductType().toLowerCase().equals("simple")) {
                                if (productDetail.getOptionList() != null) {
                                    if (productDetail.getOptionList().size() > 0) {
                                        if (productDetail.isSubscribable()) {
                                            JSONArray selectedOptionList = new JSONArray();
                                            if (productDetail.getOptionList().size() == 1) {
                                                for (int j = 0; j < productDetail.getOptionList().get(0).getOptionValueList().size(); j++) {
                                                    if (productDetail.getOptionList().get(0).getOptionValueList().get(j).getOptionTitle().toLowerCase().trim().equals("one time")) {
                                                        JSONObject jsonValue = new JSONObject();
                                                        jsonValue.put("option_value", productDetail.getOptionList().get(0).getOptionValueList().get(j).getOptionId());
                                                        jsonValue.put("option_id", productDetail.getOptionList().get(0).getOptionId());
                                                        selectedOptionList.put(jsonValue);
                                                    }
                                                }
                                                JSONObject jsonOption = new JSONObject();
                                                JSONObject jsonObjectSubName = new JSONObject();
                                                jsonOption.put("extension_attributes", jsonObjectSubName);
                                                jsonObjectSubName.put("custom_options", selectedOptionList);
                                                jsonObject.put("product_option", jsonOption);
                                            }
                                        } else {

                                            JSONArray selectedOptionList = new JSONArray();
                                            for (int i = 0; i < productDetail.getOptionList().size(); i++) {
                                                for (int j = 0; j < productDetail.getOptionList().get(i).getOptionValueList().size(); j++) {
                                                    if (productDetail.getOptionList().get(i).getOptionValueList().get(j).isSelected()) {
                                                        JSONObject jsonValue = new JSONObject();
                                                        jsonValue.put("option_value", productDetail.getOptionList().get(i).getOptionValueList().get(j).getOptionId());
                                                        jsonValue.put("option_id", productDetail.getOptionList().get(i).getOptionId());
                                                        selectedOptionList.put(jsonValue);
                                                    }
                                                }
                                            }

                                            JSONObject jsonOption = new JSONObject();
                                            JSONObject jsonObjectSubName = new JSONObject();
                                            jsonOption.put("extension_attributes", jsonObjectSubName);
                                            jsonObjectSubName.put("custom_options", selectedOptionList);
                                            jsonObject.put("product_option", jsonOption);
                                        }
                                    }
                                }
                            }

                            JSONObject jsonAddToCart = new JSONObject();
                            jsonAddToCart.put("cart_item", jsonObject);

                            Log.e("addToCartGuest: ", jsonAddToCart.toString());

                            if (menuHandler.networkError()) {
                                Log.e("addToCartGuest: ", ConstantFields.getGuestCartItems(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                        ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")));
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.getGuestCartItems(ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                        ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")), jsonAddToCart,
                                        response -> {
                                            pb_ProductDetailLoader.setVisibility(View.GONE);

                                            Log.e("addToCartGuest: ", response.toString() + "");

                                            menuHandler.updateCart();

                                            if (productDetail.getTitle() != null) {
                                                showToast(getString(R.string.product_detail_add_to_cart_success_1) + " " + productDetail.getTitle()
                                                        + " " + getString(R.string.product_detail_add_to_cart_success_2));
                                            } else {
                                                showToast(getString(R.string.product_detail_add_to_cart_success));
                                            }
                                        }, error -> {
                                    pb_ProductDetailLoader.setVisibility(View.GONE);
                                    showToast(error.getMessage() + "");
                                    try {
                                        if (error.networkResponse != null) {
                                            String responseBody = new String(error.networkResponse.data, "utf-8");
                                            JSONObject jsonObject1 = new JSONObject(responseBody);
                                            Log.e("onErrorResponse: ", jsonObject1.toString());
                                            showToast(jsonObject1.getString("message") + "");
                                        } else {
                                            showToast(getString(R.string.common_error));
                                        }
                                    } catch (Exception e) {
                                        showToast(getString(R.string.common_error));
                                    }
                                }) {

                                    @Override
                                    public Map<String, String> getHeaders() {
                                        Map<String, String> params = new HashMap<>();
                                        params.put("Content-Type", "application/json; charset=utf-8");
                                        return params;
                                    }
                                };

                                jsonObjectRequest.setShouldCache(false);
                                ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "AddToCartGuest");
                            }

                        } catch (Exception e) {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                        }
                    } else {
                        showToast(getString(R.string.product_detail_option_error));
                    }
                } else {
                    showToast(getString(R.string.product_detail_quantity_error));
                }

            } catch (Exception e) {
                pb_ProductDetailLoader.setVisibility(View.GONE);
            }
        } else {
            showToast(getString(R.string.cart_out_of_stack_message));
        }
    }

    private void addToCartCustomer() {
        if (!btn_AddToCart.getText().toString().trim().toLowerCase().equals("out of stock!")) {
            try {
                if (qty > 0) {
                    if (checkOption()) {
                        try {
                            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
                            JSONObject jsonObject = new JSONObject();

                            jsonObject.put("quote_id", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag));
                            jsonObject.put("qty", String.valueOf(qty));
                            jsonObject.put("sku", productDetail.getSku());

                            if (productDetail.getProductType().toLowerCase().equals("simple")) {
                                if (productDetail.getOptionList() != null) {
                                    if (productDetail.getOptionList().size() > 0) {
                                        JSONArray selectedOptionList = new JSONArray();
                                        if (productDetail.isSubscribable()) {
                                            if (productDetail.getOptionList().size() == 1) {
                                                for (int j = 0; j < productDetail.getOptionList().get(0).getOptionValueList().size(); j++) {
                                                    if (productDetail.getOptionList().get(0).getOptionValueList().get(j).getOptionTitle().toLowerCase().trim().equals("one time")) {
                                                        JSONObject jsonValue = new JSONObject();
                                                        jsonValue.put("option_value", productDetail.getOptionList().get(0).getOptionValueList().get(j).getOptionId());
                                                        jsonValue.put("option_id", productDetail.getOptionList().get(0).getOptionId());
                                                        selectedOptionList.put(jsonValue);
                                                    }
                                                }
                                                JSONObject jsonOption = new JSONObject();
                                                JSONObject jsonObjectSubName = new JSONObject();
                                                jsonOption.put("extension_attributes", jsonObjectSubName);
                                                jsonObjectSubName.put("custom_options", selectedOptionList);
                                                jsonObject.put("product_option", jsonOption);
                                            }
                                        } else {
                                            for (int i = 0; i < productDetail.getOptionList().size(); i++) {
                                                for (int j = 0; j < productDetail.getOptionList().get(i).getOptionValueList().size(); j++) {
                                                    if (productDetail.getOptionList().get(i).getOptionValueList().get(j).isSelected()) {
                                                        JSONObject jsonValue = new JSONObject();
                                                        jsonValue.put("option_value", productDetail.getOptionList().get(i).getOptionValueList().get(j).getOptionId());
                                                        jsonValue.put("option_id", productDetail.getOptionList().get(i).getOptionId());
                                                        selectedOptionList.put(jsonValue);
                                                    }
                                                }
                                            }

                                            JSONObject jsonOption = new JSONObject();
                                            JSONObject jsonObjectSubName = new JSONObject();
                                            jsonOption.put("extension_attributes", jsonObjectSubName);
                                            jsonObjectSubName.put("custom_options", selectedOptionList);
                                            jsonObject.put("product_option", jsonOption);
                                        }
                                    }
                                }
                            }


                            JSONObject jsonAddToCart = new JSONObject();
                            jsonAddToCart.put("cart_item", jsonObject);

                            Log.e("addToCartCustomer: ", jsonAddToCart.toString());

                            Log.e("addToCartCustomer: ", ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")));

                            if (menuHandler.networkError()) {
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                                        ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")), jsonAddToCart,
                                        response -> {
                                            Log.e("addToCartCustomer: ", response.toString() + "");
                                            pb_ProductDetailLoader.setVisibility(View.GONE);

                                            if (ConstantDataParser.getTotalStatus(response)) {
                                                menuHandler.updateCart();

                                                if (productDetail.getTitle() != null) {
                                                    showToast(getString(R.string.product_detail_add_to_cart_success_1) + " " + productDetail.getTitle()
                                                            + " " + getString(R.string.product_detail_add_to_cart_success_2));
                                                } else {
                                                    showToast(getString(R.string.product_detail_add_to_cart_success));
                                                }
                                            }

                                        }, error -> {
                                    pb_ProductDetailLoader.setVisibility(View.GONE);

                                    try {
                                        if (error.networkResponse != null) {
                                            String responseBody = new String(error.networkResponse.data, "utf-8");
                                            JSONObject jsonObject1 = new JSONObject(responseBody);
                                            Log.e("onErrorResponse: ", jsonObject1.toString());
                                            showToast(jsonObject1.getString("message") + "");
                                        } else {
                                            showToast(getString(R.string.common_error));
                                        }
                                    } catch (Exception e) {
                                        showToast(getString(R.string.common_error));
                                    }
                                }) {

                                    @Override
                                    public Map<String, String> getHeaders() {
                                        Map<String, String> params = new HashMap<>();
                                        params.put("Content-Type", "application/json; charset=utf-8");
                                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(),
                                                ConstantFields.mTokenTag));
                                        return params;
                                    }
                                };

                                jsonObjectRequest.setShouldCache(false);
                                ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "AddToCart");
                            }

                        } catch (Exception e) {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                        }
                    } else {
                        showToast(getString(R.string.product_detail_option_error));
                    }
                } else {
                    showToast(getString(R.string.product_detail_quantity_error));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void customerDummyShippingAddress() {
        if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            try {
                final JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObject.put("address", jsonObjectAddress);
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));

                StringRequest stringRequestBillingAddress = new StringRequest(Request.Method.POST, ConstantFields.mCustomerBillingAddress,
                        response -> {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                            addToCartValidate();
                        }, error -> {
                    pb_ProductDetailLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequestBillingAddress.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequestBillingAddress, "CustomerBillingAddress");


            } catch (Exception e) {
                pb_ProductDetailLoader.setVisibility(View.GONE);
            }
        }
    }

    private void guestDummyShippingAddress() {
        /*if (menuHandler.networkError()) {
            pb_ProductDetailLoader.setVisibility(View.VISIBLE);
            try {
                final JSONObject jsonObject = new JSONObject();
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObject.put("address", jsonObjectAddress);
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mGuestCartIdTag));

                StringRequest stringRequestBillingAddress = new StringRequest(Request.Method.POST, ConstantFields.mCustomerBillingAddress,
                        response -> {
                            pb_ProductDetailLoader.setVisibility(View.GONE);
                            addToCartValidate();
                        }, error -> {
                    pb_ProductDetailLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                stringRequestBillingAddress.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequestBillingAddress, "CustomerBillingAddress");


            } catch (Exception e) {
                pb_ProductDetailLoader.setVisibility(View.GONE);
            }
        }*/
        addToCartValidate();
    }

    class QuantitySpinnerAdapter extends ArrayAdapter<Integer> {

        private ArrayList<Integer> quantityList;
        private Activity activity;

        QuantitySpinnerAdapter(@NonNull Activity activity, ArrayList<Integer> quantityList) {
            super(activity, android.R.layout.simple_spinner_item, quantityList);
            this.activity = activity;
            this.quantityList = quantityList;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View v_CountryDefault = convertView;
            if (v_CountryDefault == null) {
                LayoutInflater inflater = activity.getLayoutInflater();
                v_CountryDefault = inflater.inflate(R.layout.spinner_quantity_default, parent, false);
            }
            if (quantityList.get(position) != null) {
                AppCompatTextView atv_Quantity = v_CountryDefault.findViewById(R.id.atv_product_qty_default_holder);
                if (atv_Quantity != null) {
                    if (ConstantFields.currentLanguage().equals("ar")) {
                        atv_Quantity.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.ic_expand_more_black_24dp), null, null, null);
                    } else {
                        atv_Quantity.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(activity, R.drawable.ic_expand_more_black_24dp), null);
                    }
                    atv_Quantity.setText(String.valueOf(quantityList.get(position)));
                    fontSetup(atv_Quantity, Constant.LIGHT, Constant.C_TEXT_VIEW);
                }
            }
            return v_CountryDefault;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View v_CountryDropDown = convertView;
            if (v_CountryDropDown == null) {
                LayoutInflater inflater = activity.getLayoutInflater();
                v_CountryDropDown = inflater.inflate(R.layout.spinner_quantity_drop_down, parent, false);
            }

            if (quantityList.get(position) != null) {
                AppCompatTextView atv_StateName = v_CountryDropDown.findViewById(R.id.atv_product_qty_holder);
                int color = ContextCompat.getColor(activity, android.R.color.white);
                atv_StateName.setBackgroundColor(color);
                atv_StateName.setText(String.valueOf(quantityList.get(position)));
                fontSetup(atv_StateName, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
            return v_CountryDropDown;
        }
    }

    class RelatedProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelProductDetailList> productDetailLists = new ArrayList<>();

        RelatedProductListAdapter(ArrayList<ModelProductDetailList> productDetailLists) {
            this.productDetailLists = productDetailLists;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ProductListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_related_products, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            ProductListViewHolder productListViewHolder = (ProductListViewHolder) holder;

            if (productDetailLists != null) {
                if (productDetailLists.size() > 0) {

                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                        if (WishList.getInstance(activity.getApplicationContext()).getSizeWishList() > 0)
                            if (WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetailLists.get(position).getSku())) {
                                productListViewHolder.ib_ProductFavHome.setImageResource(R.drawable.ic_favorite_selected_24dp);
                            } else {
                                productListViewHolder.ib_ProductFavHome.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                            }
                    }

                    if (productDetailLists.get(position).getAttribute_result() != null) {
                        if (productDetailLists.get(position).getAttribute_result().size() > 0) {
                            for (int i = 0; i < productDetailLists.get(position).getAttribute_result().size(); i++) {
                                if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("small_image")) {
                                    ImageLoader.glide_image_loader(ConstantFields.ImageURL + productDetailLists.get(position).getAttribute_result().get(i)[1],
                                            productListViewHolder.iv_ProductImageHome, "Original");
                                }
                            }
                        }
                    }

                    if (productDetailLists.get(position).getAttribute_result() != null) {
                        if (productDetailLists.get(position).getAttribute_result().size() > 0) {
                            for (int i = 0; i < productDetailLists.get(position).getAttribute_result().size(); i++) {
                                if (productDetailLists.get(position).getAttribute_result().get(i)[0].trim().toLowerCase().equals("special_price")) {
                                    productDetailLists.get(position).setSpecial_price(Double.valueOf(productDetailLists.get(position).getAttribute_result().get(i)[1].trim()).intValue());
                                }

                                if (productDetailLists.get(position).getAttribute_result().get(i)[0] != null) {
                                    if (productDetailLists.get(position).getAttribute_result().get(i)[0].toLowerCase().equals("meta_title")) {
                                        String title = productDetailLists.get(position).getAttribute_result().get(i)[1];
                                        if (title.substring(0, 3).toLowerCase().equals("buy")) {
                                            title = title.substring(4);
                                        }
                                        productListViewHolder.atv_ProductTitleHome.setText(title);
                                    }
                                }
                            }

                            if (productListViewHolder.atv_ProductTitleHome.getText().toString().length() == 0) {
                                productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                            }

                        } else {
                            productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                        }
                    } else {
                        productListViewHolder.atv_ProductTitleHome.setText(productDetailLists.get(position).getName());
                    }

                    if (productDetailLists.get(position).getSpecial_price() != 0) {
                        productListViewHolder.atv_ProductPriceHome.setVisibility(View.VISIBLE);
                        productListViewHolder.atv_ProductPriceHome.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        productListViewHolder.atv_ProductPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getPrice()));
                        productListViewHolder.atv_ProductSpecialPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getSpecial_price()));
                    } else {
                        productListViewHolder.atv_ProductSpecialPriceHome.setText(String.valueOf("SAR " + productDetailLists.get(position).getPrice()));
                        productListViewHolder.atv_ProductPriceHome.setVisibility(View.GONE);
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            return productDetailLists.size();
        }

        private class ProductListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView iv_ProductImageHome;
            AppCompatTextView atv_ProductTitleHome, atv_ProductPriceHome, atv_ProductSpecialPriceHome;
            ImageButton ib_ProductFavHome;

            ProductListViewHolder(View view) {
                super(view);
                iv_ProductImageHome = view.findViewById(R.id.iv_product_image_product_list_home);
                atv_ProductTitleHome = view.findViewById(R.id.atv_product_title_product_list_home);
                atv_ProductPriceHome = view.findViewById(R.id.atv_product_price_product_list_home);
                atv_ProductSpecialPriceHome = view.findViewById(R.id.atv_product_special_price_product_list_home);
                ib_ProductFavHome = view.findViewById(R.id.ib_product_wish_list_product_list_home);
                ib_ProductFavHome.setOnClickListener(this);
                iv_ProductImageHome.setOnClickListener(this);
                atv_ProductTitleHome.setOnClickListener(this);
                atv_ProductPriceHome.setOnClickListener(this);
                atv_ProductSpecialPriceHome.setOnClickListener(this);

                fontSetup(atv_ProductTitleHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductPriceHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
                fontSetup(atv_ProductSpecialPriceHome, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }

            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.ib_product_wish_list_product_list_home:
                        if (getAdapterPosition() != -1)
                            addToWishList(ib_ProductFavHome, getAdapterPosition());
                        break;
                    case R.id.iv_product_image_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_title_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_price_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                    case R.id.atv_product_special_price_product_list_home:
                        if (getAdapterPosition() != -1)
                            toProductDetail(getAdapterPosition());
                        break;
                }
            }

            private void toProductDetail(int position) {
                menuHandler.toProductDetail(productDetailLists.get(position).getSku());
            }

            private void addToWishList(ImageButton ib_ProductFav, int position) {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                    if (!WishList.getInstance(activity.getApplicationContext()).checking_wish_list(productDetailLists.get(position).getSku())) {
                        WishList.getInstance(activity.getApplicationContext()).add_to_wish_list(productDetailLists.get(position).getSku(),
                                productDetailLists.get(position).getProductDetail());
                        ib_ProductFav.setImageResource(R.drawable.ic_favorite_selected_24dp);
                    } else {
                        WishList.getInstance(activity.getApplicationContext()).remove_from_wish_list(productDetailLists.get(position).getSku());
                        ib_ProductFav.setImageResource(R.drawable.ic_favorite_grey_500_24dp);
                    }
                } else {
                    menuHandler.toSignIn();
                }
            }
        }
    }

    class SimpleProductOption extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new CustomAttributeTitle(LayoutInflater.from(activity).inflate(R.layout.rc_row_config_option, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            CustomAttributeTitle customAttributeTitle = (CustomAttributeTitle) holder;
            customAttributeTitle.atv_OptionTitle.setText(productDetail.getOptionList().get(position).getOptionTitle());
            customAttributeTitle.rc_OptionValue.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            customAttributeTitle.rc_OptionValue.setAdapter(new RadioOptionList(productDetail.getOptionList().get(position).getOptionValueList()));

        }

        @Override
        public int getItemCount() {
            if (productDetail.getOptionList() != null) {
                if (productDetail.getOptionList().size() > 0) {
                    return productDetail.getOptionList().size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (productDetail.getOptionList() != null) {
                if (productDetail.getOptionList().size() > 0) {
                    return 2;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        class CustomAttributeTitle extends RecyclerView.ViewHolder {

            AppCompatTextView atv_OptionTitle;
            RecyclerView rc_OptionValue;

            CustomAttributeTitle(View itemView) {
                super(itemView);

                atv_OptionTitle = itemView.findViewById(R.id.atv_option_title);
                rc_OptionValue = itemView.findViewById(R.id.rc_option_value_list);
                fontSetup(atv_OptionTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
    }

    class RadioOptionList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        int previousPosition = -1;
        ArrayList<ModelProductDetailOptions> list = new ArrayList<>();

        RadioOptionList(ArrayList<ModelProductDetailOptions> list) {
            this.list = list;

        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new OptionValue(LayoutInflater.from(activity).inflate(R.layout.rc_row_option_value, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            final OptionValue optionValue = (OptionValue) holder;
            optionValue.atv_OptionValue.setText(list.get(position).getOptionTitle());

            optionValue.atv_OptionValue.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_btn_non_select));


            if (list.get(position).isSelected()) {
                optionValue.atv_OptionValue.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_btn_select));
                optionValue.atv_OptionValue.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
            } else {
                optionValue.atv_OptionValue.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_btn_non_select));
                optionValue.atv_OptionValue.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
            }


            optionValue.atv_OptionValue.setOnClickListener(v -> {
                for (int i = 0; i < productDetail.getOptionList().size(); i++) {
                    for (int j = 0; j < productDetail.getOptionList().get(i).getOptionValueList().size(); j++) {
                        if (productDetail.getOptionList().get(i).getOptionValueList().get(j).getOptionId() == list.get(position).getOptionId()) {
                            productDetail.getOptionList().get(i).getOptionValueList().get(j).setSelected(true);
                        } else {
                            productDetail.getOptionList().get(i).getOptionValueList().get(j).setSelected(false);
                        }
                    }
                }

                if (previousPosition == -1) {
                    list.get(position).setSelected(true);
                } else {
                    if (previousPosition != position) {
                        list.get(previousPosition).setSelected(false);
                        list.get(position).setSelected(true);
                    }
                }
                previousPosition = position;
                notifyDataSetChanged();
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class OptionValue extends RecyclerView.ViewHolder {

            AppCompatTextView atv_OptionValue;

            OptionValue(View itemView) {
                super(itemView);

                atv_OptionValue = itemView.findViewById(R.id.atv_option_value);
                fontSetup(atv_OptionValue, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
    }

    private void fontSetup() {
        fontSetup(atv_ProductTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ProductSpecialPrice, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_WriteReview, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_RelatedProductTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_ProductPrice, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_DescriptionTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);

        fontSetup(btn_AddToCart, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
