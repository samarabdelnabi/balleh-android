package com.balleh.fragments.product;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelReviewDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 6/30/2018.
 * Review List
 */

public class FragmentReviewList extends DialogFragment {

    private View v_ReviewList;
    private Activity activity;
    private int mProductId = 0;
    private MenuHandler menuHandler;
    private AppCompatTextView atv_ReviewsTitle;
    private Button btn_WriteReview;

    public FragmentReviewList() {
    }

    public static FragmentReviewList getInstance(int productId) {
        FragmentReviewList fragmentProductList = new FragmentReviewList();
        Bundle bundle = new Bundle();
        bundle.putInt("ProductId", productId);
        fragmentProductList.setArguments(bundle);
        return fragmentProductList;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getInt("ProductId") != 0) {
                mProductId = getArguments().getInt("ProductId");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_ReviewList = inflater.inflate(R.layout.fragment_review_list, container, false);
        load();
        return v_ReviewList;
    }

    private void load() {

        final RecyclerView rc_ReviewList = v_ReviewList.findViewById(R.id.rc_review_list);
        btn_WriteReview = v_ReviewList.findViewById(R.id.btn_write_review);
        atv_ReviewsTitle=v_ReviewList.findViewById(R.id.atv_title_review_list);
        rc_ReviewList.setLayoutManager(new LinearLayoutManager(activity));

        btn_WriteReview.setOnClickListener(v -> {
            DialogFragmentWriteReview dialogFragmentWriteReview = DialogFragmentWriteReview.getInstance(mProductId);
            dialogFragmentWriteReview.show(getChildFragmentManager(), "ReviewWrite");
        });

        if (menuHandler.networkError()) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getReviewList(mProductId, ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("onResponse: ", response + "");
                        ArrayList<ModelReviewDetails> reviewList = ConstantDataParser.getReviewList(response);

                        if (reviewList != null) {
                            if (reviewList.size() > 0) {
                                rc_ReviewList.setAdapter(new ReviewList(reviewList));
                            } else {
                                rc_ReviewList.setAdapter(new ReviewList(null));
                            }
                        } else {
                            rc_ReviewList.setAdapter(new ReviewList(null));
                        }

                    }, error -> {

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "
                            + ConstantDataSaver.mRetrieveSharedPreferenceString(activity,
                            ConstantFields.mTokenTag));
                    return params;
                }

            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GetCustomerReviewRatingTypes");
        }

        fontSetup();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);

    }


    private class ReviewList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<ModelReviewDetails> reviewList = new ArrayList<>();

        ReviewList(ArrayList<ModelReviewDetails> reviewList) {
            this.reviewList = reviewList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder viewHolder;

            if (viewType == 1) {
                viewHolder = new ReviewViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_review, parent, false));
            } else {
                viewHolder = new ReviewEmptyViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

            if (holder.getItemViewType() == 1) {
                ReviewViewHolder reviewViewHolder = (ReviewViewHolder) holder;

                reviewViewHolder.atv_Title.setText(reviewList.get(position).getTitle());
                reviewViewHolder.atv_Description.setText(reviewList.get(position).getDescription());

                String date;

                date = getString(R.string.review_by) + " " + reviewList.get(position).getName() + " "
                        + getString(R.string.review_on) + " " + reviewList.get(position).getDate();

                reviewViewHolder.atv_DateWithCustomer.setText(date);

                if (reviewList.get(position).getGetRatingList() != null) {
                    if (reviewList.get(position).getGetRatingList().size() > 0) {

                        for (int i = 0; i < reviewList.get(position).getGetRatingList().size(); i++) {
                            View v_Rating = LayoutInflater.from(activity).inflate(R.layout.rc_row_rating, null);

                            AppCompatTextView atv_RatingTitle = v_Rating.findViewById(R.id.atv_rating_title);
                            RatingBar rb_Rating = v_Rating.findViewById(R.id.rb_rating_small);
                            RatingBar rb_RatingBig = v_Rating.findViewById(R.id.rb_rating_big);

                            String[] list = reviewList.get(position).getGetRatingList().get(i);
                            rb_Rating.setNumStars(5);
                            rb_Rating.setRating(Float.valueOf(list[0]));
                            atv_RatingTitle.setText(list[1]);
                            rb_Rating.setVisibility(View.VISIBLE);
                            rb_RatingBig.setVisibility(View.GONE);

                            ((ReviewViewHolder) holder).ll_RatingHolder.addView(v_Rating);
                        }

                    }
                }
            } else {
                ReviewEmptyViewHolder reviewEmptyViewHolder = (ReviewEmptyViewHolder) holder;
                reviewEmptyViewHolder.atv_EmptyMessage.setText(activity.getString(R.string.hint_review_empty_list));
            }
        }

        @Override
        public int getItemCount() {
            if (reviewList != null) {
                if (reviewList.size() > 0) {
                    return reviewList.size();
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (reviewList != null) {
                if (reviewList.size() > 0) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 2;
            }
        }

        class ReviewViewHolder extends RecyclerView.ViewHolder {

            AppCompatTextView atv_Title, atv_Description, atv_DateWithCustomer;
            LinearLayout ll_RatingHolder;

            ReviewViewHolder(View itemView) {
                super(itemView);
                atv_Title = itemView.findViewById(R.id.atv_review_title);
                atv_Description = itemView.findViewById(R.id.atv_review_description);
                atv_DateWithCustomer = itemView.findViewById(R.id.atv_review_by);
                ll_RatingHolder = itemView.findViewById(R.id.ll_rating_list);

                fontSetup(atv_Title,Constant.LIGHT,1);
                fontSetup(atv_Description,Constant.LIGHT,1);
                fontSetup(atv_DateWithCustomer,Constant.LIGHT,1);
            }
        }

        class ReviewEmptyViewHolder extends RecyclerView.ViewHolder {

            AppCompatTextView atv_EmptyMessage;

            ReviewEmptyViewHolder(View itemView) {
                super(itemView);
                atv_EmptyMessage = itemView.findViewById(R.id.atv_empty_message);
                fontSetup(atv_EmptyMessage,Constant.LIGHT,1);
            }
        }
    }

    private void fontSetup() {
        fontSetup(atv_ReviewsTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(btn_WriteReview, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
