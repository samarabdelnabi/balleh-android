package com.balleh.fragments.product;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.adapter.ProductSubImageList;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.widgets_handler.ZoomImageView;

import java.util.ArrayList;

public class FragmentProductImageList extends Fragment {

    private ArrayList<String> mImageList;
    private Activity activity;
    private View v_FragmentProductImageListContainer;

    public FragmentProductImageList() {
    }

    public static FragmentProductImageList getInstance(ArrayList<String> productDetail) {
        FragmentProductImageList fragmentProductImageList = new FragmentProductImageList();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("ImageList", productDetail);
        fragmentProductImageList.setArguments(bundle);
        return fragmentProductImageList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mImageList = getArguments().getStringArrayList("ImageList");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v_FragmentProductImageListContainer = inflater.inflate(R.layout.fragment_product_image_list, container, false);
        setting();
        return v_FragmentProductImageListContainer;
    }


    public void setting() {
        ZoomImageView ziv_ProductZoomImage = v_FragmentProductImageListContainer.findViewById(R.id.ziv_product_zoom_image);
        RecyclerView rc_ImageSubList = v_FragmentProductImageListContainer.findViewById(R.id.rc_product_sub_image_list);

        if (mImageList != null) {
            if (mImageList.size() > 0) {
                if (mImageList.get(0) != null) {
                    image_caller(ziv_ProductZoomImage, ConstantFields.ImageURL + mImageList.get(0));
                    rc_ImageSubList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                    rc_ImageSubList.setAdapter(new ProductSubImageList(activity, mImageList, ziv_ProductZoomImage));
                }
            }
        }
    }

    public void image_caller(ZoomImageView imageView, String url) {
        imageView.reset();
        imageView.setMaxZoom(3.0f);
        ImageLoader.glide_image_loader(url, imageView, "Original");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
    }

}
