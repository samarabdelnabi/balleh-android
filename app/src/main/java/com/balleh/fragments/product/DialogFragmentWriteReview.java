package com.balleh.fragments.product;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelRating;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 7/5/2018.
 * Review Write
 */

public class DialogFragmentWriteReview extends DialogFragment {

    private View v_WriteReview;
    private Activity activity;
    private String mProductId = "0";
    private ArrayList<ModelRating> modelRatingList = new ArrayList<>();
    private MenuHandler menuHandler;
    private AppCompatTextView atv_ReviewTitle, atv_NickNameTitle,atv_NickNameError, atv_SummaryTitle,
            atv_SummaryError,atv_DescriptionTitle, atv_DescriptionError;
    private EditText etNickName, etSummary, etReview;
    private Button btnSubmit;

    public DialogFragmentWriteReview() {
    }

    public static DialogFragmentWriteReview getInstance(int productId) {
        DialogFragmentWriteReview fragmentProductList = new DialogFragmentWriteReview();
        Bundle bundle = new Bundle();
        bundle.putString("product_id", String.valueOf(productId));
        fragmentProductList.setArguments(bundle);
        return fragmentProductList;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getString("product_id") != null) {
                mProductId = getArguments().getString("product_id");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v_WriteReview = inflater.inflate(R.layout.dialog_fragment_write_review, container, false);
        load();
        return v_WriteReview;
    }

    private void load() {
        atv_ReviewTitle = v_WriteReview.findViewById(R.id.atv_review_title);
        final RecyclerView rc_TypeRatingHolder = v_WriteReview.findViewById(R.id.rc_type_list_holder);
        btnSubmit = v_WriteReview.findViewById(R.id.btn_submit_review);

        etNickName = v_WriteReview.findViewById(R.id.et_nick_name);
        etSummary = v_WriteReview.findViewById(R.id.et_summary);
        etReview = v_WriteReview.findViewById(R.id.et_review_description);

        atv_NickNameError = v_WriteReview.findViewById(R.id.atv_nick_name_error);
        atv_SummaryError = v_WriteReview.findViewById(R.id.atv_summary_error);
        atv_DescriptionError = v_WriteReview.findViewById(R.id.atv_comments_error);

        atv_NickNameTitle = v_WriteReview.findViewById(R.id.atv_name_title_write_review);
        atv_SummaryTitle = v_WriteReview.findViewById(R.id.atv_summary_title_review);
        atv_DescriptionTitle = v_WriteReview.findViewById(R.id.atv_commands_review);

        rc_TypeRatingHolder.setLayoutManager(new LinearLayoutManager(activity));

        if (menuHandler.networkError()) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getReviewType(Integer.valueOf(mProductId), ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        modelRatingList = ConstantDataParser.getRatingList(response);
                        if (modelRatingList != null) {
                            if (modelRatingList.size() > 0) {
                                rc_TypeRatingHolder.setAdapter(new RatingListAdapter());
                            }
                        }
                    }, error -> {

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "
                            + ConstantDataSaver.mRetrieveSharedPreferenceString(activity,
                            ConstantFields.mTokenTag));
                    return params;
                }

            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GetCustomerReviewRatingTypes");
        }

        v_WriteReview.findViewById(R.id.btn_cancel_review).setOnClickListener(v -> dismiss());

        btnSubmit.setOnClickListener(v -> {

            if (etNickName.getText().toString().length() > 0
                    && etSummary.getText().toString().length() > 0
                    && etReview.getText().toString().length() > 0
                    && checkRatingStatus()) {

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("productId", mProductId);
                    jsonObject.put("nickname", etNickName.getText().toString());
                    jsonObject.put("atv_OptionTitle", etSummary.getText().toString());
                    jsonObject.put("detail", etReview.getText().toString());

                    JSONArray jsonArray = new JSONArray();

                    if (modelRatingList != null) {
                        if (modelRatingList.size() > 0) {
                            for (int i = 0; i < modelRatingList.size(); i++) {
                                JSONObject jsonRating = new JSONObject();
                                jsonRating.put("rating_id", modelRatingList.get(i).getRating_id());
                                jsonRating.put("ratingCode", modelRatingList.get(i).getTitle());
                                jsonRating.put("ratingValue", modelRatingList.get(i).getSelected_rating());
                                jsonArray.put(i, jsonRating);
                            }
                            jsonObject.put("storeId", modelRatingList.get(0).getStore_id() + "");
                        } else {
                            jsonObject.put("storeId", ConstantDataParser.getStoreId() + "");
                        }
                    } else {
                        jsonObject.put("storeId", ConstantDataParser.getStoreId() + "");
                    }
                    jsonObject.put("ratingData", jsonArray);


                    if (menuHandler.networkError()) {
                        StringRequest stringRequestReviewWrite = new StringRequest(Request.Method.POST, ConstantFields.CustomerWriteReview,
                                response -> {
                                    String message;
                                    try {
                                        JSONArray jsonResponseArray = new JSONArray(response);
                                        JSONObject reviewResponse = jsonResponseArray.getJSONObject(0);
                                        message = reviewResponse.getString("message") + "";
                                    } catch (Exception e) {
                                        message = getString(R.string.success);
                                    }
                                    ConstantFields.CustomToast(message, activity);
                                    dismiss();
                                }, error -> {
                            ConstantFields.CustomToast(getString(R.string.failed), activity);
                            dismiss();
                        }) {
                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> params = new HashMap<>();
                                params.put("Content-Type", "application/json; charset=utf-8");

                                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(activity, ConstantFields.mTokenTag).equals("Empty")) {
                                    params.put("Authorization", "Bearer "
                                            + ConstantDataSaver.mRetrieveSharedPreferenceString(activity,
                                            ConstantFields.mTokenTag));
                                } else {
                                    params.put("Authorization", "Bearer " + ConstantFields.getGuestToken());
                                }

                                return params;
                            }

                        };

                        stringRequestReviewWrite.setShouldCache(false);
                        ApplicationContext.getInstance().addToRequestQueue(stringRequestReviewWrite, "CustomerWriteReview");
                    }

                } catch (Exception e) {
                    dismiss();
                }

            }

            if (etNickName.getText().toString().length() == 0) {
                atv_NickNameError.setVisibility(View.VISIBLE);
            } else {
                atv_NickNameError.setVisibility(View.GONE);
            }

            if (etSummary.getText().toString().length() == 0) {
                atv_SummaryError.setVisibility(View.VISIBLE);
            } else {
                atv_SummaryError.setVisibility(View.GONE);
            }

            if (etReview.getText().toString().length() == 0) {
                atv_DescriptionError.setVisibility(View.VISIBLE);
            } else {
                atv_DescriptionError.setVisibility(View.GONE);
            }

            if (!checkRatingStatus()) {
                ConstantFields.CustomToast(getString(R.string.review_product_rating_error), activity);
            }
        });

        fontSetup();
    }

    private boolean checkRatingStatus() {
        if (modelRatingList != null) {
            if (modelRatingList.size() > 0) {
                for (int i = 0; i < modelRatingList.size(); i++) {
                    if (!modelRatingList.get(i).isRated()) {
                        return false;
                    }
                }
                return true;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        int width = getResources().getDimensionPixelSize(R.dimen._300sdp);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;

        if (getDialog().getWindow() != null)
            getDialog().getWindow().setLayout(width, height);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(AppLanguageSupport.onAttach(context));
        activity = (Activity) context;
        menuHandler = (MenuHandler) context;
    }

    private class RatingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RatingViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_rating, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            RatingViewHolder ratingViewHolder = (RatingViewHolder) holder;
            ratingViewHolder.atv_RatingTitle.setText(modelRatingList.get(position).getTitle());

            ratingViewHolder.rb_RatingBig.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
                modelRatingList.get(position).setSelected_rating((int) rating);
                modelRatingList.get(position).setRated(true);
            });
        }

        @Override
        public int getItemCount() {
            return modelRatingList.size();
        }

        class RatingViewHolder extends RecyclerView.ViewHolder {

            AppCompatTextView atv_RatingTitle;
            RatingBar rb_Rating, rb_RatingBig;

            RatingViewHolder(View itemView) {
                super(itemView);
                atv_RatingTitle = itemView.findViewById(R.id.atv_rating_title);
                rb_Rating = itemView.findViewById(R.id.rb_rating_small);
                rb_RatingBig = itemView.findViewById(R.id.rb_rating_big);

                rb_Rating.setVisibility(View.GONE);
                rb_RatingBig.setVisibility(View.VISIBLE);

                fontSetup(atv_RatingTitle, Constant.LIGHT, 1);
            }
        }

    }

    private void fontSetup() {
        fontSetup(atv_ReviewTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_NickNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_SummaryError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_DescriptionError, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_NickNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_SummaryTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
        fontSetup(atv_DescriptionTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

        fontSetup(etNickName, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(etSummary, Constant.LIGHT, Constant.C_EDIT_TEXT);
        fontSetup(etReview, Constant.LIGHT, Constant.C_EDIT_TEXT);

        fontSetup(btnSubmit, Constant.LIGHT, Constant.C_BUTTON);
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}