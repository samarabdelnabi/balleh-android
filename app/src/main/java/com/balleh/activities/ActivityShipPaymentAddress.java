package com.balleh.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.AddressListSpinnerAdapter;
import com.balleh.adapter.SpinnerCountryAdapter;
import com.balleh.adapter.SpinnerStateAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.widgets_handler.CustomSupportMapFragment;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelCountry;
import com.balleh.model.ModelState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityShipPaymentAddress extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private LinearLayout ll_PaymentAddressHolder;
    private FrameLayout ll_NewAddressHolder;
    private Spinner s_ShippingList, s_PaymentList;
    private Button btn_ShippingNewAddress;
    private RadioGroup rg_AddressList;
    private boolean isNewAddressVisible = false, isAddressListSelected = false, isNewAddressSelected = false;
    private EditText et_FirstName, et_LastName, et_Email, et_Mobile, et_Street, et_Area,/* et_City,*/
            et_ZipCode;
    private Spinner s_City, s_State, s_Country;
    private AppCompatTextView atv_FirstNameError, atv_LastNameError, atv_Email_title, atv_EmailError1, atv_EmailError2, atv_MobileError, atv_StreetError,
            atv_CityError, atv_ZipCodeError, atv_StateError, atv_CountryError, atv_ToShippingMethod;
    private CheckBox cb_SaveToAddressBook;
    private String CountryId, StateCode, onMapSelectedCountry, CityName = "Empty", cityNameMap = "Empty";
    private int StateId;
    private ArrayList<ModelCountry> CountryList;
    private GoogleMap mMap;
    private Geocoder geocoder;
    private Location location;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private int USER_LOCATION_PERMISSION_CODE = 41;
    private ProgressBar pb_ShippingAddress;
    private ModelAccountDetail modelAccountDetail = new ModelAccountDetail();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_payment_address);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        CheckBox cb_SameAsShipping = findViewById(R.id.cb_same_as_shipping_address);

        s_PaymentList = findViewById(R.id.s_payment_address_list);
        s_ShippingList = findViewById(R.id.s_shipping_address_list);
        ll_PaymentAddressHolder = findViewById(R.id.ll_payment_address_container);
        ll_NewAddressHolder = findViewById(R.id.ll_new_address_container);
        rg_AddressList = findViewById(R.id.rg_address_list);
        btn_ShippingNewAddress = findViewById(R.id.btn_shipping_new_address);
        atv_ToShippingMethod = findViewById(R.id.btn_to_shipping_method);

        et_FirstName = findViewById(R.id.et_check_out_first_name);
        et_LastName = findViewById(R.id.et_check_out_last_name);
        et_Email = findViewById(R.id.et_check_out_email);
        et_Mobile = findViewById(R.id.et_check_out_mobile_no);
        et_Street = findViewById(R.id.et_check_out_street);
        et_Area = findViewById(R.id.et_check_out_area);
        //et_City = findViewById(R.id.et_check_out_city);
        et_ZipCode = findViewById(R.id.et_check_out_zip_code);
        s_City = findViewById(R.id.s_check_out_city);
        s_State = findViewById(R.id.s_check_out_state);
        s_Country = findViewById(R.id.s_check_out_country);
        pb_ShippingAddress = findViewById(R.id.pb_shipping_address);

        atv_FirstNameError = findViewById(R.id.atv_check_out_error_first_name);
        atv_LastNameError = findViewById(R.id.tv_check_out_error_last_name);
        atv_Email_title = findViewById(R.id.atv_check_out_email_title);
        atv_EmailError1 = findViewById(R.id.atv_check_out_email_error_1);
        atv_EmailError2 = findViewById(R.id.atv_check_out_email_error_2);
        atv_MobileError = findViewById(R.id.atv_check_out_error_mobile);
        atv_StreetError = findViewById(R.id.tv_check_out_error_street);
        atv_CityError = findViewById(R.id.atv_check_out_error_city);
        atv_ZipCodeError = findViewById(R.id.atv_check_out_error_zip_code);
        atv_StateError = findViewById(R.id.atv_check_out_error_state);
        atv_CountryError = findViewById(R.id.atv_check_out_error_country);

        cb_SaveToAddressBook = findViewById(R.id.cb_add_to_address_book);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        CustomSupportMapFragment customSupportMapFragment = (CustomSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        customSupportMapFragment.getMapAsync(this);

        customSupportMapFragment.setListener(() -> {
            NestedScrollView nsv_ParentScroller = findViewById(R.id.nsv_parent_scroller);
            nsv_ParentScroller.requestDisallowInterceptTouchEvent(true);
        });

        if (ConstantFields.isNetworkAvailable()) {
            getCountry();
        } else {
            networkError();
        }

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(),
                ConstantFields.mTokenTag).equals("Empty")) {
            cb_SaveToAddressBook.setVisibility(View.VISIBLE);
            loadAccountInformation();
        } else {
            isAddressListSelected = false;
            isNewAddressSelected = true;
            ll_NewAddressHolder.setVisibility(View.VISIBLE);
            rg_AddressList.setVisibility(View.GONE);
            btn_ShippingNewAddress.setVisibility(View.GONE);
            findViewById(R.id.atv_shipping_or).setVisibility(View.GONE);
            cb_SaveToAddressBook.setVisibility(View.GONE);
            guestShippingButtonManagement();
        }

        cb_SameAsShipping.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked()) {
                ll_PaymentAddressHolder.setVisibility(View.GONE);
            } else {
                ll_PaymentAddressHolder.setVisibility(View.VISIBLE);
            }
        });

        geocoder = new Geocoder(ActivityShipPaymentAddress.this);

        checkGpsLocationEnable();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            getDeviceLocation();
        }

        if (requestCode == 5000) {
            finish();
        }
    }

    private void checkGpsLocationEnable() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        result.addOnCompleteListener(task -> {
            try {
                task.getResult(ApiException.class);
            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            resolvable.startResolutionForResult(ActivityShipPaymentAddress.this, 100);
                        } catch (IntentSender.SendIntentException e) {
                            Log.d("test", e.getMessage());
                        } catch (ClassCastException e) {
                            Log.d("test", e.getMessage());
                        }
                        break;
                }
            }
        });

    }

    private void networkError() {
        Intent intent = new Intent(ActivityShipPaymentAddress.this, ActivityError.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void getDeviceLocation() {

        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    location = task.getResult();
                    moveMarker(location);
                }
            });

        } catch (SecurityException e) {
            Log.e("getDeviceLocation: ", e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnCameraIdleListener(() -> getAddress(mMap.getCameraPosition().target));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            askPermission();
        } else {
            mMap.setMyLocationEnabled(true);
            loadLocationManagement();
        }
    }

    private void loadLocationManagement() {
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        getDeviceLocation();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGpsLocationEnable();
    }

    private void getCountry() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.mCountry,
                response -> {
                    CountryList = ConstantDataParser.getCountryList(response);

                    if (CountryList != null) {
                        if (CountryList.size() > 0) {
                            SpinnerCountryAdapter countryAdapter = new SpinnerCountryAdapter(ActivityShipPaymentAddress.this, CountryList);
                            s_Country.setAdapter(countryAdapter);

                            for (int i = 0; i < CountryList.size(); i++) {
                                if (CountryList.get(i).getId().toLowerCase().equals("sa")) {
                                    s_Country.setSelection(i);
                                }
                            }

                            s_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int positionParent, long l) {

                                    if (CountryList.get(positionParent).getId() != null) {
                                        if (!CountryList.get(positionParent).getId().equals("-1")) {

                                            CountryId = CountryList.get(positionParent).getId();

                                            onMapSelectedCountry = "FromCountry";

                                            if (CountryList.get(positionParent).getStateList() != null) {

                                                if (CountryList.get(positionParent).getStateList().size() > 0) {

                                                    final ArrayList<ModelState> stateSpinnerList = CountryList.get(positionParent).getStateList();
                                                    SpinnerStateAdapter adapterState = new SpinnerStateAdapter(ActivityShipPaymentAddress.this, stateSpinnerList);
                                                    adapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                    s_State.getBaseline();
                                                    s_State.setAdapter(adapterState);

                                                    s_State.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                                        public void onItemSelected(AdapterView<?> parent, View view, int positionChild, long id) {
                                                            if (stateSpinnerList.get(positionChild).getId() != null) {
                                                                StateCode = stateSpinnerList.get(positionChild).getCode();
                                                                StateId = Integer.valueOf(stateSpinnerList.get(positionChild).getId());

                                                                if (ConstantFields.isNetworkAvailable()) {
                                                                    pb_ShippingAddress.setVisibility(View.VISIBLE);
                                                                    StringRequest stringCityRequest = new StringRequest(Request.Method.GET, ConstantFields.getCityList(CountryId, String.valueOf(StateId)),
                                                                            responseCity -> {
                                                                                pb_ShippingAddress.setVisibility(View.GONE);
                                                                                ArrayList<String> cityList;

                                                                                if (ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this, "setDefault") != null) {
                                                                                    if (ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this, "setDefault").equals("true")) {
                                                                                        cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                    } else {
                                                                                        cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                    }
                                                                                } else {
                                                                                    cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                }

                                                                                if (cityList != null) {
                                                                                    if (cityList.size() > 0) {

                                                                                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                                                                                                (ActivityShipPaymentAddress.this, android.R.layout.simple_spinner_item, cityList);
                                                                                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                                                                                .simple_spinner_dropdown_item);

                                                                                        s_City.setAdapter(spinnerArrayAdapter);
                                                                                        s_City.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                                            @Override
                                                                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                                                                CityName = cityList.get(position);
                                                                                            }

                                                                                            @Override
                                                                                            public void onNothingSelected(AdapterView<?> parent) {

                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }

                                                                            },
                                                                            error -> {
                                                                                pb_ShippingAddress.setVisibility(View.GONE);
                                                                            }) {

                                                                        @Override
                                                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                                                            Map<String, String> params = new HashMap<>();
                                                                            params.put("Content-Type", "application/json; charset=utf-8");
                                                                            return params;
                                                                        }
                                                                    };
                                                                    stringCityRequest.setShouldCache(false);
                                                                    ApplicationContext.getInstance().addToRequestQueue(stringCityRequest, "City");
                                                                } else {
                                                                    networkError();
                                                                }


                                                            }
                                                        }

                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> arg0) {
                                                        }

                                                    });
                                                } else {
                                                    List<String> noStates = new ArrayList<>();
                                                    noStates.add("--- None ---");

                                                    ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(ActivityShipPaymentAddress.this, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                    s_State.setAdapter(adapter_StateN);
                                                    StateCode = "0";
                                                    StateId = 0;
                                                }
                                            } else {
                                                List<String> noStates = new ArrayList<>();
                                                noStates.add("--- None ---");

                                                ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(ActivityShipPaymentAddress.this, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                s_State.setAdapter(adapter_StateN);
                                                StateCode = "0";
                                                StateId = 0;
                                            }
                                        } else {
                                            List<String> noStates = new ArrayList<>();
                                            noStates.add("--- Please select country first ---");

                                            ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(ActivityShipPaymentAddress.this, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                            s_State.setAdapter(adapter_StateN);
                                        }
                                    } else {

                                        List<String> noStates = new ArrayList<>();
                                        noStates.add("--- Please select country first ---");

                                        ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(ActivityShipPaymentAddress.this, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                        s_State.setAdapter(adapter_StateN);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }
                    }
                }, error -> {
            try {
                if (error.networkResponse != null) {
                    String responseBody = new String(error.networkResponse.data, "utf-8");
                    JSONObject jsonObject = new JSONObject(responseBody);
                    showToast(jsonObject.getString("message"));
                } else {
                    showToast(getString(R.string.user_error_invalid_user_detail));
                }
            } catch (Exception e) {
                showToast(getString(R.string.user_error_invalid_user_detail));
            }
        })

        {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "Country");

    }

    private void loadAccountInformation() {
        if (ConstantFields.isNetworkAvailable()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response + "");
                            modelAccountDetail = ConstantDataParser.getAccountDetail(response);
                            btnSetup();
                        }, error -> {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(), ConstantFields.mTokenTag);
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
            }
        } else {
            networkError();
        }
    }

    private void btnSetup() {

        if (modelAccountDetail != null) {
            if (modelAccountDetail.getAddressList() != null) {
                if (modelAccountDetail.getAddressList().size() > 0) {

                    et_Email.setVisibility(View.GONE);
                    atv_Email_title.setVisibility(View.GONE);
                    atv_EmailError1.setVisibility(View.GONE);
                    atv_EmailError2.setVisibility(View.GONE);

                    s_ShippingList.setAdapter(new AddressListSpinnerAdapter(ActivityShipPaymentAddress.this,
                            android.R.layout.simple_spinner_item, modelAccountDetail.getAddressList()));

                    s_PaymentList.setAdapter(new AddressListSpinnerAdapter(ActivityShipPaymentAddress.this,
                            android.R.layout.simple_spinner_item, modelAccountDetail.getAddressList()));

                    for (int i = 0; i < modelAccountDetail.getAddressList().size(); i++) {
                        RadioButton radioButton = new RadioButton(ActivityShipPaymentAddress.this);
                        radioButton.setId(modelAccountDetail.getAddressList().get(i).getAddressId() + i);
                        radioButton.setText(modelAccountDetail.getAddressList().get(i).getAddress());
                        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(0, 20, 0, 0);
                        rg_AddressList.addView(radioButton, layoutParams);
                    }

                    btn_ShippingNewAddress.setOnClickListener(view -> {

                        isAddressListSelected = false;
                        isNewAddressSelected = true;

                        if (!isNewAddressVisible) {
                            btn_ShippingNewAddress.setText(getString(R.string.address_select_from_list));
                            findViewById(R.id.atv_shipping_or).setVisibility(View.GONE);
                            ll_NewAddressHolder.setVisibility(View.VISIBLE);
                            rg_AddressList.setVisibility(View.GONE);
                            isNewAddressVisible = true;
                        } else {
                            btn_ShippingNewAddress.setText(getString(R.string.address_new_address));
                            findViewById(R.id.atv_shipping_or).setVisibility(View.VISIBLE);
                            ll_NewAddressHolder.setVisibility(View.GONE);
                            rg_AddressList.setVisibility(View.VISIBLE);
                            isNewAddressVisible = false;
                        }
                    });

                    atv_ToShippingMethod.setOnClickListener(view -> {

                        int idAddress = rg_AddressList.getCheckedRadioButtonId();
                        for (int i = 0; i < modelAccountDetail.getAddressList().size(); i++) {
                            if (idAddress == modelAccountDetail.getAddressList().get(i).getAddressId() + i) {
                                isAddressListSelected = true;
                                isNewAddressSelected = false;
                                modelAccountDetail.getAddressList().get(i).setSelected(true);
                            }
                        }

                        if (isNewAddressSelected) {

                            et_Email.setVisibility(View.GONE);
                            atv_Email_title.setVisibility(View.GONE);
                            atv_EmailError1.setVisibility(View.GONE);
                            atv_EmailError2.setVisibility(View.GONE);

                            if (et_FirstName.getText().toString().length() > 0
                                    && et_LastName.getText().toString().length() > 0
                                    && et_Mobile.getText().toString().length() > 0

                                    && et_Street.getText().toString().length() > 0
                                    && !CityName.equals("Empty")

                                    && et_ZipCode.getText().toString().length() > 0) {

                                ModelAddress modelAddress = new ModelAddress();

                                modelAddress.setFirstName(et_FirstName.getText().toString());
                                modelAddress.setLastName(et_LastName.getText().toString());
                                modelAddress.setTelephone(et_Mobile.getText().toString());

                                String streetList;

                                if (et_Area.getText().toString().length() > 0) {
                                    streetList = et_Street.getText().toString();
                                    streetList = streetList + "," + et_Area.getText().toString();
                                } else {
                                    streetList = et_Street.getText().toString();
                                }

                                modelAddress.setStreet(streetList);

                                modelAddress.setCity(CityName);
                                modelAddress.setPost_code(et_ZipCode.getText().toString());
                                modelAddress.setCountryId(CountryId);
                                modelAddress.setRegionCode(StateCode);
                                modelAddress.setRegionId(String.valueOf(StateId));


                                if (cb_SaveToAddressBook.isChecked()) {
                                    modelAddress.setSaveAddressBook(1);
                                } else {
                                    modelAddress.setSaveAddressBook(0);
                                }

                                if (CheckOut.getInstance(ActivityShipPaymentAddress.this).getSizeAccount() > 0) {
                                    CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                            .updateAccountDetail(modelAccountDetail.getEmailId(), modelAddress);
                                } else {
                                    CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                            .insertAccountDetail(modelAccountDetail.getEmailId(), modelAddress);
                                }

                                if (cb_SaveToAddressBook.isChecked()) {
                                    try {
                                        if (modelAddress.getSaveAddressBook() == 1) {
                                            saveToAddressBook(modelAccountDetail, true);
                                        } else {
                                            updateShippingAddress("Empty", modelAddress);
                                        }
                                    } catch (Exception e) {
                                        updateShippingAddress("Empty", modelAddress);
                                    }

                                } else {
                                    updateShippingAddress("Empty", modelAddress);
                                }
                            }

                            if (et_FirstName.getText().toString().length() < 0) {
                                atv_FirstNameError.setVisibility(View.VISIBLE);
                            } else {
                                atv_FirstNameError.setVisibility(View.GONE);
                            }
                            if (et_LastName.getText().toString().length() < 0) {
                                atv_LastNameError.setVisibility(View.VISIBLE);
                            } else {
                                atv_LastNameError.setVisibility(View.GONE);
                            }
                            if (et_Mobile.getText().toString().length() < 0) {
                                atv_MobileError.setVisibility(View.VISIBLE);
                            } else {
                                atv_MobileError.setVisibility(View.GONE);
                            }

                            if (et_Street.getText().toString().length() < 0) {
                                atv_StreetError.setVisibility(View.VISIBLE);
                            } else {
                                atv_StreetError.setVisibility(View.GONE);
                            }
                            if (CityName.equals("Empty")) {
                                atv_CityError.setVisibility(View.VISIBLE);
                            } else {
                                atv_CityError.setVisibility(View.GONE);
                            }

                            if (et_ZipCode.getText().toString().length() < 0) {
                                atv_ZipCodeError.setVisibility(View.VISIBLE);
                            } else {
                                atv_ZipCodeError.setVisibility(View.GONE);
                            }

                        } else {
                            if (isAddressListSelected) {
                                int id = rg_AddressList.getCheckedRadioButtonId();
                                ModelAddress modelAddress = new ModelAddress();

                                for (int i = 0; i < modelAccountDetail.getAddressList().size(); i++) {
                                    if (id == modelAccountDetail.getAddressList().get(i).getAddressId()) {
                                        modelAddress = modelAccountDetail.getAddressList().get(i);
                                        if (modelAddress != null)
                                            modelAddress.setSaveAddressBook(0);
                                    }
                                }

                                if (modelAddress != null) {
                                    if (modelAddress.getAddress() != null) {
                                        if (CheckOut.getInstance(ActivityShipPaymentAddress.this).getSizeAccount() > 0) {
                                            CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                                    .updateAccountDetail(modelAccountDetail.getEmailId(), modelAddress);
                                        } else {
                                            CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                                    .insertAccountDetail(modelAccountDetail.getEmailId(), modelAddress);
                                        }
                                    }
                                }
                                updateShippingAddress("Empty", modelAddress);
                            } else {
                                showToast(getString(R.string.select_any_address));
                            }
                        }
                    });

                } else {
                    isNewAddressSelected = true;
                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                        customerNewAddressFiled();
                    } else {
                        guestShippingButtonManagement();
                    }

                    hideAddressList();
                }
            } else {
                isNewAddressSelected = true;
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                    customerNewAddressFiled();
                } else {
                    guestShippingButtonManagement();
                }
                hideAddressList();
            }
        } else {
            isNewAddressSelected = true;
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                customerNewAddressFiled();
            } else {
                guestShippingButtonManagement();
            }
            hideAddressList();
        }
    }

    private boolean checkCountryAvailability(String countryCode) {
        if (CountryList != null) {
            if (CountryList.size() > 0) {
                for (int i = 0; i < CountryList.size(); i++) {
                    if (countryCode != null) {
                        if (countryCode.equals("FromCountry")) {
                            return true;
                        }
                        if (countryCode.equals(CountryList.get(i).getCode())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private void guestShippingButtonManagement() {

        atv_ToShippingMethod.setOnClickListener(view -> {
            if (isNewAddressSelected) {
                if (et_FirstName.getText().toString().length() > 0
                        && et_LastName.getText().toString().length() > 0
                        && et_Mobile.getText().toString().length() > 0

                        && (et_Email.getText().toString().length() > 0
                        && ConstantFields.isEmailValidator(et_Email.getText().toString()))

                        && et_Street.getText().toString().length() > 0
                        && !CityName.equals("Empty")
                        && et_ZipCode.getText().toString().length() > 0

                        && CountryId != null
                        && CountryId.length() > 0

                        && StateCode != null
                        && StateCode.length() > 0

                        && checkCountryAvailability(onMapSelectedCountry)) {

                    ModelAddress modelAddress = new ModelAddress();

                    modelAddress.setFirstName(et_FirstName.getText().toString());
                    modelAddress.setLastName(et_LastName.getText().toString());
                    modelAddress.setTelephone(et_Mobile.getText().toString());

                    String streetList;

                    if (et_Area.getText().toString().length() > 0) {
                        streetList = et_Street.getText().toString();
                        streetList = streetList + "," + et_Area.getText().toString();
                    } else {
                        streetList = et_Street.getText().toString();
                    }

                    modelAddress.setStreet(streetList);

                    modelAddress.setCity(CityName);
                    modelAddress.setPost_code(et_ZipCode.getText().toString());
                    modelAddress.setCountryId(CountryId);
                    modelAddress.setRegionCode(StateCode);
                    modelAddress.setRegionId(String.valueOf(StateId));
                    modelAddress.setCustomerId(0);
                    modelAddress.setEmail_id(et_Email.getText().toString());

                    if (CheckOut.getInstance(ActivityShipPaymentAddress.this).getSizeAccount() > 0) {
                        CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                .updateAccountDetail(et_Email.getText().toString(), modelAddress);
                    } else {
                        CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                .insertAccountDetail(et_Email.getText().toString(), modelAddress);
                    }
                    updateShippingAddress("Empty", modelAddress);
                }

                if (et_FirstName.getText().toString().equals("")) {
                    atv_FirstNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_FirstNameError.setVisibility(View.GONE);
                }
                if (et_LastName.getText().toString().equals("")) {
                    atv_LastNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_LastNameError.setVisibility(View.GONE);
                }
                if (et_Mobile.getText().toString().equals("")) {
                    atv_MobileError.setVisibility(View.VISIBLE);
                } else {
                    atv_MobileError.setVisibility(View.GONE);
                }
                if (et_Email.getText().toString().equals("")) {
                    atv_EmailError1.setVisibility(View.VISIBLE);
                } else {
                    atv_EmailError1.setVisibility(View.GONE);
                    if (!ConstantFields.isEmailValidator(et_Email.getText().toString())) {
                        atv_EmailError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_EmailError2.setVisibility(View.GONE);
                    }
                }
                if (et_Street.getText().toString().equals("")) {
                    atv_StreetError.setVisibility(View.VISIBLE);
                } else {
                    atv_StreetError.setVisibility(View.GONE);
                }
                if (CityName.equals("Empty")) {
                    atv_CityError.setVisibility(View.VISIBLE);
                } else {
                    atv_CityError.setVisibility(View.GONE);
                }

                if (et_ZipCode.getText().toString().equals("")) {
                    atv_ZipCodeError.setVisibility(View.VISIBLE);
                } else {
                    atv_ZipCodeError.setVisibility(View.GONE);
                }

                if (CountryId != null) {
                    if (CountryId.length() > 0) {
                        atv_CountryError.setVisibility(View.GONE);
                    } else {
                        atv_CountryError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_CountryError.setVisibility(View.VISIBLE);
                }

                if (StateCode != null) {
                    if (StateCode.length() > 0) {
                        atv_StateError.setVisibility(View.GONE);
                    } else {
                        atv_StateError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_StateError.setVisibility(View.VISIBLE);
                }

                if (!checkCountryAvailability(onMapSelectedCountry)) {
                    showToast(getString(R.string.address_selection_error));
                }
            }
        });
    }

    private void customerNewAddressFiled() {
        atv_ToShippingMethod.setOnClickListener(view -> {
            if (isNewAddressSelected) {
                if (et_FirstName.getText().toString().length() > 0
                        && et_LastName.getText().toString().length() > 0
                        && et_Mobile.getText().toString().length() > 0

                        && (et_Email.getText().toString().length() > 0
                        && ConstantFields.isEmailValidator(et_Email.getText().toString()))

                        && et_Street.getText().toString().length() > 0
                        && !CityName.equals("Empty")
                        && et_ZipCode.getText().toString().length() > 0

                        && CountryId != null
                        && CountryId.length() > 0

                        && StateCode != null
                        && StateCode.length() > 0

                        && checkCountryAvailability(onMapSelectedCountry)) {

                    ModelAddress modelAddress = new ModelAddress();
                    modelAddress.setFirstName(et_FirstName.getText().toString());
                    modelAddress.setLastName(et_LastName.getText().toString());
                    modelAddress.setTelephone(et_Mobile.getText().toString());
                    String streetList;
                    if (et_Area.getText().toString().length() > 0) {
                        streetList = et_Street.getText().toString();
                        streetList = streetList + "," + et_Area.getText().toString();
                    } else {
                        streetList = et_Street.getText().toString();
                    }
                    modelAddress.setEmail_id(et_Email.getText().toString());
                    modelAddress.setStreet(streetList);
                    modelAddress.setCity(CityName);
                    modelAddress.setPost_code(et_ZipCode.getText().toString());
                    modelAddress.setCountryId(CountryId);
                    modelAddress.setRegionCode(StateCode);
                    modelAddress.setRegionId(String.valueOf(StateId));
                    if (cb_SaveToAddressBook.isChecked()) {
                        modelAddress.setSaveAddressBook(1);
                        modelAddress.setBillingAddress(true);
                        modelAddress.setShippingAddress(true);
                    } else {
                        modelAddress.setSaveAddressBook(0);
                        modelAddress.setBillingAddress(false);
                        modelAddress.setShippingAddress(false);
                    }
                    ArrayList<ModelAddress> addressList = new ArrayList<>();
                    addressList.add(modelAddress);
                    ModelAccountDetail modelAccountDetail = new ModelAccountDetail();
                    modelAccountDetail.setAddressList(addressList);


                    if (CheckOut.getInstance(ActivityShipPaymentAddress.this).getSizeAccount() > 0) {
                        CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                .updateAccountDetail(et_Email.getText().toString(), modelAddress);
                    } else {
                        CheckOut.getInstance(ActivityShipPaymentAddress.this)
                                .insertAccountDetail(et_Email.getText().toString(), modelAddress);
                    }

                    saveToAddressBook(modelAccountDetail, false);

                }

                if (et_FirstName.getText().toString().equals("")) {
                    atv_FirstNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_FirstNameError.setVisibility(View.GONE);
                }
                if (et_LastName.getText().toString().equals("")) {
                    atv_LastNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_LastNameError.setVisibility(View.GONE);
                }
                if (et_Mobile.getText().toString().equals("")) {
                    atv_MobileError.setVisibility(View.VISIBLE);
                } else {
                    atv_MobileError.setVisibility(View.GONE);
                }
                if (et_Email.getText().toString().equals("")) {
                    atv_EmailError1.setVisibility(View.VISIBLE);
                } else {
                    atv_EmailError1.setVisibility(View.GONE);
                    if (!ConstantFields.isEmailValidator(et_Email.getText().toString())) {
                        atv_EmailError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_EmailError2.setVisibility(View.GONE);
                    }
                }
                if (et_Street.getText().toString().equals("")) {
                    atv_StreetError.setVisibility(View.VISIBLE);
                } else {
                    atv_StreetError.setVisibility(View.GONE);
                }
                if (CityName.equals("Empty")) {
                    atv_CityError.setVisibility(View.VISIBLE);
                } else {
                    atv_CityError.setVisibility(View.GONE);
                }

                if (et_ZipCode.getText().toString().equals("")) {
                    atv_ZipCodeError.setVisibility(View.VISIBLE);
                } else {
                    atv_ZipCodeError.setVisibility(View.GONE);
                }

                if (CountryId != null) {
                    if (CountryId.length() > 0) {
                        atv_CountryError.setVisibility(View.GONE);
                    } else {
                        atv_CountryError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_CountryError.setVisibility(View.VISIBLE);
                }

                if (StateCode != null) {
                    if (StateCode.length() > 0) {
                        atv_StateError.setVisibility(View.GONE);
                    } else {
                        atv_StateError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_StateError.setVisibility(View.VISIBLE);
                }

                if (!checkCountryAvailability(onMapSelectedCountry)) {
                    showToast(getString(R.string.address_selection_error));
                }
            }
        });

    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, ActivityShipPaymentAddress.this);
    }

    private void updateShippingAddress(String saveAddressDetail, ModelAddress modelAddress) {

        toShipping(saveAddressDetail);

        /*if (ConstantFields.isNetworkAvailable()) {
            pb_ShippingAddress.setVisibility(View.VISIBLE);
            try {
                final JSONObject jsonObject = new JSONObject();

                jsonObject.put("useForShipping", true);

                JSONArray jsonStreetArrayDefault = new JSONArray();
                jsonStreetArrayDefault.put(modelAddress.getStreet());

                JSONObject jsonAddress = new JSONObject();
                jsonAddress.put("firstname", modelAddress.getFirstName());
                jsonAddress.put("lastname", modelAddress.getLastName());
               // jsonAddress.put("id", modelAddress.getAddressId());
              //  jsonAddress.put("telephone", modelAddress.getTelephone());
                jsonAddress.put("street", jsonStreetArrayDefault);
                jsonAddress.put("city", modelAddress.getCity());

               *//* jsonAddress.put("region_id", modelAddress.getRegionId());
                jsonAddress.put("region", modelAddress.getRegionCode());
                jsonAddress.put("region_code", modelAddress.getRegionCode());*//*

                jsonAddress.put("region_id",95);
                jsonAddress.put("region", "SA");
                jsonAddress.put("region_code", "W");

                jsonAddress.put("country_id", modelAddress.getCountryId());
                jsonAddress.put("postcode", modelAddress.getPost_code());
                jsonAddress.put("same_as_billing", 1);
                jsonAddress.put("email", modelAccountDetail.getEmailId());
                jsonObject.put("address", jsonAddress);
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this, ConstantFields.mTokenTag));
                Log.e( "updateShippingAddress: ", jsonObject.toString());

                Log.e( "updateShippingAddress: ", ConstantFields.mCustomerBillingAddress);
                StringRequest stringRequestBillingAddress = new StringRequest(Request.Method.POST, ConstantFields.mCustomerBillingAddress,
                        response -> {
                            Log.e( "updateShippingAddress: ",response+"" );
                            pb_ShippingAddress.setVisibility(View.GONE);
                            toShipping(saveAddressDetail);
                        }, error -> {
                    pb_ShippingAddress.setVisibility(View.GONE);

                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject1 = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject1.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {

                    @Override
                    public byte[] getBody() {
                        return jsonObject.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityShipPaymentAddress.this, ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequestBillingAddress.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequestBillingAddress, "CustomerBillingAddress");


            } catch (Exception e) {
                showToast(getString(R.string.common_error));
            }
        }*/


    }

    private void hideAddressList() {
        rg_AddressList.setVisibility(View.GONE);
        findViewById(R.id.atv_shipping_or).setVisibility(View.GONE);
        findViewById(R.id.atv_payment_or).setVisibility(View.GONE);
        s_ShippingList.setVisibility(View.GONE);
        s_PaymentList.setVisibility(View.GONE);
        btn_ShippingNewAddress.setVisibility(View.GONE);
        ll_NewAddressHolder.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        moveMarker(location);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void askPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle(R.string.app_name)
                        .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(ActivityShipPaymentAddress.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    USER_LOCATION_PERMISSION_CODE);
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        USER_LOCATION_PERMISSION_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 41:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadLocationManagement();
                } else {
                    askPermission();
                }
                break;
        }
    }

    private void moveMarker(Location location) {
        if (location != null) {
            LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(ll, 15.00f);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
            mMap.animateCamera(cameraUpdate);
            getAddress(ll);
        }
    }

    private void getAddress(LatLng ll) {
        try {
            List<Address> addresses;
            addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1);
            if (addresses.size() > 0) {
                onMapSelectedCountry = addresses.get(0).getCountryCode();
                /*if (addresses.get(0).getLocality() != null) {
                    et_City.setText(addresses.get(0).getLocality());
                }*/

                cityNameMap = addresses.get(0).getLocality() + "";

                if (addresses.get(0).getFeatureName() != null && addresses.get(0).getThoroughfare() != null) {
                    String street = addresses.get(0).getFeatureName() + "," + addresses.get(0).getThoroughfare();
                    et_Street.setText(street);
                } else if (addresses.get(0).getFeatureName() != null && addresses.get(0).getThoroughfare() == null) {
                    et_Street.setText(addresses.get(0).getFeatureName());
                } else if (addresses.get(0).getFeatureName() == null && addresses.get(0).getThoroughfare() != null) {
                    et_Street.setText(addresses.get(0).getThoroughfare());
                }

                if (addresses.get(0).getPostalCode() != null) {
                    et_ZipCode.setText(addresses.get(0).getPostalCode());
                }
            }
        } catch (Exception e) {
            return;
        }
    }

    private void saveToAddressBook(ModelAccountDetail accountDetail, boolean type) {
        try {
            JSONArray jsonArray = new JSONArray();
            if (type) {
                if (accountDetail.getAddressList() != null)
                    if (accountDetail.getAddressList().size() > 0)
                        for (int i = 0; i < accountDetail.getAddressList().size(); i++) {

                            JSONArray jsonStreetArrayDefault = new JSONArray();
                            jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                            JSONObject jsonAddress = new JSONObject();
                            jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                            jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                            jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                            jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                            jsonAddress.put("street", jsonStreetArrayDefault);
                            jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                            jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                            jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                            jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                            jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());
                            if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                jsonAddress.put("default_billing", true);
                            } else {
                                jsonAddress.put("default_billing", false);
                            }
                            if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                                jsonAddress.put("default_shipping", true);
                            } else {
                                jsonAddress.put("default_shipping", false);
                            }
                            jsonArray.put(jsonAddress);
                        }

                JSONArray jsonStreetArrayNew = new JSONArray();
                jsonStreetArrayNew.put(et_Street.getText().toString());

                JSONObject jsonNewAddress = new JSONObject();
                jsonNewAddress.put("firstname", et_FirstName.getText().toString());
                jsonNewAddress.put("lastname", et_LastName.getText().toString());
                jsonNewAddress.put("telephone", et_Mobile.getText().toString());
                jsonNewAddress.put("street", jsonStreetArrayNew);
                jsonNewAddress.put("city", CityName);
                jsonNewAddress.put("email", et_Email.getText().toString());
                jsonNewAddress.put("region_id", StateId);
                jsonNewAddress.put("region", StateCode);
                jsonNewAddress.put("country_id", CountryId);
                jsonNewAddress.put("postcode", et_ZipCode.getText().toString());
                jsonNewAddress.put("default_billing", false);
                jsonNewAddress.put("default_shipping", false);
                jsonArray.put(jsonNewAddress);

                JSONObject jsonObjectCustomer = new JSONObject();
                jsonObjectCustomer.put("firstname", accountDetail.getFirstName());
                jsonObjectCustomer.put("lastname", accountDetail.getLastName());
                jsonObjectCustomer.put("email", accountDetail.getEmailId());
                jsonObjectCustomer.put("websiteId", 0);
                jsonObjectCustomer.put("addresses", jsonArray);

                JSONObject jsonFinalObject = new JSONObject();
                jsonFinalObject.put("customer", jsonObjectCustomer);


                ModelAddress modelAddress = new ModelAddress();

                modelAddress.setFirstName(et_FirstName.getText().toString());
                modelAddress.setLastName(et_LastName.getText().toString());
                modelAddress.setTelephone(et_Mobile.getText().toString());

                String streetList;

                if (et_Area.getText().toString().length() > 0) {
                    streetList = et_Street.getText().toString();
                    streetList = streetList + "," + et_Area.getText().toString();
                } else {
                    streetList = et_Street.getText().toString();
                }

                modelAddress.setStreet(streetList);

                modelAddress.setCity(CityName);
                modelAddress.setPost_code(et_ZipCode.getText().toString());
                modelAddress.setCountryId(CountryId);
                modelAddress.setRegionCode(StateCode);
                modelAddress.setRegionId(String.valueOf(StateId));
                modelAddress.setCustomerId(0);
                modelAddress.setEmail_id(et_Email.getText().toString());

                updateShippingAddress(jsonFinalObject.toString(), modelAddress);

            } else {
                if (accountDetail.getAddressList() != null) {
                    if (accountDetail.getAddressList().size() > 0) {
                        ModelAddress modelAddress = new ModelAddress();
                        for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                            JSONArray jsonStreetArrayDefault = new JSONArray();
                            jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                            JSONObject jsonAddress = new JSONObject();
                            jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                            jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                            jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                            jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                            jsonAddress.put("street", jsonStreetArrayDefault);
                            jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                            jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                            jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                            jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                            jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());
                            jsonAddress.put("default_billing", true);
                            jsonAddress.put("default_shipping", true);

                            jsonArray.put(jsonAddress);

                            if (accountDetail.getAddressList().get(i).isSelected()) {
                                modelAddress = accountDetail.getAddressList().get(i);
                            }
                        }

                        JSONObject jsonObjectCustomer = new JSONObject();
                        jsonObjectCustomer.put("firstname", accountDetail.getAddressList().get(0).getFirstName());
                        jsonObjectCustomer.put("lastname", accountDetail.getAddressList().get(0).getLastName());
                        jsonObjectCustomer.put("email", accountDetail.getAddressList().get(0).getEmail_id());
                        jsonObjectCustomer.put("websiteId", 0);
                        jsonObjectCustomer.put("addresses", jsonArray);

                        modelAddress.setEmail_id(accountDetail.getAddressList().get(0).getEmail_id());

                        JSONObject jsonFinalObject = new JSONObject();
                        jsonFinalObject.put("customer", jsonObjectCustomer);

                        updateShippingAddress(jsonFinalObject.toString(), modelAddress);

                    }
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityShipPaymentAddress.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }

    private void toShipping(String saveAddressDetail) {
        Log.e("updateShippingAddress: ", saveAddressDetail + "");
        Intent toShippingMethod = new Intent(ActivityShipPaymentAddress.this, ActivityCheckOut.class);
        toShippingMethod.putExtra("Type", "2");
        toShippingMethod.putExtra("addressDetail", saveAddressDetail);
        startActivityForResult(toShippingMethod, 5000);
    }

}
