package com.balleh.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.activities.user.ActivityLogin;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.fragments.DialogNetworkError;
import com.balleh.fragments.account.DialogAddress;
import com.balleh.fragments.account.FragmentAddressBook;
import com.balleh.fragments.account.FragmentOrderHistory;
import com.balleh.fragments.product.FragmentProductDetails;
import com.balleh.fragments.product.FragmentProductImageList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ActivityError extends AppCompatActivity implements MenuHandler {

    private int mCartCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        Toolbar toolbar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String loadType = bundle.getString("LoadType");
            if (loadType != null) {
                if (loadType.length() > 0) {
                    switch (loadType) {
                        case "NetWorkError":
                            OpenNetworkErrorWindow(getSupportFragmentManager());
                            break;
                        case "ProductDetail":
                            String sku = bundle.getString("ProductSKU");
                            if (sku != null) {
                                if (sku.length() > 0) {
                                    openProductDetail(sku);
                                } else {
                                    close();
                                }
                            } else {
                                close();
                            }
                            break;
                        case "LoadAddressBook":
                            loadAddressBook();
                            break;
                        case "LoadOrderList":
                            loadOrderList();
                            break;
                        default:
                            close();
                            break;
                    }
                } else {
                    close();
                }
            } else {
                close();
            }
        } else {
            close();
        }
    }

    private void close() {
        ConstantFields.CustomToast(getString(R.string.common_error), ActivityError.this);
        finish();
    }

    private void openProductDetail(String sku) {
        FragmentProductDetails fragmentProductDetails = FragmentProductDetails.getInstance(sku);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_extra_fragment_container, fragmentProductDetails, "ProductDetail");
        fragmentTransaction.addToBackStack("ProductDetail");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }


    public static void OpenNetworkErrorWindow(FragmentManager fragmentManager) {
        DialogNetworkError dialogNetworkError = new DialogNetworkError();
        dialogNetworkError.show(fragmentManager, "NetworkError");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityError.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void changeLang(String toWhich) {

    }

    @Override
    public void toHome() {

    }

    @Override
    public void toSignIn() {
        toUser();
    }

    @Override
    public void toSignUp() {

    }

    @Override
    public void toDeals() {

    }

    @Override
    public void toProductList(int categoryId, String type, String filter) {

    }

    @Override
    public void loadFilter(int categoryId) {

    }

    @Override
    public void toLiveChat() {

    }

    @Override
    public void toTrackOrder() {

    }

    @Override
    public void toShare() {

    }

    @Override
    public void toProductDetail(String sku) {
        openProductDetail(sku);
    }

    @Override
    public void toProductImageList(ArrayList<String> imageList) {
        FragmentProductImageList fragmentProductImageList = FragmentProductImageList.getInstance(imageList);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_extra_fragment_container, fragmentProductImageList, "ProductImageList");
        fragmentTransaction.addToBackStack("ProductImageList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void updateCart() {
        updateCartCount();
    }

    @Override
    public void loadEditAccount() {

    }

    @Override
    public void loadChangePassword() {

    }

    @Override
    public void loadAddressBook() {
        FragmentAddressBook fragmentAddressBook = new FragmentAddressBook();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_extra_fragment_container, fragmentAddressBook, "AddressBook");
        fragmentTransaction.addToBackStack("AddressBook");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void reloadAddressBook() {
        onBackPressed();
        loadAddressBook();
    }

    @Override
    public void loadAddress(String accountDetail, int id, String type) {
        DialogAddress dialogAddress = new DialogAddress();
        dialogAddress.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogAddress.setCancelable(true);
        Bundle bundle = new Bundle();
        bundle.putString("CustomerDetail", accountDetail);
        bundle.putString("AddressType", type);
        bundle.putInt("AddressId", id);
        dialogAddress.setArguments(bundle);
        dialogAddress.show(getSupportFragmentManager(), "Address");
    }

    @Override
    public void loadOrderList() {
        FragmentOrderHistory fragmentOrderHistory = new FragmentOrderHistory();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_extra_fragment_container, fragmentOrderHistory, "OrderHistory");
        fragmentTransaction.addToBackStack("OrderHistory");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadSingleOrderHistory(String orderDetail) {

    }

    @Override
    public void loadDownloadableProduct() {

    }

    @Override
    public void loadAboutUs() {

    }

    @Override
    public void loadContactUs() {

    }

    @Override
    public void loadLogout() {

    }

    @Override
    public void backBtnPressed() {
        onBackPressed();
    }

    @Override
    public boolean networkError() {
        if (ConstantFields.isNetworkAvailable()) {
            return true;
        } else {
            Intent intent = new Intent(ActivityError.this, ActivityError.class);
            intent.putExtra("LoadType", "NetWorkError");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return false;
        }
    }

    @Override
    public void visibility() {

    }

    @Override
    public void changeBottomView(int type) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCartCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);
        update(menu);
        return true;
    }

    public void update(Menu menu) {
        View view = menu.findItem(R.id.menu_cart).getActionView();
        ImageView iv_ShoppingCartIcon = view.findViewById(R.id.iv_shopping_cart_icon);
        TextView tv_ShoppingCartCount = view.findViewById(R.id.tv_shopping_cart_count);
        String tempCartCount = String.valueOf(mCartCount > 0 ? mCartCount : 0);
        tv_ShoppingCartCount.setOnClickListener(view1 -> loadCartDetail());
        iv_ShoppingCartIcon.setOnClickListener(view12 -> loadCartDetail());
        if (!tempCartCount.equals("0")) {
            tv_ShoppingCartCount.setText(tempCartCount);
        } else {
            tv_ShoppingCartCount.setVisibility(View.INVISIBLE);
        }
    }

    private void loadCartDetail() {
        Intent toCart = new Intent(ActivityError.this, ActivityCheckOut.class);
        toCart.putExtra("Type", "1");
        startActivity(toCart);
    }

    private void updateCartCount() {
        if (networkError())
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(),
                    ConstantFields.mTokenTag).equals("Empty")) {

                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response);

                            mCartCount = ConstantDataParser.getCartCount(response);
                            invalidateOptionsMenu();
                        }, error -> {
                    if (error.networkResponse != null)
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mTokenTag);
                        } else if (error.networkResponse.statusCode == 404) {
                            CreateCartId();
                        }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartList");


            } else {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getGuestCartItems(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                Log.e("onResponse: ", response);
                                mCartCount = ConstantDataParser.getCartCount(response);
                                invalidateOptionsMenu();
                            }, error -> {
                        if (error.networkResponse != null)
                            if (error.networkResponse.statusCode == 401) {
                                ConstantDataSaver.mRemoveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mTokenTag);
                            }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartList");
                } else {
                    mCartCount = 0;
                }
            }
        invalidateOptionsMenu();
    }

    private void CreateCartId() {
        if (networkError())
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response + "");
                            ConstantDataSaver.mStoreSharedPreferenceString(ActivityError.this.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag, String.valueOf(ConstantDataParser.getCartId(response)));
                        }, error -> {
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityError.this.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityError.this.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartIdHome");

            }
    }

    private void toUser() {
        Intent toUser = new Intent(ActivityError.this, ActivityLogin.class);
        toUser.putExtra("WhatToLoad", "Login");
        startActivity(toUser);
    }
}
