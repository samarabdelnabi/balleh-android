package com.balleh.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.activities.user.ActivityLogin;
import com.balleh.activities.user.ActivitySignUp;
import com.balleh.adapter.HomeLeftSideMenuAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.WishList;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.additional.widgets_handler.BottomNavigationViewHelper;
import com.balleh.fragments.account.DialogAddress;
import com.balleh.fragments.account.DialogChangePassword;
import com.balleh.fragments.account.DialogWebUrlLoader;
import com.balleh.fragments.account.FragmentEditAccount;
import com.balleh.fragments.account.FragmentSingleOrderHistoryDetail;
import com.balleh.fragments.home.FragmentAccount;
import com.balleh.fragments.home.FragmentHome;
import com.balleh.fragments.home.FragmentSearch;
import com.balleh.fragments.home.FragmentWishList;
import com.balleh.fragments.product.DialogFragmentFilterList;
import com.balleh.fragments.product.FragmentProductDetails;
import com.balleh.fragments.product.FragmentProductImageList;
import com.balleh.fragments.product.FragmentProductList;
import com.balleh.model.ModelHomeMenuCategoryList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.intercom.android.sdk.Intercom;

public class ActivityHome extends AppCompatActivity implements MenuHandler {

    private RecyclerView rc_HomeMenu;
    private DrawerLayout drawer;
    private MenuHandler menuHandler;
    private int mDealsId = 0, mCartCount = 0;
    private BottomNavigationView bottomMenu;
    private boolean isHomePageVisible = false;
    private HomeLeftSideMenuAdapter homeLeftSideMenuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        menuHandler = this;

        Toolbar tb_HomeMainBar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(tb_HomeMainBar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        View appLogo = tb_HomeMainBar.getChildAt(0);

        appLogo.setOnClickListener(view -> loadHome());

        drawer = findViewById(R.id.dl_menu_drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, tb_HomeMainBar, R.string.home_ls_menu_drawer_open, R.string.home_ls_menu_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        bottomMenu = findViewById(R.id.bnv_home_bottom_menu);
        BottomNavigationViewHelper.removeShiftMode(bottomMenu, ActivityHome.this);

        rc_HomeMenu = findViewById(R.id.rc_home_menu_list);
        setBottomNavigationBar();

        rc_HomeMenu.setLayoutManager(new LinearLayoutManager(ActivityHome.this));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String mCategoryList = bundle.getString("Category");
            if (mCategoryList != null) {
                if (mCategoryList.length() > 0) {
                    setNavigationBar(ConstantDataParser.getCategoryList(mCategoryList));
                    mDealsId = ConstantDataParser.getDealId(mCategoryList);
                } else {
                    setNavigationBar(null);
                }
            } else {
                setNavigationBar(null);
            }
        } else {
            setNavigationBar(null);
        }

        loadHome();
    }

    private void setNavigationBar(ArrayList<ModelHomeMenuCategoryList> list) {
        homeLeftSideMenuAdapter = new HomeLeftSideMenuAdapter(ActivityHome.this, list, menuHandler);
        rc_HomeMenu.setAdapter(homeLeftSideMenuAdapter);
    }

    private void loadHome() {
        validateVisibility(1);
        if (!isHomePageVisible) {
            isHomePageVisible = true;
            FragmentHome fragmentHome = new FragmentHome();
            FragmentTransaction fragmentTransactionHome = getSupportFragmentManager().beginTransaction();
            fragmentTransactionHome.replace(R.id.ll_home_fragment_container, fragmentHome, "HomePage");
            fragmentTransactionHome.addToBackStack("HomePage");
            fragmentTransactionHome.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransactionHome.commit();
        }
    }

    private void validateVisibility(int type) {
        if (type == 1) {
            bottomMenu.setVisibility(View.VISIBLE);
        } else {
            bottomMenu.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);
        update(menu);
        return true;
    }

    public void update(Menu menu) {
        View view = menu.findItem(R.id.menu_cart).getActionView();
        ImageView iv_ShoppingCartIcon = view.findViewById(R.id.iv_shopping_cart_icon);
        TextView tv_ShoppingCartCount = view.findViewById(R.id.tv_shopping_cart_count);
        String tempCartCount = String.valueOf(mCartCount > 0 ? mCartCount : 0);
        tv_ShoppingCartCount.setOnClickListener(view1 -> loadCartDetail());
        iv_ShoppingCartIcon.setOnClickListener(view12 -> loadCartDetail());
        if (!tempCartCount.equals("0")) {
            tv_ShoppingCartCount.setText(tempCartCount);
        } else {
            tv_ShoppingCartCount.setVisibility(View.INVISIBLE);
        }
        if (homeLeftSideMenuAdapter != null) {
            homeLeftSideMenuAdapter.notifyDataSetChanged();
        }
    }

    private void loadCartDetail() {
        Intent toCart = new Intent(ActivityHome.this, ActivityCheckOut.class);
        toCart.putExtra("Type", "1");
        startActivity(toCart);
    }

    private void setBottomNavigationBar() {

        bottomMenu.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bnb_home:
                    item.setChecked(true);
                    loadHome();
                    break;
                case R.id.bnb_search:
                    isHomePageVisible = false;
                    item.setChecked(true);
                    FragmentSearch fragmentSearch = new FragmentSearch();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentSearch, "SearchProductList");
                    fragmentTransaction.addToBackStack("SearchProductList");
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    fragmentTransaction.commit();
                    break;
                case R.id.bnb_wish_list:
                    isHomePageVisible = false;
                    item.setChecked(true);
                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                        loadWishList();
                    } else {
                        toUser("Login");
                    }
                    break;
                case R.id.bnb_deals:
                    isHomePageVisible = false;
                    item.setChecked(true);
                    toProductList(mDealsId, "default-deals", "");
                    break;
                case R.id.bnb_account:
                    isHomePageVisible = false;
                    item.setChecked(true);
                    if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                        loadAccountMenu();
                    } else {
                        toUser("Login");
                    }
                    break;
            }
            return false;
        });
    }

    private void loadAccountMenu() {
        validateVisibility(1);
        FragmentAccount fragmentAccount = new FragmentAccount();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentAccount, "AccountMenu");
        fragmentTransaction.addToBackStack("AccountMenu");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    private void loadWishList() {
        validateVisibility(1);
        FragmentWishList fragmentWishList = new FragmentWishList();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentWishList, "WishList");
        fragmentTransaction.addToBackStack("WishList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityHome.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCartCount();
    }

    private void updateCartCount() {
        if (networkError())
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(),
                    ConstantFields.mTokenTag).equals("Empty")) {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCartProductList(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response);

                            mCartCount = ConstantDataParser.getCartCount(response);
                            invalidateOptionsMenu();
                        }, error -> {
                    if (error.networkResponse != null)
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag);
                        } else if (error.networkResponse.statusCode == 404) {
                            CreateCartId();
                        }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartList");


            } else {
                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getGuestCartItems(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")),
                            response -> {
                                Log.e("onResponse: ", response);
                                mCartCount = ConstantDataParser.getCartCount(response);
                                invalidateOptionsMenu();
                            }, error -> {
                        if (error.networkResponse != null)
                            if (error.networkResponse.statusCode == 401) {
                                ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag);
                            }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    stringRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(stringRequest, "GuestCartList");
                } else {
                    mCartCount = 0;
                }
            }
        invalidateOptionsMenu();
    }

    private void CreateCartId() {
        if (networkError())
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCustomerCartId(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            Log.e("onResponse: ", response + "");
                            ConstantDataSaver.mStoreSharedPreferenceString(ActivityHome.this.getApplicationContext(),
                                    ConstantFields.mCustomerCartIdTag, String.valueOf(ConstantDataParser.getCartId(response)));
                        }, error -> {
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartIdHome");

            }
    }

    @Override
    public void changeLang(String toWhich) {
        if (toWhich.equals("en")) {
            if (!ConstantFields.currentLanguage().equals("en")) {
                AppLanguageSupport.setLocale(ActivityHome.this, "en");
                ConstantDataSaver.mStoreSharedPreferenceString(ApplicationContext.getApplicationInstance(), "setDefault", "false");
                loadAgain();
            } else {
                toHome();
            }
        } else {
            if (!ConstantFields.currentLanguage().equals("ar")) {
                AppLanguageSupport.setLocale(ActivityHome.this, "ar");
                ConstantDataSaver.mStoreSharedPreferenceString(ApplicationContext.getApplicationInstance(), "setDefault", "true");
                loadAgain();
            } else {
                toHome();
            }
        }
    }

    @Override
    public void toHome() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            loadHome();
        }
        closeDrawer();
    }

    @Override
    public void toSignIn() {
        toUser("Login");
    }

    @Override
    public void toSignUp() {
        toUser("Register");
    }

    @Override
    public void toDeals() {
        closeDrawer();
    }

    @Override
    public void toProductList(int categoryId, String type, String filter) {
        validateVisibility(1);
        isHomePageVisible = false;
        closeDrawer();
        FragmentProductList fragmentProductList = FragmentProductList.getInstance(categoryId, type, filter);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentProductList, "ProductList");
        fragmentTransaction.addToBackStack("ProductList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadFilter(int categoryId) {
        DialogFragmentFilterList dialogFragmentFilterList = DialogFragmentFilterList.getInstance(categoryId);
        dialogFragmentFilterList.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogFragmentFilterList.setCancelable(false);
        dialogFragmentFilterList.show(getSupportFragmentManager(), "FilterList");
    }

    @Override
    public void toLiveChat() {
        closeDrawer();
        Intercom.client().displayMessenger();
    }

    @Override
    public void toTrackOrder() {
        closeDrawer();
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
            loadOrderList();
        } else {
            toUser("Login");
        }
    }

    @Override
    public void toShare() {
        closeDrawer();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Testing");
        startActivity(Intent.createChooser(intent, "Share"));
    }

    @Override
    public void toProductDetail(String sku) {
        /*validateVisibility(2);
        isHomePageVisible = false;
        closeDrawer();
        FragmentProductDetails fragmentProductDetails = FragmentProductDetails.getInstance(sku);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentProductDetails, "ProductDetail");
        fragmentTransaction.addToBackStack("ProductDetail");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();*/

        Intent intent = new Intent(ActivityHome.this, ActivityError.class);
        intent.putExtra("ProductSKU", sku);
        intent.putExtra("LoadType", "ProductDetail");
        startActivity(intent);
    }

    @Override
    public void toProductImageList(ArrayList<String> imageList) {
        validateVisibility(2);
        isHomePageVisible = false;
        closeDrawer();
        FragmentProductImageList fragmentProductImageList = FragmentProductImageList.getInstance(imageList);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentProductImageList, "ProductImageList");
        fragmentTransaction.addToBackStack("ProductImageList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void updateCart() {
        updateCartCount();
    }

    @Override
    public void loadEditAccount() {
        validateVisibility(1);
        isHomePageVisible = false;
        FragmentEditAccount fragmentEditAccount = new FragmentEditAccount();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentEditAccount, "EditAccount");
        fragmentTransaction.addToBackStack("EditAccount");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadChangePassword() {
        DialogChangePassword changePassword = new DialogChangePassword();
        changePassword.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        changePassword.setCancelable(false);
        changePassword.show(getSupportFragmentManager(), "ChangePassword");
    }

    @Override
    public void loadAddressBook() {
        validateVisibility(1);
        isHomePageVisible = false;
      /*  FragmentAddressBook fragmentAddressBook = new FragmentAddressBook();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentAddressBook, "AddressBook");
        fragmentTransaction.addToBackStack("AddressBook");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();*/

        Intent intent = new Intent(ActivityHome.this, ActivityError.class);
        intent.putExtra("LoadType", "LoadAddressBook");
        startActivity(intent);
    }

    @Override
    public void reloadAddressBook() {
        onBackPressed();
        loadAddressBook();
    }

    @Override
    public void loadAddress(String accountDetail, int id, String type) {
        DialogAddress dialogAddress = new DialogAddress();
        dialogAddress.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogAddress.setCancelable(true);
        Bundle bundle = new Bundle();
        bundle.putString("CustomerDetail", accountDetail);
        bundle.putString("AddressType", type);
        bundle.putInt("AddressId", id);
        dialogAddress.setArguments(bundle);
        dialogAddress.show(getSupportFragmentManager(), "Address");
    }

    @Override
    public void loadOrderList() {
        validateVisibility(1);
        isHomePageVisible = false;
       /* FragmentOrderHistory fragmentOrderHistory = new FragmentOrderHistory();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentOrderHistory, "OrderHistory");
        fragmentTransaction.addToBackStack("OrderHistory");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();*/

        Intent intent = new Intent(ActivityHome.this, ActivityError.class);
        intent.putExtra("LoadType", "LoadOrderList");
        startActivity(intent);
    }

    @Override
    public void loadSingleOrderHistory(String orderDetail) {
        validateVisibility(1);
        isHomePageVisible = false;
        FragmentSingleOrderHistoryDetail fragmentSingleOrderHistoryDetail = FragmentSingleOrderHistoryDetail.newInstance(orderDetail);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_home_fragment_container, fragmentSingleOrderHistoryDetail, "SingleOrderHistory");
        fragmentTransaction.addToBackStack("SingleOrderHistory");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadDownloadableProduct() {

    }

    @Override
    public void loadAboutUs() {
        DialogWebUrlLoader dialogAboutUs = new DialogWebUrlLoader();
        dialogAboutUs.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogAboutUs.setCancelable(true);
        Bundle bundle = new Bundle();
        bundle.putString("which", ConstantFields.getAboutUsLink(ConstantFields.currentLanguage()));
        dialogAboutUs.setArguments(bundle);
        dialogAboutUs.show(getSupportFragmentManager(), "AboutUs");
    }

    @Override
    public void loadContactUs() {
        DialogWebUrlLoader dialogContactUs = new DialogWebUrlLoader();
        dialogContactUs.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogContactUs.setCancelable(true);
        Bundle bundle = new Bundle();
        bundle.putString("which", ConstantFields.getContactUsLink(ConstantFields.currentLanguage()));
        dialogContactUs.setArguments(bundle);
        dialogContactUs.show(getSupportFragmentManager(), "ContactUs");
    }

    @Override
    public void loadLogout() {
        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mTokenTag);
        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHome.this.getApplicationContext(), ConstantFields.mCustomerCartIdTag);
        WishList.getInstance(ActivityHome.this).delete_wish_list();
        reload();
    }

    @Override
    public void backBtnPressed() {
        onBackPressed();
    }

    @Override
    public boolean networkError() {
        if (ConstantFields.isNetworkAvailable()) {
            return true;
        } else {
            Intent intent = new Intent(ActivityHome.this, ActivityError.class);
            intent.putExtra("LoadType", "NetWorkError");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return false;
        }
    }

    @Override
    public void visibility() {
        validateVisibility(1);
    }

    @Override
    public void changeBottomView(int type) {
        onBackPressedBottomNavigationBarListener(type);
    }

    private void toUser(String type) {

        closeDrawer();

        Intent toUser;
        if (type.equals("Register")) {
            toUser = new Intent(ActivityHome.this, ActivitySignUp.class);
        } else {
            toUser = new Intent(ActivityHome.this, ActivityLogin.class);
        }
        toUser.putExtra("WhatToLoad", type);
        startActivity(toUser);
    }

    private void loadAgain() {
        closeDrawer();
        reload();
    }

    private void reload() {
        Intent toStartAgain = new Intent(ActivityHome.this, ActivitySplashScreen.class);
        toStartAgain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(toStartAgain);
    }

    private void closeDrawer() {
        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {

            getSupportFragmentManager().popBackStack();

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.ll_home_fragment_container);

            if (fragment instanceof FragmentProductDetails) {
                if (fragment.isVisible()) {
                    validateVisibility(2);
                } else {
                    validateVisibility(1);
                }
            } else {
                validateVisibility(1);
            }
        } else {
            finish();
        }
    }


    private void onBackPressedBottomNavigationBarListener(int type) {

        if (type == 1) {
            bottomMenu.getMenu().findItem(R.id.bnb_home).setChecked(true);
        }

        if (type == 2) {
            bottomMenu.getMenu().findItem(R.id.bnb_search).setChecked(true);
        }

        if (type == 3) {
            bottomMenu.getMenu().findItem(R.id.bnb_wish_list).setChecked(true);
        }

        if (type == 4) {
            bottomMenu.getMenu().findItem(R.id.bnb_deals).setChecked(true);
        }

        if (type == 5) {
            bottomMenu.getMenu().findItem(R.id.bnb_account).setChecked(true);
        }
    }

}
