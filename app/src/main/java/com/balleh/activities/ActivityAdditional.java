package com.balleh.activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.fragments.DialogCustomMessage;


//Container - R.id.ll_additional_content_container

public class ActivityAdditional extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String loadType = bundle.getString("LoadType");


            if (loadType != null) {
                if (loadType.length() > 0) {
                    switch (loadType){
                        case "DialogCustomMessage":
                            loadCustomMessage(bundle.getString("ToCustomType"));
                            break;
                    }
                } else {
                    loadCustomMessage("Error");
                }
            } else {
                loadCustomMessage("Error");
            }
        }
    }

    private void loadCustomMessage(String type) {
        DialogCustomMessage dialogCustomMessage = new DialogCustomMessage();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        dialogCustomMessage.setArguments(bundle);
        dialogCustomMessage.show(getSupportFragmentManager(), "DialogCustomMessage - Error");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityAdditional.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
