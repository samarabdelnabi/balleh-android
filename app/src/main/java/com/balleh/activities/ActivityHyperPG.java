package com.balleh.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.adapter.SpinnerCountryAdapter;
import com.balleh.adapter.SpinnerStateAdapter;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.api.ApiClient;
import com.balleh.additional.api.ApiInterface;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.instentTransfer.PaymentRetry;
import com.balleh.fragments.DialogLoader;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelCartListProduct;
import com.balleh.model.ModelCountry;
import com.balleh.model.ModelPaymentMethod;
import com.balleh.model.ModelShippingMethod;
import com.balleh.model.ModelState;
import com.checkout.CheckoutKit;
import com.checkout.exceptions.CardException;
import com.checkout.exceptions.CheckoutException;
import com.checkout.httpconnector.Response;
import com.checkout.models.Card;
import com.checkout.models.CardTokenResponse;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityHyperPG extends AppCompatActivity implements PaymentRetry {

    private String amount = "0", order_id = "0", resourcePath, mYearValue, mMonthValue, type;
    private AppCompatTextView atv_LoadingTitlePayment;
    private ProgressBar pb_Loader;
    private boolean update = false;
    private TextInputLayout til_CardNumber, /*til_CardHolderName,*/
            til_CVVNo;
    private EditText et_CardNumber, et_CardHolderName, et_CVVNo;
    private Spinner s_Year, s_Month;
    private String orderId;
    private DialogLoader dialogLoader;


    @Override
    public void retry() {
        //toStartAgain();
        toCheckOutPay();
    }

    @Override
    public void failed() {
        orderFailed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hyper_pg);

        atv_LoadingTitlePayment = findViewById(R.id.atv_order_payment_loading);
        pb_Loader = findViewById(R.id.pb_order_payment_loader);
        LinearLayout ll_CardHolder = findViewById(R.id.ll_card_holder);

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mTokenTag).equals("Empty")) {
            type = "Customer";
        } else {
            type = "Guest";
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            order_id = bundle.getString("OrderId");
            amount = bundle.getString("Amount");

            String type = bundle.getString("Type");

            if (type != null) {
                if (type.equals("Hyper")) {
                    pb_Loader.setVisibility(View.VISIBLE);
                    atv_LoadingTitlePayment.setVisibility(View.VISIBLE);
                    ll_CardHolder.setVisibility(View.GONE);
                    toHyperCheckOutId();
                } else if (type.equals("CheckOut.com")) {
                    pb_Loader.setVisibility(View.GONE);
                    ll_CardHolder.setVisibility(View.VISIBLE);
                    atv_LoadingTitlePayment.setVisibility(View.GONE);
                    toCheckOutDotCom();
                }
            }

        }

        dialogLoader = new DialogLoader();
        dialogLoader.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogLoader.setCancelable(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("onNewIntent: ", intent.getScheme() + "");
        setIntent(intent);
        if (resourcePath != null && hasCallbackScheme(intent)) {
            if (!update)
                updatePaymentGatewayResult(resourcePath);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                // transaction completed
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);

                //resource path if needed
                resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);

                Log.e("onActivityResult: ", resourcePath + "");

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    //check the result of synchronous transaction
                    updatePaymentGatewayResult(resourcePath);
                } else {
                    // wait for the asynchronous transaction callback in the onNewIntent()
                    if (hasCallbackScheme(getIntent())) {
                        updatePaymentGatewayResult(resourcePath);
                    } else {
                        /* The on onNewIntent method wasn't called yet,
                               wait for the callback. */
                        showProgressDialog(1);
                    }
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                //shopper canceled the checkout process
                Log.e("onActivityResult: ", "Cancel");
                paymentCancel();
                break;
            case CheckoutActivity.RESULT_ERROR:
                //  error occurred
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                Log.e("onActivityResult: ", error.getErrorMessage() + " : " + error.getErrorInfo());
                paymentCancel();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.checkout_payment_cancel_text);
        alertDialogBuilder.setPositiveButton("OK",
                (dialog, arg1) -> toStartAgain());
        alertDialogBuilder.setNegativeButton("CANCEL", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityHyperPG.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }

    private void loader(boolean isLoading) {
        if (isLoading) {
            pb_Loader.setVisibility(View.VISIBLE);
        } else {
            pb_Loader.setVisibility(View.GONE);
        }
    }

    private void toCheckOutDotCom() {

        til_CardNumber = findViewById(R.id.til_card_number_holder);
        //til_CardHolderName = findViewById(R.id.til_card_holder_name_holder);
        til_CVVNo = findViewById(R.id.til_cvv_no_holder);

        Button btn_PayNow = findViewById(R.id.btn_pay_now);

        et_CardNumber = findViewById(R.id.et_card_number);
        et_CardHolderName = findViewById(R.id.et_card_holder_name);
        et_CVVNo = findViewById(R.id.et_cvv_no);
        s_Year = findViewById(R.id.s_expiry_year_data);
        s_Month = findViewById(R.id.s_expiry_month_data);

        setMonth();
        setYear();

        btn_PayNow.setOnClickListener(v -> {
            if (et_CardNumber.getText().toString().length() == 16) {
                til_CardNumber.setErrorEnabled(false);
            } else {
                til_CardNumber.setErrorEnabled(true);
                til_CardNumber.setError(getString(R.string.pg_card_no_error));
            }

          /*  if (et_CardHolderName.getText().toString().length() > 0) {
                til_CardHolderName.setErrorEnabled(false);
            } else {
                til_CardHolderName.setErrorEnabled(true);
                til_CardHolderName.setError(getString(R.string.pg_card_name_error));
            }*/

            if (et_CVVNo.getText().toString().length() >= 3) {
                til_CVVNo.setErrorEnabled(false);
            } else {
                til_CVVNo.setErrorEnabled(true);
                til_CVVNo.setError(getString(R.string.pg_card_cvv_error));
            }

            if (mYearValue.equals("Empty")) {
                showToast(getString(R.string.pg_card_expire_date_error));
            }

            if (mMonthValue.equals("Empty")) {
                showToast(getString(R.string.pg_card_expire_date_error));
            }

            if (et_CardNumber.getText().toString().length() == 16
                    /*&& et_CardHolderName.getText().toString().length() > 0*/
                    && et_CVVNo.getText().toString().length() >= 3
                    && !mYearValue.equals("Empty")
                    && !mMonthValue.equals("Empty")) {

                toCheckOutPay();
            }
        });

    }

    private void toCheckOutPay() {
        new ConnectionTask().execute();
    }

    private void toHyperCheckOutId() {
        if (ConstantFields.isNetworkAvailable()) {
            loader(true);


            ModelAddress modelAddress = CheckOut.getInstance(ActivityHyperPG.this).getAccountDetail();

            if (modelAddress != null) {
                Log.e("toHyperCheckOutId: ", ConstantFields.GetPaymentURL(/*amount, "SAR"*/));

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.GetPaymentURL(/*amount, "SAR"*/),
                        response -> {
                            loader(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (!jsonObject.isNull("id")) {
                                    startPayment(jsonObject.getString("id"));
                                } else {
                                    showToast(getString(R.string.common_error));
                                    paymentCancel();
                                }
                            } catch (Exception e) {
                                loader(false);
                                paymentCancel();
                            }

                        }, error -> {
                    loader(false);
                    paymentCancel();
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> hyperPayPostList = new HashMap<>();
                        hyperPayPostList.put("amount", String.valueOf(amount));
                        hyperPayPostList.put("currency", "SAR");
                        hyperPayPostList.put("billing_city", modelAddress.getCity());
                        hyperPayPostList.put("billing_street", modelAddress.getStreet());
                        hyperPayPostList.put("customer_name", modelAddress.getFirstName());
                        hyperPayPostList.put("customer_email", modelAddress.getEmail_id());
                        hyperPayPostList.put("customer_country", modelAddress.getCountryId());
                        hyperPayPostList.put("postcode", modelAddress.getPost_code());
                        hyperPayPostList.put("customer_surname", modelAddress.getFirstName());
                        return hyperPayPostList;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CheckOutId");
            }
        } else {
            networkError();
        }

    }

    private void networkError() {
        Intent intent = new Intent(ActivityHyperPG.this, ActivityError.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void startPayment(String checkoutId) {
        Set<String> paymentBrands = new LinkedHashSet<>();
        paymentBrands.add("VISA");
        paymentBrands.add("MASTER");
        paymentBrands.add("DIRECTDEBIT_SEPA");

        CheckoutSettings checkoutSettings = new CheckoutSettings(checkoutId, paymentBrands, Connect.ProviderMode.TEST);
        checkoutSettings.setCheckoutId(checkoutId);
        checkoutSettings.setPaymentBrands(paymentBrands);
        checkoutSettings.setWebViewEnabledFor3DSecure(false);

        Intent intent = checkoutSettings.createCheckoutActivityIntent(this);
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
    }

    private void showProgressDialog(int type) {
        if (type == 1) {
            new AlertDialog.Builder(ActivityHyperPG.this)
                    .setMessage(getString(R.string.order_payment_status))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        dialog.dismiss();
                    })
                    .setCancelable(false)

                    .show();
        } else {
            new AlertDialog.Builder(ActivityHyperPG.this)
                    .setMessage(getString(R.string.order_failed_payment_status))
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                        toStartAgain();
                        dialog.dismiss();
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();
        return "balleh".equals(scheme);
    }

    private void paymentCancel() {
        if (ConstantFields.isNetworkAvailable()) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getOrderCancel(order_id),
                    response -> showProgressDialog(2),
                    error -> showProgressDialog(2)) {
            };
            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "PaymentStatusCancelUpdate");
        } else {
            networkError();
        }
    }

    private void orderSuccess() {
        Intent intent = new Intent(ActivityHyperPG.this, ActivityCheckOutSuccess.class);
        intent.putExtra("OrderId", orderId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void toStartAgain() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void orderFailed() {
        showProgressDialog(2);
    }

    private void updatePaymentGatewayResult(String resourcePath) {

        if (ConstantFields.isNetworkAvailable()) {
            /*StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.getCheckOutResourcePath(resourcePath, order_id),
                    response -> {
                        orderSuccess();
                        Log.e("updatePaymentGatewayResult: 0", response + "");
                    },
                    error -> {
                        showProgressDialog(2);
                        Log.e("updatePaymentGatewayResult: 1", error.getMessage() + "");

                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                Log.e("updatePaymentGatewayResult:2 ", responseBody + "");
                                *//*JSONObject jsonObject = new JSONObject(responseBody);
                                Log.e("updatePaymentGatewayResult: 3", jsonObject.toString() + "");*//*
                            } else {
                                Log.e("updatePaymentGatewayResult: 4", "Null");
                            }
                        } catch (Exception e) {
                            Log.e("updatePaymentGatewayResult: 5", e.toString());
                        }
                    }
            ) {

            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "PaymentStatusUpdate");*/

            new HyperPaymentStatusUpdatePost().execute(resourcePath);

            update = true;
        } else {
            networkError();
        }

    }

    @SuppressLint("StaticFieldLeak")
    private class HyperPaymentStatusUpdatePost extends AsyncTask<String, Void, String> {

        String mResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(ConstantFields.getCheckOutResourcePath(resourcePath, order_id));
                Log.e("doInBackground", url.toString());
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("POST");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(params[0]);
                mBufferedWriter.close();
                mOutputStream.close();

                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                    Log.e("doInBackground: ", mResponse + "");

                    try {
                        JSONObject jsonObject = new JSONObject(mResponse);
                        if (!jsonObject.isNull("message")) {
                            mResponse = jsonObject.getString("message");
                        } else {
                            mResponse = "Error";
                        }
                    } catch (Exception e) {
                        mResponse = "Error";
                    }

                } else {
                    mResponse = "Error";
                    String xmResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getErrorStream())));
                    Log.e("doInBackground: ", xmResponse + "");
                    try {
                        JSONObject jsonObject = new JSONObject(xmResponse);
                        if (!jsonObject.isNull("message")) {
                            runOnUiThread(() -> {
                                try {
                                    showToast(jsonObject.getString("message"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return mResponse;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("doInBackground: ", e.toString());
                return "Error";
            }
        }

        @Override
        protected void onPostExecute(String s) {

            Log.e("onPostExecute: ", s);

            if (s.toLowerCase().equals("success")) {
                if (type.equals("Customer")) {
                    customerPlaceOrder();
                } else {
                    guestPlaceOrder();
                }
            } else {
                showProgressDialog(2);
            }
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, ActivityHyperPG.this);
    }

    /*Month*/
    public void setMonth() {

        final ArrayList<String> monthList = new ArrayList<>();
        monthList.add("Select");

        for (int i = 1; i < 13; i++) {
            monthList.add(i + "");
        }

        ArrayAdapter<String> monthAdapterMonth = new ArrayAdapter<>
                (ActivityHyperPG.this, android.R.layout.simple_spinner_item, monthList);

        monthAdapterMonth.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        s_Month.setAdapter(monthAdapterMonth);
        s_Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!monthList.get(position).equals("Select")) {
                    mMonthValue = monthList.get(position);
                } else {
                    mMonthValue = "Empty";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*Year*/
    public void setYear() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        final ArrayList<String> yearList = new ArrayList<>();
        yearList.add("Select");
        for (int i = year; i < year + 300; i++) {
            yearList.add(i + "");
        }

        ArrayAdapter<String> yearAdapterMonth = new ArrayAdapter<>
                (ActivityHyperPG.this, android.R.layout.simple_spinner_item, yearList);

        yearAdapterMonth.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        s_Year.setAdapter(yearAdapterMonth);
        s_Year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!yearList.get(position).equals("Select")) {
                    mYearValue = yearList.get(position);
                } else {
                    mYearValue = "Empty";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    class ConnectionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb_Loader.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                Card card = new Card(et_CardNumber.getText().toString(), et_CardNumber.getText().toString(),
                        mMonthValue, mYearValue,
                        et_CVVNo.getText().toString());
                CheckoutKit ck = CheckoutKit.getInstance(ConstantFields.publicKey);
                final Response<CardTokenResponse> resp = ck.createCardToken(card);
                if (resp.hasError) {
                    runOnUiThread(ActivityHyperPG.this::goToError);
                } else {
                    return resp.model.getCardToken();
                }
            } catch (final CardException e) {
                runOnUiThread(() -> {
                    if (e.getType().equals(CardException.CardExceptionType.INVALID_CVV)) {
                        til_CVVNo.setErrorEnabled(true);
                        til_CVVNo.setError(getString(R.string.pg_card_no_error));
                    } else if (e.getType().equals(CardException.CardExceptionType.INVALID_EXPIRY_DATE)) {
                        showToast(getString(R.string.pg_card_expire_date_error));
                    } else if (e.getType().equals(CardException.CardExceptionType.INVALID_NUMBER)) {
                        til_CardNumber.setErrorEnabled(true);
                        til_CardNumber.setError(getString(R.string.pg_card_no_error));
                    } else {
                        til_CVVNo.setErrorEnabled(false);
                        til_CardNumber.setErrorEnabled(false);
                        til_CVVNo.setErrorEnabled(false);
                    }
                });
            } catch (CheckoutException | IOException e2) {
                runOnUiThread(() -> goToError());
            }
            return "Empty";
        }

        @Override
        protected void onPostExecute(String s) {
            pb_Loader.setVisibility(View.GONE);
            if (s != null) {
                if (s.length() > 0) {
                    if (!s.equals("Empty")) {
                        toCheckoutPaymentGateway(s);
                    }
                }
            }

        }
    }

    private void toCheckoutPaymentGateway(String token) {
        postCheckOutPaymentGateway(token);
    }

    private void postCheckOutPaymentGateway(String token) {
        try {

            ModelAddress modelAddress = CheckOut.getInstance(ActivityHyperPG.this).getAccountDetail();

            JSONObject jsonObjectCheckOut = new JSONObject();
            jsonObjectCheckOut.put("cardToken", token);
            jsonObjectCheckOut.put("email", modelAddress.getEmail_id());
            jsonObjectCheckOut.put("value", amount);
            jsonObjectCheckOut.put("currency", "SAR");
            ModelShippingMethod modelShippingMethod = CheckOut.getInstance(ActivityHyperPG.this).getShippingType();
            if (modelShippingMethod != null) {
                jsonObjectCheckOut.put("shipping", modelShippingMethod.getMethod_code());
            }
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(modelAddress.getTelephone());
            JSONObject jsonObjectAddress = new JSONObject();
            jsonObjectAddress.put("addressLine1", modelAddress.getStreet());
            jsonObjectAddress.put("addressLine2", "");
            jsonObjectAddress.put("postcode", modelAddress.getPost_code());
            jsonObjectAddress.put("country_id", modelAddress.getCountryId());
            jsonObjectAddress.put("city", modelAddress.getCity());
            jsonObjectAddress.put("state", modelAddress.getRegionCode());
            jsonObjectAddress.put("phone", jsonArray);

            //jsonObjectCheckOut.put("shippingDetails", jsonObjectAddress);

            JSONArray jsonProductList = new JSONArray();

            String cartProductDetail = ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCartDetailTag);
            if (!cartProductDetail.equals("Empty")) {
                ArrayList<ModelCartListProduct> cartListProducts = ConstantDataParser.getCartDetails(cartProductDetail);
                if (cartListProducts != null) {
                    if (cartListProducts.size() > 0) {
                        for (int i = 0; i < cartListProducts.size(); i++) {
                            JSONObject productDetail = new JSONObject();
                            productDetail.put("id", cartListProducts.get(i).getProductId());
                            productDetail.put("quantity", cartListProducts.get(i).getQuantity());
                            if (cartListProducts.get(i).getOptionList() != null) {
                                if (cartListProducts.get(i).getOptionList().size() > 0) {
                                    JSONArray jsonOptionList = new JSONArray();
                                    for (int j = 0; j < cartListProducts.get(i).getOptionList().size(); j++) {
                                        jsonOptionList.put(cartListProducts.get(i).getOptionList().get(j).getOption_value_id());
                                    }
                                    productDetail.put("options", jsonOptionList);
                                }
                            }
                            jsonProductList.put(productDetail);
                        }
                    }
                }
            }


            jsonObjectCheckOut.put("products", jsonProductList);

            Log.e("postCOPaymentGateway: ", jsonObjectCheckOut.toString() + "");

            ApiInterface apiInterface;
            apiInterface = ApiClient.getClient().create(ApiInterface.class);

            Call<String> callCheckOutPayment = apiInterface.PaymentCharge(jsonObjectCheckOut.toString());
            callCheckOutPayment.enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response) {

                    if (response.isSuccessful()) {
                        Log.e("onResponse: ", response.body());
                        orderId = response.body() + "";
                        // orderSuccess();
                        //CustomerCartListLoader();
                        orderSuccessChecking();
                    } else {
                        try {
                            ResponseBody requestBody = response.errorBody();
                            BufferedReader r = new BufferedReader(new InputStreamReader(requestBody.byteStream()));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line).append('\n');
                            }
                            JSONObject jObjError = new JSONObject(total.toString());
                            if (!jObjError.isNull("message")) {
                                showToast(jObjError.getString("message"));
                                String responseMessage = jObjError.getString("message");
                                if (responseMessage.equals("Product that you are trying to add is not available.")) {
                                    toStartAgain();
                                } else if (responseMessage.contains("Transaction cancel")) {
                                    toStartAgain();
                                } else {
                                    //orderFailed();
                                    //showToast(getString(R.string.address_message));
                                    loadAddressList();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            showToast(getString(R.string.common_error));
                        }
                    }

                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    Log.e("onFailure: ", "Failed");
                    orderFailed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            orderFailed();
        }
    }

    private void loadAddressList() {
        if (networkChecker()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_Loader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_Loader.setVisibility(View.GONE);
                            Log.e("onResponse: ", response + "");
                            loadAddress(response);
                        }, error -> {
                    pb_Loader.setVisibility(View.GONE);
                   /* if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401) {
                            ConstantDataSaver.mRemoveSharedPreferenceString(getApplicationContext(), ConstantFields.mTokenTag);
                        }
                    }*/
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
            }
        }
    }

    public void loadAddress(String accountDetail) {
        DialogAddressNew dialogAddress = new DialogAddressNew();
        dialogAddress.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogAddress.setCancelable(true);
        Bundle bundle = new Bundle();
        bundle.putString("CustomerDetail", accountDetail);
        bundle.putString("AddressType", "New");
        bundle.putInt("AddressId", 0);
        dialogAddress.setArguments(bundle);
        dialogAddress.show(getSupportFragmentManager(), "Address");
    }

    private void goToError() {
        showToast(getString(R.string.common_error));
    }

    @SuppressLint("StaticFieldLeak")
    private class PlaceOrderPost extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogHandler(1);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url;
                if (type.equals("Customer")) {
                    url = new URL(ConstantFields.mCustomerPlaceOrder);
                } else {
                    url = new URL(ConstantFields.getGuestPlaceOrder(ConstantDataSaver
                            .mRetrieveSharedPreferenceString(getApplicationContext(), ConstantFields.mGuestCartIdTag), ConstantFields.currentLanguage().equals("en")));
                }
                Log.e("doInBackground", url.toString());
                Log.e("doInBackground: ", params[0]);
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                mUrlConnection.setRequestMethod("POST");
                mUrlConnection.setRequestProperty("Content-Type", "application/json");

                if (type.equals("Customer")) {
                    mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver
                            .mRetrieveSharedPreferenceString(getApplicationContext(),
                                    ConstantFields.mTokenTag));
                }

                mUrlConnection.setDoInput(true);
                mUrlConnection.setDoOutput(true);
                mUrlConnection.setReadTimeout(15000);
                OutputStream mOutputStream = mUrlConnection.getOutputStream();

                BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                mBufferedWriter.write(params[0]);
                mBufferedWriter.close();
                mOutputStream.close();

                String mResponse;
                if (mUrlConnection.getResponseCode() == 200) {
                    mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                    ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mGuestCartIdTag);
                    ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCustomerCartIdTag);
                    if (mResponse != null)
                        if (mResponse.length() > 0)
                            order_id = mResponse.replace("\'", "");
                } else {
                    mResponse = "Error";
                    String xmResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getErrorStream())));
                    Log.e("doInBackground: ", xmResponse + "");

                    try {
                        JSONObject jsonObject = new JSONObject(xmResponse);
                        if (!jsonObject.isNull("message")) {
                            runOnUiThread(() -> {
                                try {
                                    showToast(jsonObject.getString("message"));
                                } catch (Exception e) {

                                }
                            });
                        }
                    } catch (Exception e) {

                    }

                }
                Log.e("doInBackground: ", mResponse);
                return mResponse;
            } catch (Exception e) {
                Log.e("doInBackground: ", e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            dialogHandler(2);

            ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCustomerCartIdTag);
            Log.e("onPostExecute: ", s + "");

            if (!s.equals("Error")) {
                showToast(getString(R.string.order_placed));
                orderSuccess();
            } else {
                orderFailed();
            }
        }
    }

    private void dialogHandler(int type) {
        if (type == 1) {
            dialogLoader.show(getSupportFragmentManager(), "DialogLoader");
        } else {
            dialogLoader.dismiss();
        }
    }

    @Nullable
    public static String inputStreamToStringConversion(BufferedReader bufferedReader) {
        try {
            String line;
            StringBuilder result = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private void customerPlaceOrder() {
        try {

            ModelPaymentMethod modelPaymentMethod = CheckOut.getInstance(ActivityHyperPG.this).getPaymentType();
            ModelAddress modelAddress = CheckOut.getInstance(ActivityHyperPG.this).getAccountDetail();
            JSONObject paymentMethodJsonObject = new JSONObject();
            final JSONObject jsonObject = new JSONObject();

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this.getApplicationContext(), ConstantFields.mCustomerCartIdTag).equals("Empty")) {
                //jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mCustomerCartIdTag));
            }

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("customer_id", modelAddress.getCustomerId());
                jsonObjectAddress.put("same_as_billing", "1");
                //jsonObject.put("billing_address", jsonObjectAddress);
            }

            if (modelPaymentMethod != null) {
                new PlaceOrderPost().execute(jsonObject.toString());
            }
            Log.e("placeOrder: ", jsonObject.toString() + "");


        } catch (Exception e) {
            Log.e("placeOrder: ", e.toString());
        }
    }

    private void guestPlaceOrder() {
        try {

            ModelPaymentMethod modelPaymentMethod = CheckOut.getInstance(ActivityHyperPG.this).getPaymentType();
            ModelAddress modelAddress = CheckOut.getInstance(ActivityHyperPG.this).getAccountDetail();
            JSONObject paymentMethodJsonObject;
            final JSONObject jsonObject = new JSONObject();

            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this.getApplicationContext(), ConstantFields.mGuestCartIdTag).equals("Empty")) {
                jsonObject.put("cartId", ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this.getApplicationContext(), ConstantFields.mGuestCartIdTag));
            }

            if (modelPaymentMethod != null) {
                paymentMethodJsonObject = new JSONObject();
                paymentMethodJsonObject.put("method", modelPaymentMethod.getCode());
                jsonObject.put("paymentMethod", paymentMethodJsonObject);
            }

            ModelShippingMethod modelShippingMethod = CheckOut.getInstance(ActivityHyperPG.this).getShippingType();

            if (modelShippingMethod != null) {
                paymentMethodJsonObject = new JSONObject();
                paymentMethodJsonObject.put("shipping_carrier_code", modelShippingMethod.getCarrier_code());
                paymentMethodJsonObject.put("shipping_method_code", modelShippingMethod.getMethod_code());
                //jsonObject.put("shippingMethod", paymentMethodJsonObject);
            }

            if (modelAddress != null) {
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(modelAddress.getStreet());
                JSONObject jsonObjectAddress = new JSONObject();
                jsonObjectAddress.put("firstname", modelAddress.getFirstName());
                jsonObjectAddress.put("lastname", modelAddress.getLastName());
                jsonObjectAddress.put("city", modelAddress.getCity());
                jsonObjectAddress.put("country_id", modelAddress.getCountryId());
                jsonObjectAddress.put("region_id", modelAddress.getRegionId());
                jsonObjectAddress.put("region_code", modelAddress.getRegionCode());
                jsonObjectAddress.put("region", modelAddress.getRegionCode());
                jsonObjectAddress.put("street", jsonArray);
                jsonObjectAddress.put("postcode", modelAddress.getPost_code());
                jsonObjectAddress.put("email", modelAddress.getEmail_id());
                jsonObjectAddress.put("telephone", modelAddress.getTelephone());
                jsonObjectAddress.put("same_as_billing", "1");
                //jsonObject.put("billing_address", jsonObjectAddress);

                jsonObject.put("email", modelAddress.getEmail_id());
            }

            Log.e("placeOrder: ", jsonObject.toString() + "");

            new PlaceOrderPost().execute(jsonObject.toString());

        } catch (Exception e) {
            Log.e("placeOrder: ", e.toString());
        }
    }

    public boolean networkChecker() {
        if (ConstantFields.isNetworkAvailable()) {
            return true;
        } else {
            Intent intent = new Intent(ActivityHyperPG.this, ActivityError.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return false;
        }
    }

    private void CustomerCartListLoader() {
        if (networkChecker()) {
            pb_Loader.setVisibility(View.VISIBLE);
            Log.e("CustomerCartList: ", ConstantFields.removCartId(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCustomerCartIdTag)));
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.removCartId(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCustomerCartIdTag)),
                    response -> {
                        Log.e("CustomerCartListLoader: ", response + "");
                        ConstantDataSaver.mRemoveSharedPreferenceString(ActivityHyperPG.this, ConstantFields.mCustomerCartIdTag);
                        orderSuccess();

                    }, error -> {
                orderSuccess();
                Log.e("CustomerCartListLoader: ", "Error");
                /*if (error.networkResponse != null)
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(getApplicationContext(), ConstantFields.mTokenTag);
                    }*/
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartList");
        }
    }

    private void removeProduct(final int type, final ModelCartListProduct CartDetail) {
        if (networkChecker()) {
            if (!ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                pb_Loader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.DELETE, ConstantFields.deleteCartItem(CartDetail.getItemId(), ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            pb_Loader.setVisibility(View.GONE);
                            if (type == 1) {
                                CustomerCartListLoader();
                            } else {
                                orderSuccess();
                            }

                        }, error -> {
                    orderSuccess();
                    pb_Loader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.common_error));
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer " + ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerCartItemRemove");
            }
        }
    }

    public static class DialogAddressNew extends DialogFragment {

        private Activity activity;
        private View v_InsertAddress;
        private String addressType = "New";
        private EditText et_FirstName, et_LastName, et_MobileNumber, et_StreetAddress,/* et_City,*/
                et_ZipCode;
        private ModelAccountDetail accountDetail;
        private AppCompatTextView atv_FirstNameError, atv_LastNameError, atv_MobileNumberError,
                atv_StreetAddressError, atv_CityError, atv_StateError, atv_CountryError, atv_ZipCodeError,
                atv_AddressTitle, atv_ContactTitle, atv_FirstNameTitle, atv_LastNameTitle, atv_MobileNoTitle, atv_AddressInfoTitle,
                atv_StreetAddressTitle, atv_CityTitle, atv_StateTitle, atv_CountryTitle, atv_ZipCodeTitle;
        private Button btn_SaveAddress;
        private CheckBox cb_BillingAddress, cb_ShippingAddress;
        private Spinner s_CityList, s_StateList, s_CountryList;
        private ArrayList<ModelCountry> CountryList;
        private String CountryId, StateCode, CityName = "Empty";
        private int StateId, AddressId = 0;
        private ProgressBar pb_AddressLoader;
        private PaymentRetry paymentRetry;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle != null) {
                if (bundle.getString("CustomerDetail") != null) {
                    accountDetail = ConstantDataParser.getAccountDetail(bundle.getString("CustomerDetail"));
                } else {
                    accountDetail = null;
                }
            } else {
                accountDetail = null;
            }
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            v_InsertAddress = inflater.inflate(R.layout.dialog_address, container, false);
            load();
            return v_InsertAddress;
        }

        private void load() {
            et_FirstName = v_InsertAddress.findViewById(R.id.et_first_name_address);
            et_LastName = v_InsertAddress.findViewById(R.id.et_last_name_address);
            et_MobileNumber = v_InsertAddress.findViewById(R.id.et_mobile_no_address);
            et_StreetAddress = v_InsertAddress.findViewById(R.id.et_street_address);
            //et_City = v_InsertAddress.findViewById(R.id.et_city_address);
            et_ZipCode = v_InsertAddress.findViewById(R.id.et_zip_code_address);

            atv_ContactTitle = v_InsertAddress.findViewById(R.id.atv_title_c_us);
            atv_FirstNameTitle = v_InsertAddress.findViewById(R.id.atv_first_name_title_c_us);
            atv_LastNameTitle = v_InsertAddress.findViewById(R.id.atv_last_name_title_c_us);
            atv_MobileNoTitle = v_InsertAddress.findViewById(R.id.atv_mobile_no_title_c_us);
            atv_AddressInfoTitle = v_InsertAddress.findViewById(R.id.atv_address_title_c_us);
            atv_StreetAddressTitle = v_InsertAddress.findViewById(R.id.atv_street_address_title_c_us);
            atv_CityTitle = v_InsertAddress.findViewById(R.id.atv_city_title_c_us);
            atv_StateTitle = v_InsertAddress.findViewById(R.id.atv_state_title_c_us);
            atv_CountryTitle = v_InsertAddress.findViewById(R.id.atv_country_title_c_us);
            atv_ZipCodeTitle = v_InsertAddress.findViewById(R.id.atv_zip_code_title_c_us);

            atv_AddressTitle = v_InsertAddress.findViewById(R.id.atv_address_title);
            atv_FirstNameError = v_InsertAddress.findViewById(R.id.atv_first_name_error_address);
            atv_LastNameError = v_InsertAddress.findViewById(R.id.atv_last_name_error_address);
            atv_MobileNumberError = v_InsertAddress.findViewById(R.id.atv_mobile_error_address);
            atv_StreetAddressError = v_InsertAddress.findViewById(R.id.atv_street_error_address);
            atv_CityError = v_InsertAddress.findViewById(R.id.atv_city_error_address);
            atv_StateError = v_InsertAddress.findViewById(R.id.atv_state_error_address);
            atv_CountryError = v_InsertAddress.findViewById(R.id.atv_country_error_address);
            atv_ZipCodeError = v_InsertAddress.findViewById(R.id.atv_zip_code_error_address);

            s_CityList = v_InsertAddress.findViewById(R.id.s_city_address);
            s_StateList = v_InsertAddress.findViewById(R.id.s_state_address);
            s_CountryList = v_InsertAddress.findViewById(R.id.s_country_address);

            cb_BillingAddress = v_InsertAddress.findViewById(R.id.cb_as_billing_address);
            cb_ShippingAddress = v_InsertAddress.findViewById(R.id.cb_as_shipping_address);
            btn_SaveAddress = v_InsertAddress.findViewById(R.id.btn_save_address);

            pb_AddressLoader = v_InsertAddress.findViewById(R.id.pb_address_loader);

            if (addressType != null) {
                if (addressType.length() > 0) {
                    switch (addressType) {
                        case "New":
                            atv_AddressTitle.setText(getString(R.string.address_new_title));
                            break;
                        case "edit":
                            atv_AddressTitle.setText(getString(R.string.address_edit_title));

                            ModelAddress addressDetailEdit;

                            if (AddressId != 0) {
                                if (accountDetail != null) {
                                    if (accountDetail.getAddressList() != null) {
                                        if (accountDetail.getAddressList().size() > 0) {
                                            for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                                if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                    addressDetailEdit = accountDetail.getAddressList().get(i);

                                                    if (addressDetailEdit != null) {
                                                        if (addressDetailEdit.getFirstName() != null) {
                                                            if (addressDetailEdit.getFirstName().length() > 0) {
                                                                et_FirstName.setText(addressDetailEdit.getFirstName());
                                                            }
                                                        }
                                                        if (addressDetailEdit.getLastName() != null) {
                                                            if (addressDetailEdit.getLastName().length() > 0) {
                                                                et_LastName.setText(addressDetailEdit.getLastName());
                                                            }
                                                        }
                                                        if (addressDetailEdit.getTelephone() != null) {
                                                            if (addressDetailEdit.getTelephone().length() > 0) {
                                                                et_MobileNumber.setText(addressDetailEdit.getTelephone());
                                                            }
                                                        }
                                                   /* if (addressDetailEdit.getCity() != null) {
                                                        if (addressDetailEdit.getCity().length() > 0) {
                                                            et_City.setText(addressDetailEdit.getCity());
                                                        }
                                                    }*/
                                                        if (addressDetailEdit.getStreet() != null) {
                                                            if (addressDetailEdit.getStreet().length() > 0) {
                                                                et_StreetAddress.setText(addressDetailEdit.getStreet());
                                                            }
                                                        }
                                                        if (addressDetailEdit.getPost_code() != null) {
                                                            if (addressDetailEdit.getPost_code().length() > 0) {
                                                                et_ZipCode.setText(addressDetailEdit.getPost_code());
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case "Shipping":
                            addressType = "edit";
                            cb_ShippingAddress.setChecked(true);
                            cb_ShippingAddress.setEnabled(false);
                            cb_ShippingAddress.setText(R.string.address_shipping_address);

                            ModelAddress addressDetailShipping;

                            if (AddressId != 0) {
                                if (accountDetail != null) {
                                    if (accountDetail.getAddressList() != null) {
                                        if (accountDetail.getAddressList().size() > 0) {
                                            for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                                if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                    addressDetailShipping = accountDetail.getAddressList().get(i);

                                                    if (addressDetailShipping != null) {
                                                        if (addressDetailShipping.getFirstName() != null) {
                                                            if (addressDetailShipping.getFirstName().length() > 0) {
                                                                et_FirstName.setText(addressDetailShipping.getFirstName());
                                                            }
                                                        }
                                                        if (addressDetailShipping.getLastName() != null) {
                                                            if (addressDetailShipping.getLastName().length() > 0) {
                                                                et_LastName.setText(addressDetailShipping.getLastName());
                                                            }
                                                        }
                                                        if (addressDetailShipping.getTelephone() != null) {
                                                            if (addressDetailShipping.getTelephone().length() > 0) {
                                                                et_MobileNumber.setText(addressDetailShipping.getTelephone());
                                                            }
                                                        }
                                                   /* if (addressDetailShipping.getCity() != null) {
                                                        if (addressDetailShipping.getCity().length() > 0) {
                                                            et_City.setText(addressDetailShipping.getCity());
                                                        }
                                                    }*/
                                                        if (addressDetailShipping.getStreet() != null) {
                                                            if (addressDetailShipping.getStreet().length() > 0) {
                                                                et_StreetAddress.setText(addressDetailShipping.getStreet());
                                                            }
                                                        }
                                                        if (addressDetailShipping.getPost_code() != null) {
                                                            if (addressDetailShipping.getPost_code().length() > 0) {
                                                                et_ZipCode.setText(addressDetailShipping.getPost_code());
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case "NewShipping":
                            cb_ShippingAddress.setChecked(true);
                            cb_ShippingAddress.setEnabled(false);
                            cb_ShippingAddress.setText(R.string.address_shipping_address);
                            break;
                        case "Billing":
                            addressType = "edit";
                            cb_BillingAddress.setChecked(true);
                            cb_BillingAddress.setEnabled(false);
                            cb_BillingAddress.setText(R.string.address_billing_address);

                            ModelAddress addressDetailBilling;

                            if (AddressId != 0) {
                                if (accountDetail != null) {
                                    if (accountDetail.getAddressList() != null) {
                                        if (accountDetail.getAddressList().size() > 0) {
                                            for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                                if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                    addressDetailBilling = accountDetail.getAddressList().get(i);

                                                    if (addressDetailBilling != null) {
                                                        if (addressDetailBilling.getFirstName() != null) {
                                                            if (addressDetailBilling.getFirstName().length() > 0) {
                                                                et_FirstName.setText(addressDetailBilling.getFirstName());
                                                            }
                                                        }
                                                        if (addressDetailBilling.getLastName() != null) {
                                                            if (addressDetailBilling.getLastName().length() > 0) {
                                                                et_LastName.setText(addressDetailBilling.getLastName());
                                                            }
                                                        }
                                                        if (addressDetailBilling.getTelephone() != null) {
                                                            if (addressDetailBilling.getTelephone().length() > 0) {
                                                                et_MobileNumber.setText(addressDetailBilling.getTelephone());
                                                            }
                                                        }
                                                 /*   if (addressDetailBilling.getCity() != null) {
                                                        if (addressDetailBilling.getCity().length() > 0) {
                                                            et_City.setText(addressDetailBilling.getCity());
                                                        }
                                                    }*/
                                                        if (addressDetailBilling.getStreet() != null) {
                                                            if (addressDetailBilling.getStreet().length() > 0) {
                                                                et_StreetAddress.setText(addressDetailBilling.getStreet());
                                                            }
                                                        }
                                                        if (addressDetailBilling.getPost_code() != null) {
                                                            if (addressDetailBilling.getPost_code().length() > 0) {
                                                                et_ZipCode.setText(addressDetailBilling.getPost_code());
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case "NewBilling":
                            cb_BillingAddress.setChecked(true);
                            cb_BillingAddress.setEnabled(false);
                            cb_BillingAddress.setText(R.string.address_billing_address);
                            break;
                        case "Both":
                            addressType = "edit";
                            cb_ShippingAddress.setChecked(true);
                            cb_ShippingAddress.setEnabled(false);
                            cb_BillingAddress.setChecked(true);
                            cb_BillingAddress.setEnabled(false);
                            cb_ShippingAddress.setText(R.string.address_shipping_address);
                            cb_BillingAddress.setText(R.string.address_billing_address);
                            break;
                        default:
                            atv_AddressTitle.setText(getString(R.string.address_safe_title));
                            break;
                    }
                } else {
                    atv_AddressTitle.setText(getString(R.string.address_safe_title));
                }
            } else {
                atv_AddressTitle.setText(getString(R.string.address_safe_title));
            }

            getCountry();

            fontSetup();
        }

        private void getCountry() {
            if (ConstantFields.isNetworkAvailable()) {
                pb_AddressLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.mCountry,
                        response -> {
                            Log.e("onResponse: ", response);
                            pb_AddressLoader.setVisibility(View.GONE);

                            setupBtn();

                            CountryList = ConstantDataParser.getCountryList(response);

                            if (CountryList != null) {
                                if (CountryList.size() > 0) {
                                    SpinnerCountryAdapter countryAdapter = new SpinnerCountryAdapter(activity, CountryList);
                                    s_CountryList.setAdapter(countryAdapter);

                                    for (int i = 0; i < CountryList.size(); i++) {
                                        if (CountryList.get(i).getId().toLowerCase().equals("sa")) {
                                            s_CountryList.setSelection(i);
                                        }
                                    }

                                    s_CountryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int positionParent, long l) {

                                            if (CountryList.get(positionParent).getId() != null) {
                                                if (!CountryList.get(positionParent).getId().equals("-1")) {

                                                    CountryId = CountryList.get(positionParent).getId();

                                                    if (CountryList.get(positionParent).getStateList() != null) {

                                                        if (CountryList.get(positionParent).getStateList().size() > 0) {

                                                            final ArrayList<ModelState> stateSpinnerList = CountryList.get(positionParent).getStateList();
                                                            SpinnerStateAdapter adapterState = new SpinnerStateAdapter(activity, stateSpinnerList);
                                                            adapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                            s_StateList.getBaseline();
                                                            s_StateList.setAdapter(adapterState);

                                                            s_StateList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                                                public void onItemSelected(AdapterView<?> parent, View view, int positionChild, long id) {
                                                                    if (stateSpinnerList.get(positionChild).getId() != null) {
                                                                        StateCode = stateSpinnerList.get(positionChild).getCode();
                                                                        StateId = Integer.valueOf(stateSpinnerList.get(positionChild).getId());
                                                                        if (ConstantFields.isNetworkAvailable()) {
                                                                            pb_AddressLoader.setVisibility(View.VISIBLE);
                                                                            StringRequest stringCityRequest = new StringRequest(Request.Method.GET, ConstantFields.getCityList(CountryId, String.valueOf(StateId)),
                                                                                    responseCity -> {
                                                                                        pb_AddressLoader.setVisibility(View.GONE);
                                                                                        ArrayList<String> cityList;

                                                                                        if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault") != null) {
                                                                                            if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault").equals("true")) {
                                                                                                cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                            } else {
                                                                                                cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                            }
                                                                                        } else {
                                                                                            cityList = ConstantDataParser.getCityList(responseCity, true);
                                                                                        }

                                                                                        if (cityList != null) {
                                                                                            if (cityList.size() > 0) {

                                                                                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                                                                                                        (activity, android.R.layout.simple_spinner_item, cityList);
                                                                                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                                                                                                        .simple_spinner_dropdown_item);

                                                                                                s_CityList.setAdapter(spinnerArrayAdapter);
                                                                                                s_CityList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                                                    @Override
                                                                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                                                                        CityName = cityList.get(position);
                                                                                                    }

                                                                                                    @Override
                                                                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        }

                                                                                    },
                                                                                    error -> {
                                                                                        pb_AddressLoader.setVisibility(View.GONE);
                                                                                    }) {

                                                                                @Override
                                                                                public Map<String, String> getHeaders() throws AuthFailureError {
                                                                                    Map<String, String> params = new HashMap<>();
                                                                                    params.put("Content-Type", "application/json; charset=utf-8");
                                                                                    return params;
                                                                                }
                                                                            };
                                                                            stringCityRequest.setShouldCache(false);
                                                                            ApplicationContext.getInstance().addToRequestQueue(stringCityRequest, "City");
                                                                        }
                                                                    }
                                                                }

                                                                @Override
                                                                public void onNothingSelected(AdapterView<?> arg0) {
                                                                }

                                                            });
                                                        } else {
                                                            List<String> noStates = new ArrayList<>();
                                                            noStates.add("--- None ---");

                                                            ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                            s_StateList.setAdapter(adapter_StateN);
                                                            StateCode = "0";
                                                            StateId = 0;
                                                        }
                                                    } else {
                                                        List<String> noStates = new ArrayList<>();
                                                        noStates.add("--- None ---");

                                                        ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                        s_StateList.setAdapter(adapter_StateN);
                                                        StateCode = "0";
                                                        StateId = 0;
                                                    }
                                                } else {
                                                    List<String> noStates = new ArrayList<>();
                                                    noStates.add("--- Please select country first ---");

                                                    ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                    s_StateList.setAdapter(adapter_StateN);
                                                }
                                            } else {

                                                List<String> noStates = new ArrayList<>();
                                                noStates.add("--- Please select country first ---");

                                                ArrayAdapter<String> adapter_StateN = new ArrayAdapter<>(activity, R.layout.rc_row_empty, R.id.atv_empty_message, noStates);
                                                s_StateList.setAdapter(adapter_StateN);
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {

                                        }
                                    });
                                }
                            }
                        }, error -> {
                    pb_AddressLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            showToast(jsonObject.getString("message"));
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                })

                {

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "Country");
            }
        }

        private void setupBtn() {
            btn_SaveAddress.setOnClickListener(view -> {
                if (et_FirstName.getText().toString().length() > 0
                        && et_LastName.getText().toString().length() > 0
                        && et_MobileNumber.getText().toString().length() == 10
                        && et_StreetAddress.getText().toString().length() > 0
                        && !CityName.equals("Empty")
                        && et_ZipCode.getText().toString().length() > 0

                        && CountryId != null
                        && CountryId.length() > 0

                        && StateCode != null
                        && StateCode.length() > 0) {

                    try {
                        JSONArray jsonArray = new JSONArray();

                        if (accountDetail != null) {
                            if (accountDetail.getAddressList() != null) {
                                if (accountDetail.getAddressList().size() > 0) {
                                    for (int i = 0; i < accountDetail.getAddressList().size(); i++) {
                                        if (addressType.equals("edit")) {

                                            if (AddressId == accountDetail.getAddressList().get(i).getAddressId()) {

                                                JSONArray jsonStreetArrayEdit = new JSONArray();
                                                jsonStreetArrayEdit.put(et_StreetAddress.getText().toString());

                                                JSONObject jsonEditAddress = new JSONObject();
                                                jsonEditAddress.put("firstname", et_FirstName.getText().toString());
                                                jsonEditAddress.put("id", AddressId);
                                                jsonEditAddress.put("lastname", et_LastName.getText().toString());
                                                jsonEditAddress.put("telephone", et_MobileNumber.getText().toString());
                                                jsonEditAddress.put("street", jsonStreetArrayEdit);
                                                jsonEditAddress.put("city", CityName);
                                                jsonEditAddress.put("region_id", StateId);
                                                jsonEditAddress.put("region", StateCode);
                                                jsonEditAddress.put("country_id", CountryId);
                                                jsonEditAddress.put("postcode", et_ZipCode.getText().toString());

                                                if (cb_BillingAddress.isChecked()) {
                                                    jsonEditAddress.put("default_billing", true);
                                                } else {
                                                    if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                        jsonEditAddress.put("default_billing", true);
                                                    } else {
                                                        jsonEditAddress.put("default_billing", false);
                                                    }
                                                }
                                                if (cb_ShippingAddress.isChecked()) {
                                                    jsonEditAddress.put("default_shipping", true);
                                                } else {
                                                    if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                        jsonEditAddress.put("default_billing", true);
                                                    } else {
                                                        jsonEditAddress.put("default_shipping", false);
                                                    }
                                                }

                                                jsonArray.put(jsonEditAddress);

                                            } else {

                                                JSONArray jsonStreetArrayDefault = new JSONArray();
                                                jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                                                JSONObject jsonAddress = new JSONObject();
                                                jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                                                jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                                                jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                                                jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                                                jsonAddress.put("street", jsonStreetArrayDefault);
                                                jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                                                jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                                                jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                                                jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                                                jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());

                                                if (!cb_BillingAddress.isChecked()) {
                                                    if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                        jsonAddress.put("default_billing", true);
                                                    } else {
                                                        jsonAddress.put("default_billing", false);
                                                    }
                                                } else {
                                                    jsonAddress.put("default_billing", false);
                                                }
                                                if (!cb_ShippingAddress.isChecked()) {
                                                    if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                                                        jsonAddress.put("default_shipping", true);
                                                    } else {
                                                        jsonAddress.put("default_shipping", false);
                                                    }
                                                } else {
                                                    jsonAddress.put("default_shipping", false);
                                                }

                                                jsonArray.put(jsonAddress);

                                            }
                                        } else {

                                            JSONArray jsonStreetArrayDefault = new JSONArray();
                                            jsonStreetArrayDefault.put(accountDetail.getAddressList().get(i).getStreet());

                                            JSONObject jsonAddress = new JSONObject();
                                            jsonAddress.put("firstname", accountDetail.getAddressList().get(i).getFirstName());
                                            jsonAddress.put("lastname", accountDetail.getAddressList().get(i).getLastName());
                                            jsonAddress.put("id", accountDetail.getAddressList().get(i).getAddressId());
                                            jsonAddress.put("telephone", accountDetail.getAddressList().get(i).getTelephone());
                                            jsonAddress.put("street", jsonStreetArrayDefault);
                                            jsonAddress.put("city", accountDetail.getAddressList().get(i).getCity());
                                            jsonAddress.put("region_id", accountDetail.getAddressList().get(i).getRegionId());
                                            jsonAddress.put("region", accountDetail.getAddressList().get(i).getRegionCode());
                                            jsonAddress.put("country_id", accountDetail.getAddressList().get(i).getCountryId());
                                            jsonAddress.put("postcode", accountDetail.getAddressList().get(i).getPost_code());

                                            if (!cb_BillingAddress.isChecked()) {
                                                if (accountDetail.getAddressList().get(i).isBillingAddress()) {
                                                    jsonAddress.put("default_billing", true);
                                                } else {
                                                    jsonAddress.put("default_billing", false);
                                                }
                                            } else {
                                                jsonAddress.put("default_billing", false);
                                            }
                                            if (!cb_ShippingAddress.isChecked()) {
                                                if (accountDetail.getAddressList().get(i).isShippingAddress()) {
                                                    jsonAddress.put("default_shipping", true);
                                                } else {
                                                    jsonAddress.put("default_shipping", false);
                                                }
                                            } else {
                                                jsonAddress.put("default_shipping", false);
                                            }

                                            jsonArray.put(jsonAddress);

                                        }
                                    }
                                }
                            }
                        }

                        if (addressType.equals("New")) {

                            JSONArray jsonStreetArrayNew = new JSONArray();
                            jsonStreetArrayNew.put(et_StreetAddress.getText().toString());

                            JSONObject jsonNewAddress = new JSONObject();
                            jsonNewAddress.put("firstname", et_FirstName.getText().toString());
                            jsonNewAddress.put("lastname", et_LastName.getText().toString());
                            jsonNewAddress.put("telephone", et_MobileNumber.getText().toString());
                            jsonNewAddress.put("street", jsonStreetArrayNew);
                            jsonNewAddress.put("city", CityName);
                            jsonNewAddress.put("region_id", StateId);
                            jsonNewAddress.put("region", StateCode);
                            jsonNewAddress.put("country_id", CountryId);
                            jsonNewAddress.put("postcode", et_ZipCode.getText().toString());

                            if (cb_BillingAddress.isChecked()) {
                                jsonNewAddress.put("default_billing", true);
                            } else {
                                jsonNewAddress.put("default_billing", true);
                            }
                            if (cb_ShippingAddress.isChecked()) {
                                jsonNewAddress.put("default_shipping", true);
                            } else {
                                jsonNewAddress.put("default_shipping", true);
                            }

                            jsonArray.put(jsonNewAddress);
                        }

                        JSONObject jsonObjectCustomer = new JSONObject();
                        jsonObjectCustomer.put("firstname", accountDetail.getFirstName());
                        jsonObjectCustomer.put("lastname", accountDetail.getLastName());
                        jsonObjectCustomer.put("email", accountDetail.getEmailId());
                        jsonObjectCustomer.put("websiteId", 0);
                        jsonObjectCustomer.put("addresses", jsonArray);

                        JSONObject jsonFinalObject = new JSONObject();
                        jsonFinalObject.put("customer", jsonObjectCustomer);

                        new AsyncTaskForPost().execute(jsonFinalObject.toString());

                        Log.e("onClick: ", jsonFinalObject.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("onClick: ", e.toString());
                    }

                }

                if (et_FirstName.getText().toString().equals("")) {
                    atv_FirstNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_FirstNameError.setVisibility(View.GONE);
                }
                if (et_LastName.getText().toString().equals("")) {
                    atv_LastNameError.setVisibility(View.VISIBLE);
                } else {
                    atv_LastNameError.setVisibility(View.GONE);
                }
                if (et_MobileNumber.getText().toString().equals("")) {
                    atv_MobileNumberError.setVisibility(View.VISIBLE);
                } else {
                    if (et_MobileNumber.getText().toString().length() == 10) {
                        atv_MobileNumberError.setVisibility(View.GONE);
                    } else {
                        atv_MobileNumberError.setVisibility(View.VISIBLE);
                    }
                }
                if (et_StreetAddress.getText().toString().equals("")) {
                    atv_StreetAddressError.setVisibility(View.VISIBLE);
                } else {
                    atv_StreetAddressError.setVisibility(View.GONE);
                }
                if (CityName.equals("Empty")) {
                    atv_CityError.setVisibility(View.VISIBLE);
                } else {
                    atv_CityError.setVisibility(View.GONE);
                }

                if (et_ZipCode.getText().toString().equals("")) {
                    atv_ZipCodeError.setVisibility(View.VISIBLE);
                } else {
                    atv_ZipCodeError.setVisibility(View.GONE);
                }

                if (CountryId != null) {
                    if (CountryId.length() > 0) {
                        atv_CountryError.setVisibility(View.GONE);
                    } else {
                        atv_CountryError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_CountryError.setVisibility(View.VISIBLE);
                }

                if (StateCode != null) {
                    if (StateCode.length() > 0) {
                        atv_StateError.setVisibility(View.GONE);
                    } else {
                        atv_StateError.setVisibility(View.VISIBLE);
                    }
                } else {
                    atv_StateError.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public void onStart() {
            super.onStart();
            // safety check
            if (getDialog() == null)
                return;

            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;

            if (getDialog().getWindow() != null)
                getDialog().getWindow().setLayout(width, height);
        }

        private void showToast(String message) {
            ConstantFields.CustomToast(message, activity);
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(AppLanguageSupport.onAttach(context));
            activity = (Activity) context;
            paymentRetry = (PaymentRetry) context;
        }

        @SuppressLint("StaticFieldLeak")
        private class AsyncTaskForPost extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pb_AddressLoader.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")));
                    Log.d("POST url", url.toString());
                    HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                    mUrlConnection.setRequestMethod("PUT");
                    mUrlConnection.setRequestProperty("Content-Type", "application/json");
                    mUrlConnection.setRequestProperty("Authorization", "Bearer " + ConstantDataSaver.
                            mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag));

                    mUrlConnection.setDoInput(true);
                    mUrlConnection.setDoOutput(true);
                    mUrlConnection.setReadTimeout(15000);
                    OutputStream mOutputStream = mUrlConnection.getOutputStream();

                    BufferedWriter mBufferedWriter = new BufferedWriter(new OutputStreamWriter(mOutputStream, "UTF-8"));
                    mBufferedWriter.write(params[0]);
                    mBufferedWriter.close();
                    mOutputStream.close();

                    String mResponse;
                    if (mUrlConnection.getInputStream() != null) {
                        mResponse = inputStreamToStringConversion(new BufferedReader(new InputStreamReader(mUrlConnection.getInputStream())));
                    } else {
                        mResponse = "Error";
                    }
                    Log.e("doInBackground: ", mResponse);
                    return mResponse;
                } catch (Exception e) {
                    Log.e("doInBackground: ", e.toString());
                    return "Error";
                }
            }

            @Override
            protected void onPostExecute(String s) {
                pb_AddressLoader.setVisibility(View.GONE);
                if (!s.equals("Error")) {
                    paymentRetry.retry();
                } else {
                    paymentRetry.failed();
                }
                dismiss();
            }

            @Nullable
            String inputStreamToStringConversion(BufferedReader bufferedReader) {
                try {
                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        result.append(line);
                    }
                    return result.toString();
                } catch (Exception e) {
                    return null;
                }
            }
        }

        private void fontSetup() {
            fontSetup(et_FirstName, Constant.LIGHT, Constant.C_EDIT_TEXT);
            fontSetup(et_LastName, Constant.LIGHT, Constant.C_EDIT_TEXT);
            fontSetup(et_MobileNumber, Constant.LIGHT, Constant.C_EDIT_TEXT);
            fontSetup(et_StreetAddress, Constant.LIGHT, Constant.C_EDIT_TEXT);
            fontSetup(et_ZipCode, Constant.LIGHT, Constant.C_EDIT_TEXT);

            fontSetup(atv_FirstNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_LastNameError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_MobileNumberError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_StreetAddressError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_CityError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_StateError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_CountryError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_ZipCodeError, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_AddressTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_ContactTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_FirstNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_LastNameTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_MobileNoTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_AddressInfoTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_StreetAddressTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_CityTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_StateTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_CountryTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);
            fontSetup(atv_ZipCodeTitle, Constant.LIGHT, Constant.C_TEXT_VIEW);

            fontSetup(cb_BillingAddress, Constant.LIGHT, Constant.C_CHECKBOX);
            fontSetup(cb_ShippingAddress, Constant.LIGHT, Constant.C_CHECKBOX);

            fontSetup(btn_SaveAddress, Constant.LIGHT, Constant.C_BUTTON);
        }

        private void fontSetup(Object o_SetFont, int type, int which) {
            Constant.SetFontStyle(o_SetFont, type, which);
        }

    }

    private void orderSuccessChecking() {
        StringRequest stringRequestPaymentStatus = new StringRequest(Request.Method.GET, ConstantFields.getPaymentStatus(String.valueOf(orderId)),
                response -> {
                    if (response != null) {
                        String responseTemp = response.replace("\"", "");
                        if (responseTemp.trim().toLowerCase().equals("success")) {
                            CustomerCartListLoader();
                            showToast(getString(R.string.order_placed));
                        } else {
                            failed();
                        }
                    } else {
                        failed();
                    }
                }, error -> {
            failed();
        });
        stringRequestPaymentStatus.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequestPaymentStatus, "PaymentStatus");
    }

}

