package com.balleh.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.activities.user.ActivityLogin;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.db.CheckOut;
import com.balleh.additional.instentTransfer.CheckOutHandler;
import com.balleh.fragments.DialogLoader;
import com.balleh.fragments.checkout.FragmentCartList;
import com.balleh.fragments.checkout.FragmentCheckoutChooser;
import com.balleh.fragments.checkout.FragmentPaymentMethod;
import com.balleh.fragments.checkout.FragmentPlaceOrder;
import com.balleh.fragments.checkout.FragmentShippingMethod;

import java.util.HashMap;
import java.util.Map;

public class ActivityCheckOut extends AppCompatActivity implements CheckOutHandler {

    private String addressDetail = "Empty", orderId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        Toolbar toolbar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String type = bundle.getString("Type");
            if (type != null) {
                if (type.equals("1")) {
                    loadCartList();
                } else {
                    loadShippingMethodList();
                }
            }
            if (bundle.getString("addressDetail") != null)
                addressDetail = bundle.getString("addressDetail");

            Log.e("onCreate: ", addressDetail);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void loadCartList() {
        FragmentCartList fragmentCartList = new FragmentCartList();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_check_out_container, fragmentCartList, "CartProductList");
        fragmentTransaction.addToBackStack("CartProductList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void askPurchaseType() {
        FragmentCheckoutChooser fragmentCheckoutChooser = new FragmentCheckoutChooser();
        fragmentCheckoutChooser.show(getSupportFragmentManager(), "CheckoutChooser");
    }

    @Override
    public void loadLogin() {
        //Intent toUser = new Intent(ActivityCheckOut.this, ActivityUser.class);
        Intent toUser = new Intent(ActivityCheckOut.this, ActivityLogin.class);
        toUser.putExtra("WhatToLoad", "Login");
        startActivity(toUser);
    }

    @Override
    public void loadAddressList() {
        Intent intent = new Intent(ActivityCheckOut.this, ActivityShipPaymentAddress.class);
        startActivity(intent);
    }

    @Override
    public void loadShippingMethodList() {
        FragmentShippingMethod fragmentShippingMethod = new FragmentShippingMethod();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_check_out_container, fragmentShippingMethod, "ShippingList");
        fragmentTransaction.addToBackStack("ShippingList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadPaymentMethodList() {
        FragmentPaymentMethod fragmentPaymentMethod = new FragmentPaymentMethod();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_check_out_container, fragmentPaymentMethod, "PaymentList");
        fragmentTransaction.addToBackStack("PaymentList");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void loadPlaceOrder(String response) {
        FragmentPlaceOrder fragmentPlaceOrder = FragmentPlaceOrder.getInstance(response);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ll_check_out_container, fragmentPlaceOrder, "PlaceOrder");
        fragmentTransaction.addToBackStack("PlaceOrder");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void placeOrder(String orderId) {
        this.orderId = orderId;
        if (addressDetail.equals("Empty")) {
            deleteCheckoutDetail();
            Intent intent = new Intent(ActivityCheckOut.this, ActivityCheckOutSuccess.class);
            intent.putExtra("OrderId", orderId);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            saveToAddressBook(addressDetail);
        }
    }

    @Override
    public void loadPayment(String amount, String orderId, String paymentType) {
        Intent intent = new Intent(ActivityCheckOut.this, ActivityHyperPG.class);
        intent.putExtra("OrderId", orderId);
        intent.putExtra("Amount", amount);
        intent.putExtra("Type", paymentType);
        startActivityForResult(intent, 2000);
    }

    @Override
    public void backPressed(String type, String where) {
        switch (type) {
            case "ToShipping":
                if (where.equals("Payment")) {
                    onBackPressed();
                } else if (where.equals("PlaceOrder")) {
                    getSupportFragmentManager().popBackStack("ShippingList", 2);
                }
                break;
            case "ToPayment":
                onBackPressed();
                break;
            case "ToAddress":
                finish();
                break;
        }
    }

    @Override
    public boolean networkChecker() {
        if (ConstantFields.isNetworkAvailable()) {
            return true;
        } else {
            Intent intent = new Intent(ActivityCheckOut.this, ActivityError.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return false;
        }
    }

    @Override
    public void orderSuccess(String orderId) {
        Intent intent = new Intent(ActivityCheckOut.this, ActivityCheckOutSuccess.class);
        intent.putExtra("OrderId", orderId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2000) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {

        if (orderId.equals("0")) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                deleteCheckoutDetail();
                finish();
            }
        } else {
            toStartAgain();
        }

    }

    private void toStartAgain() {
        Intent intent = new Intent(ActivityCheckOut.this, ActivitySplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void deleteCheckoutDetail() {
        CheckOut.getInstance(ActivityCheckOut.this).deleteAddressDetail();
        CheckOut.getInstance(ActivityCheckOut.this).deletePaymentType();
        CheckOut.getInstance(ActivityCheckOut.this).deleteShippingType();
    }


    private void saveToAddressBook(final String addressDetail) {
        final DialogLoader dialogLoader = new DialogLoader();
        dialogLoader.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        dialogLoader.setCancelable(false);

        if (addressDetail != null) {
            if (ConstantFields.isNetworkAvailable()) {
                dialogLoader.show(getSupportFragmentManager(), "DialogLoader");
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                        response -> {
                            dialogLoader.dismiss();
                            Log.e("onResponse: ", response);
                            toOrderSuccess();
                        }, error -> {
                    dialogLoader.dismiss();
                    toOrderSuccess();

                }) {
                    @Override
                    public byte[] getBody() {
                        return addressDetail.getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        params.put("Authorization", "Bearer "
                                + ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(),
                                ConstantFields.mTokenTag));
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SaveBillingAddress");
            } else {
                networkError();
            }
        } else {
            toOrderSuccess();
        }
    }

    private void networkError() {
        Intent intent = new Intent(ActivityCheckOut.this, ActivityError.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void toOrderSuccess() {
        addressDetail = "Empty";
        placeOrder(orderId);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityCheckOut.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
