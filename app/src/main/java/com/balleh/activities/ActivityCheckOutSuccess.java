package com.balleh.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;

public class ActivityCheckOutSuccess extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_success);

        AppCompatTextView atv_OrderNo = findViewById(R.id.atv_order_success_order_no);
        findViewById(R.id.btn_order_continue).setOnClickListener(view -> toStartAgain());

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String orderId = bundle.getString("OrderId");
            if (orderId != null) {
                if (!orderId.equals("Error")) {
                    String orderNo = getString(R.string.order_placed_order_no_hint) + " " + orderId;
                    atv_OrderNo.setVisibility(View.VISIBLE);
                    atv_OrderNo.setText(orderNo);
                } else {
                    atv_OrderNo.setVisibility(View.GONE);
                }
            } else {
                atv_OrderNo.setVisibility(View.GONE);
            }
        } else {
            atv_OrderNo.setVisibility(View.GONE);
        }

        ConstantDataSaver.mStoreSharedPreferenceString(ActivityCheckOutSuccess.this.getApplicationContext(),
                ConstantFields.mCustomerSessionTag, "1");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        toStartAgain();
    }

    private void toStartAgain() {
        Intent intent = new Intent(ActivityCheckOutSuccess.this, ActivitySplashScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivityCheckOutSuccess.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
