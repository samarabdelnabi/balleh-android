package com.balleh.activities.user;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.activities.ActivityError;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.CustomerResponse;
import com.balleh.fragments.user.DialogSocialLogin;
import com.balleh.model.ModelAccountDetail;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivitySignUp extends AppCompatActivity implements CustomerResponse {

    private String from = "Empty";
    private ProgressBar pb_UserLoader;
    private AppCompatTextView atv_RegDOB;
    private AppCompatTextView atv_RegFirstNameError;
    private AppCompatTextView atv_RegLastNameError;
    private AppCompatTextView atv_RegMobileNumberError;
    private AppCompatTextView atv_RegEmailError1;
    private AppCompatTextView atv_RegEmailError2;
    private AppCompatTextView atv_PasswordError1;
    private AppCompatTextView atv_PasswordError2;
    private AppCompatTextView atv_ConfirmPasswordError1;
    private AppCompatTextView atv_ConfirmPasswordError2;
    private EditText et_FirstName, et_LastName, et_MobileNumber, et_EmailId, et_Password, et_ConfirmPassword;
    private String mGender = null, mDOBDate = null, first_name, last_name;
    private DatePickerDialog datePickerDialog;
    private boolean fromSocialLogin = false;

    /*Social Login*/
    /*Twitter*/
    private TwitterLoginButton mTwitterLoginButton;
    /*Facebook*/
    CallbackManager callbackManager;
    LoginButton loginButton;
    Button facebook_custom_button;
    String mUserSocialID = null, mEmail_ID = null, social_login_type = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(ActivitySignUp.this);
        callbackManager = CallbackManager.Factory.create();
        AppEventsLogger.activateApp(ActivitySignUp.this);
        setContentView(R.layout.activity_sign_up);


        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            if (bundle.getString("From") != null) {
                from = bundle.getString("From");
            }
        }

        Toolbar toolbar = findViewById(R.id.tb_main_bar);
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pb_UserLoader = findViewById(R.id.pb_user_loader);
        loginButton = findViewById(R.id.login_button);
        facebook_custom_button = findViewById(R.id.facebook_login_custom);

        List<String> permissionList = new ArrayList<>();
        permissionList.add("public_profile");
        permissionList.add("email");
        loginButton.setReadPermissions(permissionList);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                Bundle parameters = new Bundle();
                parameters.putString("fields", "email,first_name,last_name");

                GraphRequest requestEmail = new GraphRequest(loginResult.getAccessToken(), "me", parameters, null, response -> {
                    if (response != null) {
                        GraphRequest.GraphJSONObjectCallback callbackEmail = (json, response1) -> {
                            try {
                                mUserSocialID = loginResult.getAccessToken().getUserId();
                                social_login_type = "Facebook";

                                if (loginResult.getAccessToken() != null) {
                                    if (response1.getError() == null) {

                                        first_name = json.getString("first_name");
                                        last_name = json.getString("last_name");

                                        if (json.optString("email") != null && !json.optString("email").equals("")) {

                                            social_login(loginResult.getAccessToken().getUserId(), json.optString("email"), "Facebook");

                                            mEmail_ID = json.optString("email");

                                        } else {
                                            openSocialDialog(json.getString("first_name"), json.getString("last_name"), loginResult.getAccessToken().getUserId(), "Facebook");
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                showToast(e.toString());
                            }
                        };
                        callbackEmail.onCompleted(response.getJSONObject(), response);
                    }
                });

                requestEmail.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                showToast(error.getMessage());
            }
        });

        facebook_custom_button.setOnClickListener(v -> LoginManager.getInstance().logInWithReadPermissions(ActivitySignUp.this, Arrays.asList("public_profile", "email")));

        load();
    }

    @Override
    public void onBackPressed() {
        if (from.equals("Login")) {
            startActivity(new Intent(ActivitySignUp.this, ActivityLogin.class));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void load() {

        mTwitterLoginButton = findViewById(R.id.btn_twitter_social_login);
        Button btn_CreateAnAccount;
        btn_CreateAnAccount = findViewById(R.id.btn_sign_up_submit);
        AppCompatTextView atv_BackToLogin = findViewById(R.id.atv_reg_to_login);
        Spinner s_GenderList = findViewById(R.id.s_gender_list);

        mTwitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e("success: ", result.data.getUserId() + "");

                first_name = result.data.getUserName();
                last_name = result.data.getUserName();

                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(result.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> resultEmail) {
                        // Do something with the result, which provides the email address
                        if (result.data.getUserId() != 0 && resultEmail.data != null) {
                            if (resultEmail.data.length() > 0) {
                                social_login(String.valueOf(result.data.getUserId()), resultEmail.data, "Twitter");
                            } else {
                                openSocialDialog(first_name, last_name, String.valueOf(result.data.getUserId()), "Twitter");
                            }
                        } else if (result.data.getUserId() != 0 && resultEmail.data == null) {
                            openSocialDialog(first_name, last_name, String.valueOf(result.data.getUserId()), "Twitter");
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Do something on failure
                        if (result.data.getUserId() != 0) {
                            social_login(String.valueOf(result.data.getUserId()), "", "Twitter");
                        } else {
                            showToast(getString(R.string.common_error));
                        }
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                showToast(getString(R.string.common_error));
            }
        });

        et_FirstName = findViewById(R.id.et_reg_first_name);
        et_LastName = findViewById(R.id.et_reg_last_name);
        et_MobileNumber = findViewById(R.id.et_reg_mobile_no);
        et_EmailId = findViewById(R.id.et_reg_email);
        et_Password = findViewById(R.id.et_reg_password);
        et_ConfirmPassword = findViewById(R.id.et_reg_confirm_password);

        atv_RegFirstNameError = findViewById(R.id.atv_reg_error_first_name);
        atv_RegLastNameError = findViewById(R.id.tv_reg_error_last_name);
        atv_RegDOB = findViewById(R.id.atv_reg_dob);
        atv_RegEmailError1 = findViewById(R.id.atv_reg_email_error_1);
        atv_RegEmailError2 = findViewById(R.id.atv_reg_email_error_2);
        atv_PasswordError1 = findViewById(R.id.atv_reg_password_error_1);
        atv_PasswordError2 = findViewById(R.id.atv_reg_password_error_2);
        atv_ConfirmPasswordError1 = findViewById(R.id.tv_reg_confirm_password_error1);
        atv_ConfirmPasswordError2 = findViewById(R.id.tv_reg_confirm_password_error2);
        atv_RegMobileNumberError = findViewById(R.id.atv_reg_error_mobile);

        int mYear, mMonth, mDay;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(ActivitySignUp.this, (datePicker, Year, Month, Day) -> {
            mDOBDate = Day + "-" + (Month + 1) + "-" + Year;
            atv_RegDOB.setText(mDOBDate);
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(ActivitySignUp.this,
                R.array.user_reg_gender_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        s_GenderList.setAdapter(adapter);

        s_GenderList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView.getSelectedItem().toString() != null) {
                    if (adapterView.getSelectedItem().toString().length() > 0) {
                        mGender = adapterView.getSelectedItem().toString().trim();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        atv_RegDOB.setOnClickListener(view -> datePickerDialog.show());

        btn_CreateAnAccount.setOnClickListener(view -> {


            if (!et_FirstName.getText().toString().equals("")
                    && (et_FirstName.getText().toString().length() > 3)

                    && !et_LastName.getText().toString().equals("")
                    && (et_LastName.getText().toString().length() > 3)

                    && !et_EmailId.getText().toString().equals("")
                    && ConstantFields.isEmailValidator(et_EmailId.getText().toString())

                    && !et_MobileNumber.getText().toString().equals("")

                    && et_MobileNumber.getText().toString().length() == 10

                    && !et_Password.getText().toString().equals("")
                    && !et_ConfirmPassword.getText().toString().equals("")

                    && (et_Password.getText().toString().length() >= 8)
                    && (et_ConfirmPassword.getText().toString().length() >= 8)

                    && ConstantFields.isValidPassword(et_Password.getText().toString())

                    && et_Password.getText().toString().equals(et_ConfirmPassword.getText().toString())) {

                try {
                    JSONObject jsonObjectCustom = new JSONObject();
                    jsonObjectCustom.put("attribute_code", "reg_mobile_no");
                    jsonObjectCustom.put("value", et_MobileNumber.getEditableText().toString());

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObjectCustom);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("firstname", et_FirstName.getEditableText().toString());
                    jsonObject.put("lastname", et_LastName.getEditableText().toString());
                    jsonObject.put("email", et_EmailId.getEditableText().toString());
                    jsonObject.put("custom_attributes", jsonArray);
                    if (mGender != null) {
                        if (mGender.length() > 0) {
                            switch (mGender) {
                                case "Male":
                                    jsonObject.put("gender", "1");
                                    break;
                                case "Female":
                                    jsonObject.put("gender", "2");
                                    break;
                                case "Not Specified":
                                    jsonObject.put("gender", "3");
                                    break;
                            }
                        }
                    }

                    if (atv_RegDOB.getText().toString().length() > 0) {
                        jsonObject.put("dob", atv_RegDOB.getText().toString());
                    }

                    JSONObject mSignUpDetail = new JSONObject();
                    mSignUpDetail.put("customer", jsonObject);
                    mSignUpDetail.put("password", et_Password.getEditableText().toString());

                    JSONObject jsonObjectSignIn = new JSONObject();
                    jsonObjectSignIn.put("username", et_EmailId.getEditableText().toString());
                    jsonObjectSignIn.put("password", et_Password.getEditableText().toString());

                    SignUp(mSignUpDetail, jsonObjectSignIn);
                } catch (Exception e) {
                    ConstantFields.CustomToast(getString(R.string.common_error), ActivitySignUp.this);
                }
            }

            if (et_FirstName.getText().toString().equals("")) {
                atv_RegFirstNameError.setVisibility(View.VISIBLE);
            } else {
                atv_RegFirstNameError.setVisibility(View.GONE);
            }
            if (et_LastName.getText().toString().equals("")) {
                atv_RegLastNameError.setVisibility(View.VISIBLE);
            } else {
                atv_RegLastNameError.setVisibility(View.GONE);
            }
            if (et_MobileNumber.getText().toString().equals("")) {
                atv_RegMobileNumberError.setVisibility(View.VISIBLE);
            } else {
                if (et_MobileNumber.getText().toString().length() == 10) {
                    atv_RegMobileNumberError.setVisibility(View.GONE);
                } else {
                    atv_RegMobileNumberError.setVisibility(View.VISIBLE);
                }
            }
            if (et_EmailId.getText().toString().equals("")) {
                atv_RegEmailError1.setVisibility(View.VISIBLE);
            } else {
                atv_RegEmailError1.setVisibility(View.GONE);
                if (!ConstantFields.isEmailValidator(et_EmailId.getText().toString())) {
                    atv_RegEmailError2.setVisibility(View.VISIBLE);
                } else {
                    atv_RegEmailError2.setVisibility(View.GONE);
                }
            }

            if (et_Password.getText().toString().equals("")) {
                atv_PasswordError1.setVisibility(View.VISIBLE);
            } else {
                atv_PasswordError1.setVisibility(View.GONE);
                if ((et_Password.getText().toString().length() >= 8)) {
                    if (!ConstantFields.isValidPassword(et_Password.getText().toString())) {
                        atv_PasswordError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_PasswordError2.setVisibility(View.GONE);
                    }
                } else {
                    atv_PasswordError2.setVisibility(View.VISIBLE);
                }
            }

            if (et_ConfirmPassword.getText().toString().equals("")) {
                atv_ConfirmPasswordError1.setVisibility(View.VISIBLE);
            } else {
                atv_ConfirmPasswordError1.setVisibility(View.GONE);
                if ((et_ConfirmPassword.getText().toString().length() >= 8)) {
                    if (!et_Password.getText().toString().equals(et_ConfirmPassword.getText().toString())
                            && !et_ConfirmPassword.getText().toString().equals("") && !et_Password.getText().toString().equals("")) {
                        atv_ConfirmPasswordError2.setVisibility(View.VISIBLE);
                    } else {
                        atv_ConfirmPasswordError2.setVisibility(View.GONE);
                    }
                } else {
                    atv_ConfirmPasswordError2.setVisibility(View.VISIBLE);
                }
            }
        });

        atv_BackToLogin.setOnClickListener(view -> {
            Intent toSignIn = new Intent(ActivitySignUp.this, ActivityLogin.class);
            toSignIn.putExtra("From", "SignUp");
            startActivity(toSignIn);
            finish();
        });

    }

    public void SignUp(final JSONObject SignUpDetail, final JSONObject LoginDetail) {
        try {
            if (SignUpDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                Log.e("SignUp: ", SignUpDetail.toString());
                if (ConstantFields.isNetworkAvailable()) {
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConstantFields.mRegistration, SignUpDetail,
                            response -> SignIn(LoginDetail), error -> {
                        pb_UserLoader.setVisibility(View.GONE);
                        try {
                            if (error.networkResponse != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);
                                Log.e("SignUp: ", jsonObject.toString());
                                showToast(jsonObject.getString("message"));
                            } else {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        } catch (Exception e) {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    }) {

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json; charset=utf-8");
                            return params;
                        }
                    };

                    jsonObjectRequest.setShouldCache(false);
                    ApplicationContext.getInstance().addToRequestQueue(jsonObjectRequest, "SignUp");
                } else {
                    networkError();
                }
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }
    }

    private void networkError() {
        Intent intent = new Intent(ActivitySignUp.this, ActivityError.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void SignIn(final JSONObject LoginDetail) {
        try {
            if (LoginDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.mLogin,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            Log.e("onResponse(Token): ", response);

                            if (response != null) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivitySignUp.this.getApplicationContext(),
                                        ConstantFields.mTokenTag, response.replace("\"", ""));
                                loadAccountInformation();
                                showToast(getString(R.string.user_login_success));
                            } else {
                                showToast(getString(R.string.user_error_invalid_user_detail));
                            }
                        }, error -> {
                    pb_UserLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            showToast(jsonObject.getString("message"));
                            Log.e("onErrorResponse: ", jsonObject.toString());
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                }) {
                    @Override
                    public byte[] getBody() {
                        return LoginDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SignIn");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }

    }

    private void loadAccountInformation() {
        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySignUp.this.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {

            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCustomerDetail(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("onResponse: account", response + "");
                        ModelAccountDetail modelAccountDetail = ConstantDataParser.getAccountDetail(response);

                        if (modelAccountDetail != null) {
                            if (modelAccountDetail.getEmailId() != null) {
                                ConstantDataSaver.mStoreSharedPreferenceString(ActivitySignUp.this.getApplicationContext(),
                                        ConstantFields.mCustomerEmailIdTag, modelAccountDetail.getEmailId());

                                ConstantDataSaver.mStoreSharedPreferenceString(ActivitySignUp.this.getApplicationContext(),
                                        ConstantFields.mCustomerAccountTag, response);

                            }
                        }
                        finish();
                    }, error -> {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 401) {
                        ConstantDataSaver.mRemoveSharedPreferenceString(ActivitySignUp.this.getApplicationContext(), ConstantFields.mTokenTag);
                    }
                }
                finish();
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", "Bearer "
                            + ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySignUp.this.getApplicationContext(),
                            ConstantFields.mTokenTag));
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CustomerAccountInformation");
        }
    }

    private void showToast(String message) {
        ConstantFields.CustomToast(message, ActivitySignUp.this);
    }

    private void openSocialDialog(String first_name, String last_name, String user_id, String type) {
        socialLoginSessionLogout();
        DialogSocialLogin socialLogin = DialogSocialLogin.getInstance(first_name, last_name, user_id, type, null, "Email", "Sign up");
        socialLogin.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        socialLogin.setCancelable(false);
        socialLogin.show(getFragmentManager(), "SocialLogin");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else {
            mTwitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socialLoginSessionLogout();
    }

    private void socialLoginSessionLogout() {
        if (social_login_type != null)
            if (social_login_type.equals("Facebook")) {
                facebook_logout();
            } else {
                twitter_logout();
            }
    }

    private void facebook_logout() {
         /*Facebook logout*/
        LoginManager.getInstance().logOut();
    }

    private void twitter_logout() {
        /*Twitter Logout*/
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
    }

    private void social_login(String userId, String email_id, String type) {
        facebook_logout();

        try {
            JSONObject jsonObject = new JSONObject();
            if (type.equals("Facebook")) {
                jsonObject.put("socialType", "Facebook");
            } else {
                jsonObject.put("socialType", "Twitter");
            }
            if (email_id != null) {
                if (email_id.length() > 0) {
                    jsonObject.put("email", email_id);
                } else {
                    jsonObject.put("email", "");
                }
            } else {
                jsonObject.put("email", "");
            }
            jsonObject.put("socialId", userId);
            jsonObject.put("firstname", first_name);
            jsonObject.put("lastname", last_name);

            postSocialLogin(jsonObject.toString());

        } catch (Exception e) {
            showFailedMessage();
            Log.e("social_login: ", e.toString() + "");
        }

    }

    private void showFailedMessage() {
        ConstantFields.CustomToast(getString(R.string.failed), ActivitySignUp.this);
    }

    private void postSocialLogin(String request) {
        pb_UserLoader.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConstantFields.CustomerSocialLogin,
                response -> {
                    pb_UserLoader.setVisibility(View.GONE);

                    try {
                        JSONArray jsonArrayResult = new JSONArray(response);
                        JSONObject jsonObjectResult = jsonArrayResult.getJSONObject(0);

                        if (!jsonObjectResult.isNull("customer_token")) {
                            ConstantDataSaver.mStoreSharedPreferenceString(ActivitySignUp.this.getApplicationContext(),
                                    ConstantFields.mTokenTag, jsonObjectResult.getString("customer_token"));

                            if (!jsonObjectResult.isNull("mail_required")) {
                                if (jsonObjectResult.getInt("mail_required") == 1) {
                                    try {
                                        if (!jsonObjectResult.isNull("email")) {
                                            JSONObject jsonForgotPassword = new JSONObject();
                                            jsonForgotPassword.put("email", jsonObjectResult.getString("email"));
                                            jsonForgotPassword.put("template", "email_reset");
                                            jsonForgotPassword.put("websiteId", "1");
                                            fromSocialLogin = true;
                                            ForgetPassword(jsonForgotPassword);
                                        }

                                    } catch (Exception e) {
                                        showFailedMessage();
                                        finish();
                                    }
                                } else {
                                    successLogin();
                                }
                            } else {
                                successLogin();
                            }

                        }
                    } catch (Exception e) {
                        showFailedMessage();
                        finish();
                    }

                    Log.e("social_login: ", response + "");
                },
                error -> {
                    pb_UserLoader.setVisibility(View.GONE);

                    Log.e("social_login: ", error.getMessage() + "");

                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject2 = new JSONObject(responseBody);
                            Log.e("social_login: ", jsonObject2.getString("message") + "");

                        } else {
                            showFailedMessage();
                        }
                    } catch (Exception e) {
                        showFailedMessage();
                    }
                    finish();
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return request.getBytes();
            }
        };

        stringRequest.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "SocialLogin");
    }

    private void successLogin() {
        loadAccountInformation();
        showToast(getString(R.string.user_login_success));
        finish();
    }

    public void ForgetPassword(JSONObject ForgetPasswordDetail) {
        try {
            if (ForgetPasswordDetail != null) {
                pb_UserLoader.setVisibility(View.VISIBLE);
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, ConstantFields.mForgotPassword,
                        response -> {
                            pb_UserLoader.setVisibility(View.GONE);
                            if (fromSocialLogin) {
                                successLogin();
                            } else {
                                showToast(response);
                            }
                        }, error -> {

                    pb_UserLoader.setVisibility(View.GONE);
                    try {
                        if (error.networkResponse != null) {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject jsonObject = new JSONObject(responseBody);
                            showToast(jsonObject.getString("message"));
                        } else {
                            showToast(getString(R.string.user_error_invalid_user_detail));
                        }
                    } catch (Exception e) {
                        showToast(getString(R.string.user_error_invalid_user_detail));
                    }
                }) {
                    @Override
                    public byte[] getBody() {
                        return ForgetPasswordDetail.toString().getBytes();
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json; charset=utf-8");
                        return params;
                    }
                };

                stringRequest.setShouldCache(false);
                ApplicationContext.getInstance().addToRequestQueue(stringRequest, "ForgetPassword");
            }

        } catch (Exception e) {
            pb_UserLoader.setVisibility(View.GONE);
            showToast(getString(R.string.common_error));
        }
    }

    @Override
    public void getResult(String response, String source) {
        if (response.equals("Exit")) {
            socialLoginSessionLogout();
        } else {
            postSocialLogin(response);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivitySignUp.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
