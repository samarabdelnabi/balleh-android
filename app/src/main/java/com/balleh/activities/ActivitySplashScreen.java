package com.balleh.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.AppLanguageSupport;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.onesignal.OneSignal;

import java.util.HashMap;
import java.util.Map;

public class ActivitySplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Log.e("onCreate: ", ConstantDataSaver.mRetrieveSharedPreferenceString(getApplicationContext(), ConstantFields.mTokenTag));

        if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(), ConstantFields.mCustomerSessionTag).equals("Empty")) {
            if (Integer.valueOf(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                    ConstantFields.mCustomerSessionTag)) > 0
                    && Integer.valueOf(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                    ConstantFields.mCustomerSessionTag)) < 5) {
                ConstantDataSaver.mStoreSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                        ConstantFields.mCustomerSessionTag, String.valueOf(Integer.valueOf(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                                ConstantFields.mCustomerSessionTag)) + 1));
                OneSignal.sendTag("user_type", "Non-Purchased");

            } else if (Integer.valueOf(ConstantDataSaver.mRetrieveSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                    ConstantFields.mCustomerSessionTag)) == 5) {
                OneSignal.sendTag("user_type", "Purchased");
                ConstantDataSaver.mStoreSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                        ConstantFields.mCustomerSessionTag, "Empty");
            } else {
                OneSignal.sendTag("user_type", "Non-Purchased");
            }


        } else {
            OneSignal.sendTag("user_type", "Non-Purchased");
        }

        if (ConstantFields.isNetworkAvailable()) {
            loadBrandList();
        } else {
            networkError();
        }
        /*if (ConstantFields.isNetworkAvailable()) {
            loadBrandList();
        } else {
            startActivity(new Intent(ActivitySplashScreen.this, ActivityError.class));
            finish();
        }*/

    }

    private void networkError() {
        Intent intent = new Intent(ActivitySplashScreen.this, ActivityError.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void loadBrandList() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getAttributeList("Brand", ConstantFields.currentLanguage().equals("en")),
                response -> {
                    Log.e("onResponse: ", response);
                    ConstantDataSaver.mStoreSharedPreferenceString(ActivitySplashScreen.this.getApplicationContext(),
                            ConstantFields.mBrandTag, response);
                    loadCategory();
                }, error -> {
            Log.e("onErrorResponse: ", "Error");
            loadCategory();
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", ConstantFields.getGuestToken());
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "BrandListSplashScreen");
    }

    private void loadCategory() {

        if (ConstantFields.isNetworkAvailable()) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getCategoryDetail(ConstantFields.currentLanguage().equals("en")),
                    response -> {
                        Log.e("onResponse: ", response);
                        toHome(response);
                    }, error -> {
                Log.e("onErrorResponse: ", "Error");
                toHome(null);
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("Authorization", ConstantFields.getGuestToken());
                    return params;
                }
            };

            stringRequest.setShouldCache(false);
            ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CategoryListSplashScreen");
        } else {
            networkError();
        }
    }

    private void toHome(String categoryDetail) {
        Intent intent = new Intent(ActivitySplashScreen.this, ActivityHome.class);
        if (categoryDetail != null)
            intent.putExtra("Category", categoryDetail);
        startActivity(intent);
        finish();

      /*  Intent intent = new Intent(ActivitySplashScreen.this, ActivityHyperPG.class);
        intent.putExtra("OrderId", "15");
        intent.putExtra("Amount", "12");
        intent.putExtra("Type","CheckOut.com");
        startActivityForResult(intent, 2000);*/
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(AppLanguageSupport.onAttach(base));
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(
                    "ar".equals(AppLanguageSupport.getLanguage(ActivitySplashScreen.this)) ?
                            View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
    }
}
