package com.balleh.model;

import java.util.ArrayList;

public class ModelProductDetailList {

    private String sku, name, description, small_image, image, thump_image,productDetail,en_title,ar_title;
    private int price, special_price, option_required,total_count;
    private ArrayList<String[]> attribute_result;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSmall_image() {
        return small_image;
    }

    public void setSmall_image(String small_image) {
        this.small_image = small_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThump_image() {
        return thump_image;
    }

    public void setThump_image(String thump_image) {
        this.thump_image = thump_image;
    }

    public int getOption_required() {
        return option_required;
    }

    public void setOption_required(int option_required) {
        this.option_required = option_required;
    }

    public int getSpecial_price() {
        return special_price;
    }

    public void setSpecial_price(int special_price) {
        this.special_price = special_price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ArrayList<String[]> getAttribute_result() {
        return attribute_result;
    }

    public void setAttribute_result(ArrayList<String[]> attribute_result) {
        this.attribute_result = attribute_result;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }
}
