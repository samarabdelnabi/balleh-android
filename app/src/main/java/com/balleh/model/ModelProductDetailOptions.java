package com.balleh.model;

/**
 * Created by user on 7/24/2018.
 * Product Option
 */

public class ModelProductDetailOptions {
    private int optionId;
    private String optionTitle;
    private boolean isSelected;

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public String getOptionTitle() {
        return optionTitle;
    }

    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
