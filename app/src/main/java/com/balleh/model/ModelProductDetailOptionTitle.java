package com.balleh.model;

import java.util.ArrayList;

/**
 * Created by user on 7/24/2018.
 * Product Option title with list
 */

public class ModelProductDetailOptionTitle {

    private String optionTitle, optionType;
    private int optionId;
    private boolean isOptionRequired;
    private ArrayList<ModelProductDetailOptions> optionValueList;


    public String getOptionTitle() {
        return optionTitle;
    }

    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public boolean isOptionRequired() {
        return isOptionRequired;
    }

    public void setOptionRequired(boolean optionRequired) {
        isOptionRequired = optionRequired;
    }

    public ArrayList<ModelProductDetailOptions> getOptionValueList() {
        return optionValueList;
    }

    public void setOptionValueList(ArrayList<ModelProductDetailOptions> optionValueList) {
        this.optionValueList = optionValueList;
    }
}
