package com.balleh.model;

/**
 * Created by user on 7/26/2018.
 * Cart Option List
 */

public class ModelCartListOptions {
    private int option_id, option_value_id;

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(int option_value_id) {
        this.option_value_id = option_value_id;
    }
}
