package com.balleh.model;

/**
 * Created by user on 7/6/2018.
 * Rating model
 */

public class ModelRating {

    private String title;
    private boolean rated;
    private int rating_id, entity_id, store_id, selected_rating;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRating_id() {
        return rating_id;
    }

    public void setRating_id(int rating_id) {
        this.rating_id = rating_id;
    }

    public int getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(int entity_id) {
        this.entity_id = entity_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public boolean isRated() {
        return rated;
    }

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    public int getSelected_rating() {
        return selected_rating;
    }

    public void setSelected_rating(int selected_rating) {
        this.selected_rating = selected_rating;
    }
}
