package com.balleh.model;

import java.util.ArrayList;

public class ModelOrderHistory {

    private String orderId, orderStatus, orderDate, orderTotal, orderCustomerFirstName, orderCustomerLastName,
            orderShippingDescription, orderShippingPrice, orderDetail, orderPaymentMethod, orderAdditionCharges,
            orderGrandTotalExclTax, orderTaxTotal, orderSubtotal;
    private int orderListCount;
    private ArrayList<ModelOrderHistoryProductDetail> orderProductDetail;
    private ModelAddress modelAddress;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderCustomerFirstName() {
        return orderCustomerFirstName;
    }

    public void setOrderCustomerFirstName(String orderCustomerFirstName) {
        this.orderCustomerFirstName = orderCustomerFirstName;
    }

    public int getOrderListCount() {
        return orderListCount;
    }

    public void setOrderListCount(int orderListCount) {
        this.orderListCount = orderListCount;
    }

    public String getOrderCustomerLastName() {
        return orderCustomerLastName;
    }

    public void setOrderCustomerLastName(String orderCustomerLastName) {
        this.orderCustomerLastName = orderCustomerLastName;
    }

    public String getOrderShippingDescription() {
        return orderShippingDescription;
    }

    public void setOrderShippingDescription(String orderShippingDescription) {
        this.orderShippingDescription = orderShippingDescription;
    }

    public String getOrderShippingPrice() {
        return orderShippingPrice;
    }

    public void setOrderShippingPrice(String orderShippingPrice) {
        this.orderShippingPrice = orderShippingPrice;
    }

    public String getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(String orderDetail) {
        this.orderDetail = orderDetail;
    }

    public ArrayList<ModelOrderHistoryProductDetail> getOrderProductDetail() {
        return orderProductDetail;
    }

    public void setOrderProductDetail(ArrayList<ModelOrderHistoryProductDetail> orderProductDetail) {
        this.orderProductDetail = orderProductDetail;
    }

    public ModelAddress getModelAddress() {
        return modelAddress;
    }

    public void setModelAddress(ModelAddress modelAddress) {
        this.modelAddress = modelAddress;
    }

    public String getOrderPaymentMethod() {
        return orderPaymentMethod;
    }

    public void setOrderPaymentMethod(String orderPaymentMethod) {
        this.orderPaymentMethod = orderPaymentMethod;
    }

    public String getOrderGrandTotalExclTax() {
        return orderGrandTotalExclTax;
    }

    public void setOrderGrandTotalExclTax(String orderGrandTotalExclTax) {
        this.orderGrandTotalExclTax = orderGrandTotalExclTax;
    }

    public String getOrderTaxTotal() {
        return orderTaxTotal;
    }

    public void setOrderTaxTotal(String orderTaxTotal) {
        this.orderTaxTotal = orderTaxTotal;
    }

    public String getOrderSubtotal() {
        return orderSubtotal;
    }

    public void setOrderSubtotal(String orderSubtotal) {
        this.orderSubtotal = orderSubtotal;
    }

    public String getOrderAdditionCharges() {
        return orderAdditionCharges;
    }

    public void setOrderAdditionCharges(String orderAdditionCharges) {
        this.orderAdditionCharges = orderAdditionCharges;
    }
}
