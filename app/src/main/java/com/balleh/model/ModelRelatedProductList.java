package com.balleh.model;

/**
 * Created by user on 7/12/2018.
 * Related Product list
 */

public class ModelRelatedProductList {

    private String productSKU;

    public String getProductSKU() {
        return productSKU;
    }

    public void setProductSKU(String productSKU) {
        this.productSKU = productSKU;
    }
}
