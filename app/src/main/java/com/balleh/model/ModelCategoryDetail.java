package com.balleh.model;

import java.util.ArrayList;

public class ModelCategoryDetail {

    private String parentCategoryName,subcategory;
    private ArrayList<String[]> customAttributes=new ArrayList<>();

    public ArrayList<String[]> getCustomAttributes() {
        return customAttributes;
    }

    public void setCustomAttributes(ArrayList<String[]> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }
}
