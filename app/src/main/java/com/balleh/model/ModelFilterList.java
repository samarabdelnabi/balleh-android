package com.balleh.model;

import java.util.ArrayList;

/**
 * Created by user on 7/25/2018.
 * Filter title List
 */

public class ModelFilterList {

    private String attributeName, FilterTitle;
    private ArrayList<ModelFilterValue> filterValueList;
    private boolean select;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getFilterTitle() {
        return FilterTitle;
    }

    public void setFilterTitle(String filterTitle) {
        FilterTitle = filterTitle;
    }

    public ArrayList<ModelFilterValue> getFilterValueList() {
        return filterValueList;
    }

    public void setFilterValueList(ArrayList<ModelFilterValue> filterValueList) {
        this.filterValueList = filterValueList;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
