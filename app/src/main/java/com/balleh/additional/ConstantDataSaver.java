package com.balleh.additional;

import android.content.Context;
import android.content.SharedPreferences;

public class ConstantDataSaver {

    public static void mStoreSharedPreferenceString(Context context,String name,String value){
        SharedPreferences pref = context.getSharedPreferences(ConstantFields.AppTag, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        if( pref.contains(name)){
            ConstantDataSaver.mRemoveSharedPreferenceString(context,name);
            editor.putString(name, value);
            editor.apply();
        }else {
            editor.putString(name, value);
            editor.apply();
        }
    }

    public static String mRetrieveSharedPreferenceString(Context context, String name){
        SharedPreferences pref = context.getSharedPreferences(ConstantFields.AppTag,Context.MODE_PRIVATE);
        return pref.getString(name,"Empty");
    }

    public static void mRemoveSharedPreferenceString(Context context,String name){
        SharedPreferences pref = context.getSharedPreferences(ConstantFields.AppTag,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(name);
        editor.apply();
    }

}
