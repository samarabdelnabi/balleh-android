package com.balleh.additional.db.wishlist;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by user on 8/8/2018.
 * Custom DataBase
 */

@Entity(tableName = "WishListDB")
public class WishListRoom {

    @PrimaryKey(autoGenerate = true)
    private int productRow;

    @ColumnInfo(name = "product_id")
    private String ProductId;

    @ColumnInfo(name = "product_detail")
    private String ProductDetails;

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductDetails() {
        return ProductDetails;
    }

    public void setProductDetails(String productDetails) {
        ProductDetails = productDetails;
    }

    public int getProductRow() {
        return productRow;
    }

    public void setProductRow(int productRow) {
        this.productRow = productRow;
    }
}
