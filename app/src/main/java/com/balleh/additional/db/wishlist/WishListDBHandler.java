package com.balleh.additional.db.wishlist;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by user on 8/8/2018.
 * WishList Room DB Handler
 */

@Database(entities = {WishListRoom.class}, version = 1,exportSchema = false)
public abstract class WishListDBHandler extends RoomDatabase {

    public abstract WishListDao getWishList();

    private static WishListDBHandler wishListDBHandler;

    public static WishListDBHandler getDatabase(final Context context) {
        if (wishListDBHandler == null) {
            synchronized (WishListDBHandler.class) {
                if (wishListDBHandler == null) {
                    wishListDBHandler = Room.databaseBuilder(context.getApplicationContext(),
                            WishListDBHandler.class, "WishList")
                            .build();

                }
            }
        }
        return wishListDBHandler;
    }
}
