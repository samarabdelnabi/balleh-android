package com.balleh.additional.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.balleh.additional.ConstantDataParser;
import com.balleh.model.ModelProductDetailList;

import java.util.ArrayList;

public class WishList extends SQLiteOpenHelper {

    private final static String name = "db_wish_list";
    private final static int version = 1;
    private final static String TABLE_NAME_WISH_LIST = "wish_list";
    private final static String PRODUCT_SKU = "product_sku";
    private final static String PRODUCT_DETAILS = "product_details";
    private final static String CREATE_WISH_LIST = "create table " + TABLE_NAME_WISH_LIST + "(" + PRODUCT_SKU + " text primary key,"
            + PRODUCT_DETAILS + " text);";
    private final static String DROP_TABLE_WISH_LIST = "DROP TABLE IF EXISTS " + TABLE_NAME_WISH_LIST;
    private final static String DELETE_TABLE_WISH_LIST = "DELETE FROM " + TABLE_NAME_WISH_LIST;
    private final static String SELECT_VALUE_SELECT = "select ";
    private final static String SELECT_VALUE_FROM = "from ";
    private final static String SELECT_WHERE = "where";
    private Cursor cursor;
    private static WishList sInstance;

    public static synchronized WishList getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new WishList(context.getApplicationContext());
        }
        return sInstance;
    }

    private WishList(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WISH_LIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        drop_table__wish_list();
        onCreate(db);
    }

    public void add_to_wish_list(String productSku, String productDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCT_SKU, productSku);
        contentValues.put(PRODUCT_DETAILS, productDetail);
        db.insert(TABLE_NAME_WISH_LIST, null, contentValues);
    }

    public void remove_from_wish_list(String productSku) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_WISH_LIST, PRODUCT_SKU + "=" + productSku, null);
    }

    private void drop_table__wish_list() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_TABLE_WISH_LIST);
    }

    public boolean checking_wish_list(String productSku) {
        try {
            String query = SELECT_VALUE_SELECT + PRODUCT_SKU + " " + SELECT_VALUE_FROM + TABLE_NAME_WISH_LIST + " " + SELECT_WHERE + " "
                    + PRODUCT_SKU + "=" + productSku;
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(query, null);
            if (cursor != null) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    String result = cursor.getString(cursor.getColumnIndex(PRODUCT_SKU));
                    if (result != null) {
                        return true;
                    }
                }
                cursor.close();
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public ArrayList<ModelProductDetailList> getWishListProductDetail() {
        ArrayList<ModelProductDetailList> productDetailLists = new ArrayList<>();

        String query = SELECT_VALUE_SELECT + " * " + SELECT_VALUE_FROM + TABLE_NAME_WISH_LIST + "; ";
        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery(query, null);
        if (cursor != null) {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                String result = cursor.getString(cursor.getColumnIndex(PRODUCT_DETAILS));
                if (result != null) {
                    ModelProductDetailList modelProductDetail = ConstantDataParser.getWishListProduct(result);
                    if (modelProductDetail != null)
                        productDetailLists.add(modelProductDetail);
                }
            }
            cursor.close();
        }

        return productDetailLists;
    }

    public void delete_wish_list() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DELETE_TABLE_WISH_LIST);
    }

    public int getSizeWishList() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        return (int) DatabaseUtils.queryNumEntries(sqLiteDatabase, TABLE_NAME_WISH_LIST);
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
}
