package com.balleh.additional;

import android.support.annotation.Nullable;
import android.util.Log;

import com.balleh.R;
import com.balleh.model.ModelAccountDetail;
import com.balleh.model.ModelAddress;
import com.balleh.model.ModelBanner;
import com.balleh.model.ModelBrandList;
import com.balleh.model.ModelCartListOptions;
import com.balleh.model.ModelCartListProduct;
import com.balleh.model.ModelCategoryDetail;
import com.balleh.model.ModelCountry;
import com.balleh.model.ModelFilterList;
import com.balleh.model.ModelFilterValue;
import com.balleh.model.ModelHomeMenuCategory;
import com.balleh.model.ModelHomeMenuCategoryList;
import com.balleh.model.ModelOrderDetail;
import com.balleh.model.ModelOrderHistory;
import com.balleh.model.ModelOrderHistoryProductDetail;
import com.balleh.model.ModelPaymentMethod;
import com.balleh.model.ModelProductDetail;
import com.balleh.model.ModelProductDetailList;
import com.balleh.model.ModelProductDetailOptionTitle;
import com.balleh.model.ModelProductDetailOptions;
import com.balleh.model.ModelProductList;
import com.balleh.model.ModelRating;
import com.balleh.model.ModelRelatedProductList;
import com.balleh.model.ModelReviewDetails;
import com.balleh.model.ModelShippingMethod;
import com.balleh.model.ModelState;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ConstantDataParser {

    public static int getDealId(String categoryDetail) {
        try {
            JSONObject jsonObject = new JSONObject(categoryDetail);
            JSONArray jsonCategoryList = jsonObject.getJSONArray("children_data");
            if (jsonCategoryList != null) {
                if (jsonCategoryList.length() > 0) {
                    for (int i = 0; i < jsonCategoryList.length(); i++) {
                        JSONObject jsonParentCategory = jsonCategoryList.getJSONObject(i);
                        if (!jsonParentCategory.isNull("is_active")) {
                            if (jsonParentCategory.getBoolean("is_active")) {
                                String name;
                                if (!jsonParentCategory.isNull("name")) {
                                    name = jsonParentCategory.getString("name");
                                    if (name != null) {
                                        if (name.length() > 0) {
                                            if (name.toLowerCase().equals("deals") || name.toLowerCase().contains("deals")) {
                                                if (!jsonParentCategory.isNull("id")) {
                                                    return jsonParentCategory.getInt("id");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    @Nullable
    public static ArrayList<ModelHomeMenuCategoryList> getCategoryList(String categoryDetail) {

        try {
            ArrayList<ModelHomeMenuCategoryList> homeMenuFullList = new ArrayList<>();
            ModelHomeMenuCategoryList modelHomeMenuCategoryList;
            ModelHomeMenuCategory homeMenuParent, homeMenuParentDummy, homeMenuSubCategory;
            ArrayList<ModelHomeMenuCategory> subCategoryList;

            JSONObject jsonObject = new JSONObject(categoryDetail);
            JSONArray jsonCategoryList = jsonObject.getJSONArray("children_data");

            if (jsonCategoryList != null) {
                if (jsonCategoryList.length() > 0) {

                    for (int i = 0; i < jsonCategoryList.length(); i++) {
                        JSONObject jsonParentCategory = jsonCategoryList.getJSONObject(i);
                        homeMenuParent = new ModelHomeMenuCategory();
                        homeMenuParentDummy = new ModelHomeMenuCategory();
                        modelHomeMenuCategoryList = new ModelHomeMenuCategoryList();

                        if (!jsonParentCategory.isNull("is_active")) {
                            if (jsonParentCategory.getBoolean("is_active")) {
                                if (!jsonParentCategory.isNull("name")) {
                                    String Name = jsonParentCategory.getString("name");
                                    if (!Name.toLowerCase().equals("deals") && !Name.toLowerCase().equals("subscription") && !Name.toLowerCase().equals("brands")) {
                                        if (!jsonParentCategory.isNull("id")) {
                                            homeMenuParent.setId(jsonParentCategory.getString("id"));
                                            homeMenuParentDummy.setId(jsonParentCategory.getString("id"));
                                        } else {
                                            homeMenuParent.setId("-1");
                                            homeMenuParentDummy.setId("-1");
                                        }
                                        if (!jsonParentCategory.isNull("parent_id")) {
                                            homeMenuParent.setParentId(jsonParentCategory.getString("parent_id"));
                                            homeMenuParentDummy.setParentId(jsonParentCategory.getString("parent_id"));
                                        } else {
                                            homeMenuParent.setParentId("-1");
                                            homeMenuParentDummy.setParentId("-1");
                                        }
                                        if (!jsonParentCategory.isNull("name")) {
                                            homeMenuParent.setName(jsonParentCategory.getString("name"));
                                            homeMenuParentDummy.setName("All " + jsonParentCategory.getString("name"));
                                        } else {
                                            homeMenuParent.setName("-1");
                                            homeMenuParentDummy.setName("-1");
                                        }
                                        if (!jsonParentCategory.isNull("product_count")) {
                                            homeMenuParent.setProductCount(jsonParentCategory.getInt("product_count"));
                                            homeMenuParentDummy.setProductCount(jsonParentCategory.getInt("product_count"));
                                        } else {
                                            homeMenuParent.setProductCount(0);
                                            homeMenuParentDummy.setProductCount(0);
                                        }

                                        modelHomeMenuCategoryList.setHomeMenuCategory(homeMenuParent);

                                        JSONArray jsonSubCategoryList = jsonParentCategory.getJSONArray("children_data");

                                        if (jsonSubCategoryList != null) {
                                            if (jsonSubCategoryList.length() > 0) {
                                                subCategoryList = new ArrayList<>();
                                                //subCategoryList.add(homeMenuParentDummy);

                                                for (int j = 0; j < jsonSubCategoryList.length(); j++) {
                                                    JSONObject jsonSubCategory = jsonSubCategoryList.getJSONObject(j);
                                                    homeMenuSubCategory = new ModelHomeMenuCategory();

                                                    if (!jsonSubCategory.isNull("parent_id")) {
                                                        homeMenuSubCategory.setParentId(jsonSubCategory.getString("parent_id"));
                                                    } else {
                                                        homeMenuSubCategory.setParentId("-1");
                                                    }
                                                    if (!jsonSubCategory.isNull("id")) {
                                                        homeMenuSubCategory.setId(jsonSubCategory.getString("id"));
                                                    } else {
                                                        homeMenuSubCategory.setId("-1");
                                                    }
                                                    if (!jsonSubCategory.isNull("name")) {
                                                        homeMenuSubCategory.setName(jsonSubCategory.getString("name"));
                                                    } else {
                                                        homeMenuSubCategory.setName("-1");
                                                    }
                                                    if (!jsonSubCategory.isNull("product_count")) {
                                                        homeMenuSubCategory.setProductCount(jsonSubCategory.getInt("product_count"));
                                                    } else {
                                                        homeMenuSubCategory.setProductCount(0);
                                                    }

                                                    if (!homeMenuSubCategory.getId().equals(homeMenuSubCategory.getParentId())) {
                                                        subCategoryList.add(homeMenuSubCategory);
                                                    }
                                                }

                                                modelHomeMenuCategoryList.setHomeMenuCategories(subCategoryList);
                                            } else {
                                                subCategoryList = new ArrayList<>();
                                                subCategoryList.add(homeMenuParentDummy);
                                                modelHomeMenuCategoryList.setHomeMenuCategories(subCategoryList);
                                            }
                                        } else {
                                            subCategoryList = new ArrayList<>();
                                            subCategoryList.add(homeMenuParentDummy);
                                            modelHomeMenuCategoryList.setHomeMenuCategories(subCategoryList);
                                        }

                                        homeMenuFullList.add(modelHomeMenuCategoryList);
                                    }
                                }
                            }
                        }
                    }

                    return homeMenuFullList;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelProductDetailList> getProductList(String productList) {
        try {
            if (productList != null) {
                JSONObject jsonProductListObject = new JSONObject(productList);
                if (!jsonProductListObject.isNull("items")) {
                    JSONArray jsonProductList = jsonProductListObject.getJSONArray("items");
                    if (jsonProductList != null) {
                        if (jsonProductList.length() > 0) {

                            ArrayList<ModelProductDetailList> modelProductDetailLists = new ArrayList<>();
                            ModelProductDetailList productDetail;

                            for (int i = 0; i < jsonProductList.length(); i++) {
                                JSONObject singleProductList = jsonProductList.getJSONObject(i);
                                productDetail = new ModelProductDetailList();
                                if (singleProductList != null) {
                                    if (!singleProductList.isNull("sku")) {

                                        productDetail.setProductDetail(singleProductList.toString());

                                        productDetail.setSku(singleProductList.getString("sku"));

                                        if (!jsonProductListObject.isNull("total_count")) {
                                            productDetail.setTotal_count(jsonProductListObject.getInt("total_count"));
                                        } else {
                                            productDetail.setTotal_count(0);
                                        }

                                        if (!singleProductList.isNull("name")) {
                                            productDetail.setName(singleProductList.getString("name"));
                                        } else {
                                            productDetail.setName(null);
                                        }

                                        if (!singleProductList.isNull("price")) {
                                            productDetail.setPrice(singleProductList.getInt("price"));
                                        } else {
                                            productDetail.setPrice(0);
                                        }

                                        if (!singleProductList.isNull("custom_attributes")) {
                                            JSONArray jsonCustomAttribute = singleProductList.getJSONArray("custom_attributes");

                                            if (jsonCustomAttribute != null) {
                                                if (jsonCustomAttribute.length() > 0) {
                                                    ArrayList<String[]> customList = new ArrayList<>();

                                                    for (int j = 0; j < jsonCustomAttribute.length(); j++) {

                                                        JSONObject jsonCustom = jsonCustomAttribute.getJSONObject(j);

                                                        String data[] = new String[2];
                                                        String code = "a";

                                                        if (!jsonCustom.isNull("attribute_code")) {
                                                            data[0] = (jsonCustom.getString("attribute_code"));
                                                            code = data[0];
                                                        } else {
                                                            data[0] = null;
                                                        }

                                                        if (!jsonCustom.isNull("value")) {
                                                            data[1] = (jsonCustom.getString("value"));
                                                            switch (code) {
                                                                case "full_name_ar":
                                                                    productDetail.setAr_title(data[1]);
                                                                    break;
                                                                case "full_name_en":
                                                                    productDetail.setEn_title(data[1]);
                                                                    break;
                                                                default:
                                                                    productDetail.setAr_title("a");
                                                                    productDetail.setEn_title("a");
                                                                    break;
                                                            }
                                                        } else {
                                                            data[1] = null;
                                                        }

                                                        customList.add(data);
                                                    }

                                                    productDetail.setAttribute_result(customList);
                                                }
                                            }
                                        }

                                        modelProductDetailLists.add(productDetail);
                                    }
                                }
                            }

                            return modelProductDetailLists;
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ModelCategoryDetail getCategoryDetail(String categoryDetail) {
        try {

            ModelCategoryDetail modelCategoryDetail = new ModelCategoryDetail();
            JSONObject jsonObject = new JSONObject(categoryDetail);

            if (!jsonObject.isNull("name")) {
                modelCategoryDetail.setParentCategoryName(jsonObject.getString("name"));
            } else {
                modelCategoryDetail.setParentCategoryName(null);
            }

            if (!jsonObject.isNull("children")) {
                modelCategoryDetail.setSubcategory(jsonObject.getString("children"));
            } else {
                modelCategoryDetail.setSubcategory(null);
            }

            if (!jsonObject.isNull("custom_attributes")) {
                JSONArray jsonCustomAttribute = jsonObject.getJSONArray("custom_attributes");

                if (jsonCustomAttribute != null) {
                    if (jsonCustomAttribute.length() > 0) {
                        ArrayList<String[]> customList = new ArrayList<>();

                        for (int j = 0; j < jsonCustomAttribute.length(); j++) {

                            JSONObject jsonCustom = jsonCustomAttribute.getJSONObject(j);

                            String data[] = new String[2];

                            if (!jsonCustom.isNull("attribute_code")) {
                                data[0] = (jsonCustom.getString("attribute_code"));
                            } else {
                                data[0] = null;
                            }

                            if (!jsonCustom.isNull("value")) {
                                data[1] = (jsonCustom.getString("value"));
                            } else {
                                data[1] = null;
                            }

                            customList.add(data);
                        }

                        modelCategoryDetail.setCustomAttributes(customList);
                    }
                }

            } else {
                modelCategoryDetail.setCustomAttributes(null);
            }

            return modelCategoryDetail;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelHomeMenuCategory> getSubCategoryList(String subcategoryList) {

        try {
            ModelHomeMenuCategory homeMenuParent;
            ArrayList<ModelHomeMenuCategory> subCategoryList = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(subcategoryList);
            JSONArray jsonCategoryList = jsonObject.getJSONArray("children_data");

            if (jsonCategoryList != null) {
                if (jsonCategoryList.length() > 0) {

                    for (int i = 0; i < jsonCategoryList.length(); i++) {
                        JSONObject jsonParentCategory = jsonCategoryList.getJSONObject(i);
                        homeMenuParent = new ModelHomeMenuCategory();

                        if (!jsonParentCategory.isNull("is_active")) {
                            if (jsonParentCategory.getBoolean("is_active")) {

                                if (!jsonParentCategory.isNull("id")) {
                                    homeMenuParent.setId(jsonParentCategory.getString("id"));
                                } else {
                                    homeMenuParent.setId("-1");
                                }
                                if (!jsonParentCategory.isNull("parent_id")) {
                                    homeMenuParent.setParentId(jsonParentCategory.getString("parent_id"));
                                } else {
                                    homeMenuParent.setParentId("-1");
                                }
                                if (!jsonParentCategory.isNull("name")) {
                                    homeMenuParent.setName(jsonParentCategory.getString("name"));
                                } else {
                                    homeMenuParent.setName("-1");
                                }
                                if (!jsonParentCategory.isNull("product_count")) {
                                    homeMenuParent.setProductCount(jsonParentCategory.getInt("product_count"));
                                } else {
                                    homeMenuParent.setProductCount(0);
                                }

                                subCategoryList.add(homeMenuParent);
                            }
                        }
                    }

                    return subCategoryList;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ModelProductDetail getProductDetail(String productDetail) {
        try {
            ModelProductDetail productDetails = new ModelProductDetail();

            JSONObject jsonObject = new JSONObject(productDetail);

            productDetails.setProductDetail(productDetail);

            if (!jsonObject.isNull("sku")) {
                productDetails.setSku(jsonObject.getString("sku"));
            } else {
                productDetails.setSku(null);
            }
            if (!jsonObject.isNull("type_id")) {
                productDetails.setProductType(jsonObject.getString("type_id"));
            } else {
                productDetails.setProductType(null);
            }

            if (!jsonObject.isNull("name")) {
                productDetails.setTitle(jsonObject.getString("name"));
            } else {
                productDetails.setTitle(null);
            }
            if (!jsonObject.isNull("id")) {
                productDetails.setProductId(jsonObject.getInt("id"));
            } else {
                productDetails.setProductId(0);
            }

            if (!jsonObject.isNull("price")) {
                productDetails.setPrice(jsonObject.getInt("price"));
            } else {
                productDetails.setPrice(0);
            }

            if (!jsonObject.isNull("sku")) {
                productDetails.setSku(jsonObject.getString("sku"));
            } else {
                productDetails.setSku(null);
            }

            if (!jsonObject.isNull("sku")) {
                productDetails.setSku(jsonObject.getString("sku"));
            } else {
                productDetails.setSku(null);
            }


            if (!jsonObject.isNull("options")) {
                Log.e("getProductDetail: ", jsonObject.toString() + "");
                JSONArray jsonOptionList = jsonObject.getJSONArray("options");

                if (jsonOptionList != null) {
                    if (jsonOptionList.length() > 0) {

                        ArrayList<ModelProductDetailOptionTitle> optionList = new ArrayList<>();

                        for (int i = 0; i < jsonOptionList.length(); i++) {
                            ModelProductDetailOptionTitle modelProductDetailOptionTitle = new ModelProductDetailOptionTitle();
                            JSONObject jsonSingleOptionList = jsonOptionList.getJSONObject(i);

                            if (!jsonSingleOptionList.isNull("title")) {
                                modelProductDetailOptionTitle.setOptionTitle(jsonSingleOptionList.getString("title"));
                            } else {
                                modelProductDetailOptionTitle.setOptionTitle("");
                            }
                            if (!jsonSingleOptionList.isNull("option_id")) {
                                modelProductDetailOptionTitle.setOptionId(jsonSingleOptionList.getInt("option_id"));
                            } else {
                                modelProductDetailOptionTitle.setOptionId(0);
                            }
                            if (!jsonSingleOptionList.isNull("type")) {
                                modelProductDetailOptionTitle.setOptionType(jsonSingleOptionList.getString("type"));
                            } else {
                                modelProductDetailOptionTitle.setOptionType("");
                            }
                            if (!jsonSingleOptionList.isNull("is_require")) {
                                modelProductDetailOptionTitle.setOptionRequired(jsonSingleOptionList.getBoolean("is_require"));
                            } else {
                                modelProductDetailOptionTitle.setOptionRequired(false);
                            }

                            JSONArray jsonOptionValueList = jsonSingleOptionList.getJSONArray("values");
                            if (jsonOptionValueList != null) {
                                if (jsonOptionValueList.length() > 0) {

                                    ArrayList<ModelProductDetailOptions> optionValueList = new ArrayList<>();
                                    for (int j = 0; j < jsonOptionValueList.length(); j++) {
                                        JSONObject jsonOptionValue = jsonOptionValueList.getJSONObject(j);

                                        ModelProductDetailOptions modelProductDetailOptions = new ModelProductDetailOptions();

                                        modelProductDetailOptions.setSelected(false);

                                        if (!jsonOptionValue.isNull("option_type_id")) {
                                            modelProductDetailOptions.setOptionId(jsonOptionValue.getInt("option_type_id"));
                                        } else {
                                            modelProductDetailOptions.setOptionId(0);
                                        }
                                        if (!jsonOptionValue.isNull("title")) {
                                            modelProductDetailOptions.setOptionTitle(jsonOptionValue.getString("title"));
                                        } else {
                                            modelProductDetailOptions.setOptionTitle("");
                                        }

                                        optionValueList.add(modelProductDetailOptions);
                                    }
                                    modelProductDetailOptionTitle.setOptionValueList(optionValueList);
                                }
                            }

                            optionList.add(modelProductDetailOptionTitle);
                        }

                        if (optionList.size() == 1) {
                            int count = 0;
                            for (int i = 0; i < optionList.get(0).getOptionValueList().size(); i++) {
                                if (optionList.get(0).getOptionValueList().get(i).getOptionTitle().trim().toLowerCase().equals("one time")) {
                                    count = 1;
                                }
                            }
                            if (count == 1) {
                                productDetails.setSubscribable(true);
                            } else {
                                productDetails.setSubscribable(false);
                            }
                        } else {
                            productDetails.setSubscribable(false);
                        }
                        productDetails.setOptionList(optionList);
                    }
                }
            }

            if (!jsonObject.isNull("media_gallery_entries")) {

                ArrayList<String> imageList = new ArrayList<>();
                JSONArray jsonArray = jsonObject.getJSONArray("media_gallery_entries");

                if (jsonArray != null) {
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonImageList = jsonArray.getJSONObject(i);
                            if (!jsonImageList.isNull("file")) {
                                imageList.add(jsonImageList.getString("file"));
                            }
                        }
                        productDetails.setImageList(imageList);
                    }
                }
            }

            if (!jsonObject.isNull("custom_attributes")) {
                JSONArray jsonCustomAttribute = jsonObject.getJSONArray("custom_attributes");

                if (jsonCustomAttribute != null) {
                    if (jsonCustomAttribute.length() > 0) {
                        ArrayList<String[]> customList = new ArrayList<>();

                        for (int j = 0; j < jsonCustomAttribute.length(); j++) {

                            JSONObject jsonCustom = jsonCustomAttribute.getJSONObject(j);

                            String data[] = new String[2];
                            String code = "a";

                            if (!jsonCustom.isNull("attribute_code")) {
                                data[0] = (jsonCustom.getString("attribute_code"));
                                code = data[0];
                            } else {
                                data[0] = null;
                            }

                            if (!jsonCustom.isNull("value")) {
                                data[1] = (jsonCustom.getString("value"));
                                switch (code) {
                                    case "full_name_ar":
                                        productDetails.setAr_title(data[1]);
                                        break;
                                    case "full_name_en":
                                        productDetails.setEn_title(data[1]);
                                        break;
                                    default:
                                        productDetails.setAr_title("a");
                                        productDetails.setEn_title("a");
                                        break;
                                }
                            } else {
                                data[1] = null;
                            }

                            customList.add(data);
                        }

                        productDetails.setCustomAttributes(customList);
                    }
                }
            }

            return productDetails;
        } catch (Exception e) {
            return null;
        }
    }

    public static int getCartCount(String CartCount) {
        try {
            int count = 0;
            JSONArray jsonArray = new JSONArray(CartCount);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (!jsonObject.isNull("qty")) {
                        count = count + jsonObject.getInt("qty");
                    }
                }

            }
            return count;
        } catch (Exception e) {
            return 0;
        }
    }

    public static int getCartId(String CartId) {
        try {
            JSONObject jsonObject = new JSONObject(CartId);
            if (!jsonObject.isNull("id")) {
                return jsonObject.getInt("id");
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    @Nullable
    public static ArrayList<ModelCartListProduct> getCartDetails(String CartDetail) {

        try {
            JSONArray jsonArray = new JSONArray(CartDetail);
            if (jsonArray.length() > 0) {
                ArrayList<ModelCartListProduct> cartProductList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    ModelCartListProduct modelCartListProduct = new ModelCartListProduct();

                    if (!jsonObject.isNull("sku")) {
                        modelCartListProduct.setSku(jsonObject.getString("sku"));
                    } else {
                        modelCartListProduct.setSku(null);
                    }
                    if (!jsonObject.isNull("qty")) {
                        modelCartListProduct.setQuantity(jsonObject.getInt("qty"));
                    } else {
                        modelCartListProduct.setQuantity(0);
                    }
                    if (!jsonObject.isNull("name")) {
                        modelCartListProduct.setName(jsonObject.getString("name"));
                    } else {
                        modelCartListProduct.setName(null);
                    }
                    if (!jsonObject.isNull("price")) {
                        modelCartListProduct.setPrice(jsonObject.getString("price"));
                    } else {
                        modelCartListProduct.setPrice(null);
                    }
                    if (!jsonObject.isNull("product_id")) {
                        modelCartListProduct.setProductId(jsonObject.getInt("product_id"));
                    } else {
                        modelCartListProduct.setProductId(0);
                    }
                    if (!jsonObject.isNull("product_type")) {
                        modelCartListProduct.setProductType(jsonObject.getString("product_type"));
                    } else {
                        modelCartListProduct.setProductType(null);
                    }
                    /*if (!jsonObject.isNull("quote_id")) {
                        modelCartListProduct.setCartId(jsonObject.getInt("quote_id"));
                    } else {
                        modelCartListProduct.setCartId(0);
                    }*/
                    if (!jsonObject.isNull("item_id")) {
                        modelCartListProduct.setItemId(jsonObject.getInt("item_id"));
                    } else {
                        modelCartListProduct.setItemId(0);
                    }

                    if (!jsonObject.isNull("product_option")) {
                        JSONObject jsonOptionObject1 = jsonObject.getJSONObject("product_option");
                        if (!jsonOptionObject1.isNull("extension_attributes")) {
                            JSONObject jsonOptionObject2 = jsonOptionObject1.getJSONObject("extension_attributes");
                            if (jsonOptionObject2 != null) {
                                JSONArray jsonOptionList = jsonOptionObject2.getJSONArray("custom_options");
                                if (jsonOptionList != null) {
                                    if (jsonOptionList.length() > 0) {
                                        ArrayList<ModelCartListOptions> optionList = new ArrayList<>();
                                        for (int j = 0; j < jsonOptionList.length(); j++) {

                                            JSONObject jsonOption = jsonOptionList.getJSONObject(j);

                                            ModelCartListOptions modelCartListOptions = new ModelCartListOptions();

                                            if (!jsonOption.isNull("option_id")) {
                                                modelCartListOptions.setOption_id(jsonOption.getInt("option_id"));
                                            } else {
                                                modelCartListOptions.setOption_id(0);
                                            }
                                            if (!jsonOption.isNull("option_value")) {
                                                modelCartListOptions.setOption_value_id(jsonOption.getInt("option_value"));
                                            } else {
                                                modelCartListOptions.setOption_value_id(0);
                                            }
                                            optionList.add(modelCartListOptions);
                                        }

                                        modelCartListProduct.setOptionList(optionList);
                                    }
                                }
                            }
                        }
                    }


                    cartProductList.add(modelCartListProduct);
                }
                return cartProductList;
            }
            return null;
        } catch (Exception e) {
            Log.e("getCartDetails: ", e.toString());
            return null;
        }

    }

    @Nullable
    public static ArrayList<ModelCartListProduct> getSetCartIDCartDetails(String CartDetail) {

        try {
            JSONObject jsonParentObject = new JSONObject(CartDetail);
            if (!jsonParentObject.isNull("items")) {
                JSONArray jsonArray = jsonParentObject.getJSONArray("items");
                if (jsonArray.length() > 0) {
                    ArrayList<ModelCartListProduct> cartProductList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        ModelCartListProduct modelCartListProduct = new ModelCartListProduct();

                        if (!jsonObject.isNull("sku")) {
                            modelCartListProduct.setSku(jsonObject.getString("sku"));
                        } else {
                            modelCartListProduct.setSku(null);
                        }
                        if (!jsonObject.isNull("qty")) {
                            modelCartListProduct.setQuantity(jsonObject.getInt("qty"));
                        } else {
                            modelCartListProduct.setQuantity(0);
                        }
                        if (!jsonObject.isNull("name")) {
                            modelCartListProduct.setName(jsonObject.getString("name"));
                        } else {
                            modelCartListProduct.setName(null);
                        }
                        if (!jsonObject.isNull("price")) {
                            modelCartListProduct.setPrice(jsonObject.getString("price"));
                        } else {
                            modelCartListProduct.setPrice(null);
                        }
                        if (!jsonObject.isNull("product_type")) {
                            modelCartListProduct.setProductType(jsonObject.getString("product_type"));
                        } else {
                            modelCartListProduct.setProductType(null);
                        }
                        if (!jsonObject.isNull("item_id")) {
                            modelCartListProduct.setItemId(jsonObject.getInt("item_id"));
                        } else {
                            modelCartListProduct.setItemId(0);
                        }

                        cartProductList.add(modelCartListProduct);
                    }
                    return cartProductList;
                }
            }


            return null;
        } catch (Exception e) {
            return null;
        }

    }

    public static int getCartTotal(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.isNull("base_grand_total")) {
                return jsonObject.getInt("base_grand_total");
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    @Nullable
    public static ArrayList<ModelAddress> getAddressList(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            ArrayList<ModelAddress> addressList = new ArrayList<>();
            if (!jsonObject.isNull("addresses")) {
                JSONArray jsonArray = jsonObject.getJSONArray("addresses");
                if (jsonArray != null) {
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ModelAddress modelAddress = new ModelAddress();

                            String address = null;
                            JSONObject addressObject = jsonArray.getJSONObject(i);

                            modelAddress.setJsonAddress(addressObject.toString());

                            if (!addressObject.isNull("firstname")) {
                                address = addressObject.getString("firstname");
                                modelAddress.setFirstName(addressObject.getString("firstname"));
                            } else {
                                modelAddress.setFirstName(null);
                            }

                            if (!addressObject.isNull("lastname")) {
                                address = address + " " + addressObject.getString("lastname");
                                modelAddress.setLastName(addressObject.getString("lastname"));
                            } else {
                                modelAddress.setLastName(null);
                            }

                            if (!addressObject.isNull("street")) {
                                JSONArray jsonArrayStreet = addressObject.getJSONArray("street");
                                if (jsonArrayStreet != null) {
                                    if (jsonArrayStreet.length() > 0) {
                                        StringBuilder street = new StringBuilder();
                                        for (int j = 0; j < jsonArrayStreet.length(); j++) {
                                            if (jsonArrayStreet.getString(j) != null) {
                                                if (j == 0) {
                                                    street = new StringBuilder(jsonArrayStreet.getString(j));
                                                } else {
                                                    street.append(",").append(jsonArrayStreet.getString(j));
                                                }
                                                address = address + ",\n" + jsonArrayStreet.getString(j);

                                            }
                                        }
                                        modelAddress.setStreet(street.toString());
                                    }
                                }
                            }

                            if (!addressObject.isNull("city")) {
                                address = address + "," + addressObject.getString("city");
                                modelAddress.setCity(addressObject.getString("city"));
                            } else {
                                modelAddress.setCity(null);
                            }

                            if (!addressObject.isNull("region")) {
                                JSONObject jsonObjectRegion = addressObject.getJSONObject("region");
                                if (!jsonObjectRegion.isNull("region")) {
                                    address = address + ",\n" + jsonObjectRegion.getString("region");
                                    modelAddress.setRegionName(jsonObjectRegion.getString("region"));
                                } else {
                                    modelAddress.setRegionName(null);
                                }
                            }

                            if (!addressObject.isNull("postcode")) {
                                address = address + ",\n" + addressObject.getString("postcode");
                                modelAddress.setPost_code(addressObject.getString("postcode"));
                            } else {
                                modelAddress.setPost_code(null);
                            }

                            if (!addressObject.isNull("country_id")) {
                                address = address + "," + addressObject.getString("country_id");
                                modelAddress.setCountryId(addressObject.getString("country_id"));
                            } else {
                                modelAddress.setCountryId(null);
                            }

                            if (!addressObject.isNull("telephone")) {
                                address = address + ",\n" + addressObject.getString("telephone");
                                modelAddress.setTelephone(addressObject.getString("telephone"));
                            } else {
                                modelAddress.setTelephone(null);
                            }

                            if (!addressObject.isNull("customer_id")) {
                                modelAddress.setCustomerId(addressObject.getInt("customer_id"));
                            } else {
                                modelAddress.setCustomerId(0);
                            }
                            if (!addressObject.isNull("id")) {
                                modelAddress.setAddressId(addressObject.getInt("id"));
                            } else {
                                modelAddress.setAddressId(0);
                            }
                            if (!addressObject.isNull("default_shipping")) {
                                modelAddress.setShippingAddress(addressObject.getBoolean("default_shipping"));
                            } else {
                                modelAddress.setShippingAddress(false);
                            }
                            if (!addressObject.isNull("default_billing")) {
                                modelAddress.setBillingAddress(addressObject.getBoolean("default_billing"));
                            } else {
                                modelAddress.setBillingAddress(false);
                            }

                            modelAddress.setAddress(address);

                            addressList.add(modelAddress);
                        }

                        return addressList;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ModelAccountDetail getAccountDetail(String response) {
        try {

            ModelAccountDetail modelAccountDetail = new ModelAccountDetail();
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.isNull("id")) {
                modelAccountDetail.setCustomerId(jsonObject.getString("id"));
            } else {
                modelAccountDetail.setCustomerId(null);
            }
            if (!jsonObject.isNull("firstname")) {
                modelAccountDetail.setFirstName(jsonObject.getString("firstname"));
            } else {
                modelAccountDetail.setFirstName(null);
            }
            if (!jsonObject.isNull("lastname")) {
                modelAccountDetail.setLastName(jsonObject.getString("lastname"));
            } else {
                modelAccountDetail.setLastName(null);
            }
            if (!jsonObject.isNull("email")) {
                modelAccountDetail.setEmailId(jsonObject.getString("email"));
            } else {
                modelAccountDetail.setEmailId(null);
            }
            if (!jsonObject.isNull("gender")) {
                modelAccountDetail.setGender_id(jsonObject.getInt("gender"));
            } else {
                modelAccountDetail.setGender_id(-1);
            }
            if (!jsonObject.isNull("website_id")) {
                modelAccountDetail.setWebsiteId(jsonObject.getInt("website_id"));
            } else {
                modelAccountDetail.setWebsiteId(1);
            }
            if (!jsonObject.isNull("dob")) {
                modelAccountDetail.setDob(jsonObject.getString("dob"));
            } else {
                modelAccountDetail.setDob(null);
            }

            ArrayList<ModelAddress> addressList = new ArrayList<>();
            if (!jsonObject.isNull("addresses")) {
                JSONArray jsonArray = jsonObject.getJSONArray("addresses");
                if (jsonArray != null) {
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ModelAddress modelAddress = new ModelAddress();

                            String address = null;
                            JSONObject addressObject = jsonArray.getJSONObject(i);

                            if (!addressObject.isNull("firstname")) {
                                address = addressObject.getString("firstname");
                                modelAddress.setFirstName(addressObject.getString("firstname"));
                            } else {
                                modelAddress.setFirstName(null);
                            }

                            if (!addressObject.isNull("lastname")) {
                                address = address + "," + addressObject.getString("lastname");
                                modelAddress.setLastName(addressObject.getString("lastname"));
                            } else {
                                modelAddress.setLastName(null);
                            }

                            if (!addressObject.isNull("street")) {
                                JSONArray jsonArrayStreet = addressObject.getJSONArray("street");
                                if (jsonArrayStreet != null) {
                                    if (jsonArrayStreet.length() > 0) {
                                        StringBuilder street = new StringBuilder();
                                        for (int j = 0; j < jsonArrayStreet.length(); j++) {
                                            if (jsonArrayStreet.getString(j) != null) {
                                                if (j == 0) {
                                                    street = new StringBuilder(jsonArrayStreet.getString(j));
                                                } else {
                                                    street.append(",").append(jsonArrayStreet.getString(j));
                                                }
                                                address = address + "," + jsonArrayStreet.getString(j);

                                            }
                                        }
                                        modelAddress.setStreet(street.toString());
                                    }
                                }
                            }

                            if (!addressObject.isNull("city")) {
                                address = address + "," + addressObject.getString("city");
                                modelAddress.setCity(addressObject.getString("city"));
                            } else {
                                modelAddress.setCity(null);
                            }

                            if (!addressObject.isNull("region")) {
                                JSONObject jsonObjectRegion = addressObject.getJSONObject("region");
                                if (!jsonObjectRegion.isNull("region")) {
                                    address = address + "," + jsonObjectRegion.getString("region");
                                    modelAddress.setRegionName(jsonObjectRegion.getString("region"));
                                } else {
                                    modelAddress.setRegionName(null);
                                }

                                if (!jsonObjectRegion.isNull("region_code")) {
                                    modelAddress.setRegionCode(jsonObjectRegion.getString("region_code"));
                                } else {
                                    modelAddress.setRegionCode(null);
                                }

                                if (!jsonObjectRegion.isNull("region_id")) {
                                    modelAddress.setRegionId(jsonObjectRegion.getString("region_id"));
                                } else {
                                    modelAddress.setRegionId(null);
                                }
                            }

                            if (!addressObject.isNull("postcode")) {
                                address = address + "," + addressObject.getString("postcode");
                                modelAddress.setPost_code(addressObject.getString("postcode"));
                            } else {
                                modelAddress.setPost_code(null);
                            }

                            if (!addressObject.isNull("country_id")) {
                                address = address + "," + addressObject.getString("country_id");
                                modelAddress.setCountryId(addressObject.getString("country_id"));
                            } else {
                                modelAddress.setCountryId(null);
                            }

                            if (!addressObject.isNull("telephone")) {
                                address = address + "," + addressObject.getString("telephone");
                                modelAddress.setTelephone(addressObject.getString("telephone"));
                            } else {
                                modelAddress.setTelephone(null);
                            }

                            if (!addressObject.isNull("customer_id")) {
                                modelAddress.setCustomerId(addressObject.getInt("customer_id"));
                            } else {
                                modelAddress.setCustomerId(0);
                            }

                            if (!addressObject.isNull("id")) {
                                modelAddress.setAddressId(addressObject.getInt("id"));
                            } else {
                                modelAddress.setAddressId(0);
                            }

                            if (!addressObject.isNull("default_billing")) {
                                modelAddress.setBillingAddress(addressObject.getBoolean("default_billing"));
                            } else {
                                modelAddress.setBillingAddress(false);
                            }
                            if (!addressObject.isNull("default_shipping")) {
                                modelAddress.setShippingAddress(addressObject.getBoolean("default_shipping"));
                            } else {
                                modelAddress.setShippingAddress(false);
                            }

                            modelAddress.setAddress(address);

                            addressList.add(modelAddress);
                        }
                        modelAccountDetail.setAddressList(addressList);
                    }
                }
            }

            if (!jsonObject.isNull("custom_attributes")) {
                JSONArray jsonCustomArray = jsonObject.getJSONArray("custom_attributes");
                if (jsonCustomArray.length() > 0) {
                    for (int i = 0; i < jsonCustomArray.length(); i++) {
                        JSONObject jsonObjectCustom = jsonCustomArray.getJSONObject(i);
                        if (!jsonObjectCustom.isNull("attribute_code")) {
                            if (jsonObjectCustom.getString("attribute_code").equals("reg_mobile_no")) {
                                if (!jsonObjectCustom.isNull("value"))
                                    modelAccountDetail.setMobileNumber(jsonObjectCustom.getString("value"));
                            }
                        }
                    }
                }
            }

            return modelAccountDetail;

        } catch (Exception e) {
            return null;
        }
    }

    public static int getStoreId() {
        try {

            String response = ConstantDataSaver.mRetrieveSharedPreferenceString(ApplicationContext.getApplicationInstance(), ConstantFields.mCustomerAccountTag);

            if (!response.equals("Empty")) {
                JSONObject jsonObject = new JSONObject(response);

                if (!jsonObject.isNull("store_id")) {
                    return jsonObject.getInt("store_id");
                }
            }

            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    @Nullable
    public static ArrayList<ModelShippingMethod> getShippingMethod(String response) {

        try {
            JSONArray jsonArray = new JSONArray(response);

            if (jsonArray.length() > 0) {
                ArrayList<ModelShippingMethod> shippingList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    ModelShippingMethod modelShippingMethod = new ModelShippingMethod();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if (!jsonObject.isNull("carrier_code")) {
                        modelShippingMethod.setCarrier_code(jsonObject.getString("carrier_code"));
                    } else {
                        modelShippingMethod.setCarrier_code(null);
                    }
                    if (!jsonObject.isNull("carrier_title")) {
                        modelShippingMethod.setCarrier_title(jsonObject.getString("carrier_title"));
                    } else {
                        modelShippingMethod.setCarrier_title(null);
                    }
                    if (!jsonObject.isNull("method_code")) {
                        modelShippingMethod.setMethod_code(jsonObject.getString("method_code"));
                    } else {
                        modelShippingMethod.setMethod_code(null);
                    }
                    if (!jsonObject.isNull("method_title")) {
                        modelShippingMethod.setMethod_title(jsonObject.getString("method_title"));
                    } else {
                        modelShippingMethod.setMethod_title(null);
                    }
                    if (!jsonObject.isNull("amount")) {
                        modelShippingMethod.setAmount("SAR " + jsonObject.getInt("amount"));
                    } else {
                        modelShippingMethod.setAmount(null);
                    }
                    if (!jsonObject.isNull("available")) {
                        modelShippingMethod.setActive(jsonObject.getBoolean("available"));
                    } else {
                        modelShippingMethod.setActive(false);
                    }
                    shippingList.add(modelShippingMethod);
                }
                return shippingList;
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelPaymentMethod> getPaymentMethod(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("payment_methods");

            if (jsonArray != null) {
                if (jsonArray.length() > 0) {

                    ArrayList<ModelPaymentMethod> paymentList = new ArrayList<>();
                    ModelPaymentMethod modelPaymentMethod;

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonPaymentObject = jsonArray.getJSONObject(i);
                        if (!jsonPaymentObject.isNull("code")) {
                            if (!jsonPaymentObject.getString("code").equals("hyperpay_cc_vault")) {
                                modelPaymentMethod = new ModelPaymentMethod();

                                if (!jsonPaymentObject.isNull("code")) {
                                    modelPaymentMethod.setCode(jsonPaymentObject.getString("code"));
                                } else {
                                    modelPaymentMethod.setCode(null);
                                }

                                if (!jsonPaymentObject.isNull("title")) {
                                    modelPaymentMethod.setTitle(jsonPaymentObject.getString("title"));
                                } else {
                                    modelPaymentMethod.setTitle(null);
                                }

                                if (!ConstantDataSaver.mRetrieveSharedPreferenceString(ApplicationContext.getApplicationInstance(), ConstantFields.mTokenTag).equals("Empty")) {
                                    paymentList.add(modelPaymentMethod);
                                } else {
                                    if (!modelPaymentMethod.getCode().equals("checkout_com")) {
                                        paymentList.add(modelPaymentMethod);
                                    }
                                }
                            }
                        }
                    }
                    return paymentList;
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelOrderDetail> getOrderProductList(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.isNull("totals")) {
                JSONObject jsonItemObject = jsonObject.getJSONObject("totals");
                if (!jsonItemObject.isNull("items")) {
                    JSONArray jsonArray = jsonItemObject.getJSONArray("items");

                    if (jsonArray != null) {
                        if (jsonArray.length() > 0) {

                            ArrayList<ModelOrderDetail> orderProductList = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonProductObject = jsonArray.getJSONObject(i);
                                ModelOrderDetail modelOrderDetail = new ModelOrderDetail();

                                if (!jsonProductObject.isNull("name")) {
                                    modelOrderDetail.setName(jsonProductObject.getString("name"));
                                } else {
                                    modelOrderDetail.setName(null);
                                }

                                if (!jsonProductObject.isNull("price")) {
                                    modelOrderDetail.setPrice(jsonProductObject.getString("price"));
                                } else {
                                    modelOrderDetail.setPrice(null);
                                }

                                if (!jsonProductObject.isNull("item_id")) {
                                    modelOrderDetail.setItemId(jsonProductObject.getString("item_id"));
                                } else {
                                    modelOrderDetail.setItemId(null);
                                }

                                if (!jsonProductObject.isNull("qty")) {
                                    modelOrderDetail.setQuantity(jsonProductObject.getString("qty"));
                                } else {
                                    modelOrderDetail.setQuantity(null);
                                }

                                orderProductList.add(modelOrderDetail);

                            }
                            return orderProductList;
                        }
                    }
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelOrderDetail> getCartOrderProductList(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.isNull("totals")) {
                JSONObject jsonItemObject = jsonObject.getJSONObject("totals");
                if (!jsonItemObject.isNull("items")) {
                    JSONArray jsonArray = jsonItemObject.getJSONArray("items");

                    if (jsonArray != null) {
                        if (jsonArray.length() > 0) {

                            ArrayList<ModelOrderDetail> orderProductList = new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonProductObject = jsonArray.getJSONObject(i);
                                ModelOrderDetail modelOrderDetail = new ModelOrderDetail();

                                if (!jsonProductObject.isNull("name")) {
                                    modelOrderDetail.setName(jsonProductObject.getString("name"));
                                } else {
                                    modelOrderDetail.setName(null);
                                }

                                if (!jsonProductObject.isNull("price")) {
                                    modelOrderDetail.setPrice(jsonProductObject.getString("price"));
                                } else {
                                    modelOrderDetail.setPrice(null);
                                }

                                if (!jsonProductObject.isNull("item_id")) {
                                    modelOrderDetail.setItemId(jsonProductObject.getString("item_id"));
                                } else {
                                    modelOrderDetail.setItemId(null);
                                }

                                if (!jsonProductObject.isNull("qty")) {
                                    modelOrderDetail.setQuantity(jsonProductObject.getString("qty"));
                                } else {
                                    modelOrderDetail.setQuantity(null);
                                }

                                orderProductList.add(modelOrderDetail);

                            }
                            return orderProductList;
                        }
                    }
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<String[]> getTotalListFromQuote(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.isNull("total_segments")) {
                JSONArray jsonArray = jsonObject.getJSONArray("total_segments");

                if (jsonArray != null) {
                    if (jsonArray.length() > 0) {

                        ArrayList<String[]> getTotalList = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonProductObject = jsonArray.getJSONObject(i);
                            if (jsonProductObject.isNull("area"))
                                if (!jsonProductObject.isNull("value")) {
                                    if (jsonProductObject.getInt("value") != 0) {
                                        String[] list = new String[2];
                                        if (!jsonProductObject.isNull("title")) {
                                            list[0] = jsonProductObject.getString("title");
                                        } else {
                                            list[0] = null;
                                        }

                                        if (!jsonProductObject.isNull("value")) {
                                            list[1] = jsonProductObject.getString("value");
                                        } else {
                                            list[1] = null;
                                        }
                                        getTotalList.add(list);
                                    }
                                }
                        }

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonProductObject = jsonArray.getJSONObject(i);
                            if (!jsonProductObject.isNull("area"))
                                if (!jsonProductObject.isNull("value")) {
                                    if (jsonProductObject.getInt("value") != 0) {
                                        String[] list = new String[2];
                                        if (!jsonProductObject.isNull("title")) {
                                            list[0] = jsonProductObject.getString("title");
                                        } else {
                                            list[0] = null;
                                        }

                                        if (!jsonProductObject.isNull("value")) {
                                            list[1] = jsonProductObject.getString("value");
                                        } else {
                                            list[1] = null;
                                        }
                                        getTotalList.add(list);
                                    }
                                }
                        }

                        return getTotalList;
                    }
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static String getTotal(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.isNull("base_grand_total")) {
                return String.valueOf(jsonObject.getInt("base_grand_total"));
            }
            return "0";
        } catch (Exception e) {
            return "0";
        }
    }

    @Nullable
    public static ArrayList<ModelBanner> getBannerCategoryList(String response, int type) {
        try {

            ArrayList<ModelBanner> modelBannerList = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(response);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (!jsonObject.isNull("banner_type")) {
                        if (jsonObject.getInt("banner_type") == type) {
                            ModelBanner modelBanner = new ModelBanner();

                            if (!jsonObject.isNull("banner_type")) {
                                modelBanner.setBanner_type(jsonObject.getString("banner_type"));
                            } else {
                                modelBanner.setBanner_type(null);
                            }
                            if (!jsonObject.isNull("title")) {
                                modelBanner.setCategoryTitle(jsonObject.getString("title"));
                            } else {
                                modelBanner.setCategoryTitle(null);
                            }
                            if (!jsonObject.isNull("category_id")) {
                                modelBanner.setCategory_id(jsonObject.getString("category_id"));
                            } else {
                                modelBanner.setCategory_id(null);
                            }
                            if (!jsonObject.isNull("image_url")) {
                                modelBanner.setImage_url(jsonObject.getString("image_url"));
                            } else {
                                modelBanner.setImage_url(null);
                            }
                            if (!jsonObject.isNull("product_id")) {
                                modelBanner.setProduct_id(jsonObject.getString("product_id"));
                            } else {
                                modelBanner.setProduct_id(null);
                            }

                            modelBannerList.add(modelBanner);
                        }
                    }
                }
                return modelBannerList;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelProductList> getHomeProductList(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            if (jsonArray.length() > 0) {
                ArrayList<ModelProductList> modelHomeProductList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ModelProductList modelProductList = new ModelProductList();

                    if (!jsonObject.isNull("title")) {
                        modelProductList.setTitle(jsonObject.getString("title"));
                    } else {
                        modelProductList.setTitle(null);
                    }
                    if (!jsonObject.isNull("title_ar")) {
                        modelProductList.setTitle_ar(jsonObject.getString("title_ar"));
                    } else {
                        modelProductList.setTitle_ar(null);
                    }
                    if (!jsonObject.isNull("category_id")) {
                        modelProductList.setCategoryId(jsonObject.getInt("category_id"));
                    } else {
                        modelProductList.setCategoryId(0);
                    }
                    if (!jsonObject.isNull("product_limit")) {
                        modelProductList.setProductLimit(jsonObject.getInt("product_limit"));
                    } else {
                        modelProductList.setProductLimit(0);
                    }
                    modelHomeProductList.add(modelProductList);
                }
                return modelHomeProductList;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelCountry> getCountryList(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            ArrayList<ModelCountry> countryList = new ArrayList<>();

            if (jsonArray.length() > 0) {

                ModelCountry dummyList = new ModelCountry();
                dummyList.setName(ApplicationContext.getApplicationInstance().getString(R.string.address_country_err));
                dummyList.setId("-1");
                countryList.add(dummyList);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonCountryObject = jsonArray.getJSONObject(i);

                    ModelCountry modelCountry = new ModelCountry();
                    if (!jsonCountryObject.isNull("full_name_english")) {
                        modelCountry.setName(jsonCountryObject.getString("full_name_english"));
                    } else {
                        modelCountry.setName(null);
                    }
                    if (!jsonCountryObject.isNull("id")) {
                        modelCountry.setId(jsonCountryObject.getString("id"));
                    } else {
                        modelCountry.setId(null);
                    }
                    if (!jsonCountryObject.isNull("two_letter_abbreviation")) {
                        modelCountry.setCode(jsonCountryObject.getString("two_letter_abbreviation"));
                    } else {
                        modelCountry.setCode(null);
                    }

                    if (!jsonCountryObject.isNull("available_regions")) {
                        JSONArray jsonStateArray = jsonCountryObject.getJSONArray("available_regions");
                        if (jsonStateArray != null) {
                            if (jsonStateArray.length() > 0) {

                                ArrayList<ModelState> stateList = new ArrayList<>();
                                ModelState modelState;
                                for (int j = 0; j < jsonStateArray.length(); j++) {
                                    JSONObject jsonStateObject = jsonStateArray.getJSONObject(j);
                                    modelState = new ModelState();

                                    if (!jsonStateObject.isNull("name")) {
                                        modelState.setName(jsonStateObject.getString("name"));
                                    } else {
                                        modelState.setName(null);
                                    }
                                    if (!jsonStateObject.isNull("code")) {
                                        modelState.setCode(jsonStateObject.getString("code"));
                                    } else {
                                        modelState.setCode(null);
                                    }
                                    if (!jsonStateObject.isNull("id")) {
                                        modelState.setId(jsonStateObject.getString("id"));
                                    } else {
                                        modelState.setId(null);
                                    }

                                    stateList.add(modelState);
                                }

                                modelCountry.setStateList(stateList);
                            }
                        }
                    }

                    countryList.add(modelCountry);
                }

                return countryList;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<String> getCityList(String response, boolean type) {
        try {
            ArrayList<String> cityList = new ArrayList<>();

            JSONArray jsonCityArray = new JSONArray(response);

            if (jsonCityArray.length() > 0) {

                for (int i = 0; i < jsonCityArray.length(); i++) {
                    JSONObject jsonObjectCity = jsonCityArray.getJSONObject(i);
                    if (type) {
                        if (!jsonObjectCity.isNull("city")) {
                            cityList.add(jsonObjectCity.getString("city"));
                        }
                    } else {
                        if (!jsonObjectCity.isNull("city_ar")) {
                            cityList.add(jsonObjectCity.getString("city_ar"));
                        }
                    }
                }
            }

            return cityList;
        } catch (Exception e) {

            return null;
        }
    }

    public static int getStockQuantity(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.isNull("qty")) {
                return jsonObject.getInt("qty");
            }

            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    @Nullable
    public static ArrayList<ModelOrderHistory> getOrderList(String response) {
        try {

            ArrayList<ModelOrderHistory> orderHistories = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.isNull("items")) {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ModelOrderHistory modelOrderHistory = new ModelOrderHistory();
                        JSONObject jsonOrderObject = jsonArray.getJSONObject(i);

                        modelOrderHistory.setOrderDetail(jsonOrderObject.toString());

                        if (!jsonObject.isNull("total_count")) {
                            modelOrderHistory.setOrderListCount(jsonObject.getInt("total_count"));
                        } else {
                            modelOrderHistory.setOrderListCount(0);
                        }

                        if (!jsonOrderObject.isNull("billing_address")) {
                            JSONObject jsonAddress = jsonOrderObject.getJSONObject("billing_address");

                            if (!jsonAddress.isNull("firstname")) {
                                modelOrderHistory.setOrderCustomerFirstName(jsonAddress.getString("firstname"));
                            } else {
                                if (!jsonOrderObject.isNull("customer_firstname")) {
                                    modelOrderHistory.setOrderCustomerFirstName(jsonOrderObject.getString("customer_firstname"));
                                } else {
                                    modelOrderHistory.setOrderCustomerFirstName(null);
                                }
                            }

                            if (!jsonAddress.isNull("lastname")) {
                                modelOrderHistory.setOrderCustomerLastName(jsonAddress.getString("lastname"));
                            } else {
                                Log.e("getOrderList: ", "LastName Error");
                                if (!jsonOrderObject.isNull("customer_lastname")) {
                                    modelOrderHistory.setOrderCustomerLastName(jsonOrderObject.getString("customer_lastname"));
                                } else {
                                    modelOrderHistory.setOrderCustomerLastName(null);
                                }
                            }
                        } else {
                            if (!jsonOrderObject.isNull("customer_firstname")) {
                                modelOrderHistory.setOrderCustomerFirstName(jsonOrderObject.getString("customer_firstname"));
                            } else {
                                modelOrderHistory.setOrderCustomerFirstName(null);
                            }

                            if (!jsonOrderObject.isNull("customer_lastname")) {
                                modelOrderHistory.setOrderCustomerLastName(jsonOrderObject.getString("customer_lastname"));
                            } else {
                                modelOrderHistory.setOrderCustomerLastName(null);
                            }
                        }

                        if (!jsonOrderObject.isNull("created_at")) {
                            modelOrderHistory.setOrderDate(jsonOrderObject.getString("created_at"));
                        } else {
                            modelOrderHistory.setOrderDate(null);
                        }
                        if (!jsonOrderObject.isNull("increment_id")) {
                            modelOrderHistory.setOrderId("#" + jsonOrderObject.getString("increment_id"));
                        } else {
                            modelOrderHistory.setOrderId(null);
                        }
                        if (!jsonOrderObject.isNull("status")) {
                            modelOrderHistory.setOrderStatus(jsonOrderObject.getString("status"));
                        } else {
                            modelOrderHistory.setOrderStatus(null);
                        }
                        if (!jsonOrderObject.isNull("grand_total")) {
                            String price = "SAR " + jsonOrderObject.getString("grand_total");
                            modelOrderHistory.setOrderTotal(price);
                        } else {
                            modelOrderHistory.setOrderTotal(null);
                        }
                        if (!jsonOrderObject.isNull("shipping_amount")) {
                            String price = "SAR " + jsonOrderObject.getString("shipping_amount");
                            modelOrderHistory.setOrderShippingPrice(price);
                        } else {
                            modelOrderHistory.setOrderShippingPrice(null);
                        }
                        if (!jsonOrderObject.isNull("shipping_description")) {
                            modelOrderHistory.setOrderShippingDescription(jsonOrderObject.getString("shipping_description"));
                        } else {
                            modelOrderHistory.setOrderShippingDescription(null);
                        }

                        orderHistories.add(modelOrderHistory);
                    }
                }
            }
            return orderHistories;
        } catch (Exception e) {
            return null;
        }

    }

    @Nullable
    public static ModelOrderHistory getSingleOrderDetail(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            ModelOrderHistory modelOrderHistory = new ModelOrderHistory();

            if (!jsonObject.isNull("created_at")) {
                modelOrderHistory.setOrderDate(jsonObject.getString("created_at"));
            } else {
                modelOrderHistory.setOrderDate(null);
            }
            if (!jsonObject.isNull("increment_id")) {
                modelOrderHistory.setOrderId(jsonObject.getString("increment_id"));
            } else {
                modelOrderHistory.setOrderId(null);
            }
            if (!jsonObject.isNull("status")) {
                modelOrderHistory.setOrderStatus(jsonObject.getString("status"));
            } else {
                modelOrderHistory.setOrderStatus(null);
            }

            Double mGrandTotal = 0.0, mSubTotal = 0.0, mTax = 0.0, mShippingTotal = 0.0;

            if (!jsonObject.isNull("base_tax_amount")) {
                String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", jsonObject.getDouble("base_tax_amount"));
                mTax = jsonObject.getDouble("base_tax_amount");
                modelOrderHistory.setOrderTaxTotal(price);
            } else {
                modelOrderHistory.setOrderTaxTotal(null);
            }
            if (!jsonObject.isNull("base_grand_total")) {
                String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", jsonObject.getDouble("base_grand_total"));
                mGrandTotal = jsonObject.getDouble("base_grand_total");
                modelOrderHistory.setOrderTotal(price);
            } else {
                modelOrderHistory.setOrderTotal(null);
            }
            if (!jsonObject.isNull("base_subtotal")) {
                String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", jsonObject.getDouble("base_subtotal"));
                mSubTotal = jsonObject.getDouble("base_subtotal");
                modelOrderHistory.setOrderSubtotal(price);
            } else {
                modelOrderHistory.setOrderSubtotal(null);
            }
            if (!jsonObject.isNull("shipping_amount")) {
                if (jsonObject.getInt("shipping_amount") != 0) {
                    String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", jsonObject.getDouble("shipping_amount"));
                    modelOrderHistory.setOrderShippingPrice(price);
                    mShippingTotal = jsonObject.getDouble("shipping_amount");
                } else {
                    modelOrderHistory.setOrderShippingPrice(null);
                }
            } else {
                modelOrderHistory.setOrderShippingPrice(null);
            }


            if ((mGrandTotal - mTax) > 0) {
                String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", (mGrandTotal - mTax));
                modelOrderHistory.setOrderGrandTotalExclTax(price);
            } else {
                modelOrderHistory.setOrderGrandTotalExclTax(null);
            }

            if ((mGrandTotal - (mSubTotal + mShippingTotal + mTax)) > 0) {
                String price = "SAR " + String.format(Locale.ENGLISH, "%.2f", (mGrandTotal - (mSubTotal + mShippingTotal + mTax)));
                modelOrderHistory.setOrderAdditionCharges(price);
            } else {
                modelOrderHistory.setOrderAdditionCharges(null);
            }


            if (!jsonObject.isNull("shipping_description")) {
                modelOrderHistory.setOrderShippingDescription(jsonObject.getString("shipping_description"));
            } else {
                modelOrderHistory.setOrderShippingDescription(null);
            }

            if (!jsonObject.isNull("billing_address")) {
                JSONObject jsonAddress = jsonObject.getJSONObject("billing_address");

                ModelAddress modelAddress = new ModelAddress();

                String address = "";

                if (!jsonAddress.isNull("firstname")) {
                    modelAddress.setFirstName(jsonAddress.getString("firstname"));
                    address = jsonAddress.getString("firstname");
                    modelOrderHistory.setOrderCustomerFirstName(jsonAddress.getString("firstname"));
                } else {
                    modelAddress.setFirstName(null);
                    if (!jsonObject.isNull("customer_firstname")) {
                        modelOrderHistory.setOrderCustomerFirstName(jsonObject.getString("customer_firstname"));
                    } else {
                        modelOrderHistory.setOrderCustomerFirstName(null);
                    }
                    if (!jsonObject.isNull("customer_lastname")) {
                        modelOrderHistory.setOrderCustomerLastName(jsonObject.getString("customer_lastname"));
                    } else {
                        modelOrderHistory.setOrderCustomerLastName(null);
                    }
                }
                if (!jsonAddress.isNull("lastname")) {
                    modelAddress.setLastName(jsonAddress.getString("lastname"));
                    address = address + " " + jsonAddress.getString("lastname");
                    modelOrderHistory.setOrderCustomerLastName(jsonAddress.getString("lastname"));
                } else {
                    modelAddress.setLastName(null);
                    if (!jsonObject.isNull("customer_firstname")) {
                        modelOrderHistory.setOrderCustomerFirstName(jsonObject.getString("customer_firstname"));
                    } else {
                        modelOrderHistory.setOrderCustomerFirstName(null);
                    }
                    if (!jsonObject.isNull("customer_lastname")) {
                        modelOrderHistory.setOrderCustomerLastName(jsonObject.getString("customer_lastname"));
                    } else {
                        modelOrderHistory.setOrderCustomerLastName(null);
                    }
                }

                if (!jsonAddress.isNull("street")) {
                    JSONArray jsonArrayStreet = jsonAddress.getJSONArray("street");
                    if (jsonArrayStreet != null) {
                        if (jsonArrayStreet.length() > 0) {
                            StringBuilder street = new StringBuilder();
                            for (int j = 0; j < jsonArrayStreet.length(); j++) {
                                if (jsonArrayStreet.getString(j) != null) {
                                    if (j == 0) {
                                        street.append(jsonArrayStreet.getString(j));
                                    } else {
                                        street.append(",").append(jsonArrayStreet.getString(j));
                                    }
                                }
                            }

                            address = address + ",\n" + street.toString();

                            modelAddress.setStreet(street.toString());
                        }
                    }
                }

                if (!jsonAddress.isNull("city")) {
                    modelAddress.setCity(jsonAddress.getString("city"));
                    address = address + ",\n" + jsonAddress.getString("city");
                } else {
                    modelAddress.setCity(null);
                }
                if (!jsonAddress.isNull("country_id")) {
                    modelAddress.setCountryId(jsonAddress.getString("country_id"));
                    address = address + ",\n" + jsonAddress.getString("country_id");
                } else {
                    modelAddress.setCountryId(null);
                }
                if (!jsonAddress.isNull("postcode")) {
                    modelAddress.setPost_code(jsonAddress.getString("postcode"));
                    address = address + ",\n" + jsonAddress.getString("postcode");
                } else {
                    modelAddress.setPost_code(null);
                }
                if (!jsonAddress.isNull("telephone")) {
                    modelAddress.setTelephone(jsonAddress.getString("telephone"));
                    address = address + ",\n" + jsonAddress.getString("telephone");
                } else {
                    modelAddress.setTelephone(null);
                }

                modelAddress.setAddress(address);
                modelOrderHistory.setModelAddress(modelAddress);

            } else {
                if (!jsonObject.isNull("customer_firstname")) {
                    modelOrderHistory.setOrderCustomerFirstName(jsonObject.getString("customer_firstname"));
                } else {
                    modelOrderHistory.setOrderCustomerFirstName(null);
                }
                if (!jsonObject.isNull("customer_lastname")) {
                    modelOrderHistory.setOrderCustomerLastName(jsonObject.getString("customer_lastname"));
                } else {
                    modelOrderHistory.setOrderCustomerLastName(null);
                }
            }

            if (!jsonObject.isNull("items")) {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                if (jsonArray.length() > 0) {
                    ArrayList<ModelOrderHistoryProductDetail> productList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonProductObject = jsonArray.getJSONObject(i);
                        ModelOrderHistoryProductDetail productDetail = new ModelOrderHistoryProductDetail();

                        if (!jsonProductObject.isNull("sku")) {
                            productDetail.setSku(jsonProductObject.getString("sku"));
                        } else {
                            productDetail.setSku(null);
                        }
                        if (!jsonProductObject.isNull("name")) {
                            productDetail.setName(jsonProductObject.getString("name"));
                        } else {
                            productDetail.setName(null);
                        }
                        if (!jsonProductObject.isNull("original_price")) {
                            productDetail.setProductPrice(jsonProductObject.getString("original_price"));
                        } else {
                            productDetail.setProductPrice(null);
                        }
                        if (!jsonProductObject.isNull("base_row_total")) {
                            productDetail.setTotalPrice(jsonProductObject.getString("base_row_total"));
                        } else {
                            productDetail.setTotalPrice(null);
                        }
                        if (!jsonProductObject.isNull("qty_ordered")) {
                            productDetail.setOrderQuantity(jsonProductObject.getString("qty_ordered"));
                        } else {
                            productDetail.setOrderQuantity(null);
                        }
                        if (!jsonProductObject.isNull("qty_canceled")) {
                            productDetail.setCancelQuantity(jsonProductObject.getString("qty_canceled"));
                        } else {
                            productDetail.setCancelQuantity(null);
                        }

                        productList.add(productDetail);
                    }
                    modelOrderHistory.setOrderProductDetail(productList);

                    if (!jsonObject.isNull("payment")) {

                        JSONObject jsonObjectPaymentMethod = jsonObject.getJSONObject("payment");
                        String paymentMethodTitle = jsonObjectPaymentMethod.getString("method");

                        if (paymentMethodTitle != null) {
                            if (paymentMethodTitle.length() > 0) {
                                if (paymentMethodTitle.contains("cashon")) {
                                    JSONArray jsonPaymentArray = jsonObjectPaymentMethod.getJSONArray("additional_information");
                                    if (jsonPaymentArray.length() > 0) {
                                        modelOrderHistory.setOrderPaymentMethod(jsonPaymentArray.getString(0));
                                    } else {
                                        modelOrderHistory.setOrderPaymentMethod(null);
                                    }
                                } else {
                                    modelOrderHistory.setOrderPaymentMethod(paymentMethodTitle);
                                }
                            } else {
                                modelOrderHistory.setOrderPaymentMethod(null);
                            }
                        } else {
                            modelOrderHistory.setOrderPaymentMethod(null);
                        }
                    } else {
                        modelOrderHistory.setOrderPaymentMethod(null);
                    }
                }
            }

            return modelOrderHistory;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ModelProductDetailList getWishListProduct(String productList) {
        try {
            if (productList != null) {

                ModelProductDetailList productDetail;

                JSONObject singleProductList = new JSONObject(productList);
                productDetail = new ModelProductDetailList();

                if (!singleProductList.isNull("sku")) {
                    productDetail.setProductDetail(singleProductList.toString());
                    productDetail.setSku(singleProductList.getString("sku"));
                    if (!singleProductList.isNull("name")) {
                        productDetail.setName(singleProductList.getString("name"));
                    } else {
                        productDetail.setName(null);
                    }

                    if (!singleProductList.isNull("price")) {
                        productDetail.setPrice(singleProductList.getInt("price"));
                    } else {
                        productDetail.setPrice(0);
                    }

                    if (!singleProductList.isNull("custom_attributes")) {
                        JSONArray jsonCustomAttribute = singleProductList.getJSONArray("custom_attributes");

                        if (jsonCustomAttribute != null) {
                            if (jsonCustomAttribute.length() > 0) {
                                ArrayList<String[]> customList = new ArrayList<>();

                                for (int j = 0; j < jsonCustomAttribute.length(); j++) {

                                    JSONObject jsonCustom = jsonCustomAttribute.getJSONObject(j);

                                    String data[] = new String[2];
                                    String code = "";

                                    if (!jsonCustom.isNull("attribute_code")) {
                                        data[0] = (jsonCustom.getString("attribute_code"));
                                        code = data[0];
                                    } else {
                                        data[0] = null;
                                    }

                                    if (!jsonCustom.isNull("value")) {
                                        data[1] = (jsonCustom.getString("value"));

                                        switch (code) {
                                            case "full_name_ar":
                                                productDetail.setAr_title(data[1]);
                                                break;
                                            case "full_name_en":
                                                productDetail.setEn_title(data[1]);
                                                break;
                                            default:
                                                productDetail.setAr_title("a");
                                                productDetail.setEn_title("a");
                                                break;
                                        }
                                    } else {
                                        data[1] = null;
                                    }

                                    customList.add(data);
                                }

                                productDetail.setAttribute_result(customList);
                            }
                        }
                    }
                }

                return productDetail;

            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static String getCouponCode(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.isNull("coupon_code")) {
                return jsonObject.getString("coupon_code");
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelBrandList> getBrandList(String response) {
        try {
            if (response != null) {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("options")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("options");
                    if (jsonArray != null) {
                        if (jsonArray.length() > 0) {
                            ArrayList<ModelBrandList> brandLists = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ModelBrandList modelBrandList = new ModelBrandList();
                                JSONObject jsonAttributeObject = jsonArray.getJSONObject(i);
                                if (!jsonAttributeObject.isNull("label")) {
                                    if (jsonAttributeObject.getString("label").length() > 0) {
                                        if (!jsonAttributeObject.isNull("value")) {
                                            if (jsonAttributeObject.getString("value").length() > 0) {
                                                modelBrandList.setName(jsonAttributeObject.getString("label"));
                                                modelBrandList.setId(jsonAttributeObject.getString("value"));
                                                modelBrandList.setImagePath("https://static-3.springest.com/uploads/institute/logo/17440/thumb_tedtalks-02433a05119bcb71a49f073413eed843.png");
                                                brandLists.add(modelBrandList);
                                            }
                                        }
                                    }
                                }
                            }
                            return brandLists;
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelBrandList> getAttributeList(String response) {
        try {
            if (response != null) {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("options")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("options");
                    if (jsonArray != null) {
                        if (jsonArray.length() > 0) {
                            ArrayList<ModelBrandList> brandLists = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ModelBrandList modelBrandList = new ModelBrandList();
                                JSONObject jsonAttributeObject = jsonArray.getJSONObject(i);
                                if (!jsonAttributeObject.isNull("label")) {
                                    if (jsonAttributeObject.getString("label").length() > 0) {
                                        if (!jsonAttributeObject.isNull("value")) {
                                            if (jsonAttributeObject.getString("value").length() > 0) {
                                                modelBrandList.setName(jsonAttributeObject.getString("label"));
                                                modelBrandList.setId(jsonAttributeObject.getString("value"));
                                                modelBrandList.setImagePath("https://static-3.springest.com/uploads/institute/logo/17440/thumb_tedtalks-02433a05119bcb71a49f073413eed843.png");
                                                brandLists.add(modelBrandList);
                                            }
                                        }
                                    }
                                }
                            }
                            return brandLists;
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static String getProductImage(String response) {
        try {

            if (response != null) {
                JSONArray jsonArray = new JSONArray(response);
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if (jsonObject != null) {
                            if (!jsonObject.isNull("types")) {
                                if (jsonObject.getJSONArray("types").length() > 0) {
                                    return ConstantFields.ImageURL + jsonObject.getString("file");
                                }
                            }
                        }
                    }
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean getTotalStatus(JSONObject response) {
        try {

            JSONObject jsonObject = new JSONObject(response.toString());
            if (!jsonObject.isNull("price")) {
                if (jsonObject.getInt("price") > 0) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    public static ArrayList<ModelRating> getRatingList(String response) {
        try {
            ArrayList<ModelRating> modelRatingsList = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(response);

            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject != null) {

                        ModelRating modelRating = new ModelRating();

                        if (!jsonObject.isNull("rating_code")) {
                            modelRating.setTitle(jsonObject.getString("rating_code"));
                        } else {
                            modelRating.setTitle(null);
                        }
                        if (!jsonObject.isNull("rating_id")) {
                            modelRating.setRating_id(jsonObject.getInt("rating_id"));
                        } else {
                            modelRating.setRating_id(0);
                        }
                        if (!jsonObject.isNull("entity_id")) {
                            modelRating.setEntity_id(jsonObject.getInt("entity_id"));
                        } else {
                            modelRating.setEntity_id(0);
                        }
                        if (!jsonObject.isNull("store_id")) {
                            modelRating.setStore_id(jsonObject.getInt("store_id"));
                        } else {
                            modelRating.setStore_id(0);
                        }

                        modelRatingsList.add(modelRating);
                    }
                }
            }
            return modelRatingsList;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelReviewDetails> getReviewList(String response) {
        try {

            ArrayList<ModelReviewDetails> reviewList = new ArrayList<>();

            JSONArray jsonArrayParent = new JSONArray(response);

            if (jsonArrayParent.length() > 0) {
                JSONObject jsonObject = jsonArrayParent.getJSONObject(0);


                JSONArray jsonArray = jsonObject.getJSONArray("reviews");

                if (jsonArray != null) {
                    if (jsonArray.length() > 0) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectReview = jsonArray.getJSONObject(i);
                            ModelReviewDetails modelReviewDetails = new ModelReviewDetails();

                            if (!jsonObject.isNull("avg_rating_percent")) {
                                modelReviewDetails.setReview(jsonObject.getInt("avg_rating_percent"));
                            } else {
                                modelReviewDetails.setReview(0);
                            }

                            if (!jsonObjectReview.isNull("created_at")) {
                                DateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
                                Date d = f.parse(jsonObjectReview.getString("created_at"));
                                DateFormat date = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
                                modelReviewDetails.setDate(date.format(d) + "");
                            } else {
                                modelReviewDetails.setDate(null);
                            }
                            if (!jsonObjectReview.isNull("nickname")) {
                                modelReviewDetails.setName(jsonObjectReview.getString("nickname"));
                            } else {
                                modelReviewDetails.setName(null);
                            }
                            if (!jsonObjectReview.isNull("title")) {
                                modelReviewDetails.setTitle(jsonObjectReview.getString("title"));
                            } else {
                                modelReviewDetails.setTitle(null);
                            }
                            if (!jsonObjectReview.isNull("detail")) {
                                modelReviewDetails.setDescription(jsonObjectReview.getString("detail"));
                            } else {
                                modelReviewDetails.setDescription(null);
                            }

                            JSONArray jsonArrayRating = jsonObjectReview.getJSONArray("rating_votes");

                            if (jsonArrayRating != null) {
                                if (jsonArrayRating.length() > 0) {
                                    ArrayList<String[]> ratingList = new ArrayList<>();
                                    for (int j = 0; j < jsonArrayRating.length(); j++) {
                                        String[] list = new String[2];
                                        JSONObject jsonObjectRating = jsonArrayRating.getJSONObject(j);
                                        if (!jsonObjectRating.isNull("value")) {
                                            list[0] = String.valueOf(jsonObjectRating.getInt("value"));
                                        } else {
                                            list[0] = "0";
                                        }
                                        if (!jsonObjectRating.isNull("rating_code")) {
                                            list[1] = String.valueOf(jsonObjectRating.getString("rating_code"));
                                        } else {
                                            list[1] = "0";
                                        }
                                        ratingList.add(list);

                                        Log.e("getReviewList: ", ratingList.get(j)[0] + " : " + ratingList.get(j)[1]);
                                    }
                                    modelReviewDetails.setGetRatingList(ratingList);
                                }
                            }

                            reviewList.add(modelReviewDetails);
                        }
                    }
                }
            }
            return reviewList;
        } catch (Exception e) {
            Log.e("getReviewList: ", e.toString());
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelRelatedProductList> getRelatedProductSKU(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            if (jsonArray.length() > 0) {

                ArrayList<ModelRelatedProductList> modelRelatedProductLists = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    ModelRelatedProductList modelRelatedProductList = new ModelRelatedProductList();

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    if (jsonObject != null) {
                        if (!jsonObject.isNull("sku")) {
                            modelRelatedProductList.setProductSKU(jsonObject.getString("sku"));
                            modelRelatedProductLists.add(modelRelatedProductList);
                        }
                    }
                }

                return modelRelatedProductLists;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public static ArrayList<ModelFilterList> getFilterList(String response) {
        try {

            Log.e("getFilterList: ", response);
            ArrayList<ModelFilterList> arrayList = new ArrayList<>();
            JSONArray jsonArrayFilter = new JSONArray(response);

            if (jsonArrayFilter.length() > 0) {
                for (int i = 0; i < jsonArrayFilter.length(); i++) {

                    JSONObject jsonObjectFilter = jsonArrayFilter.getJSONObject(i);
                    ModelFilterList modelFilterList = new ModelFilterList();

                    if (!jsonObjectFilter.isNull("code")) {
                        if (!jsonObjectFilter.getString("code").equals("cat")) {

                            if (!jsonObjectFilter.isNull("code")) {
                                modelFilterList.setFilterTitle(jsonObjectFilter.getString("code"));
                            } else {
                                modelFilterList.setFilterTitle(null);
                            }
                            if (!jsonObjectFilter.isNull("label")) {
                                modelFilterList.setAttributeName(jsonObjectFilter.getString("label"));
                            } else {
                                modelFilterList.setAttributeName(null);
                            }
                            if (i == 0) {
                                modelFilterList.setSelect(true);
                            } else {
                                modelFilterList.setSelect(false);
                            }

                            JSONArray jsonArrayValue = jsonObjectFilter.getJSONArray("values");

                            if (jsonArrayValue != null) {
                                if (jsonArrayValue.length() > 0) {
                                    ArrayList<ModelFilterValue> filterValueArrayList = new ArrayList<>();
                                    for (int j = 0; j < jsonArrayValue.length(); j++) {

                                        JSONObject jsonValueObject = jsonArrayValue.getJSONObject(j);

                                        ModelFilterValue modelFilterValue = new ModelFilterValue();

                                        if (!jsonValueObject.isNull("display")) {
                                            modelFilterValue.setValueTitle(jsonValueObject.getString("display"));
                                        } else {
                                            modelFilterValue.setValueTitle(null);
                                        }
                                        if (!jsonValueObject.isNull("value")) {
                                            modelFilterValue.setValueId(jsonValueObject.getString("value"));
                                        } else {
                                            modelFilterValue.setValueId(null);
                                        }

                                        if (!jsonObjectFilter.isNull("code")) {
                                            modelFilterValue.setParentCode(jsonObjectFilter.getString("code"));
                                        } else {
                                            modelFilterValue.setParentCode(null);
                                        }

                                        filterValueArrayList.add(modelFilterValue);
                                    }
                                    modelFilterList.setFilterValueList(filterValueArrayList);
                                }
                            }

                            arrayList.add(modelFilterList);

                        }
                    }
                }
            }
            return arrayList;
        } catch (Exception e) {
            Log.e("getFilterList: ", e.toString() + "");
            return null;
        }
    }


}
