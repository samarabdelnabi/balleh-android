package com.balleh.additional.instentTransfer;

/**
 * Created by user on 10/4/2018.
 * CheckOut payment gateway
 */

public interface PaymentRetry {
    void retry();
    void failed();
}
