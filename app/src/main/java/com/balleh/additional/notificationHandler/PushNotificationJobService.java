package com.balleh.additional.notificationHandler;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.balleh.R;
import com.balleh.activities.ActivitySplashScreen;
import com.balleh.additional.ApplicationContext;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PushNotificationJobService extends JobService {

    String NOTIFICATION_CHANNEL_ID = "com_balleh_channel_id";
    String message, url;

    @Override
    public boolean onStartJob(JobParameters job) {
        Bundle mNotificationDetail = job.getExtras();
        if (mNotificationDetail != null) {
            if (mNotificationDetail.getString("ImageURL") != null && mNotificationDetail.getString("Message") != null) {
                message = mNotificationDetail.getString("Message");
                url = mNotificationDetail.getString("ImageURL");
                new ImageDownloader().execute(url);
            } else if (mNotificationDetail.getString("ImageURL") == null && mNotificationDetail.getString("Message") != null) {
                sendNotification(mNotificationDetail.getString("Message"));
            } else {
                if (mNotificationDetail.getString("Message") != null) {
                    sendNotification(mNotificationDetail.getString("Message"));
                }
            }
        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private void sendNotification(Bitmap imageUri, String message) {
        Bitmap bitmap;

        //Intent Registration
        Intent intent = new Intent(this, ActivitySplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //Take Notification Sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //Generate the Notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);

        bitmap = imageUri;

        if (bitmap != null) {
            /*Notification with Image*/
            notificationBuilder
                    .setSmallIcon(R.drawable.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(ApplicationContext.getApplicationInstance().getString(R.string.app_name))
                    .setContentText(message)
                    .setTicker(message)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmap).setSummaryText(message))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

        } else {
            /*Notification without Image*/
            notificationBuilder
                    .setSmallIcon(R.drawable.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(ApplicationContext.getApplicationInstance().getString(R.string.app_name))
                    .setContentText(message)
                    .setTicker(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentIntent(pendingIntent);

        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Create Push Notification
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());

    }

    private void sendNotification(String message) {

        //Intent Registration
        Intent intent = new Intent(this, ActivitySplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //Take Notification Sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //Generate the Notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);

        /*Notification without Image*/
        notificationBuilder
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(ApplicationContext.getApplicationInstance().getString(R.string.app_name))
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .setTicker(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Create Push Notification
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());
    }

    /*
     *To get a Bitmap image from the URL received
     * */
    @SuppressLint("StaticFieldLeak")
    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0].replace(" ", "%20"));
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

                InputStream is = httpCon.getInputStream();

                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[2048];

                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }

                byte[] image = buffer.toByteArray();
                return BitmapFactory.decodeByteArray(image, 0, image.length);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap data) {
            sendNotification(data, message);
        }
    }

}
