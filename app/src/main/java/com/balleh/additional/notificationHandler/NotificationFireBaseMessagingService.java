package com.balleh.additional.notificationHandler;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.balleh.R;
import com.balleh.activities.ActivitySplashScreen;
import com.balleh.additional.ApplicationContext;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Trigger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;

public class NotificationFireBaseMessagingService extends FirebaseMessagingService {

    String NOTIFICATION_CHANNEL_ID = "com_balleh_channel_id";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData() != null) {
            if (remoteMessage.getData().size() > 0) {
                if (remoteMessage.getData().get("image") != null) {
                    String imageUri = remoteMessage.getData().get("image");
                    if (imageUri.contains(".jpg") || imageUri.contains(".jpeg") || imageUri.contains(".png") || imageUri.contains(".webp")) {
                        scheduler(getApplicationContext(), imageUri, remoteMessage.getData().get("message"));
                    } else {
                        sendNotification(remoteMessage);
                    }
                } else {
                    sendNotification(remoteMessage);
                }
            }
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {
        //Intent Registration
        Intent intent = new Intent(this, ActivitySplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Intent intentUpdate = new Intent();
        intentUpdate.setAction("onMessageReceived");


        //Take Notification Sound
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //Generate the Notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        notificationBuilder
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                .setContentTitle(ApplicationContext.getApplicationInstance().getString(R.string.app_name))
                .setContentText(remoteMessage.getData().get("message"))
                .setTicker(remoteMessage.getData().get("message"))
                .setBadgeIconType(R.drawable.ic_notification)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(remoteMessage.getData().get("message")))
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Create Push Notification
        assert notificationManager != null;
        notificationManager.notify(1, notificationBuilder.build());

        sendBroadcast(intentUpdate);
    }

    public void scheduler(Context context, String imageURL, String message) {
        Bundle notificationDetail = new Bundle();
        if (imageURL != null) {
            notificationDetail.putString("ImageURL", imageURL);
        }
        notificationDetail.putString("Message", message);

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));

        Job mJobPushNotification = dispatcher.newJobBuilder()
                .setService(PushNotificationJobService.class)
                .setTag("push_notification")
                .setExtras(notificationDetail)
                .setTrigger(Trigger.NOW)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();

        dispatcher.mustSchedule(mJobPushNotification);
    }
}
