package com.balleh.additional;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sasikumar on 3/23/2018.
 * Constant fields and methods
 */

public class ConstantFields {

    static String AppTag = "com.balleh.SaveDetail";

    public static String mTokenTag = "CustomerToken";

    public static String mCustomerCartIdTag = "CustomerCartId";

    public static String mCustomerEmailIdTag = "CustomerEmailId";

    public static String mCustomerAccountTag = "CustomerAccountId";

    public static String mCustomerSessionTag = "CustomerSessionCount";

    public static String mBrandTag = "Brand";

    public static String mSizeTag = "Size";

    public static String mCountTag = "Count";

    public static String mGuestCartIdTag = "GuestCartId";

    public static String mCartDetailTag = "CartDetail";

    public static String searchHistory = "Search history";

    public static String BaseURL = "https://balleh.com/index.php/rest/V1/";

    public static String ImageURL = "https://balleh.com/media/catalog/product/";

    public static String mBanner = "https://balleh.com/rest/V1/bannerimages";

    public static String mHomeContent = "https://balleh.com/rest/V1/categoryslider";

    public static String publicKey = "pk_test_32cc90a6-d3dc-4c54-b805-6dd27e8d29b9";

   /* private static String BaseURL = "http://lingeeshop.com/balleh/index.php/rest/V1/";

    public static String ImageURL = "http://lingeeshop.com/balleh/media/catalog/product/";

    public static String mBanner = "http://lingeeshop.com/balleh/rest/V1/bannerimages";

    public static String mHomeContent = "http://lingeeshop.com/balleh/rest/V1/categoryslider";*/

   /* private static String BaseURL = "http://magento2.adyas.com/index.php/rest/V1/";

    public static String ImageURL = "http://magento2.adyas.com/media/catalog/product";

    public static String mBanner = "http://magento2.adyas.com/rest/V1/bannerimages";

    public static String mHomeContent = "http://magento2.adyas.com/rest/V1/categoryslider";*/

    public static String CustomerSocialLogin = "https://balleh.com/rest/V1/social/customer_token";

    private static String getBaseURL(boolean type) {
        if (type) {
            return "https://balleh.com/index.php/rest/en/V1/";
        } else {
            return "https://balleh.com/index.php/rest/ar/V1/";
        }

       /* if (type) {
            return "http://lingeeshop.com/balleh/index.php/rest/en/V1/";
        } else {
            return "http://lingeeshop.com/balleh/index.php/rest/ar/V1/";
        }*/
    }

    public static String getCityList(String countryId, String stateId) {
        return "https://balleh.com/mobifilter/index/mcity?country_id=" + countryId + "&state_id=" + stateId;
    }

    public static String getFilterList(int category_id) {
        return "https://balleh.com/mobifilter/index/mfilter?category_id=" + category_id;
    }

    public static String getOrderCancel(String orderId) {
        //return "http://magento2.adyas.com/hyperpay/index/cancel?order_id=6" + orderId;
        //return "https://balleh.com/hyperpay/index/cancel?order_id=" + orderId;
        return "https://balleh.com/hyperpayex/index/cancel?order_id=" + orderId;
        //return "http://lingeeshop.com/balleh/hyperpayex/index/cancel?order_id=" + orderId;
    }

    public static String getCheckOutResourcePath(String resourcePath, String orderId) {
        //return "http://magento2.adyas.com/hyperpay/index/status?resource_path=" + resourcePath + "&order_id=" + orderId;
        // return "https://balleh.com/hyperpay/index/status?resource_path=" + resourcePath + "&order_id=" + orderId;
        return "https://balleh.com/hyperpayex/index/status?resource_path=" + resourcePath/* + "&order_id=" + orderId*/;
        //return "http://lingeeshop.com/balleh/hyperpayex/index/status?resource_path=" + resourcePath + "&order_id=" + orderId;
    }

    //public static String mCheckOutPayment = "https://balleh.com/index.php/rest/en/v1/charge";

    //public static String mCheckOutPayment = "http://lingeeshop.com/balleh/index.php/rest/v1/charge";

    public static String mLogin = BaseURL + "integration/customer/token";

    public static String mRegistration = BaseURL + "customers";

    public static String mForgotPassword = BaseURL + "customers/password";

    public static String mCustomerPlaceOrder = BaseURL + "carts/mine/payment-information";

    public static String mCustomerBillingAddress = BaseURL + "carts/mine/billing-address";

    public static String mCustomerQuoteTotal = BaseURL + "carts/mine/collect-totals";

    public static String mCountry = BaseURL + "directory/countries";

    public static String mChangePassword = BaseURL + "customers/me/password";

    public static String CustomerCoupon = BaseURL + "carts/mine/coupons";

    public static String GuestCoupon = BaseURL + "guest-carts";

    public static String CustomerWriteReview = BaseURL + "review/mine/post";

    //public static String mCustomerCartProductList = BaseURL + "carts/mine/items";

    //public static String mCustomerAddToCart = mCustomerCartProductList;

    //public static String mGuestCartId = BaseURL + "guest-carts/";

    //public static String mGetCategoryList = BaseURL + "categories?depth=2";

    //public static String mCustomerCartId = BaseURL + "carts/mine";

    //public static String mCustomerDetail = BaseURL + "customers/me";

    //public static String mCustomerCartTotalList = BaseURL + "carts/mine/totals";

    //public static String mCustomerShippingMethod = BaseURL + "carts/mine/estimate-shipping-methods";

    //public static String mCustomerPaymentMethod = BaseURL + "carts/mine/shipping-information";

    public static String getCustomerPaymentMethod(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "carts/mine/shipping-information";
    }

    public static String getCustomerShippintMethod(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "carts/mine/estimate-shipping-methods";
    }

    public static String getCustomerCartTotalList(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "carts/mine/totals";
    }

    public static String getCustomerDetail(boolean typeLanguage) {
        //return getBaseURL(typeLanguage) + "customers/me";
        return BaseURL + "customers/me";
    }

    public static String getCustomerCartId(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "carts/mine";
    }

    public static String getCategoryDetail(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "categories?depth=2";
    }

    public static String getGuestCartId(boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "guest-carts/";
    }

    public static String getCartProductList(boolean type) {
        return getBaseURL(type) + "carts/mine/items";
    }

    public static String removCartId(String id) {
        return "https://balleh.com/index.php/rest/V1/quotedelete/"+id;
    }

    public static String getPaymentStatus(String orderId) {
        return "https://balleh.com/index.php/rest/V1/Orderstatuscheck/" + orderId;
    }

    public static String getRelatedProductList(String productSKU, boolean type) {
        return getBaseURL(type) + "products/" + productSKU + "/links/related";
    }

    public static String getRelatedProductListValue(String productSKU, int limit, int page, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "products?fields=items[id,sku,name,price,custom_attributes],total_count" +
                "&searchCriteria[filterGroups][0][filters][0][field]=sku" +
                "&searchCriteria[filterGroups][0][filters][0][value]=" + productSKU +
                "&searchCriteria[filterGroups][0][filters][0][conditionType]=in" +
                "&searchCriteria[filterGroups][1][filters][0][field]=status" +
                "&searchCriteria[filterGroups][1][filters][0][value]=1" +
                "&searchCriteria[filterGroups][1][filters][0][conditionType]=eq" +
                "&searchCriteria[sortOrders][0][field]=created_at" +
                "& searchCriteria[sortOrders][0][direction]=ASC" +
                "&searchCriteria[pageSize]=" + limit +
                "&searchCriteria[currentPage]=" + page;

    }

    public static String GetPaymentURL(/*String amount, String Currency*/) {
        //return "https://balleh.com/hyperpay/index/index?amount=" + amount + "&currency=" + Currency;
        //return "https://balleh.com/hyperpayex/index/index?amount=" + amount + "&currency=" + Currency;
        return "https://balleh.com/hyperpayex/index/index";
    }

    public static String getReviewType(int productId, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "rating/ratings/" + productId;
    }

    public static String getReviewList(int productId, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "review/reviews/" + productId;
    }

    public static String getProductImageList(String sku, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "products/" + sku + "/media";
    }

    public static String getAttributeList(String which, boolean typeLanguage) {
        String url = getBaseURL(typeLanguage) + "products/attributes/";
        switch (which) {
            case "Brand":
                return url + "brand";
            case "Size":
                return url + "size";
            default:
                return url + "count";
        }
    }

    public static String applyCustomerCoupon(String couponCode) {
        return CustomerCoupon + "/" + couponCode;
    }

    public static String applyGuestCoupon(String guestCartId, String couponCode) {
        return GuestCoupon + "/" + guestCartId + "/coupons/" + couponCode;
    }

    public static String deleteGuestCoupon(String guestCartId) {
        return GuestCoupon + "/" + guestCartId + "/coupons";
    }

    public static String getGuestCartItems(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/items";
    }

    public static String getGuestCartItemDelete(String guestToken, int itemId, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/items/" + itemId;
    }

    public static String getGuestCartItemsTotal(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/totals";
    }

    public static String getGuestShippingMethod(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/estimate-shipping-methods";
    }

    public static String getGuestPaymentMethod(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/shipping-information";
    }

    public static String getGuestQuoteTotal(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/collect-totals";
    }

    public static String getGuestPlaceOrder(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken + "/payment-information";
    }

    public static String getGuestToCustomer(String guestToken, boolean typeLanguage) {
        return getGuestCartId(typeLanguage) + guestToken;
    }

    public static String getCategoryDetail(int categoryId, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "categories/" + categoryId;
    }

    public static String getSubCategoryList(int categoryId, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "categories?rootCategoryId=" + categoryId + "&depth=1";
    }

    public static String getProductDetails(String productSKU, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "products/" + productSKU;
    }

    public static String getProductStatus(String productSKU, boolean typeLanguage) {
        return getBaseURL(typeLanguage) + "stockStatuses/" + productSKU;
    }

    public static String getProductList(String categoryId, int page, String type, String filter, boolean typeLanguage) {
        String url = getBaseURL(typeLanguage) + "products?fields=items[id,sku,name,price,custom_attributes],total_count"
                + "&searchCriteria[filterGroups][0][filters][0][field]=category_id"
                + "&searchCriteria[filterGroups][0][filters][0][value]=" + categoryId
                + "&searchCriteria[filterGroups][0][filters][0][conditionType]=in"
                + "&searchCriteria[filterGroups][1][filters][0][field]=status"
                + "&searchCriteria[filterGroups][1][filters][0][value]=1"
                + "&searchCriteria[filterGroups][1][filters][0][conditionType]=eq";

        switch (type) {
            case "A-Z":
                url = url + "&searchCriteria[sortOrders][0][field]=name"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "Z-A":
                url = url + "&searchCriteria[sortOrders][0][field]=name"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            case "prL-prH":
                url = url + "&searchCriteria[sortOrders][0][field]=price"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "prH-prL":
                url = url + "&searchCriteria[sortOrders][0][field]=price"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            case "poL-poH":
                url = url + "&searchCriteria[sortOrders][0][field]=position"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "poH-poL":
                url = url + "&searchCriteria[sortOrders][0][field]=position"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            default:
                url = url + "&searchCriteria[sortOrders][0][field]=created_at"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
        }

        if (filter != null) {
            if (filter.length() > 0) {
                url = url + filter;
            }
        }

        url = url + "&searchCriteria[pageSize]=10"
                + "&searchCriteria[currentPage]=" + page;


        Log.e("getProductList: ", url + "");

        return url;
    }

    public static String getSearchProductList(String key, int page, String type, boolean typeLanguage) {
        String url = getBaseURL(typeLanguage) + "products?"
                + "searchCriteria[filter_groups][0][filters][0][field]=name"
                + "&searchCriteria[filter_groups][0][filters][0][value]=%25" + key + "%25"
                + "&searchCriteria[filter_groups][0][filters][0][condition_type]=like";

        switch (type) {
            case "A-Z":
                url = url + "&searchCriteria[sortOrders][0][field]=name"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "Z-A":
                url = url + "&searchCriteria[sortOrders][0][field]=name"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            case "prL-prH":
                url = url + "&searchCriteria[sortOrders][0][field]=price"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "prH-prL":
                url = url + "&searchCriteria[sortOrders][0][field]=price"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            case "poL-poH":
                url = url + "&searchCriteria[sortOrders][0][field]=position"
                        + "&searchCriteria[sortOrders][0][direction]=ASC";
                break;
            case "poH-poL":
                url = url + "&searchCriteria[sortOrders][0][field]=position"
                        + "&searchCriteria[sortOrders][0][direction]=DESC";
                break;
            default:
                url = url + "&searchCriteria[sortOrders][0][field]=created_at"
                        + "& searchCriteria[sortOrders][0][direction]=DESC";
                break;
        }

        url = url + "&searchCriteria[pageSize]=10"
                + "&searchCriteria[currentPage]=" + page;


        return url;
    }

    public static String deleteCartItem(int item_id, boolean typeLanguage) {
        return getCartProductList(typeLanguage) + "/" + item_id;
    }

    public static String getCustomerOrderListWithPagination(String email, int page, boolean type) {
        return getBaseURL(type) + "orders?" +
                "searchCriteria[filter_groups][0][filters][0][field]=customer_email" +
                "&searchCriteria[filter_groups][0][filters][0][value]=" + email +
                "&searchCriteria[sortOrders][0][field]=created_at" +
                "& searchCriteria[sortOrders][0][direction]=DESC" +
                "&searchCriteria[pageSize]=6" +
                "&searchCriteria[currentPage]=" + page;
    }

    @NonNull
    public static String getAboutUsLink(String languageType) {
        if (languageType.equals("en")) {
            return "https://balleh.com/en/about-us";
        } else {
            return "https://balleh.com/ar/about-us";
        }
    }

    @NonNull
    public static String getContactUsLink(String languageType) {
        if (languageType.equals("en")) {
            return "https://balleh.com/en/contact";
        } else {
            return "https://balleh.com/ar/contact";
        }
    }

    public static String getGuestToken() {

        //return "Bearer jhjowbukxd14yolglclcjm28un97edt3"; /*magento2.exlcart.com*/

        return "Bearer ljeygdxd6krq1uqxos1gk33568ivrgcx"; /*Balleh.com*/

    }

    public static void CustomToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isEmailValidator(String inputEmail) {
        Pattern pattern;
        Matcher matcher;
        String mMail_Pattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(mMail_Pattern);
        matcher = pattern.matcher(inputEmail);
        return matcher.matches();
    }

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @NonNull
    public static String currentLanguage() {
        if (ConstantDataSaver.mRetrieveSharedPreferenceString(ApplicationContext.getApplicationInstance(), "setDefault") != null) {
            if (ConstantDataSaver.mRetrieveSharedPreferenceString(ApplicationContext.getApplicationInstance(), "setDefault").equals("true")) {
                return "ar";
            } else {
                return "en";
            }
        } else {
            return "ar";
        }
    }

    @NonNull
    public static Boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) ApplicationContext.getApplicationInstance()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connectivityManager != null;
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                } else {
                    switch (info.getSubtype()) {
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                            return false;
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                            return false;
                        case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                            return false;
                        default:
                            return true;
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
