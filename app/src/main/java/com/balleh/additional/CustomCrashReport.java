package com.balleh.additional;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.balleh.activities.ActivityAdditional;

/**
 * Created by Sasikumar on 3/22/2018.
 * Custom Exception Handling
 */

public class CustomCrashReport implements Thread.UncaughtExceptionHandler {

    private Context mContext;

    public CustomCrashReport(String tag,Context context) {
        this.mContext = context;
        Log.e(tag,Thread.getDefaultUncaughtExceptionHandler().toString());
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        Intent intent = new Intent(mContext, ActivityAdditional.class);
        intent.putExtra("LoadType", "DialogCustomMessage");
        intent.putExtra("ToCustomType", "Exception");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

}
