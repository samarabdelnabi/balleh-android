package com.balleh.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.widgets_handler.ZoomImageView;

import java.util.ArrayList;

public class ProductSubImageList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<String> mImageList;
    private ZoomImageView ziv_ProductPrimaryImage;
    private int previousPosition = -1, currentPosition = -1;

    public ProductSubImageList(Activity activity, ArrayList<String> list, ZoomImageView ziv_ProductPrimaryImage) {
        this.activity = activity;
        this.mImageList = list;
        this.ziv_ProductPrimaryImage = ziv_ProductPrimaryImage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubImageViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_product_sub_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        SubImageViewHolder subImageViewHolder = (SubImageViewHolder) holder;
        if (currentPosition == position) {
            subImageViewHolder.mChildView.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_iv_select));
        } else {
            subImageViewHolder.mChildView.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_iv_non_select));
        }

        final int dummyPosition = position;

        image_caller(ConstantFields.ImageURL + mImageList.get(position), subImageViewHolder.mChildView);

        subImageViewHolder.mChildView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPosition != dummyPosition) {

                    ziv_ProductPrimaryImage.reset();
                    ziv_ProductPrimaryImage.setMaxZoom(3.0f);
                    image_caller(ConstantFields.ImageURL + mImageList.get(dummyPosition), ziv_ProductPrimaryImage);

                    if (previousPosition < 0 && currentPosition < 0) {
                        currentPosition = dummyPosition;
                    } else {
                        previousPosition = currentPosition;
                        currentPosition = dummyPosition;
                    }

                    notifyDataSetChanged();
                }
            }
        });
    }

    private void image_caller(String url, ImageView imageView) {
        ImageLoader.glide_image_loader(url, imageView, "Original");
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public class SubImageViewHolder extends RecyclerView.ViewHolder {
        ImageView mChildView;

        SubImageViewHolder(View itemView) {
            super(itemView);
            mChildView = itemView.findViewById(R.id.iv_product_sub_image);
        }
    }
}
