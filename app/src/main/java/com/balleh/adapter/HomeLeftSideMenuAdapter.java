package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataSaver;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelHomeMenuCategoryList;

import java.util.ArrayList;

public class HomeLeftSideMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ModelHomeMenuCategoryList> modelHomeMenuCategoryLists;
    private MenuHandler menuHandler;

    public HomeLeftSideMenuAdapter(Activity activity, ArrayList<ModelHomeMenuCategoryList> categoryLists, MenuHandler menuHandler) {
        this.activity = activity;
        this.modelHomeMenuCategoryLists = categoryLists;
        this.menuHandler = menuHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;
        if (viewType == 1) {
            holder = new MenuTopContentHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_ls_menu_static_top_content, parent, false));
        } else if (viewType == 2) {
            holder = new MenuExpandableViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_ls_menu_expandable_view, parent, false));
        } else {
            holder = new MenuBottomContentHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_ls_menu_static_bottom_content, parent, false));
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == 1) {
            final MenuTopContentHolder menuTopContentHolder = (MenuTopContentHolder) holder;

            if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault") != null) {
                if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity, "setDefault").equals("true")) {
                    /*English*/
                    menuTopContentHolder.atv_English.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_selected));
                    menuTopContentHolder.atv_Arabic.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_un_selected));
                    menuTopContentHolder.atv_English.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                    menuTopContentHolder.atv_Arabic.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
                } else {
                    /*Arabic*/
                    menuTopContentHolder.atv_Arabic.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_selected));
                    menuTopContentHolder.atv_English.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_un_selected));
                    menuTopContentHolder.atv_Arabic.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                    menuTopContentHolder.atv_English.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
                }
            } else {
                /*English*/
                menuTopContentHolder.atv_English.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_selected));
                menuTopContentHolder.atv_Arabic.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_un_selected));
                menuTopContentHolder.atv_English.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                menuTopContentHolder.atv_Arabic.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
            }

            menuTopContentHolder.atv_English.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuTopContentHolder.atv_English.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_selected));
                    menuTopContentHolder.atv_Arabic.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_un_selected));
                    menuTopContentHolder.atv_English.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                    menuTopContentHolder.atv_Arabic.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
                    menuHandler.changeLang("en");
                }
            });

            menuTopContentHolder.atv_Arabic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuTopContentHolder.atv_Arabic.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_selected));
                    menuTopContentHolder.atv_English.setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.draw_lang_un_selected));
                    menuTopContentHolder.atv_Arabic.setTextColor(ContextCompat.getColor(activity, android.R.color.white));
                    menuTopContentHolder.atv_English.setTextColor(ContextCompat.getColor(activity, android.R.color.black));
                    menuHandler.changeLang("ar");
                }
            });

            if (ConstantDataSaver.mRetrieveSharedPreferenceString(activity.getApplicationContext(), ConstantFields.mTokenTag).equals("Empty")) {
                menuTopContentHolder.atv_SignIn.setVisibility(View.VISIBLE);
            } else {
                menuTopContentHolder.atv_SignIn.setVisibility(View.GONE);
            }

        } else if (holder.getItemViewType() == 2) {
            MenuExpandableViewHolder menuExpandableViewHolder = (MenuExpandableViewHolder) holder;
            menuExpandableViewHolder.newExpandableView.setLayoutManager(new LinearLayoutManager(activity));
            menuExpandableViewHolder.newExpandableView.setAdapter(new HomeLSCustomExpandableViewAdapter(activity, modelHomeMenuCategoryLists, menuHandler));
        } else {
            MenuBottomContentHolder menuBottomContentHolder = (MenuBottomContentHolder) holder;
            menuBottomContentHolder.atv_LiveChat.setOnClickListener(view -> menuHandler.toLiveChat());
        }

    }

    @Override
    public int getItemCount() {

        if (modelHomeMenuCategoryLists != null) {
            if (modelHomeMenuCategoryLists.size() > 0) {
                return 3;
            } else {
                return 2;
            }
        } else {
            return 2;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelHomeMenuCategoryLists != null) {
            if (modelHomeMenuCategoryLists.size() > 0) {
                if (position == 0) {
                    return 1;
                } else if (position == 1) {
                    return 2;
                } else {
                    return 3;
                }
            } else {
                if (position == 0) {
                    return 1;
                } else {
                    return 3;
                }
            }
        } else {
            if (position == 0) {
                return 1;
            } else {
                return 3;
            }
        }
    }

    private class MenuExpandableViewHolder extends RecyclerView.ViewHolder {
        RecyclerView newExpandableView;

        MenuExpandableViewHolder(View itemView) {
            super(itemView);
            newExpandableView = itemView.findViewById(R.id.custom_expandable_view);
        }
    }

    private class MenuTopContentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView atv_English, atv_Arabic, atv_Home, atv_SignIn, atv_SignUp, atv_Deals;

        MenuTopContentHolder(View itemView) {
            super(itemView);

            atv_English = itemView.findViewById(R.id.atv_ls_menu_lang_en);
            atv_Arabic = itemView.findViewById(R.id.atv_ls_menu_lang_ar);
            atv_Home = itemView.findViewById(R.id.atv_ls_menu_home);
            atv_SignUp = itemView.findViewById(R.id.atv_ls_menu_sign_up);
            atv_SignIn = itemView.findViewById(R.id.atv_ls_menu_sign_in);
            atv_Deals = itemView.findViewById(R.id.atv_ls_menu_deals);

            atv_Home.setOnClickListener(this);
            atv_SignUp.setOnClickListener(this);
            atv_SignIn.setOnClickListener(this);
            atv_Deals.setOnClickListener(this);

            fontSetup(atv_English,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_Arabic,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_Home,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_SignUp,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_SignIn,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_Deals,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.atv_ls_menu_home:
                    menuHandler.toHome();
                    break;
                case R.id.atv_ls_menu_lang_en:
                    menuHandler.changeLang("en");
                    break;
                case R.id.atv_ls_menu_lang_ar:
                    menuHandler.changeLang("ar");
                    break;
                case R.id.atv_ls_menu_sign_up:
                    menuHandler.toSignUp();
                    break;
                case R.id.atv_ls_menu_sign_in:
                    menuHandler.toSignIn();
                    break;
                case R.id.atv_ls_menu_deals:
                    menuHandler.toDeals();
                    break;
            }
        }

    }

    private class MenuBottomContentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView atv_LiveChat, atv_TrackOrder, atv_Share;

        MenuBottomContentHolder(View itemView) {
            super(itemView);

            atv_LiveChat = itemView.findViewById(R.id.atv_ls_menu_live_chat);
            atv_TrackOrder = itemView.findViewById(R.id.atv_ls_menu_track_order);
            atv_Share = itemView.findViewById(R.id.atv_ls_menu_share);

            atv_TrackOrder.setOnClickListener(this);
            atv_Share.setOnClickListener(this);
            fontSetup(atv_LiveChat,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_TrackOrder,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_Share,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.atv_ls_menu_track_order:
                    menuHandler.toTrackOrder();
                    break;
                case R.id.atv_ls_menu_share:
                    menuHandler.toShare();
                    break;
            }
        }

    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
