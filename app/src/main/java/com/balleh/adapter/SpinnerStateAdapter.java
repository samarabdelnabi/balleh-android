package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.model.ModelState;

import java.util.ArrayList;

public class SpinnerStateAdapter extends ArrayAdapter<ModelState> {

    private ArrayList<ModelState> stateList;
    private Activity activity;

    public SpinnerStateAdapter(@NonNull Activity activity, ArrayList<ModelState> stateList) {
        super(activity, android.R.layout.simple_spinner_item, stateList);
        this.activity = activity;
        this.stateList = stateList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v_StateDefault = convertView;
        if (v_StateDefault == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v_StateDefault = inflater.inflate(R.layout.rc_row_empty, parent, false);
        }
        if (stateList.get(position) != null) {
            AppCompatTextView atv_StateName = v_StateDefault.findViewById(R.id.atv_empty_message);
            if (atv_StateName != null) {
                atv_StateName.setText(stateList.get(position).getName());
                // atv_StateName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_24dp, 0);
                fontSetup(atv_StateName, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
        return v_StateDefault;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v_StateDropDown = convertView;
        if (v_StateDropDown == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v_StateDropDown = inflater.inflate(R.layout.rc_row_empty, parent, false);
        }

        if (stateList.get(position) != null) {
            AppCompatTextView atv_StateName = v_StateDropDown.findViewById(R.id.atv_empty_message);
            int color = ContextCompat.getColor(activity, android.R.color.white);
            atv_StateName.setBackgroundColor(color);
            atv_StateName.setText(stateList.get(position).getName());
            fontSetup(atv_StateName, Constant.LIGHT, Constant.C_TEXT_VIEW);
        }
        return v_StateDropDown;
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
