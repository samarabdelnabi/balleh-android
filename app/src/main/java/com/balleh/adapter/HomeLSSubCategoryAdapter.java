package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.additional.instentTransfer.MenuHandler;
import com.balleh.model.ModelHomeMenuCategory;

import java.util.ArrayList;

public class HomeLSSubCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ModelHomeMenuCategory> mSubFullList;
    private MenuHandler menuHandler;

    HomeLSSubCategoryAdapter(Activity activity, ArrayList<ModelHomeMenuCategory> subList, MenuHandler menuHandler) {
        this.activity = activity;
        this.mSubFullList = subList;
        this.menuHandler = menuHandler;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubCategoryHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_home_ls_menu_sub_category_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        SubCategoryHolder subCategoryHolder = (SubCategoryHolder) holder;
        subCategoryHolder.atv_SubCategoryTitle.setText(mSubFullList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mSubFullList.size();
    }

    private class SubCategoryHolder extends RecyclerView.ViewHolder {
        AppCompatTextView atv_SubCategoryTitle;

        SubCategoryHolder(View itemView) {
            super(itemView);
            atv_SubCategoryTitle = itemView.findViewById(R.id.atv_sub_category_title);
            fontSetup(atv_SubCategoryTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
            atv_SubCategoryTitle.setOnClickListener(v -> {
                if (getAdapterPosition() != -1) {
                    menuHandler.toProductList(Integer.valueOf(mSubFullList.get(getAdapterPosition()).getId()), "default", "");
                }
            });
        }
    }


    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
