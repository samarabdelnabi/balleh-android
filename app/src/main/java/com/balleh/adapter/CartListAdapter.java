package com.balleh.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.balleh.R;
import com.balleh.additional.ApplicationContext;
import com.balleh.additional.Constant;
import com.balleh.additional.ConstantDataParser;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.instentTransfer.CartInstantHelper;
import com.balleh.model.ModelCartListProduct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
    private ArrayList<ModelCartListProduct> cartProductList;
    private CartInstantHelper cartInstantHelper;
    private ArrayList<String[]> totalList;

    public CartListAdapter(Activity activity, ArrayList<ModelCartListProduct> cartProductList,
                           ArrayList<String[]> totalList,
                           CartInstantHelper cartInstantHelper) {
        this.activity = activity;
        this.cartProductList = cartProductList;
        this.cartInstantHelper = cartInstantHelper;
        this.totalList = totalList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        if (viewType == 1) {
            viewHolder = new CartProduct(LayoutInflater.from(activity).inflate(R.layout.rc_row_cart_product, parent, false));
        } else if (viewType == 2) {
            viewHolder = new TotalListViewHolder(LayoutInflater.from(activity).inflate(R.layout.rc_row_order_total, parent, false));
        } else {
            viewHolder = new EmptyCartProductList(LayoutInflater.from(activity).inflate(R.layout.rc_row_empty, parent, false));
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder.getItemViewType() == 1) {
            CartProduct cartProduct = (CartProduct) holder;
            cartProduct.atv_CartProductTitle.setText(cartProductList.get(position).getName());
            String price = "SAR " + cartProductList.get(position).getPrice();
            cartProduct.atv_CartProductPrice.setText(price);
            cartProduct.atv_CartProductCount.setText(String.valueOf(cartProductList.get(position).getQuantity()));
            cartProduct.ib_CartProductAddQuantity.setOnClickListener(view -> cartInstantHelper.cartUpdate(cartProductList.get(position), "Add"));

            cartProduct.ib_CartProductCountReduceQuantity.setOnClickListener(view -> {

                /*if ((cartProductList.get(position).getQuantity() - 1) > 0) {
                    cartInstantHelper.cartUpdate(cartProductList.get(position), "Reduce");
                } else {
                    ConstantFields.CustomToast(activity.getString(R.string.cart_error_message), activity);
                }*/

                cartInstantHelper.cartUpdate(cartProductList.get(position), "Reduce");
            });

            cartProduct.ib_CartProductRemove.setOnClickListener(view -> cartInstantHelper.cartUpdate(cartProductList.get(position), "Remove"));

            setImage(cartProduct.iv_CartProductImage, cartProductList.get(position).getSku());

        } else if (holder.getItemViewType() == 2) {
            TotalListViewHolder totalListViewHolder = (TotalListViewHolder) holder;

            if (totalList.get(position - (cartProductList.size()))[0] != null)
                totalListViewHolder.atv_TotalTitle.setText(totalList.get(position - (cartProductList.size()))[0]);

            if (totalList.get(position - (cartProductList.size()))[1] != null) {
                String price = "SAR " + totalList.get(position - (cartProductList.size()))[1];
                totalListViewHolder.atv_TotalValue.setText(price);
            }
        } else {
            EmptyCartProductList emptyCartProductList = (EmptyCartProductList) holder;
            emptyCartProductList.atv_EmptyMessage.setText(R.string.cart_empty_message);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (cartProductList != null) {
            if (cartProductList.size() > 0) {
                if (cartProductList.size() > position) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        if (cartProductList != null) {
            if (cartProductList.size() > 0) {
                return cartProductList.size() + totalList.size();
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    private class CartProduct extends RecyclerView.ViewHolder {

        ImageView iv_CartProductImage;
        AppCompatTextView atv_CartProductTitle, atv_CartProductPrice, atv_CartProductCount, atv_CartProductStockError;
        ImageButton ib_CartProductRemove, ib_CartProductAddQuantity, ib_CartProductCountReduceQuantity;
        LinearLayout ll_CartOptionHolder;

        CartProduct(View view) {
            super(view);
            iv_CartProductImage = view.findViewById(R.id.iv_cart_product_image);

            atv_CartProductTitle = view.findViewById(R.id.atv_cart_product_title);
            atv_CartProductPrice = view.findViewById(R.id.atv_cart_product_price);
            atv_CartProductCount = view.findViewById(R.id.atv_cart_product_quantity_count);
            atv_CartProductStockError = view.findViewById(R.id.atv_cart_product_stock_error);

            ib_CartProductCountReduceQuantity = view.findViewById(R.id.ib_cart_product_count_remove);
            ib_CartProductAddQuantity = view.findViewById(R.id.ib_cart_product_count_add);
            ib_CartProductRemove = view.findViewById(R.id.ib_cart_product_remove_item);

            ll_CartOptionHolder = view.findViewById(R.id.ll_cart_option_holder);

            fontSetup(atv_CartProductTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_CartProductPrice,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_CartProductCount,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_CartProductStockError,Constant.LIGHT,Constant.C_TEXT_VIEW);

        }
    }

    private class TotalListViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView atv_TotalTitle, atv_TotalValue;

        TotalListViewHolder(View itemView) {
            super(itemView);
            atv_TotalTitle = itemView.findViewById(R.id.atv_total_title_order);
            atv_TotalValue = itemView.findViewById(R.id.atv_total_value_order);

            fontSetup(atv_TotalTitle,Constant.LIGHT,Constant.C_TEXT_VIEW);
            fontSetup(atv_TotalValue,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }
    }

    private class EmptyCartProductList extends RecyclerView.ViewHolder {
        AppCompatTextView atv_EmptyMessage;

        EmptyCartProductList(View view) {
            super(view);
            atv_EmptyMessage = view.findViewById(R.id.atv_empty_message);
            fontSetup(atv_EmptyMessage,Constant.LIGHT,Constant.C_TEXT_VIEW);
        }
    }

    private void setImage(final ImageView iv_TargetHolder, String sku) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ConstantFields.getProductImageList(sku, ConstantFields.currentLanguage().equals("en")),
                response -> {
                    Log.e("onResponse: ", response);
                    String url = ConstantDataParser.getProductImage(response);
                    if (url != null) {
                        if (url.length() > 0) {
                            ImageLoader.glide_image_loader(url, iv_TargetHolder, "Static");
                        } else {
                            iv_TargetHolder.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));
                        }
                    } else {
                        iv_TargetHolder.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder));
                    }
                }, error -> iv_TargetHolder.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.placeholder))) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        ApplicationContext.getInstance().addToRequestQueue(stringRequest, "CartImage");
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }

}
