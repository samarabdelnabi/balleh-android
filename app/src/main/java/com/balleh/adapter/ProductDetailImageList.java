package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.balleh.R;
import com.balleh.additional.ConstantFields;
import com.balleh.additional.ImageLoader;
import com.balleh.additional.instentTransfer.MenuHandler;

import java.util.ArrayList;

public class ProductDetailImageList extends PagerAdapter {

    private Activity activity;
    private ArrayList<String> value;
    private MenuHandler menuHandler;

    public ProductDetailImageList(Activity activity, ArrayList<String> sources, MenuHandler menuHandler) {
        this.activity = activity;
        this.value = sources;
        this.menuHandler = menuHandler;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return value.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = LayoutInflater.from(activity);
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.vp_row_product_image_list, container, false);
        ImageView iv_ProductImage = view.findViewById(R.id.iv_product_image);

        ImageLoader.glide_image_loader(ConstantFields.ImageURL + value.get(position), iv_ProductImage, "Original");

        iv_ProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuHandler.toProductImageList(value);
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
