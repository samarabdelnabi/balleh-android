package com.balleh.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.balleh.R;
import com.balleh.additional.Constant;
import com.balleh.model.ModelCountry;

import java.util.ArrayList;

public class SpinnerCountryAdapter extends ArrayAdapter<ModelCountry> {

    private ArrayList<ModelCountry> countryList;
    private Activity activity;

    public SpinnerCountryAdapter(@NonNull Activity activity, ArrayList<ModelCountry> countryList) {
        super(activity, android.R.layout.simple_spinner_item, countryList);
        this.activity = activity;
        this.countryList = countryList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v_CountryDefault = convertView;
        if (v_CountryDefault == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v_CountryDefault = inflater.inflate(R.layout.rc_row_empty, parent, false);
        }
        if (countryList.get(position) != null) {
            AppCompatTextView atv_StateName = v_CountryDefault.findViewById(R.id.atv_empty_message);
            if (atv_StateName != null) {
                atv_StateName.setText(countryList.get(position).getName());
                // atv_StateName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_24dp, 0);
                fontSetup(atv_StateName, Constant.LIGHT, Constant.C_TEXT_VIEW);
            }
        }
        return v_CountryDefault;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v_CountryDropDown = convertView;
        if (v_CountryDropDown == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v_CountryDropDown = inflater.inflate(R.layout.rc_row_empty, parent, false);
        }

        if (countryList.get(position) != null) {
            AppCompatTextView atv_StateName = v_CountryDropDown.findViewById(R.id.atv_empty_message);
            int color = ContextCompat.getColor(activity, android.R.color.white);
            atv_StateName.setBackgroundColor(color);
            atv_StateName.setText(countryList.get(position).getName());
            fontSetup(atv_StateName, Constant.LIGHT, Constant.C_TEXT_VIEW);
        }
        return v_CountryDropDown;
    }

    private void fontSetup(Object o_SetFont, int type, int which) {
        Constant.SetFontStyle(o_SetFont, type, which);
    }
}
